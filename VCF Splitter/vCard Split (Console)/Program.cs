﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace vCard_Split__Console_
{
    class Program
    {
        static void Main(string[] args)
        {
            String filePath;
            String dumpPath;
            Console.WriteLine("Enter file path.......");
            filePath = Console.ReadLine();
            Console.WriteLine("Enter Dump Path.......");
            dumpPath = Console.ReadLine();
            ParseTools.ParsevCard(filePath, dumpPath);
        }
    }

    class ParseTools
    {
        static String cardName;
        public static void ParsevCard(String filePath, String dumpPath)
        {
            int count = 0;

            string[] lines = File.ReadAllLines(filePath);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i] == "BEGIN:VCARD")
                {
                    List<string> newLines = new List<string>();

                    for (int j = i; j < lines.Length; j++)
                    {
                        newLines.Add(lines[j]);
                        if (lines[j] == "END:VCARD")
                            break;
                    }

                    File.WriteAllLines(Path.Combine(dumpPath, count.ToString() + ".vcf"), newLines);
                    count++;
                }
            }

            Console.WriteLine(count.ToString());
            Console.ReadKey();
        }
    }
}
