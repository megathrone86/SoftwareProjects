﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureTest1
{
    public static class Base64Helper
    {
        //===============кошерные функции
        public static string GetBase64String(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        public static string GetBase64String(string byteString, Encoding encoding)
        {
            return Convert.ToBase64String(encoding.GetBytes(byteString));
        }

        public static byte[] GetBytesFromBase64String(string base64)
        {
            return Convert.FromBase64String(base64);
        }

        public static string GetStringFromBase64String(string base64, Encoding encoding)
        {
            return encoding.GetString(Convert.FromBase64String(base64));
        }

        //===============некошерные функции
        public static string GetBase64String(string byteString)
        {
            try
            {
                byte[] b = new byte[byteString.Length / 2];

                for (int i = 0; i < 40; i += 2)
                    b[i / 2] = Convert.ToByte(byteString.Substring(i, 2), 16);

                return Convert.ToBase64String(b);
            }
            catch (Exception ex)
            {
                return byteString;
            }
        }

        public static string GetByteStringFromBase64String(string base64)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(base64);
                string res = "";

                //System.IO.File.WriteAllBytes(@"C:\decoded.txt", bytes);
                foreach (byte b in bytes)
                    res += b.ToString("X2");

                return res;
            }
            catch
            {
                return "N/A";
            }
        }
    }
}
