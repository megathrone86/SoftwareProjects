﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure;
using System.Windows.Forms;

namespace AzureTest1
{
    public class StorageAccessor
    {
        public string GetMessage(Exception ex)
        {
            if (ex.InnerException != null)
                return GetMessage(ex.InnerException);

            return ex.Message;
        }

        public void Test()
        {
            try
            {
                string account = "bksp";
                string key = "ag8DOX4FhWForUqZXn1chwARPQN3ce+KbKSu0Bbx5bdPob2mJnoaQdJQQm/rXi/BM0XpYKo/VPRTjsageCJBCQ==";
                string address = "http://bksp.blob.core.windows.net/workflows";

                string connString = "DefaultEndpointsProtocol=http;AccountName=" + account + ";AccountKey=" +
                    key;// +";TableEndpoint=" + address;

                //string address = "http://bksp.blob.core.windows.net/workflows";
                //StorageCredentialsAccountAndKey creds = new StorageCredentialsAccountAndKey("test",
                //    Base64Helper.GetBase64String(Encoding.UTF8.GetBytes("asd")));

                //CloudTableClient ctClient = new CloudTableClient(address, creds);

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                List<CloudBlobContainer> containers = new List<CloudBlobContainer>(blobClient.ListContainers());

                //CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                //List<string> tables = new List<string>(tableClient.ListTables());
                //MessageBox.Show(tables.Count.ToString());
            }
            catch (Exception ex)
            {
                string s = GetMessage(ex);
                MessageBox.Show(s);
            }
        }
    }
}
