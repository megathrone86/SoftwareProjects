<?php 
	try {
		error_reporting(E_ALL);
		ini_set('display_errors', 'on');
		$page = main();
	} catch (Exception $error) {
		echo 'Ошибка :( <br> '.$error->getMessage();
	}
	print $page;

	function main() {
		//считаем основной шаблон
		$templateFileName = "hidden/template.htm";
		$templateFile = fopen($templateFileName, "r");
		$templateText = fread($templateFile, 1000000);
		fclose($templateFile);
		
		$contentFileName = "about.htm"; //имя файла с контентом (зависит от параметра page)
		$customCssFileName = ""; //имя дополнительного CSS (зависит от параметра page)
		$mode = 0;  //режим (0 - статичный контент, 1 - новости)
		
		//определимся с контентом и режимом работы
		if(isset($_GET['page'])) {
			$contentName = $_GET['page'];
			
			if ($contentName == "about")
				$contentFileName = "about.htm";
			if ($contentName == "contacts")
				$contentFileName = "contacts.htm";
			if ($contentName == "news") {
				$contentFileName = "news.dat";
				$customCssFileName = "news.css";
				$mode = 1;
			}
			if ($contentName == "projects") {
				$contentFileName = "projects.htm";					
			}
			if ($contentName == "project-sb") {
				$contentFileName = "sb";
				$mode = 2;
			}
			if ($contentName == "project-sdm") {
				$contentFileName = "sdm";
				$mode = 2;				
			}
			if ($contentName == "project-spherix") {
				$contentFileName = "spherix";
				$mode = 2;
			}
		}	
		
		//считаем контент
		$contentFile = fopen("hidden/". $contentFileName, "r");
		if ($mode==2)
			$contentFile = fopen("hidden/projects/". $contentFileName. "/project.htm", "r");
		
		$content = fread($contentFile, 1000000);		
		fclose($contentFile);
		
		//новости
		if ($mode==1) {			
			$content = getNews(0, 10, $content);			
		}
		if ($mode==2) {
			$content = str_replace("src=\"", "src=\"". "hidden/projects/". $contentFileName. "/", $content);
		}
		
		$result = str_replace("!!!CONTENT!!!", $content, $templateText);
		
		//дополнительный CSS
		if ($customCssFileName!="") {
			$customCssFileName = "<link rel=\"stylesheet\" href=\"". $customCssFileName. "\" type=\"text/css\" />";			
		}
		
		$result = str_replace("!!!CUSTOM_CSS!!!", $customCssFileName, $result);
			
		return $result;
	}
	
	//парсер новостей (вспомогательная функция)
	function getNews($startNum, $count, $lines) {
		$result = "";
		
		$arrDelimiters = array("\r", "\n", "\r\n", "\n\r");
		$lines = str_replace($arrDelimiters, "-|-", $lines);
		$lines = explode("-|-", $lines);		
		
		$startNum = $startNum*2 + 1;		
		$startLine = 0;
		//определим $startLine (начало откуда будем читать новости
		for ($i = 0; $i < count($lines); $i++) { 
		
			if ($lines[$i]=="====================") {
					$startNum--;
					//$result = $result. "startNum = ". $startNum. "<br>\r\n";
				}
				
			if ($startNum<=0) {
				$startLine = $i;
				break;
			}
		}
		
		//теперь идет непосредственно парсинг
		$total_limiters = 0;
		$last_limiter = 0;
		$reading = false;
		
		for ($i = $startLine; $i < sizeof($lines); $i++) : 
			if ($lines[$i]=="====================") {
				$total_limiters++;
				$last_limiter = $i;
				$reading = !$reading;
				$reading_date = true;				
				
				if ($reading) {
					//начало блока новости
					$result = $result. "<div class=\"news_body\">";
				} else{
					//завершение блока новости. вставим дату.
					$result = $result. "</div>\r\n<div class=\"news_date\">". $news_date. "</div>\r\n";
				}
			}
				
			if ($total_limiters >= $count*2)
				break;
				
			if ($reading and ($last_limiter!=$i)) {
				if ($reading_date) {
					$news_date = $lines[$i];					
					$reading_date = false;
				} else
					$result = $result. "<p>". $lines[$i]. "</p>";
			}
			
		endfor;
		
		return $result;
	}
?> 