﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace K2IT.Xrm.EGRZ.Api.Helpers
{
    public class ThreadSafeHelper
    {
        public static void RunThreadSafe(object locker, ref bool executing, Action action, Action failAction = null)
        {
            lock (locker)
            {
                if (executing)
                {
                    failAction?.Invoke();
                    return;
                }
                executing = true;
            }

            try
            {
                action?.Invoke();
            } finally
            {
                executing = false;
            }
        }
    }
}