package com.mycompany.xdef;
import android.view.*;
import android.content.*;
import android.util.*;

public class MainMenuView extends SurfaceView implements SurfaceHolder.Callback
{
    private SurfaceHolder mSurfaceHolder;	
	private DrawerThread mDrawerThread;

    public MainMenuView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
 
        mSurfaceHolder = getHolder();
        mSurfaceHolder.addCallback(this);
		
		mDrawerThread = new DrawerThread();
    }
 
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
    }
 
    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
    }
 
    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
    }
}
