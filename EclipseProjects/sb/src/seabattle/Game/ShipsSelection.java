package seabattle.Game;

import java.util.Random;

import seabattle.*;
import seabattle.GameObjects.*;

public class ShipsSelection {

    public static SBShip CurrentSelShip;
    public static int ShipNumber;
    private static Random random = new Random();

    public static void Start() {
        GameLogic.Winner = 0;
        Global.Instance.Player1BF.Clear();
        Global.Instance.Player2BF.Clear();

        ShipNumber = 0;
        CurrentSelShip = new SBShip();
        SelectNextShip();

        Global.Instance.SelectionCnv.Show(true);
    }

    public static int GetShipsLeft() {
        return 11 - ShipNumber;
    }

    private static int GetCurrentShipSize(int shipNumber) {
        if (shipNumber == 1) {
            return 4;
        }

        if ((shipNumber == 2) || (shipNumber == 3)) {
            return 3;
        }

        if ((shipNumber >= 4) && (shipNumber <= 6)) {
            return 2;
        }

        if ((shipNumber >= 7) && (shipNumber <= 10)) {
            return 1;
        }

        return 0;
    }

    public static void SelectNextShip() {
        ShipNumber++;
        CurrentSelShip.bf = Global.Instance.Player1BF;

        int size = GetCurrentShipSize(ShipNumber);

        if (size > 0) {
            CurrentSelShip.Set(Global.Instance.BFWidth / 2 - 1,
                    Global.Instance.BFHeight / 2 - 1, size, (random.nextInt(2) == 0));
        } else {
            CurrentSelShip.size = 0;
            EndSelection();
        }
    }

    public static void EndSelection() {

        //если игрок 2 - комп, расставим его корабли в случайном порядке        
        if (GameLogic.Player2IsBot) {
            int botShipNumber = 1;
            SBShip tempShip = new SBShip();
            tempShip.bf = Global.Instance.Player2BF;

            while (true) {
                int size = GetCurrentShipSize(botShipNumber);
                if (size == 0) {
                    break;
                }

                int x = random.nextInt(Global.Instance.BFWidth);
                int y = random.nextInt(Global.Instance.BFHeight);
                boolean is_vertical = (random.nextInt(2) == 0);

                tempShip.Set(x, y, size, is_vertical);
                if (Global.Instance.Player2BF.CheckShipPositionAndIntersect(tempShip)) {
                    Global.Instance.Player2BF.AddShip(tempShip);
                    botShipNumber++;
                }
            }
        }

        //небольшая пауза
        Global.Instance.SelectionCnv.forceRefresh();
        GameLogic.Wait(800, null);

        //запустим игру
        Global.Instance.PlayCnv.Show(true);
    }
}
