package seabattle.Game;

import seabattle.*;
import seabattle.Forms.*;

public class AIWorking extends Thread {

    public static AIWorking Instance = new AIWorking();
    public int new_target_x, new_target_y;

    public void run() {
        Battlefield targetBF = Global.Instance.Player1BF;
        PlayCanvas cnv = Global.Instance.PlayCnv;
        cnv.forceRefresh();

        while (true) {
            //определим куда стрелять
            int[] coords = new int[2];
            AITargetSelection.AISelectTarget(coords);
            AIWorking.Instance.new_target_x = coords[0];
            AIWorking.Instance.new_target_y = coords[1];

            //плавно подведем туда курсор
            while (true) {
                if (targetBF.target_x < new_target_x) {
                    targetBF.target_x++;
                }
                if (targetBF.target_x > new_target_x) {
                    targetBF.target_x--;
                }

                if (targetBF.target_y < new_target_y) {
                    targetBF.target_y++;
                }
                if (targetBF.target_y > new_target_y) {
                    targetBF.target_y--;
                }

                cnv.forceRefresh();
                GameLogic.Wait(100, cnv);
                checkPause();

                if ((targetBF.target_x == new_target_x) && (targetBF.target_y == new_target_y)) {
                    break;
                }
            }

            boolean result = cnv.PerformFire(targetBF.target_x,
                    targetBF.target_y);

            checkPause();

            GameLogic.CheckWin();
            if (GameLogic.Winner != 0) {
                return;
            }

            if (!result) {
                break;
            }
        }
        cnv.Targeting = false;

        cnv.forceRefresh();

        //подождем подольше чтобы враг боялсо
        GameLogic.Wait(200, cnv);

        GameLogic.CheckWin();

        cnv.ChangePlayer();

        GameLogic.AIIsWorking = false;
    }

    private void checkPause() {
        while (GameLogic.Paused) {
            try {
                Thread.sleep(100);
            } catch (Exception ex) {
            }
        }
    }
}
