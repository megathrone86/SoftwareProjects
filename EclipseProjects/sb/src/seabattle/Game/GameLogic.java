package seabattle.Game;

import seabattle.*;
import seabattle.Forms.*;

public class GameLogic {
    public static boolean Paused;
    public static boolean DebugMode = false;
    public static boolean ShowEnemyShips = false;
    public static int Winner = 0;

    public static void CheckWin() {
        if (Winner != 0) {
            return;
        }

        if (Global.Instance.Player1BF.CountOfShipUnits == 0) {
            Winner = 2;
        }
        if (Global.Instance.Player2BF.CountOfShipUnits == 0) {
            Winner = 1;
        }

        if (Winner != 0) {
            Global.Instance.PlayCnv.winNotify(Winner);
        }
    }
    public static boolean Player2IsBot;
    public static int SkillLevel;

    public static void Wait(int ms, PlayCanvas cnv) {
        try {
            long endTime = System.currentTimeMillis() + ms;
            while (true) {
                if (cnv != null) {
                    cnv.forceRefresh();
                }

                long delta = endTime - System.currentTimeMillis();
                if (delta <= 0) {
                    break;
                }

                int sleepInterval = 30;
                if (delta < sleepInterval) {
                    sleepInterval = (int) delta;
                }
                Thread.sleep(sleepInterval);
            }
        } catch (Exception ex) {
            System.out.println("Exception in Wait()");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public static boolean AIIsWorking = false;
    public static void AIWork() {
        AIIsWorking = true;
        AIWorking.Instance = null;
        AIWorking.Instance = new AIWorking();
        AIWorking.Instance.start();
    }
}
