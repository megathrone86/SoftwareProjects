package seabattle.Game;

import java.util.*;

import seabattle.*;
import seabattle.Helpers.*;
import seabattle.GameObjects.*;

public class AITargetSelection {
    private static Random rnd = new Random();

    private static int getBFCell(int x, int y) {
        return Global.Instance.Player1BF.GetCell(x, y);
    }

    private static SBArray coeffArray = new SBArray(Global.Instance.BFWidth,
        Global.Instance.BFHeight);
    public static void AISelectTarget(int[] coords) {
        //определим какой длины корабли остались в живых у игрока
        boolean ships_alive[] = new boolean[4];
        for (int i=0; i<Global.Instance.Player1BF.ships.size(); i++) {
            SBShip ship = Global.Instance.Player1BF.getShipByIndex(i);
            if (!ship.IsDead())
                ships_alive[ship.size-1] = true;
        }

        //поставим всем клеткам значение по умолчанию (100 баллов)
        for (int i=0; i<coeffArray.width*coeffArray.height; i++) {
            coeffArray.cells[i] = 100;
        }

        for (int x = 0; x < Global.Instance.BFWidth; x++)
            for (int y = 0; y < Global.Instance.BFHeight; y++) {
                //обнулим баллы для клеток в которые нельзя выстрелить
                if (!Global.Instance.Player1BF.CanShootToCell(x, y))
                {
                    coeffArray.SetValue(x, y, 0);
                    continue;
                }

                //добавим баллы для клеток где есть рядом раненые корабли
                if (getBFCell(x-1, y)==2)
                    coeffArray.AddValue(x, y, 100);
                if (getBFCell(x+1, y)==2)
                    coeffArray.AddValue(x, y, 100);
                if (getBFCell(x, y-1)==2)
                    coeffArray.AddValue(x, y, 100);
                if (getBFCell(x, y+1)==2)
                    coeffArray.AddValue(x, y, 100);

                //анализ того какие оставшиеся в живых корабли можно впихнуть
                //в данную клетку
                {
                    int x_free_space, y_free_space;

                    //горизонталь
                    for (x_free_space = 0; x_free_space < 4; x_free_space++)
                        if (!Global.Instance.Player1BF.CanShootToCell(x +
                            x_free_space + 1, y))
                            break;
                    for (int i=0; i<4; i++)
                        if (ships_alive[i] && (x_free_space>=i) ) {
                            for (int j=0; j<i+1; j++)
                                coeffArray.AddValue(x + j, y, 5);
                        }
                    
                    //вертикаль
                    for (y_free_space = 0; y_free_space < 4; y_free_space++)
                        if (!Global.Instance.Player1BF.CanShootToCell(x,
                            y + y_free_space + 1))
                            break;
                    for (int i=0; i<4; i++)
                        if (ships_alive[i] && (y_free_space>=i) ) {
                            for (int j=0; j<i+1; j++)
                                coeffArray.AddValue(x, y + j, 5);
                        }
                }

                //на тяжелом уровне сложности есть 0.5% шанс пропалить корабли
                //игрока
                if ((GameLogic.SkillLevel==3) && (rnd.nextInt(1000) <= 5) &&
                        getBFCell(x, y)==1)
                    coeffArray.AddValue(x, y, 50);
            }

        //определим клетку с максимальными баллами (их допускается несколько)
        int max = 0;
        for (int i=0; i<coeffArray.width*coeffArray.height; i++) {
            if (coeffArray.cells[i] > max)
                max = coeffArray.cells[i];
        }

        //выберем любую случайную клетку с максимальным количеством баллов
        System.out.println("max = " + max);
        while (true) {
            coords[0] = rnd.nextInt(Global.Instance.BFWidth);
            coords[1] = rnd.nextInt(Global.Instance.BFHeight);

            boolean cellOK = false;
            if (coeffArray.GetValue(coords[0], coords[1])==max)
                cellOK = true;
            else {
                //на легком уровне сложности есть 0.8% шанс ударить по не самой
                //привлекательной точке
                if ((GameLogic.SkillLevel==1) && (rnd.nextInt(1000) <= 8)) {
                    System.out.println("AI missed!");
                    cellOK = true;
                }
                //на среднем уровне сложности есть 0.3% шанс ударить по не самой
                //привлекательной точке
                if ((GameLogic.SkillLevel==2) && (rnd.nextInt(1000) <= 3)) {
                    System.out.println("AI missed!");
                    cellOK = true;
                }
            }

            if (cellOK)
                return;            
        }
    }    
}
