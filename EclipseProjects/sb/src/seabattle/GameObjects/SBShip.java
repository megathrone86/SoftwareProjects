package seabattle.GameObjects;

import seabattle.*;
import seabattle.Game.*;

public class SBShip {
    public int pos_x, pos_y;
    public int size;
    public boolean is_vertical;
    public Battlefield bf;

    public int pos2_x() {
        if (is_vertical)
            return pos_x;
        else
            return pos_x + size-1;
    }    

    public int pos2_y() {
        if (is_vertical)
            return pos_y + size-1;
        else
            return pos_y;
    }    

    private long shipsBlinkTime = 0;
    public boolean IsDead() {
        boolean result = true;
        for (int x=pos_x; x<=pos2_x(); x++) {
            for (int y=pos_y; y<=pos2_y(); y++) {
                if (bf.GetCell(x, y)==1) {
                    result = false;
                    break;
                }
            }
        }

        //моргание после победы
        if ((GameLogic.Winner != 0) && (!result)) {
            if (System.currentTimeMillis()-shipsBlinkTime>1000)
                shipsBlinkTime = System.currentTimeMillis();
            if (System.currentTimeMillis()-shipsBlinkTime>500)
                result = true;
        }
        
        return result;
    }

    public void Set (int _pos_x, int _pos_y, int _size, boolean _is_vertical) {
        pos_x = _pos_x;
        pos_y = _pos_y;
        size = _size;
        is_vertical = _is_vertical;
    }

    public void CopyFrom(SBShip ship) {
        bf = ship.bf;
        pos_x = ship.pos_x;
        pos_y = ship.pos_y;
        size = ship.size;
        is_vertical = ship.is_vertical;
    }

    public void moveLeft() {
        pos_x--;

        if (!bf.CheckShipPosition(this))
            pos_x++;
    }

    public void moveRight() {
        pos_x++;

        if (!bf.CheckShipPosition(this))
            pos_x--;
    }

    public void moveUp() {
        pos_y--;

        if (!bf.CheckShipPosition(this))
            pos_y++;
    }

    public void moveDown() {
        pos_y++;

        if (!bf.CheckShipPosition(this))
            pos_y--;
    }

    public void rotate() {
        is_vertical = !is_vertical;
        if (is_vertical) {
            if (pos_y + size>=Global.Instance.BFHeight)
                pos_y = Global.Instance.BFHeight - size;
        } else {
            if (pos_x + size>=Global.Instance.BFWidth)
                pos_x = Global.Instance.BFWidth - size;
        }
    }
}