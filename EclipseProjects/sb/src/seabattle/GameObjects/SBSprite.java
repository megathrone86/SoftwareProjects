package seabattle.GameObjects;

import javax.microedition.lcdui.*;

import seabattle.*;

public class SBSprite {
    public boolean active;

    private int pos_x, pos_y;
    private Image[] imgs;
    private int frameIndex;
    private long frameStart;
    private int frameDelta;
    private boolean looped;
    public int coordsDelta;

    public Image[] imgsAfterAnimationEnds;
    public int frameDeltaAfterAnimationEnds;
    public boolean loopedAfterAnimationEnds;

    public SBSprite() {
        active = false;
    }

    public void MoveTo(int _pos_x, int _pos_y) {
        pos_x = _pos_x;
        pos_y = _pos_y;
    }

    public void Set(int _pos_x, int _pos_y, Image[] _imgs, int _frameDelta, boolean _looped) {
        pos_x = _pos_x;
        pos_y = _pos_y;
        imgs = _imgs;
        frameDelta = _frameDelta;
        looped = _looped;        

        coordsDelta = 0;
        frameIndex = 0;
        frameStart = System.currentTimeMillis();
        active = true;

        imgsAfterAnimationEnds = null;
        frameDeltaAfterAnimationEnds = 0;
    }

    protected void ProcessAfterAnimationEnds() {
        frameIndex = 0;
        if (!looped) {
            if (imgsAfterAnimationEnds==null) {
                active = false;
            }
            else {
                imgs = imgsAfterAnimationEnds;
                frameDelta = frameDeltaAfterAnimationEnds;
                looped = loopedAfterAnimationEnds;

                coordsDelta = 0;
                imgsAfterAnimationEnds = null;
                frameDeltaAfterAnimationEnds = 0;
                loopedAfterAnimationEnds = false;
            }
        }
    }

    public void Draw(Graphics g, int offset_x, int offset_y) {
        if (!active)
            return;

        if (frameDelta>0) {
            if (frameStart+frameDelta<System.currentTimeMillis()) {
                frameIndex++;
                frameStart = System.currentTimeMillis();

                if (frameIndex>=imgs.length)
                    ProcessAfterAnimationEnds();
            }
        }

        int ix = offset_x + pos_x*(Global.Instance.CellSize) - coordsDelta;
        int iy = offset_y + pos_y*(Global.Instance.CellSize) - coordsDelta;

        g.drawImage(imgs[frameIndex], ix, iy, 0);
    }
    
}