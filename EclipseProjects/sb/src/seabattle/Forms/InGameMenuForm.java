package seabattle.Forms;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.Alert;

import seabattle.*;
import seabattle.GUI.*;
import seabattle.Game.GameLogic;
import seabattle.Helpers.*;

public class InGameMenuForm extends SBForm {
    private Canvas NextForm;
    
    public InGameMenuForm() {
        super(LocalizationDict.GetString(20));

        addButton(LocalizationDict.GetString(19), 10);
        addButton(LocalizationDict.GetString(13), 20);
        addButton(LocalizationDict.GetString(6), 30);
    }

    public void ProcessCommand(int id) {
        switch (id) {
            case 10: //Назад
                if (NextForm!=null)
                    Global.Instance.MidletDisplay.setCurrent(NextForm);
                break;
            case 20: //Помощь
                Global.Instance.HelpMenu.Show(this);
                break;
            case 30: //Выход
                Global.Instance.MainMenu.Show();
                break;
        }
    }

    public void Show() {
        super.Show();

        NextForm = null;
        GameLogic.Paused = true;
    }

    public void Show(Canvas _NextForm) {
        Show();

        NextForm = _NextForm;
    }

    protected void hideNotify() {
        super.hideNotify();

        GameLogic.Paused = false;
    }
}
