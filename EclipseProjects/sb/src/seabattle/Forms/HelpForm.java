package seabattle.Forms;

import javax.microedition.lcdui.*;

import seabattle.*;
import seabattle.GUI.*;
import seabattle.Game.GameLogic;
import seabattle.Helpers.*;

public class HelpForm extends SBForm {

    private Canvas NextForm;

    public HelpForm() {
        super(LocalizationDict.GetString(13));

        addLabel(null, LocalizationDict.GetString(26));
        lastObject().HorzAlignment = SBFormObject.HA_LEFT;
        addLabel(LocalizationDict.GetString(22), LocalizationDict.GetString(23));
        lastObject().HorzAlignment = SBFormObject.HA_LEFT;
        addLabel(LocalizationDict.GetString(24), LocalizationDict.GetString(25));
        lastObject().HorzAlignment = SBFormObject.HA_LEFT;

        addButton(LocalizationDict.GetString(19), 10);
    }

    protected void Init() {
        super.Init();

        selectedObjectIndex = 3;
    }

    public void ProcessCommand(int id) {
        switch (id) {
            case 10: //Назад
                if (NextForm != null) {
                    Global.Instance.MidletDisplay.setCurrent(NextForm);
                }
                break;
        }
    }

    public void Show() {
        super.Show();

        NextForm = null;
        GameLogic.Paused = true;
    }

    public void Show(Canvas _NextForm) {
        Show();

        NextForm = _NextForm;
    }

    protected void hideNotify() {
        super.hideNotify();

        GameLogic.Paused = false;
    }
}
