package seabattle.Forms;

import javax.microedition.lcdui.Alert;
import seabattle.*;
import seabattle.GUI.*;
import seabattle.Game.*;
import seabattle.Helpers.*;

public class MainForm extends SBForm {
    public MainForm() {
        super(LocalizationDict.GetString(7));

        addButton(LocalizationDict.GetString(5), 10);
        addButton(LocalizationDict.GetString(12), 20);
        lastObject().Enabled = false;
        addButton(LocalizationDict.GetString(13), 30);        
        addButton(LocalizationDict.GetString(14), 40);
        addButton(LocalizationDict.GetString(6), 50);
    }

    public void ProcessCommand(int id) {
        switch (id) {
            case 10: //Играть против AI
                GameLogic.Player2IsBot = true;
                Global.Instance.SelectSkillMenu.Show();
                break;
            case 20: //Играть против человека
                break;
            case 30: //Помощь
                Global.Instance.HelpMenu.Show(this);
                break;
            case 40: //Об авторах
                Global.Instance.AboutMenu.Show(this);
                break;
            case 50: //Выход
                SBMIDlet.Instance.destroyApp(false);
                SBMIDlet.Instance.notifyDestroyed();
                break;
        }
    }
}
