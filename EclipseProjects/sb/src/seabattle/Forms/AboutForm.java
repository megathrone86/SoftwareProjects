package seabattle.Forms;

import javax.microedition.lcdui.*;

import seabattle.*;
import seabattle.GUI.*;
import seabattle.Game.GameLogic;
import seabattle.Helpers.*;

public class AboutForm extends SBForm {
    private Canvas NextForm;
    
    public AboutForm() {
        super(LocalizationDict.GetString(14));

        addLabel(null, LocalizationDict.GetString(21));
        lastObject().HorzAlignment = SBFormObject.HA_LEFT;
        addButton(LocalizationDict.GetString(19), 10);        
    }

    protected void Init() {
        super.Init();

        selectedObjectIndex = 1;
    }

    public void ProcessCommand(int id) {
        switch (id) {
            case 10: //Назад
                if (NextForm!=null)
                    Global.Instance.MidletDisplay.setCurrent(NextForm);
                break;            
        }
    }

    public void Show() {
        super.Show();

        NextForm = null;
        GameLogic.Paused = true;
    }

    public void Show(Canvas _NextForm) {
        Show();

        NextForm = _NextForm;
    }

    protected void hideNotify() {
        super.hideNotify();

        GameLogic.Paused = false;
    }
}
