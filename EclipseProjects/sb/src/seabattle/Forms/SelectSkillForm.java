package seabattle.Forms;

import seabattle.GUI.*;
import seabattle.Game.*;
import seabattle.Global;
import seabattle.Helpers.*;

public class SelectSkillForm extends SBForm {
    public SelectSkillForm() {
        super(LocalizationDict.GetString(15));

        addButton(LocalizationDict.GetString(16), 10);
        addButton(LocalizationDict.GetString(17), 20);
        addButton(LocalizationDict.GetString(18), 30);
        addButton(LocalizationDict.GetString(19), 40);
    }

    protected void Init() {
        super.Init();

        selectedObjectIndex = 1;
    }

    public void ProcessCommand(int id) {
        switch (id) {
            case 10: //Легкий
                GameLogic.SkillLevel = 1;
                ShipsSelection.Start();
                break;
            case 20: //Средний
                GameLogic.SkillLevel = 2;
                ShipsSelection.Start();
                break;
            case 30: //Сложный
                GameLogic.SkillLevel = 3;
                ShipsSelection.Start();
                break;
            case 40: //Сложный
                Global.Instance.MainMenu.Show();
                break;
        }
    }
}
