package seabattle.Forms;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;
import java.util.*;

import seabattle.*;
import seabattle.Helpers.*;
import seabattle.Game.*;

public class SelectionCanvas extends GameCanvas {

    private class UpdateTimerTask extends TimerTask {

        private SelectionCanvas cnv;

        public UpdateTimerTask(SelectionCanvas _cnv) {
            cnv = _cnv;
        }

        public void run() {
            if ((System.currentTimeMillis() - LastPaintTime) > 50) {
                cnv.repaint();
            }
        }
    }
    public String TopMessage;
    public int TopMessageHeight;
    Timer blinkTimer;
    public boolean SelectionVisible;
    public boolean SelectionEnabled;
    int screen_width, screen_height;

    public void Init() {
        SelectionVisible = true;
        SelectionEnabled = true;

        screen_width = getWidth();
        screen_height = getHeight();

        blinkTimer = new Timer();
        blinkTimer.schedule(new UpdateTimerTask(this), 0, 30);
    }

    public SelectionCanvas() {
        super(false);

        setFullScreenMode(true);

        TopMessage = LocalizationDict.GetString(8);

        TopMessageHeight = Global.Instance.MessagesFont.getHeight();

        Init();
    }

    public void forceRefresh() {
        paint(getGraphics());
        flushGraphics();
    }

    public void keyPressed(int keyCode) {
        switch (keyCode) {
            case -7:
            case 51:
                Global.Instance.InGameMenu.Show(this);
                break;
                
            case LEFT:
            case KEY_NUM4:
            case -3:
                ShipsSelection.CurrentSelShip.moveLeft();
                break;

            case RIGHT:
            case KEY_NUM6:
            case -4:
                ShipsSelection.CurrentSelShip.moveRight();
                break;

            case UP:
            case KEY_NUM2:
            case -1:
                ShipsSelection.CurrentSelShip.moveUp();
                break;

            case DOWN:
            case KEY_NUM8:
            case -2:
                ShipsSelection.CurrentSelShip.moveDown();
                break;

            case FIRE:
            case KEY_NUM5:
            case -5:
                if (SelectionEnabled) {
                    Global.Instance.Player1BF.AddShip(ShipsSelection.CurrentSelShip);
                    ShipsSelection.SelectNextShip();
                }
                break;

            case KEY_NUM0:
                ShipsSelection.CurrentSelShip.rotate();
                break;
        }

        SelectionEnabled = Global.Instance.Player1BF.CheckShipPositionAndIntersect(ShipsSelection.CurrentSelShip);

        // Перерисовываем
        repaint();
    }
    public long LastSelectionChange = 0;

    public void recalc_animation() {
        if ((System.currentTimeMillis() - LastSelectionChange) > 300) {
            SelectionVisible = !SelectionVisible;
            LastSelectionChange = System.currentTimeMillis();
        }
    }
    public long LastPaintTime = 0;

    public void paint(Graphics g) {
        LastPaintTime = System.currentTimeMillis();

        recalc_animation();

        //заливка
        g.setColor(255, 255, 255);
        g.fillRect(0, 0, screen_width, screen_height);

        //сообщение
        g.setColor(10, 10, 200);
        g.setFont(Global.Instance.MessagesFont);
        g.drawString(TopMessage, 5, 1, 0);

        //поле
        int offset_x = (screen_width - Global.Instance.BFWidth *
                Global.Instance.CellSize) / 2;
        int offset_y = TopMessageHeight + 1;
        Global.Instance.Player1BF.Draw(g, screen_width, screen_height,
                offset_x, offset_y, true);

        //сколько кораблей осталось расставить
        int end_y = offset_y + Global.Instance.BFHeight * Global.Instance.CellSize;
        g.setColor(10, 10, 200);
        g.setFont(Global.Instance.MessagesFont);
        g.drawString(LocalizationDict.GetString(9) + " " +
                ShipsSelection.GetShipsLeft(), 3, end_y + 1, 0);

        //текущий выбор
        if (ShipsSelection.CurrentSelShip.size > 0) {
            int ix = offset_x +
                    ShipsSelection.CurrentSelShip.pos_x * (Global.Instance.CellSize);
            int iy = offset_y +
                    ShipsSelection.CurrentSelShip.pos_y * (Global.Instance.CellSize);

            Image img = ImagesHelper.GetShipImage(ShipsSelection.CurrentSelShip.size,
                    ShipsSelection.CurrentSelShip.is_vertical);

            g.drawImage(img, ix, iy, 0);

            //рамка
            if (SelectionVisible) {
                if (SelectionEnabled) {
                    g.setColor(0, 250, 0);
                } else {
                    g.setColor(250, 0, 0);
                }

                int sx, sy;
                if (ShipsSelection.CurrentSelShip.is_vertical) {
                    sx = Global.Instance.CellSize;
                    sy = ShipsSelection.CurrentSelShip.size * Global.Instance.CellSize;
                } else {
                    sx = ShipsSelection.CurrentSelShip.size * Global.Instance.CellSize;
                    sy = Global.Instance.CellSize;
                }

                g.drawRoundRect(ix, iy, sx, sy, 5, 5);
                g.drawRoundRect(ix + 1, iy + 1, sx - 2, sy - 2, 4, 4);
            }
        }
    }

    public void Show(boolean doInit) {
        if (doInit)
            Init();
        
        Global.Instance.MidletDisplay.setCurrent(this);
    }

    protected void hideNotify() {
        blinkTimer.cancel();
    }
}