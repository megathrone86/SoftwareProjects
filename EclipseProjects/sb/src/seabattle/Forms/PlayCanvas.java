package seabattle.Forms;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;
import java.util.*;

import seabattle.*;
import seabattle.Helpers.*;
import seabattle.Game.*;
import seabattle.GameObjects.*;

public class PlayCanvas extends GameCanvas {

    private class RefreshTimerTask extends TimerTask {

        private PlayCanvas cnv;

        public RefreshTimerTask(PlayCanvas _cnv) {
            cnv = _cnv;
        }

        public void run() {
            if ((System.currentTimeMillis() - lastPaintTime) > 50) {
                cnv.repaint();
            }
        }
    }
    Timer refreshTimer;
    public boolean Targeting;
    public boolean Fading;
    public Battlefield CurrentBF;
    int playerNum;
    int screen_width, screen_height;
    int field_offset_x, field_offset_y;
    long firingStarted;
    long winMessageStarted;
    SBSprite aimSprite;

    public boolean PerformFire(int x, int y) {
        int cellValue = CurrentBF.GetCell(x, y);

        //промазал. ход переходит к другому игроку
        if (cellValue == 0) {
            CurrentBF.SetCell(x, y, 3);
            Targeting = false;
            forceRefresh();

            GameLogic.Wait(300, this);

            return false;
        }

        //попал, ход сохраняется
        if (cellValue == 1) {
            CurrentBF.SetCell(x, y, 2);
            CurrentBF.SetCell(x + 1, y + 1, 3);
            CurrentBF.SetCell(x + 1, y - 1, 3);
            CurrentBF.SetCell(x - 1, y - 1, 3);
            CurrentBF.SetCell(x - 1, y + 1, 3);
            Targeting = true;

            SBSprite spr = CurrentBF.AddSprite(x, y, ImagesHelper.hitImages,
                    100, false);
            if (spr != null) {
                spr.coordsDelta = Global.Instance.CellSize / 2;
                spr.imgsAfterAnimationEnds = ImagesHelper.fireImages;
                spr.frameDeltaAfterAnimationEnds = 80;
                spr.loopedAfterAnimationEnds = true;
            }

            Targeting = false;
            GameLogic.Wait(450, this);
            Targeting = true;
        }

        return true;
    }

    public boolean IsHumanActing() {
        return (playerNum == 1) || ((playerNum == 2) &&
                !GameLogic.Player2IsBot);
    }

    public void CurrentPlayerFire() {
        if (IsHumanActing()) {
            if (!PerformFire(CurrentBF.target_x, CurrentBF.target_y)) {
                ChangePlayer();
            }
        }

        GameLogic.CheckWin();
    }

    public void Init() {
        playerNum = 1;
        CurrentBF = Global.Instance.Player2BF;
        Targeting = true;

        refreshTimer = new Timer();
        refreshTimer.schedule(new RefreshTimerTask(this), 0, 50);

        screen_width = getWidth();
        screen_height = getHeight();
        field_offset_x = (screen_width - Global.Instance.BFWidth *
                Global.Instance.CellSize) / 2;
        field_offset_y = Global.Instance.MessagesFontHeight + 1;

        aimSprite.Set(0, 0, ImagesHelper.aimImages, 120, true);
    }

    public PlayCanvas() {
        super(false);
        setFullScreenMode(true);

        aimSprite = new SBSprite();

        Init();
    }

    public void ChangePlayer() {
        fadein_oit();

        if (playerNum == 1) {
            playerNum = 2;
            CurrentBF = Global.Instance.Player1BF;
        } else {
            playerNum = 1;
            CurrentBF = Global.Instance.Player2BF;
        }

        Targeting = true;

        repaint();

        if ((playerNum == 2) && (GameLogic.Player2IsBot)) {
            GameLogic.AIWork();
        }
    }

    public void keyPressed(int keyCode) {
        switch (keyCode) {
            case -7:
            case 51:
                Global.Instance.InGameMenu.Show(this);
                break;

            case LEFT:
            case KEY_NUM4:
            case -3:
                if (/*(!IsHumanActing()) ||*/(GameLogic.Winner != 0)) {
                    break;
                }

                Global.Instance.Player1BF.target_x--;
                if (Global.Instance.Player1BF.target_x < 0) {
                    Global.Instance.Player1BF.target_x = 0;
                }
                break;

            case RIGHT:
            case KEY_NUM6:
            case -4:
                if (/*(!IsHumanActing()) ||*/(GameLogic.Winner != 0)) {
                    break;
                }

                Global.Instance.Player1BF.target_x++;
                if (Global.Instance.Player1BF.target_x > Global.Instance.BFWidth - 1) {
                    Global.Instance.Player1BF.target_x = Global.Instance.BFWidth - 1;
                }
                break;

            case UP:
            case KEY_NUM2:
            case -1:
                if (/*(!IsHumanActing()) ||*/(GameLogic.Winner != 0)) {
                    break;
                }

                Global.Instance.Player1BF.target_y--;
                if (Global.Instance.Player1BF.target_y < 0) {
                    Global.Instance.Player1BF.target_y = 0;
                }
                break;

            case DOWN:
            case KEY_NUM8:
            case -2:
                if (/*(!IsHumanActing()) ||*/(GameLogic.Winner != 0)) {
                    break;
                }

                Global.Instance.Player1BF.target_y++;
                if (Global.Instance.Player1BF.target_y > Global.Instance.BFHeight - 1) {
                    Global.Instance.Player1BF.target_y = Global.Instance.BFHeight - 1;
                }
                break;

            case FIRE:
            case KEY_NUM5:
            case -5:
                if (GameLogic.Winner != 0) {
                    Global.Instance.MainMenu.Show();
                }
                if (!IsHumanActing()) {
                    break;
                }

                CurrentPlayerFire();
                break;
        }

        // Перерисовываем
        repaint();
    }

    public void fadein_oit() {
        Fading = true;
        Graphics g = getGraphics();

        g.setColor(200, 200, 200);
        g.fillRect(0, 0, g.getClipWidth(), g.getClipHeight());
        g.setColor(0, 0, 0);
        g.drawString(LocalizationDict.GetString(2), (int) (g.getClipWidth() * 0.3),
                (int) (g.getClipHeight() * 0.45), 0);
        flushGraphics();

        GameLogic.Wait(500, this);

        Fading = false;
    }

    public void forceRefresh() {
        paint(getGraphics());
        flushGraphics();
    }
    private long lastPaintTime = 0;

    public void paint(Graphics g) {
        lastPaintTime = System.currentTimeMillis();

        //периодическая смена экранов через каждые 5 секунд
        //если кто-то победил
        if (GameLogic.Winner != 0) {
            if (lastPaintTime - winMessageStarted > 5000) {
                winMessageStarted = lastPaintTime;

                if (CurrentBF == Global.Instance.Player1BF) {
                    CurrentBF = Global.Instance.Player2BF;
                } else {
                    CurrentBF = Global.Instance.Player1BF;
                }
            }
        }

        //если сейчас выведен экран с сообщением о смене хода
        //ничего не делаем чтобы не стереть его
        if (Fading) {
            return;
        }

        //заливка
        g.setColor(255, 255, 255);
        g.fillRect(0, 0, screen_width, screen_height);

        //сообщение
        {
            String TopMessage = "";
            if (GameLogic.Winner == 0) {
                if (playerNum == 1) {
                    TopMessage = LocalizationDict.GetString(0);
                }
                if (playerNum == 2) {
                    TopMessage = LocalizationDict.GetString(1);
                }
            } else {
                if (CurrentBF == Global.Instance.Player1BF) {
                    TopMessage = LocalizationDict.GetString(10);
                }
                if (CurrentBF == Global.Instance.Player2BF) {
                    TopMessage = LocalizationDict.GetString(11);
                }
            }

            g.setColor(10, 10, 200);
            g.setFont(Global.Instance.MessagesFont);
            g.drawString(TopMessage, 5, 1, 0);
        }

        //поле
        boolean paintShips = false;
        if ((CurrentBF == Global.Instance.Player1BF) &&
                (GameLogic.Player2IsBot) && (GameLogic.Winner == 0)) {
            paintShips = true;
        }
        if (GameLogic.ShowEnemyShips) {
            paintShips = true;
        }

        CurrentBF.Draw(g, screen_width, screen_height, field_offset_x,
                field_offset_y, paintShips);

        //текущий выбор
        if (Targeting /*&& (System.currentTimeMillis() > firingStarted + 400)*/) {
            aimSprite.MoveTo(CurrentBF.target_x, CurrentBF.target_y);

            aimSprite.Draw(g, field_offset_x, field_offset_y);
        }

        //сколько у кого осталось кораблей
        {
            //подготовка
            int offset_y = Global.Instance.MessagesFontHeight + 1;
            int end_y = offset_y + Global.Instance.BFHeight *
                    Global.Instance.CellSize;
            int offset2_y = end_y + Global.Instance.MessagesFontHeight;
            g.setColor(10, 10, 200);
            g.setFont(Global.Instance.MessagesFont);

            //игрок 1
            String s1 = LocalizationDict.GetString(10) +
                    Global.Instance.Player1BF.CountOfShipUnits;
            g.drawString(s1, 3, end_y + 1, 0);

            //игрок 2
            String s2 = LocalizationDict.GetString(11) +
                    Global.Instance.Player2BF.CountOfShipUnits;
            int sw = Global.Instance.MessagesFont.stringWidth(s2);
            g.drawString(s2, screen_width - sw - 3, end_y + 1, 0);

            //полоска игрока 1
            int player1ShipsRatio = 1000 *
                    Global.Instance.Player1BF.CountOfShipUnits /
                    Global.Instance.Player1BF.MaxCountOfShipUnits;
            DrawHealthBar(g, 1, screen_width * player1ShipsRatio / 2000,
                    offset2_y, 1);

            //полоска игрока 2
            int player2ShipsRatio = 1000 *
                    Global.Instance.Player2BF.CountOfShipUnits /
                    Global.Instance.Player2BF.MaxCountOfShipUnits;
            DrawHealthBar(g, screen_width - 1,
                    screen_width - 1 - screen_width * player2ShipsRatio / 2000,
                    offset2_y, 2);
        }

        //вывод экрана с сообщением о победе
        if (GameLogic.Winner != 0) {
            g.setFont(Global.Instance.MessagesFont2);

            String s = LocalizationDict.GetString(2 + GameLogic.Winner);
            int stringWidth = Global.Instance.MessagesFont2.stringWidth(s);

            int messageX = (g.getClipWidth() - stringWidth) / 2;
            int messageY = (int) (g.getClipHeight() * 0.45);

            g.setColor(0, 0, 0);
            g.fillRect(messageX - 1, messageY - 1, stringWidth + 2,
                    Global.Instance.MessagesFont2.getHeight() + 2);

            g.setColor(250, 250, 250);
            g.drawString(s, messageX, messageY, 0);
        }
    }

    public void DrawHealthBar(Graphics g, int x1, int x2, int y, int playerNum) {
        if (playerNum == 1) {
            g.setColor(250, 0, 0);
        } else {
            g.setColor(0, 250, 0);
        }
        g.drawLine(x1, y, x2, y);
        g.drawLine(x1, y + 1, x2, y + 1);
    }

    public void Show(boolean doInit) {
        if (doInit) {
            Init();
        }

        Global.Instance.MidletDisplay.setCurrent(this);
    }

    protected void hideNotify() {
        refreshTimer.cancel();
    }

    public void winNotify(int playerNum) {
        Global.Instance.PlayCnv.forceRefresh();

        try {
            Global.Instance.MidletDisplay.vibrate(300);
        } catch (Exception ex) {
        }

        GameLogic.Wait(500, Global.Instance.PlayCnv);
        winMessageStarted = System.currentTimeMillis();
    }
}