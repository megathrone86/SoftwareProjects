package seabattle;

import javax.microedition.lcdui.*;

import seabattle.Forms.*;

public class Global {
    public static Global Instance = new Global();

    //экраны
    public MainForm MainMenu;
    public SelectSkillForm SelectSkillMenu;
    public InGameMenuForm InGameMenu;
    public AboutForm AboutMenu;
    public HelpForm HelpMenu;

    public SelectionCanvas SelectionCnv;
    public PlayCanvas PlayCnv;

    public Display MidletDisplay;

    public Battlefield Player1BF;
    public Battlefield Player2BF;

    public Font MessagesFont;
    public int MessagesFontHeight;
    public Font MessagesFont2;
    public int MessagesFont2Height;
    public int BFWidth = 10;
    public int BFHeight = 10;
    public int CellSize;

    public int DisplayWidth;
    public int DisplayHeight;

    public Global() {
        Player1BF = new Battlefield(BFWidth, BFHeight);
        Player2BF = new Battlefield(BFWidth, BFHeight);

        MessagesFont = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN,
            Font.SIZE_SMALL);
        MessagesFontHeight = MessagesFont.getHeight();

        MessagesFont2 = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD,
            Font.SIZE_SMALL);
        MessagesFont2Height = MessagesFont2.getHeight();
    }
}


