package seabattle;

import javax.microedition.lcdui.*;
import java.util.*;

import seabattle.Helpers.*;
import seabattle.GameObjects.*;

public class Battlefield {
    SBArray Cells; //0 - пусто, 1 - корабль, 2 - убитый корабль, 3 - дырка от пули    

    public Vector ships;    

    private SBSprite[] sprites;

    public int target_x, target_y;

    public int CountOfShipUnits, MaxCountOfShipUnits;

    public SBShip getShipByIndex(int i) {
        return (SBShip) ships.elementAt(i);
    }

    //инициализация
    public Battlefield(int width, int height) {
        Cells = new SBArray(width, height);
        
        ships = new Vector();

        sprites = new SBSprite[25];
        for (int i=0; i<sprites.length; i++) {
            sprites[i] = new SBSprite();
        }        
    }

    public boolean CanShootToCell(int x, int y) {
        int cellValue = GetCell(x, y);

        return ((cellValue == 0) || (cellValue == 1));
    }

    public int GetCell(int x, int y) {
        return Cells.GetValue(x, y);
    }

    public void SetCell(int x, int y, int value) {
        Cells.SetValue(x, y, value);
        
        //если это попадание в корабль
        if (value==2) {
            //пересчитаем все клетки в которые нельзя стрелять
            RecalcUnshottableCells();
            //пересчитаем количество оставшихся живых клеток
            RecalcCountOfShipUnits();
        }
    }

    private void RecalcUnshottableCells() {
        for (int i=0; i<ships.size(); i++) {
            SBShip ship = getShipByIndex(i);

            if (ship.IsDead()) {
                for (int tx=ship.pos_x-1; tx<=ship.pos2_x()+1; tx++)
                    for (int ty=ship.pos_y-1; ty<=ship.pos2_y()+1; ty++)
                        if (GetCell(tx, ty)==0)
                            SetCell(tx, ty, 3);                    
            }
        }
    }

    private void RecalcCountOfShipUnits() {
        CountOfShipUnits=0;
        for (int i=0; i<Cells.width*Cells.height; i++) {
            if (Cells.cells[i] == 1)
                CountOfShipUnits++;
        }
        if (CountOfShipUnits>MaxCountOfShipUnits)
            MaxCountOfShipUnits = CountOfShipUnits;
    }

    //очистить поле
    public void Clear() {
        ships.removeAllElements();
        
        for (int i=0; i<Cells.width*Cells.height; i++) {
            Cells.cells[i] = 0;
        }        

        for (int i=0; i<sprites.length; i++) {
            sprites[i].active = false;
        }
    }

    private int fieldToScreenX(int x, int offset_x) {
        return offset_x + x*(Global.Instance.CellSize);
    }

    private int fieldToScreenY(int y, int offset_y) {
        return offset_y + y*(Global.Instance.CellSize);
    }

    //вывести поле на экран
    public void Draw(Graphics g, int w, int h, int offset_x, int offset_y, boolean paint_ships) {
        g.setColor(10, 10, 200);

        //рассчитаем координаты сетки
        int end_x = offset_x + Global.Instance.BFWidth*Global.Instance.CellSize;
        int end_y = offset_y + Global.Instance.BFHeight*Global.Instance.CellSize;

        //нарисуем горизонтальные линии
        for (int i=0; i<Global.Instance.BFWidth+1; i++) {
            g.drawLine(offset_x + i*(Global.Instance.CellSize), offset_y,
                    offset_x + i*(Global.Instance.CellSize), end_y);        
        }

        //нарисуем вертикальные линии
        for (int i=0; i<Global.Instance.BFHeight+1; i++) {
            g.drawLine(offset_x, offset_y + i*(Global.Instance.CellSize),
                    end_x, offset_y + i*(Global.Instance.CellSize));
        }

        //нарисуем фон
        for (int x=0; x<Global.Instance.BFWidth; x++)
            for (int y=0; y<Global.Instance.BFHeight; y++) {
                int cellValue = GetCell(x, y);
                if (((cellValue==1) && paint_ships) || (cellValue==2) || (cellValue==3)) {
                    int ix = offset_x + x*(Global.Instance.CellSize);
                    int iy = offset_y + y*(Global.Instance.CellSize);

                    g.drawImage(ImagesHelper.seaImage, ix, iy, 0);
                }
            }

        //нарисуем корабли
        for (int i=0; i<ships.size(); i++) {
            SBShip ship = getShipByIndex(i);

            if (ship.size==0)
                continue;

            //если paint_ships=true, рисуем корабли всегда (этап расстановки)
            //в противном случае рисуем только если корабль полностью подбит
            if (paint_ships || ship.IsDead()) {
                int ix = fieldToScreenX(ship.pos_x, offset_x);
                int iy = fieldToScreenY(ship.pos_y, offset_y);

                g.drawImage(ImagesHelper.GetShipImage(ship.size, ship.is_vertical),
                        ix, iy, 0);
            }
        }

        //нарисуем метки
        for (int i=0; i<sprites.length; i++) {
            sprites[i].Draw(g, offset_x, offset_y);
        }
    }

    //проверить положение корабля (чтоб не выходил за рамки поля)
    public boolean CheckShipPosition(SBShip ship) {
        return (ship.pos_x>=0) && (ship.pos_y>=0) && (ship.pos2_x() < Global.Instance.BFWidth) &&
                    (ship.pos2_y() < Global.Instance.BFHeight);
    }

    //проверить положение корабля (чтоб не выходил за рамки поля
    // и не пересекался с другими кораблями)
    public boolean CheckShipPositionAndIntersect(SBShip ship) {
        if (!CheckShipPosition(ship))
            return false;

        for (int x = ship.pos_x-1; x <= ship.pos2_x()+1; x++)
            for (int y = ship.pos_y-1; y <= ship.pos2_y()+1; y++) {
            if ((GetCell(x, y)!=0) && (GetCell(x, y)!=-1))
                return false;
        }
        
        return true;
    }

    //добавить корабль на поле
    public void AddShip(SBShip ship) {
        SBShip tship = new SBShip();
        tship.CopyFrom(ship);
        ships.addElement(tship);

        for (int x = ship.pos_x; x <= ship.pos2_x(); x++)
            for (int y = ship.pos_y; y <= ship.pos2_y(); y++) {
                SetCell(x, y, 1);
            }        

        RecalcCountOfShipUnits();
    }

    public SBSprite AddSprite(int _pos_x, int _pos_y, Image[] _imgs,
            int _frameDelta, boolean _looped) {
        for (int i=0; i<sprites.length; i++) {
            if (!sprites[i].active) {
                sprites[i].Set(_pos_x, _pos_y, _imgs, _frameDelta, _looped);
                return sprites[i];
            }
        }

        return null;
    }
}
