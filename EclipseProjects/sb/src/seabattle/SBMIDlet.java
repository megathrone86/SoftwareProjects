package seabattle;

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

import seabattle.Helpers.*;
import seabattle.Forms.*;

public class SBMIDlet extends MIDlet {
    public static SBMIDlet Instance = null;
    public static boolean firstLaunch = true;

    public void CreateForms() {
        //игровые формы
        Global.Instance.MidletDisplay = Display.getDisplay(this);
        Global.Instance.SelectionCnv = new SelectionCanvas();
        Global.Instance.PlayCnv = new PlayCanvas();        

        //менюшные формы
        Global.Instance.MainMenu = new MainForm();
        Global.Instance.MainMenu.ObjectsFont =
                Global.Instance.MessagesFont;
        Global.Instance.MainMenu.Resize();

        Global.Instance.SelectSkillMenu = new SelectSkillForm();
        Global.Instance.SelectSkillMenu.ObjectsFont =
                Global.Instance.MessagesFont;
        Global.Instance.SelectSkillMenu.Resize();

        Global.Instance.InGameMenu = new InGameMenuForm();
        Global.Instance.InGameMenu.ObjectsFont =
                Global.Instance.MessagesFont;
        Global.Instance.InGameMenu.Resize();
        
        Global.Instance.AboutMenu = new AboutForm();
        Global.Instance.AboutMenu.ObjectsFont =
                Global.Instance.MessagesFont;
        Global.Instance.AboutMenu.Resize();

        Global.Instance.HelpMenu = new HelpForm();
        Global.Instance.HelpMenu.ObjectsFont =
                Global.Instance.MessagesFont;
        Global.Instance.HelpMenu.Resize();
    }

    public SBMIDlet() {
        Instance = this;

        //такой вот быдлокод чтобы определить размер экрана :(
        PlayCanvas cnv = new PlayCanvas();
        Global.Instance.DisplayWidth = cnv.getWidth();
        Global.Instance.DisplayHeight = cnv.getHeight();

        CreateForms();
        ImagesHelper.Init();
    }

    public void startApp() {
        if (!firstLaunch)
            return;
        firstLaunch = false;
        
        Global.Instance.MainMenu.Show();        
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }
}
