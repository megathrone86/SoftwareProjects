package seabattle.Helpers;

import javax.microedition.lcdui.*;

import seabattle.*;

public class ImagesHelper {
    private static Image[] ShipsImages;
    public static Image seaImage;
    public static Image[] hitImages;
    public static Image[] fireImages;
    public static Image[] aimImages;

    private static int[] resizeArray(int[] array, int srcWidth, int srcHeight, int dstWidth, int dstHeight) {
        int result[] = new int[dstWidth * dstHeight];
        for (int y = 0; y < dstHeight; y++) {
            int dy = y * srcHeight / dstHeight;
            for (int x = 0; x < dstWidth; x++) {
                int dx = x * srcWidth / dstWidth;
                result[(dstWidth * y) + x] = array[(srcWidth * dy) + dx];
            }
        }
        return result;
    }

    public static Image ResizeImage(Image src, int dstWidth, int dstHeight) {
        int srcRgb[] = new int[src.getWidth() * src.getHeight()];
        src.getRGB(srcRgb, 0, src.getWidth(), 0, 0, src.getWidth(), src.getHeight());
        int dstRgb[] = resizeArray(srcRgb, src.getWidth(), src.getHeight(), dstWidth, dstHeight);
        return Image.createRGBImage(dstRgb, dstWidth, dstHeight, true);
    }    

    public static Image GetShipImage(int size, boolean vertical) {
        int orientation = 0;
        if (!vertical)
            orientation = 1;
        
        int shipIndex = (size-1)*2 + orientation;

        return ShipsImages[shipIndex];
    }

    public static Image LoadImage(String path, int sx, int sy) throws Exception {
        try {
            return ResizeImage(Image.createImage(path), sx, sy);
        } catch (Exception ex) {
            throw new Exception("Ошибка при загрузке файла " + path + ". " + ex.getMessage());
        }
    }

    public static void Init() {
        try {
            //==================== расчет размера ячейки
            System.out.println(Global.Instance.DisplayWidth + " " +
                    Global.Instance.DisplayHeight);

            int sx = Global.Instance.DisplayWidth-2;
            int sy = Global.Instance.DisplayHeight - 2 -
                    Global.Instance.MessagesFont.getHeight()*2;
            int cs  = Math.min(sx, sy);
            cs = cs / Math.max(Global.Instance.BFWidth,
                    Global.Instance.BFHeight);

            Global.Instance.CellSize = cs;

            //==================== загрузка картинок
            ShipsImages = new Image[8];
            ShipsImages[0] = LoadImage("/ships/1_v.png", cs, cs);
            ShipsImages[1] = LoadImage("/ships/1_h.png", cs, cs);
            ShipsImages[2] = LoadImage("/ships/2_v.png", cs, cs*2);
            ShipsImages[3] = LoadImage("/ships/2_h.png", cs*2, cs);
            ShipsImages[4] = LoadImage("/ships/3_v.png", cs, cs*3);
            ShipsImages[5] = LoadImage("/ships/3_h.png", cs*3, cs);
            ShipsImages[6] = LoadImage("/ships/4_v.png", cs, cs*4);
            ShipsImages[7] = LoadImage("/ships/4_h.png", cs*4, cs);

            hitImages = new Image[5];
            hitImages[0] = LoadImage("/hit/0.png", cs*2, cs*2);
            hitImages[1] = LoadImage("/hit/1.png", cs*2, cs*2);
            hitImages[2] = LoadImage("/hit/2.png", cs*2, cs*2);
            hitImages[3] = LoadImage("/hit/3.png", cs*2, cs*2);
            hitImages[4] = LoadImage("/hit/4.png", cs*2, cs*2);

            fireImages = new Image[6];
            fireImages[0] = LoadImage("/fire/0.png", cs, cs);
            fireImages[1] = LoadImage("/fire/1.png", cs, cs);
            fireImages[2] = LoadImage("/fire/2.png", cs, cs);
            fireImages[3] = LoadImage("/fire/3.png", cs, cs);
            fireImages[4] = LoadImage("/fire/4.png", cs, cs);
            fireImages[5] = LoadImage("/fire/5.png", cs, cs);

            aimImages = new Image[4];
            aimImages[0] = LoadImage("/aim/0.png", cs, cs);
            aimImages[1] = LoadImage("/aim/1.png", cs, cs);
            aimImages[2] = LoadImage("/aim/2.png", cs, cs);
            aimImages[3] = LoadImage("/aim/3.png", cs, cs);            

            seaImage = LoadImage("/sea.png", cs, cs);
            

        } catch (Exception ex) {
            ExceptionHelper.ProcessException(ex);
        }
    }
}


