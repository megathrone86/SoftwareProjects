package seabattle.Helpers;

public class SBArray {
    public int[] cells;
    public int height, width;

    public SBArray(int _width, int _height) {
        height = _height;
        width = _width;
        cells = new int[width*height];
    }

    public int GetValue(int x, int y) {
        if ((x<0) || (x>=width))
            return -1;
        if ((y<0) || (y>=height))
            return -1;

        return cells[x+y*width];
    }

    public void SetValue(int x, int y, int value) {
        if ((x<0) || (x>=width))
            return;
        if ((y<0) || (y>=height))
            return;

        cells[x+y*width] = value;
    }

    public void AddValue(int x, int y, int value) {
        int t = GetValue(x, y);
        SetValue(x, y, t + value);
    }

}
