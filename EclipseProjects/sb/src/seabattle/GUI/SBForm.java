package seabattle.GUI;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;
import java.util.*;

import seabattle.*;

public abstract class SBForm extends GameCanvas {

    private class RefreshTimerTask extends TimerTask {

        private SBForm cnv;

        public RefreshTimerTask(SBForm _cnv) {
            cnv = _cnv;
        }

        public void run() {
            if ((System.currentTimeMillis() - LastPaintTime) > 40) {
                cnv.repaint();
            }
        }
    }
    public Font ObjectsFont;
    private Timer refreshTimer;
    private int screen_width, screen_height;
    private Vector objects;
    public int selectedObjectIndex;
    private SBFormLabel captionLabel;

    private int formHeight;
    public int ScrollY;

    public SBForm(String _caption) {
        super(false);
        setFullScreenMode(true);
        objects = new Vector();
        ObjectsFont = Font.getDefaultFont();

        captionLabel = new SBFormLabel();
        captionLabel.Parent = this;
        captionLabel.SetText(_caption, null);
    }

    protected void Init() {
        refreshTimer = new Timer();
        refreshTimer.schedule(new RefreshTimerTask(this), 0, 100);

        selectedObjectIndex = 0;

        Resize();
    }

    public void Show() {
        Init();
        Global.Instance.MidletDisplay.setCurrent(this);
    }

    public abstract void ProcessCommand(int id);

    protected void hideNotify() {
        refreshTimer.cancel();
    }

    public SBFormLabel addLabel(String caption, String message) {
        SBFormLabel lbl = new SBFormLabel();
        lbl.Parent = this;
        lbl.SetText(caption, message);

        addObject(lbl);
        return lbl;
    }

    public SBFormButton addButton(String message, int id) {
        SBFormButton bt = new SBFormButton();
        bt.Parent = this;
        bt.message = message;
        bt.id = id;

        addObject(bt);
        return bt;
    }

    public SBFormObject lastObject() {
        SBFormObject o = (SBFormObject) objects.lastElement();
        return o;
    }

    private void addObject(SBFormObject o) {
        objects.addElement(o);
        o.Parent = this;
        Resize();
    }

    //хитрая рекурсивная функция
    private void ChangeSelectedIndex(int currentIndex, int delta) {
        selectedObjectIndex += delta;

        //перескакивать на конец или начало списка надо только если камера
        //дошла до края экрана
        if (selectedObjectIndex < 0) {
            selectedObjectIndex = objects.size() - 1;
        }
        if (selectedObjectIndex > objects.size() - 1) {
            selectedObjectIndex = 0;
        }

        SBFormObject o = (SBFormObject) objects.elementAt(selectedObjectIndex);
        if (!o.Enabled || !o.Clickable) {
            ChangeSelectedIndex(currentIndex, delta);
        }

        if (currentIndex == selectedObjectIndex) {
            return;
        }
    }

    private void ChangeSelectedIndex(int delta) {
        //если мы движеимся в сторону выбранного объекта, но он еще не показался,
        //по идее не надо выбирать следующий
        
        int oldSelectedObjectIndex = selectedObjectIndex;
        ChangeSelectedIndex(selectedObjectIndex, delta);

        if (oldSelectedObjectIndex != selectedObjectIndex) {
            //если выбранный объект сменился, поидее надо проскроллить
            //экран чтобы показать его
        }
    }

    private void MoveScroll(int delta) {
        ScrollY += delta;

        System.out.println(getHeight() + " " + formHeight);

        if (ScrollY + getHeight() > formHeight)
            ScrollY = formHeight - getHeight();
        if (ScrollY<0)
            ScrollY = 0;        
    }

    public void keyPressed(int keyCode) {
        switch (keyCode) {
            case UP:
            case KEY_NUM2:
            case -1:
                MoveScroll(-30);
                ChangeSelectedIndex(-1);
                break;

            case DOWN:
            case KEY_NUM8:
            case -2:
                MoveScroll(30);
                ChangeSelectedIndex(1);
                break;

            case FIRE:
            case KEY_NUM5:
            case -5:
                SBFormObject o = (SBFormObject) objects.elementAt(selectedObjectIndex);
                if (o.id != Integer.MIN_VALUE) {
                    ProcessCommand(o.id);
                }
                break;
        }

        // Перерисовываем
        repaint();
    }
    public long LastPaintTime = 0;

    public void paint(Graphics g) {
        LastPaintTime = System.currentTimeMillis();

        //очистка
        g.setColor(255, 255, 255);
        g.fillRect(0, 0, screen_width, screen_height);

        //заголовок
        captionLabel.Draw(g);

        //компоненты
        for (int i = 0; i < objects.size(); i++) {
            SBFormObject o = (SBFormObject) objects.elementAt(i);
            o.selected = (i == selectedObjectIndex);
            o.Draw(g);
        }

        //полоса прокрутки
        if (getHeight() < formHeight) {
            g.setColor(115, 115, 115);

            int baseOffsetY = 1;
            int baseHeight = getHeight()-2;
            int baseWidth = 4;
            g.fillRect(getWidth()- baseWidth, baseOffsetY,
                    baseWidth, baseHeight);

            g.setColor(25, 25, 25);
            int yStart = baseHeight * ScrollY/formHeight + baseOffsetY;
            int yHeight = baseHeight * getHeight()/formHeight;
            g.fillRect(getWidth()- baseWidth+1, yStart,
                    baseWidth-1, yHeight);
        }
    }

    public void Resize() {
        screen_width = getWidth();
        screen_height = getHeight();

        int objectDist = 5;

        //название формы
        captionLabel.x = (screen_width) / 10;
        captionLabel.y = 3;

        //все остальные компоненты
        int ty = captionLabel.get_bottom() + objectDist * 2;
        for (int i = 0; i < objects.size(); i++) {
            SBFormObject o = (SBFormObject) objects.elementAt(i);

            if (o.HorzAlignment==SBFormObject.HA_LEFT) {
                o.x = (int) (screen_width *0.02);
            }
            if (o.HorzAlignment==SBFormObject.HA_CENTER) {
                o.x = (screen_width - o.get_sx()) / 2;
            }
            
            o.y = ty;

            ty = o.get_bottom() + objectDist;

            o.RearrangeContent();
            formHeight = o.get_bottom()+5;
            ScrollY = 0;
        }
    }
}
