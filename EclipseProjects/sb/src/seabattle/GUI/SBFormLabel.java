package seabattle.GUI;

import javax.microedition.lcdui.*;
import java.util.*;

import seabattle.*;

public class SBFormLabel extends SBFormObject {

    private String srcCaption;
    private String srcText;

    private boolean hasCaption;
    private Vector StringLines;

    public SBFormLabel() {
        Clickable = false;
    }

    public static Vector SplitTextIntoLines(String text, int maxWidth, Font font) {
        Vector result = new Vector(1);

        int textCurrentIndex = 0;
        String currentLine = "";
        while (textCurrentIndex < text.length()) {
            //флаг принудительного перехода на следующую строку
            boolean finish = false;
            //найдем где кончается следующее слово
            int newWordEnd = textCurrentIndex;
            for (; newWordEnd < text.length(); newWordEnd++) {
                char tc = text.charAt(newWordEnd);
                if ((tc == ' ') || (tc == '\t') || (tc == '\r')) {
                    break;
                }

                //если встречаем символы переноса строки, поставим флаг
                //принудительного перехода (даже если место еще есть)
                if ((tc == '\n') || (tc == '\r')) {
                    finish = true;
                    break;
                }
            }

            //вырежем его из исходной строки
            String newWord = text.substring(textCurrentIndex, newWordEnd);

            //теперь проверим влезает ли текущая строка вместе с новым словом
            //в ширину
            if (font.stringWidth(currentLine + newWord) < maxWidth) {
                //влезает. добавим слово к текущей строке
                currentLine = currentLine + " " + newWord;
                textCurrentIndex = newWordEnd + 1;

                //установлен флаг принудительного перехода.
                //перейдем на следующую строку
                if (finish) {
                    result.addElement(currentLine);
                    currentLine = "";
                }
            } else {
                //не влезает.
                //перейдем на следующую строку (текущее слово будет повторно
                //обработано в следующей итерации)
                result.addElement(currentLine);
                currentLine = "";
            }
        }

        //добавим текущую строку если в ней еще что-то осталось
        if (currentLine.length() > 0) {
            result.addElement(currentLine);
        }

        return result;
    }

    public void SetText(String _caption, String _text) {
        srcCaption = _caption;
        srcText = _text;
        
        if (_text==null)
            StringLines = new Vector(0);
        else
            StringLines = SplitTextIntoLines(_text, get_sx(), Parent.ObjectsFont);
        
        if (_caption==null) {
            hasCaption = false;
        } else {
            hasCaption = true;
            StringLines.insertElementAt(_caption, 0);
        }
    }

    public void Draw(Graphics g) {
        if (!Visible) {
            return;
        }

        int ty = y - Parent.ScrollY;
        
        g.setFont(Parent.ObjectsFont);
        
        for (int i = 0; i < StringLines.size(); i++) {
            String s = (String) StringLines.elementAt(i);

            if ((i==0) && (hasCaption))
                g.setColor(100, 100, 255);
            else
                g.setColor(58, 58, 150);

            g.drawString(s, x, ty + Parent.ObjectsFont.getHeight()*i, 0);
        }

        g.drawLine(0, ty, g.getClipWidth(), ty);
        g.drawLine(0, ty + get_sy(), g.getClipWidth(), ty + get_sy());
    }

    public int get_sx() {
        return (int) (Global.Instance.DisplayWidth * 0.96);
    }

    public int get_sy() {
        return Parent.ObjectsFont.getHeight() * StringLines.size();
    }

    public void RearrangeContent() {
        SetText(srcCaption, srcText);
    }
}