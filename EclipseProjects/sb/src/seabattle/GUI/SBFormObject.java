package seabattle.GUI;

import javax.microedition.lcdui.*;

public abstract class SBFormObject {
    public static final int HA_LEFT = 0;
    public static final int HA_CENTER = 1;

    public SBForm Parent;
    public int id;
    public int x, y;
    public boolean Visible;
    protected boolean selected;
    public boolean Enabled;
    public boolean Clickable;

    public int HorzAlignment;

    public SBFormObject() {
        id = Integer.MIN_VALUE;
        Visible = true;
        selected = false;
        Enabled = true;
        HorzAlignment = HA_CENTER;
        Clickable = true;
    }

    public abstract void Draw(Graphics g);
    public abstract int get_sx();
    public abstract int get_sy();
    public abstract void RearrangeContent();

    public int get_bottom() {
        return y + get_sy();
    }
}
