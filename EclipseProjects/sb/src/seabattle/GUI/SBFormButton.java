package seabattle.GUI;

import java.util.Random;
import javax.microedition.lcdui.*;

public class SBFormButton extends SBFormObject {

    String message;
    static Random rnd = new Random();

    public SBFormButton() {
    }

    public void Draw(Graphics g) {
        if (!Visible) {
            return;
        }

        int ty = y - Parent.ScrollY;

        if (!Enabled) {
            g.setColor(200, 200, 200);
        } else if (selected) {
            g.setColor(200, 0, 0);

            int x1 = 3, y1 = ty + 2, x2 = 3, y2 = ty + get_sy() - 2,
                    x3 = 3 + (y2 - y1) * 13 / 10, y3 = ty + get_sy() / 2;
            g.fillTriangle(x1, y1, x2, y2, x3, y3);

        } else {
            g.setColor(0, 0, 0);
        }

        g.drawLine(0, ty, g.getClipWidth(), ty);
        g.drawLine(0, ty + get_sy(), g.getClipWidth(), ty + get_sy());

        g.setColor(0, 0, 0);
        g.setFont(Parent.ObjectsFont);
        g.drawString(message, x, ty, 0);
    }

    public int get_sx() {
        return Parent.ObjectsFont.stringWidth(message);
    }

    public int get_sy() {
        return Parent.ObjectsFont.getHeight();
    }

    public void RearrangeContent() {
    }
}