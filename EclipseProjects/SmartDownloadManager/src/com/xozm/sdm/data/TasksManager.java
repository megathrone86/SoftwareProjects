package com.xozm.sdm.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.xozm.sdm.helpers.ExtStorageHelper;

import android.util.Log;

public class TasksManager extends SyncronizedObject {
	public static TasksManager Instance = new TasksManager();

	private static boolean SaveNeeded = false;

	private static String FileName = "sdm_tasks.dat";

	private volatile List<Task> _tasks = null;

	public synchronized List<Task> GetFinishedTasks() {
		boolean locked = Lock();

		try {
			return GetTasks(true);
		} finally {
			Unlock(locked);
		}
	}

	public synchronized List<Task> GetCurrentTasks() {
		boolean locked = Lock();

		try {
			return GetTasks(false);
		} finally {
			Unlock(locked);
		}
	}

	private synchronized List<Task> GetTasks(Boolean finished) {
		boolean locked = Lock();

		try {
			List<Task> result = new ArrayList<Task>();

			List<Task> tasks = GetTasks();
			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				if (finished
						&& (task.SavingData.State != TaskSubtypes.TaskState.Finished))
					continue;
				if (!finished
						&& (task.SavingData.State == TaskSubtypes.TaskState.Finished))
					continue;

				result.add(task);
			}

			return result;
		} finally {
			Unlock(locked);
		}
	}

	public synchronized Task GetByGuid(String guid) {
		boolean locked = Lock();

		try {
			List<Task> tasks = GetTasks();

			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				if (task.SavingData.Guid.equals(guid))
					return task;
			}

			return null;
		} finally {
			Unlock(locked);
		}
	}

	private synchronized List<Task> GetTasks() {
		boolean locked = Lock();

		FileInputStream stream = null;
		try {
			if (_tasks != null)
				return _tasks;

			_tasks = new ArrayList<Task>();

			File tasksFile = new File(
					ExtStorageHelper.getExternalStorageFilesDir() + "/"
							+ FileName);
			if (tasksFile.exists()) {
				if (tasksFile.length() == 0)
					System.out.print("ERROR tasks file size is zero");

				stream = new FileInputStream(tasksFile.getPath());

				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(stream);
				Element root = doc.getDocumentElement();
				NodeList items = root.getElementsByTagName("Task");

				for (int i = 0; i < items.getLength(); i++) {
					Node node = items.item(i);
					NodeList attrs = node.getChildNodes();

					try {
						Task task = new Task();

						for (int j = 0; j < attrs.getLength(); j++) {
							node = attrs.item(j);

							if (node.getNodeName().equals("Guid"))
								task.SavingData.Guid = node.getTextContent();
							if (node.getNodeName().equals("URL"))
								task.SavingData.URL = node.getTextContent();
							if (node.getNodeName().equals("LocalDir"))
								task.SavingData.LocalDir = node
										.getTextContent();
							if (node.getNodeName().equals("LocalFileName"))
								task.SavingData.LocalFileName = node
										.getTextContent();
							if (node.getNodeName().equals("TotalTaskSize"))
								task.SavingData.TotalTaskSize = Integer
										.parseInt(node.getTextContent());
							if (node.getNodeName().equals("State"))
								task.SavingData.State = TaskSubtypes.TaskState
										.values()[(Integer.parseInt(node
										.getTextContent()))];
							if (node.getNodeName().equals("Type"))
								task.SavingData.Type = TaskSubtypes.TaskType
										.values()[(Integer.parseInt(node
										.getTextContent()))];
							if (node.getNodeName().equals("Message"))
								task.SavingData.Message = node.getTextContent();
						}

						_tasks.add(task);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (stream != null)
					stream.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			Unlock(locked);
		}

		return _tasks;
	}

	public synchronized void Add(Task task) {
		boolean locked = Lock();

		try {
			if ((task == null) || (task.SavingData.Guid.length() == 0))
				return;

			_tasks.add(task);
		} finally {
			Unlock(locked);
		}
	}

	public synchronized boolean Delete(Task task) {
		boolean locked = Lock();

		try {
			if ((task == null) || (task.SavingData.Guid.length() == 0))
				return false;

			GetTasks();

			for (int i = 0; i < _tasks.size(); i++) {
				if (_tasks.get(i).SavingData.Guid.equals(task.SavingData.Guid)) {
					_tasks.remove(i);
					return true;
				}
			}

			return false;
		} finally {
			Unlock(locked);
		}
	}

	private synchronized void SaveTasks() {
		boolean locked = Lock();

		try {
			String path = ExtStorageHelper.getExternalStorageFilesDir() + "/"
					+ FileName;
			File file = new File(path);
			File tmpFile = new File(path + ".tmp");
			PrintStream printStream = null;
			try {
				printStream = new PrintStream(tmpFile, "utf-8");
				printStream
						.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
				printStream.println("<TasksList>\r\n");

				for (Task task : GetTasks()) {
					boolean taskLocked = task.Lock();

					try {
						printStream.println("\t<Task>\r\n");

						printStream.println("\t\t<Guid>" + task.SavingData.Guid
								+ "</Guid>\r\n");
						printStream.println("\t\t<URL>" + task.SavingData.URL
								+ "</URL>\r\n");
						printStream.println("\t\t<LocalDir>"
								+ task.SavingData.LocalDir + "</LocalDir>\r\n");
						printStream.println("\t\t<LocalFileName>"
								+ task.SavingData.LocalFileName
								+ "</LocalFileName>\r\n");
						printStream.println("\t\t<TotalTaskSize>"
								+ task.SavingData.TotalTaskSize
								+ "</TotalTaskSize>\r\n");
						printStream.println("\t\t<State>"
								+ task.SavingData.State.ordinal()
								+ "</State>\r\n");
						printStream.println("\t\t<Type>"
								+ task.SavingData.Type.ordinal()
								+ "</Type>\r\n");
						printStream.println("\t\t<Message>"
								+ task.SavingData.Message + "</Message>\r\n");

						printStream.println("\t</Task>\r\n");
					} finally {
						task.Unlock(taskLocked);
					}
				}

				printStream.println("</TasksList>");
			} catch (Exception ex) {
				Log.w("Ошибка при сохранении задач.", ex.toString());
			} finally {
				if (printStream != null) {
					printStream.flush();
					printStream.close();
				}

				if (tmpFile.exists()) {
					if (file.exists())
						file.delete();
					tmpFile.renameTo(file);
				}
			}

		} catch (Exception ex) {
			Log.w("", ex.toString());
		} finally {
			Unlock(locked);
		}
	}

	public void SetSaveNeeded() {
		SaveNeeded = true;
	}

	public void SaveIfNeeded() {
		if (SaveNeeded) {
			SaveNeeded = false;
			SaveTasks();
			Log.w("", "tasks saved");
		}
	}
}
