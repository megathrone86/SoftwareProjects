package com.xozm.sdm.data;

public class TaskSubtypes {
	// подтипы

	public enum TaskState {
		Stopped, Waiting, Running, Finished, StoppedByError
	}

	public enum TaskType {
		Unknown, Http, Ftp, Torrent
	}

	public enum PendingOperation {
		None, //ничего не делать 
		Stop, //остановить
		CleanAndRestart, //удалить данные и перезапустить
		DeleteTask, //удалить задачу, оставить данные
		DeleteAll, //удалить задачу и данные
		Restart //перезапустить
	}
}
