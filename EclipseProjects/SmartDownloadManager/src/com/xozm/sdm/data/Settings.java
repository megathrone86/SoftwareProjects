package com.xozm.sdm.data;

import android.os.Environment;

public class Settings {

	public boolean AutoStartNewTasks;
	public String DownloadDir;
	public int MaxTasks;
	public int MaxSpeedKbytes;
	public int Timeout;
	public boolean UseProxy;
	public String ProxyAddress;
	public int ProxyPort;

	public Settings() {
		AutoStartNewTasks = true;
		DownloadDir = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
		MaxTasks = 1;
		MaxSpeedKbytes = 1;
		Timeout = 50 * 1000;
		UseProxy = false;
		ProxyAddress = "";
		ProxyPort = 8080;
	}

}
