package com.xozm.sdm.data;

import java.io.File;
import java.util.UUID;

import com.xozm.sdm.helpers.SpeedMeasurer;
import com.xozm.sdm.helpers.StringHelper;
import com.xozm.sdm.service.DownloadThread;
import com.xozm.sdm.service.HttpDownloadThread;

public class Task extends SyncronizedObject {

	public TaskSavingData SavingData;

	public Task() {
		SavingData = new TaskSavingData();
		SpeedHelper = new SpeedMeasurer();

		SavingData.Guid = UUID.randomUUID().toString();
		SavingData.State = TaskSubtypes.TaskState.Waiting;
		SavingData.Type = TaskSubtypes.TaskType.Unknown;
		SavingData.TotalTaskSize = -1;
		SavingData.LocalDir = "";
		SavingData.LocalFileName = "";
		DownloadThread = null;
		Operation = TaskSubtypes.PendingOperation.None;
		SavingData.Message = "";
	}

	// вычисляемые

	public SpeedMeasurer SpeedHelper;
	public TaskSubtypes.PendingOperation Operation;

	public int getSelectedTaskSize() {
		return SavingData.TotalTaskSize;
	}

	public String getFullLocalPath() {

		if ((SavingData.LocalDir == null)
				|| (SavingData.LocalDir.length() == 0)
				|| (SavingData.LocalFileName == null)
				|| (SavingData.LocalFileName.length() == 0))
			return "";

		return SavingData.LocalDir + File.separator + SavingData.LocalFileName;
	}

	public long GetDownloadedSize() {
		String path = this.getFullLocalPath();
		if (StringHelper.StringIsEmpty(path))
			return 0;

		File file = new File(path);
		if ((file == null) || !file.exists())
			return 0;
		return file.length();
	}

	protected DownloadThread DownloadThread;

	// методы

	public void SetState(TaskSubtypes.TaskState newState) {
		boolean locked = Lock();

		try {
			SavingData.State = newState;
			CheckState();
		} finally {
			Unlock(locked);
		}
	}

	public void CheckState() {
		boolean locked = Lock();

		try {
			switch (SavingData.State) {
			case Running: {
				if ((DownloadThread == null) || !DownloadThread.isAlive()) {
					DownloadThread = new HttpDownloadThread(this);
					DownloadThread.start();
				}
				break;
			}
			case Waiting:
			case Stopped:
			case StoppedByError: {
				if ((DownloadThread != null) && (DownloadThread.isAlive())) {
					DownloadThread.RequestStop();
				}
				break;
			}
			}
		} finally {
			Unlock(locked);
		}
	}

	public void DeleteData() {
		boolean locked = Lock();

		try {
			if (!StringHelper.StringIsEmpty(this.getFullLocalPath())) {
				File file = new File(this.getFullLocalPath());
				if (file.exists())
					file.delete();
			}
		} finally {
			Unlock(locked);
		}
	}

	public void RequestThreadStop() {
		boolean locked = Lock();

		try {
			if ((DownloadThread == null) || (!DownloadThread.isAlive()))
				ProcessPendingOperation();
			else
				DownloadThread.RequestStop();
		} finally {
			Unlock(locked);
		}
	}

	/*public void Rename(String newFileName) {
		boolean locked = Lock();

		RequestThreadStop();

		try {
			if (!StringHelper.StringIsEmpty(this.getFullLocalPath())) {
				File oldFile = new File(this.getFullLocalPath());
				File newFile = new File(newFileName);

				if (oldFile.exists()) {
					if (newFile.exists())
						newFile.delete();
					try {
						FileHelper.copyFile(oldFile, newFile);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				oldFile.delete();

				File file = new File(newFileName);
				this.SavingData.LocalDir = file.getParentFile().getPath();
				this.SavingData.LocalFileName = file.getName();
			}

			Operation = PendingOperation.Restart;
			RequestThreadStop();
		} finally {
			Unlock(locked);
		}
	}*/

	public void ProcessPendingOperation() {
		boolean locked = Lock();

		try {
			if (Operation == TaskSubtypes.PendingOperation.None)
				return;

			switch (Operation) {
			case DeleteTask:
				TasksManager.Instance.Delete(this);
				break;
			case DeleteAll:
				DeleteData();
				TasksManager.Instance.Delete(this);
				break;
			case Stop:
				SetState(TaskSubtypes.TaskState.Stopped);
				break;
			case CleanAndRestart:
				DeleteData();
				SavingData.TotalTaskSize = -1;
				SetState(TaskSubtypes.TaskState.Waiting);
				break;
			case Restart:
				SetState(TaskSubtypes.TaskState.Waiting);
				break;
			}

			Operation = TaskSubtypes.PendingOperation.None;
		} finally {
			Unlock(locked);
		}
		TasksManager.Instance.SetSaveNeeded();
	}
}
