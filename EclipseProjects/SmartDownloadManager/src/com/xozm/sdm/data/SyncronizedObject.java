package com.xozm.sdm.data;

public abstract class SyncronizedObject {
	private volatile Thread _lockerThread;

	public boolean Lock() {
		if (_lockerThread == Thread.currentThread())
			return false;

		synchronized(this)
		{
			while (_lockerThread != null) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			_lockerThread = Thread.currentThread();
		}
		
		return true;
	}

	public void Unlock(boolean doUnlock) {
		if (doUnlock)
			_lockerThread = null;
	}
}
