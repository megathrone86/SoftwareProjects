package com.xozm.sdm.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import android.util.Log;

import com.xozm.sdm.helpers.ExtStorageHelper;

public class SettingsManager extends SyncronizedObject {
	public static SettingsManager Instance = new SettingsManager();

	private static String FileName = "sdm_settings.xml";

	private volatile Settings _settings = null;

	public Settings GetSettings() {
		boolean locked = Lock();

		FileInputStream stream = null;
		try {
			if (_settings != null)
				return _settings;

			_settings = new Settings();

			File settingsFile = new File(
					ExtStorageHelper.getExternalStorageFilesDir() + "/"
							+ FileName);
			if (settingsFile.exists()) {
				if (settingsFile.length() == 0)
					System.out.print("ERROR tasks file size is zero");

				stream = new FileInputStream(settingsFile.getPath());

				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(stream);

				Node node = doc.getDocumentElement();
				NodeList attrs = node.getChildNodes();

				for (int j = 0; j < attrs.getLength(); j++) {
					node = attrs.item(j);

					if (node.getNodeName().equals("DownloadDir"))
						_settings.DownloadDir = node.getTextContent();
					if (node.getNodeName().equals("MaxSpeedKbytes"))
						_settings.MaxSpeedKbytes = Integer.parseInt(node
								.getTextContent());
					if (node.getNodeName().equals("MaxTasks"))
						_settings.MaxTasks = Integer.parseInt(node
								.getTextContent());
					if (node.getNodeName().equals("Timeout"))
						_settings.Timeout = Integer.parseInt(node
								.getTextContent());
					if (node.getNodeName().equals("AutoStartNewTasks"))
						_settings.AutoStartNewTasks = Boolean.parseBoolean(node
								.getTextContent());
					if (node.getNodeName().equals("UseProxy"))
						_settings.UseProxy = Boolean.parseBoolean(node
								.getTextContent());
					if (node.getNodeName().equals("ProxyAddress"))
						_settings.ProxyAddress = node.getTextContent();
					if (node.getNodeName().equals("ProxyPort"))
						_settings.ProxyPort = Integer.parseInt(node
								.getTextContent());
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (stream != null)
					stream.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			Unlock(locked);
		}

		return _settings;
	}

	public void SaveSettings() {
		boolean locked = Lock();

		try {
			String path = ExtStorageHelper.getExternalStorageFilesDir() + "/"
					+ FileName;
			File file = new File(path);
			File tmpFile = new File(path + ".tmp");
			PrintStream printStream = null;
			try {
				printStream = new PrintStream(tmpFile, "utf-8");
				printStream
						.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
				printStream.println("<Settings>\r\n");

				{
					printStream.println("\t<DownloadDir>"
							+ _settings.DownloadDir + "</DownloadDir>\r\n");
					printStream.println("\t<MaxSpeedKbytes>"
							+ _settings.MaxSpeedKbytes
							+ "</MaxSpeedKbytes>\r\n");
					printStream.println("\t<MaxTasks>" + _settings.MaxTasks
							+ "</MaxTasks>\r\n");
					printStream.println("\t<Timeout>" + _settings.Timeout
							+ "</Timeout>\r\n");
					printStream.println("\t<AutoStartNewTasks>"
							+ _settings.AutoStartNewTasks
							+ "</AutoStartNewTasks>\r\n");
					printStream.println("\t<UseProxy>" + _settings.UseProxy
							+ "</UseProxy>\r\n");
					printStream.println("\t<ProxyAddress>"
							+ _settings.ProxyAddress + "</ProxyAddress>\r\n");
					printStream.println("\t<ProxyPort>" + _settings.ProxyPort
							+ "</ProxyPort>\r\n");
				}

				printStream.println("</Settings>");
			} catch (Exception ex) {
				Log.w("Ошибка при сохранении настроек.", ex.toString());
			} finally {
				if (printStream != null) {
					printStream.flush();
					printStream.close();
				}

				if (tmpFile.exists()) {
					if (file.exists())
						file.delete();
					tmpFile.renameTo(file);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			Unlock(locked);
		}

		Log.w("", "settings saved");
	}

}
