package com.xozm.sdm.data;

public class TaskSavingData {

	// сохраняемые

	public String Guid;
	public String URL;
	public String LocalDir;
	public String LocalFileName;
	public int TotalTaskSize;
	public TaskSubtypes.TaskState State;
	public TaskSubtypes.TaskType Type;
	public String Message;

}
