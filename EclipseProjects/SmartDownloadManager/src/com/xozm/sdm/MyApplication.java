package com.xozm.sdm;

import java.util.Locale;

import com.xozm.sdm.data.SettingsManager;
import com.xozm.sdm.data.TasksManager;
import com.xozm.sdm.helpers.Utils;
import com.xozm.sdm.service.MainDownloadService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

public class MyApplication {
	private static Context _applicationContext = null;
	private static Context _serviceContext = null;

	public static int SelectFolderDialogRequest = 666;
	public static int TasksActionsDialogRequest = 667;

	public static TasksListActivity TasksListActivity;
	public static AddTaskActivity AddTaskActivity;
	public static SettingsActivity SettingsActivity;

	public static void setApplicationContext(Context context) {
		_applicationContext = context;
	}

	public static void setServiceContext(Context context) {
		_serviceContext = context;
	}

	public static Context getContext() {
		if (_applicationContext != null)
			return _applicationContext;
		else
			return _serviceContext;
	}

	public static void SetExceptionHandlerForCurrentThread() {
		String logFileName = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/sdm_log.txt";
		Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(
				logFileName));
	}

	public static void Start(Context appContext) {
		_applicationContext = appContext;
		SetExceptionHandlerForCurrentThread();
		Utils.Random.setSeed(System.currentTimeMillis());

		StartMainService();
	}

	public static void Exit() {
		// TODO: debug
		StopMainService();
		
		SettingsManager.Instance.SaveSettings();

		if (TasksListActivity != null)
			TasksListActivity.finish();
		if (AddTaskActivity != null)
			AddTaskActivity.finish();

		TasksManager.Instance.SetSaveNeeded();
	}

	public static void StartAddTaskActivity(Activity currentActivity) {
		try {
			Intent myIntent = new Intent(getContext(), AddTaskActivity.class);
			currentActivity.startActivity(myIntent);
		} catch (Exception e) {
			Log.w("", e.toString());
			e.printStackTrace();
		}
	}
	
	public static void StartSettingsActivity(Activity currentActivity) {
		try {
			Intent myIntent = new Intent(getContext(), SettingsActivity.class);
			currentActivity.startActivity(myIntent);
		} catch (Exception e) {
			Log.w("", e.toString());
			e.printStackTrace();
		}
	}

	public static void RefreshTasks() {
		if (TasksListActivity != null)
			TasksListActivity.RefreshTasks();
	}

	private static void StartMainService() {
		if (MainService.Instance == null) {
			Intent myIntent = new Intent(TasksListActivity, MainService.class);
			_applicationContext.startService(myIntent);
		}
	}

	private static void StopMainService() {
		MainDownloadService.Instance.RequestStop();

		Intent myIntent = new Intent(TasksListActivity, MainService.class);
		_applicationContext.stopService(myIntent);
	}

	public static Locale GetLocale() {
		return getContext().getResources().getConfiguration().locale;
	}
}