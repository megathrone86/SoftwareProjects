package com.xozm.sdm;

import com.xozm.sdm.data.SettingsManager;
import com.xozm.sdm.data.Task;
import com.xozm.sdm.data.TaskSubtypes;
import com.xozm.sdm.data.TasksManager;
import com.xozm.sdm.dialogs.SelectFolderDialog;
import com.xozm.sdm.helpers.HttpHelper;
import com.xozm.sdm.helpers.Utils;
import com.xozm.smartdownloadmanager.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddTaskActivity extends Activity implements OnClickListener {

	private EditText txtUrlText;
	private EditText txtLocalPathText;
	private Button btOK;
	private Button btCancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MyApplication.AddTaskActivity = this;

		try {
			InitGui();
		} catch (Exception e) {
			Log.w(this.getClass().getName(), e.toString());
			e.printStackTrace();
		}
	}

	private void InitGui() {
		this.setContentView(R.layout.add_task_layout);

		{
			txtUrlText = (EditText) this.findViewById(R.id.at_url);

			ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			CharSequence t = clipboard.getText();
			if ((t != null) && (t.length() > 0) && Utils.URLGood(t.toString()))
				txtUrlText.setText(t);
		}

		{
			txtLocalPathText = (EditText) this.findViewById(R.id.at_localpath);
			txtLocalPathText.setOnClickListener(this);
			txtLocalPathText.setInputType(InputType.TYPE_NULL);
		}

		btOK = (Button) this.findViewById(R.id.at_btOK);
		btOK.setOnClickListener(this);

		btCancel = (Button) this.findViewById(R.id.at_btCancel);
		btCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		if (arg0 == btOK) {
			CharSequence url = txtUrlText.getText();
			CharSequence localpath = txtLocalPathText.getText();

			if (!Utils.URLGood(url.toString())) {
				Utils.ShowMessage(MyApplication.getContext().getString(
						R.string.msg_wrongurl));
				return;
			}

			if (!Utils.DirectoryGood(localpath.toString())) {
				Utils.ShowMessage(MyApplication.getContext().getString(
						R.string.msg_wronglocalpath));
				return;
			}

			Task task = new Task();
			task.SavingData.Type = TaskSubtypes.TaskType.Http;
			task.SavingData.LocalDir = localpath.toString();
			if (SettingsManager.Instance.GetSettings().AutoStartNewTasks)
				task.SavingData.State = TaskSubtypes.TaskState.Running;
			task.SavingData.URL = HttpHelper.MakeURLCool(url.toString());

			TasksManager.Instance.Add(task);
			MyApplication.RefreshTasks();

			finish();
		}

		if (arg0 == btCancel) {
			finish();
		}

		if (arg0 == txtLocalPathText) {
			String initialDir = txtLocalPathText.getText().toString();
			if ((initialDir == null) || (initialDir.length() == 0))
				initialDir = SettingsManager.Instance.GetSettings().DownloadDir;

			SelectFolderDialog.ShowDialog(this, initialDir,
					MyApplication.SelectFolderDialogRequest,
					R.drawable.bt_cancel, R.drawable.bt_ok);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ((requestCode == MyApplication.SelectFolderDialogRequest)
				&& (resultCode == RESULT_OK)) {
			txtLocalPathText.setText(data.getStringExtra("currentDir"));
		}
	}
}
