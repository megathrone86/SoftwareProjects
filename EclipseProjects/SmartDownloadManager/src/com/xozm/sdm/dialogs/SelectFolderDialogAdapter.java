package com.xozm.sdm.dialogs;

import java.io.File;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SelectFolderDialogAdapter extends BaseAdapter implements
		OnClickListener {
	public File[] Files;
	private SelectFolderDialog parentDlg;

	public SelectFolderDialogAdapter(File[] files,
			SelectFolderDialog parentDlg) {
		this.Files = files;
		this.parentDlg = parentDlg;
	}

	@Override
	public int getCount() {
		return Files.length;
	}

	@Override
	public Object getItem(int i) {
		return Files[i];
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int index, View view, final ViewGroup parent) {

		TextView textView = null;

		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			view = inflater.inflate(android.R.layout.select_dialog_item,
					parent, false);

			textView = (TextView) view.findViewById(android.R.id.text1);
			textView.setTextSize(18);
			textView.setOnClickListener(this);
			textView.setMinHeight(0);

			// чтобы элементы списка подсвечивались при нажатии
			textView.setBackgroundResource(android.R.drawable.list_selector_background);
		}

		final File file = Files[index];

		if (textView == null)
			textView = (TextView) view.findViewById(android.R.id.text1);

		textView.setText(file.getName());
		textView.setHint(file.getAbsolutePath());
		

		return view;
	}

	@Override
	public void onClick(View arg0) {
		TextView textView = (TextView) arg0;
		parentDlg.SetCurrentDir(new File(textView.getHint().toString()));
	}
}
