package com.xozm.sdm.dialogs;

import com.xozm.sdm.MyApplication;
import com.xozm.smartdownloadmanager.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class ConfirmDialog implements OnClickListener {
	private Activity parentActivity;
	private DialogListener dialogListener;
	private AlertDialog.Builder alert;
	
	public int OperationCode;
	
	public ConfirmDialog(Activity parentActivity, int operationCode) {
		this.parentActivity = parentActivity;
		this.OperationCode = operationCode;
		this.dialogListener = null;
	}

	public void SetDialogListener(DialogListener dialogListener) {
		this.dialogListener = dialogListener;
	}

	public void ShowDialog(String message) {
		alert = new AlertDialog.Builder(parentActivity);
		alert.setMessage(message);

		alert.setPositiveButton(
				MyApplication.getContext().getString(R.string.bt_ok), this);
		alert.setNegativeButton(
				MyApplication.getContext().getString(R.string.bt_cancel), this);		

		parentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				alert.show();
			}
		});		
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		if ((arg1 == DialogInterface.BUTTON_POSITIVE) && (dialogListener != null))
			dialogListener.DialogClosed(this, false);

		if ((arg1 == DialogInterface.BUTTON_NEGATIVE) && (dialogListener != null))
			dialogListener.DialogClosed(this, true);
	}
}
