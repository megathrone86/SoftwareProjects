package com.xozm.sdm.dialogs;

import com.xozm.sdm.MyApplication;
import com.xozm.sdm.data.Task;
import com.xozm.sdm.data.TaskSubtypes;
import com.xozm.sdm.data.TasksManager;
import com.xozm.sdm.helpers.Utils;
import com.xozm.smartdownloadmanager.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class TasksActionsDialog implements OnClickListener, DialogListener {
	private Activity _parentActivity;
	private AlertDialog.Builder _builder;
	private AlertDialog _dialog;

	private Task _task;

	private Button _btStart;
	private Button _btStop;
	private Button _btOpenLocation;
	private Button _btRestart;
	private Button _btDelete;
	private Button _btDeleteAll;
	private Button _btCancel;

	public TasksActionsDialog(Activity parentActivity, Task task) {
		_parentActivity = parentActivity;
		_task = task;

		_builder = new AlertDialog.Builder(_parentActivity);

		InitGui();
	}

	public void Show() {
		_dialog = _builder.show();
		_dialog.setCanceledOnTouchOutside(true);
	}

	public static void ShowDialog(Activity parentActivity, String taskGuid) {
		try {
			Task task = TasksManager.Instance.GetByGuid(taskGuid);

			TasksActionsDialog dlg = new TasksActionsDialog(parentActivity,
					task);
			dlg.Show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void InitGui() {
		Thread.currentThread().setName("TasksActionsDialog thread");

		try {
			_builder.setMessage(String.format(MyApplication.getContext()
					.getString(R.string.tad_name), _task.SavingData.URL));

			// главный контейнер
			LinearLayout mainLayout = new LinearLayout(_parentActivity);
			mainLayout.setId(Utils.Random.nextInt());
			mainLayout.setOrientation(LinearLayout.VERTICAL);

			// кнопки
			{
				if ((_task.SavingData.State != TaskSubtypes.TaskState.Running)
						&& (_task.SavingData.State != TaskSubtypes.TaskState.Waiting)
						&& (_task.SavingData.State != TaskSubtypes.TaskState.Finished)) {
					_btStart = new Button(_parentActivity);
					_btStart.setText(MyApplication.getContext().getString(
							R.string.bt_start));
					_btStart.setOnClickListener(this);
					_btStart.setId(Utils.Random.nextInt());
					_btStart.setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					_btStart.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					mainLayout.addView(_btStart);
				}

				if ((_task.SavingData.State == TaskSubtypes.TaskState.Running)
						|| (_task.SavingData.State == TaskSubtypes.TaskState.Waiting)) {
					_btStop = new Button(_parentActivity);
					_btStop.setText(MyApplication.getContext().getString(
							R.string.bt_stop));
					_btStop.setOnClickListener(this);
					_btStop.setId(Utils.Random.nextInt());
					_btStop.setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					_btStop.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					mainLayout.addView(_btStop);
				}

				{
					_btRestart = new Button(_parentActivity);
					_btRestart.setText(MyApplication.getContext().getString(
							R.string.bt_restart));
					_btRestart.setOnClickListener(this);
					_btRestart.setId(Utils.Random.nextInt());
					_btRestart
							.setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					_btRestart
							.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					mainLayout.addView(_btRestart);
				}

				// временно спрятана т.к. не удалось сходу найти как это сделать
				// в андроиде
				/*
				 * { _btOpenLocation = new Button(_parentActivity);
				 * _btOpenLocation.setText(MyApplication.getContext()
				 * .getString(R.string.bt_openlocation));
				 * _btOpenLocation.setOnClickListener(this);
				 * _btOpenLocation.setId(Utils.Random.nextInt());
				 * _btOpenLocation
				 * .setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
				 * _btOpenLocation
				 * .setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
				 * mainLayout.addView(_btOpenLocation); }
				 */

				{
					_btDelete = new Button(_parentActivity);
					_btDelete.setText(MyApplication.getContext().getString(
							R.string.bt_deletetask));
					_btDelete.setOnClickListener(this);
					_btDelete.setId(Utils.Random.nextInt());
					_btDelete
							.setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					_btDelete
							.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					mainLayout.addView(_btDelete);
				}

				{
					_btDeleteAll = new Button(_parentActivity);
					_btDeleteAll.setText(MyApplication.getContext().getString(
							R.string.bt_deleteall));
					_btDeleteAll.setOnClickListener(this);
					_btDeleteAll.setId(Utils.Random.nextInt());
					_btDeleteAll
							.setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					_btDeleteAll
							.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					mainLayout.addView(_btDeleteAll);
				}

				{
					_btCancel = new Button(_parentActivity);
					_btCancel.setText(MyApplication.getContext().getString(
							R.string.bt_cancel));
					_btCancel.setOnClickListener(this);
					_btCancel.setId(Utils.Random.nextInt());
					_btCancel
							.setWidth(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					_btCancel
							.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
					mainLayout.addView(_btCancel);
				}

				_builder.setView(mainLayout);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		final View clickedView = arg0;

		Thread t = new Thread() {
			@Override
			public void run() {
				onClickThreaded(clickedView);				
			}
		};
		t.start();
	}

	private final int actionRestart = 200;
	private final int actionDelete = 210;
	private final int actionDeleteAll = 220;

	public void onClickThreaded(View arg0) {
		if (arg0 == _btStart) {
			boolean locked = _task.Lock();
			try {
				_task.SetState(TaskSubtypes.TaskState.Waiting);
			} finally {
				_task.Unlock(locked);
			}
		}

		if (arg0 == _btRestart) {			
			boolean locked = _task.Lock();
			try {
				ConfirmDialog dlg = new ConfirmDialog(_parentActivity,
						actionRestart);
				dlg.SetDialogListener(this);
				dlg.ShowDialog(MyApplication.getContext().getString(
						R.string.question_restart));				
			} finally {
				_task.Unlock(locked);
			}
		}

		if (arg0 == _btDelete) {
			boolean locked = _task.Lock();
			try {
				ConfirmDialog dlg = new ConfirmDialog(_parentActivity,
						actionDelete);
				dlg.SetDialogListener(this);
				dlg.ShowDialog(MyApplication.getContext().getString(
						R.string.question_delete));
			} finally {
				_task.Unlock(locked);
			}
		}

		if (arg0 == _btDeleteAll) {
			boolean locked = _task.Lock();
			try {
				ConfirmDialog dlg = new ConfirmDialog(_parentActivity,
						actionDeleteAll);
				dlg.SetDialogListener(this);
				dlg.ShowDialog(MyApplication.getContext().getString(
						R.string.question_deleteall));
			} finally {
				_task.Unlock(locked);
			}
		}

		if (arg0 == _btStop) {
			boolean locked = _task.Lock();
			try {
				_task.Operation = TaskSubtypes.PendingOperation.Stop;
				_task.RequestThreadStop();
			} finally {
				_task.Unlock(locked);
			}
		}

		if (arg0 == _btOpenLocation) {
			try {
				Intent intent = new Intent(android.content.Intent.ACTION_PICK);
				Uri data = Uri.parse("folder://" + _task.SavingData.LocalDir);
				intent.setData(data);
				_parentActivity.startActivity(intent);
			} catch (Exception ex) {
				final String s = ex.getLocalizedMessage();
				_parentActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Utils.ShowMessage(s);
					}
				});
			}
		}		
	}

	@Override
	public void DialogClosed(Object sender, boolean cancelled) {
		if (cancelled)
			return;

		ConfirmDialog dlg = (ConfirmDialog) sender;
		if (dlg != null) {
			try {
				switch (dlg.OperationCode) {
				case actionRestart: {
					_task.Operation = TaskSubtypes.PendingOperation.CleanAndRestart;
					_task.RequestThreadStop();
					break;
				}
				case actionDelete: {
					_task.Operation = TaskSubtypes.PendingOperation.DeleteTask;
					_task.RequestThreadStop();
					break;
				}
				case actionDeleteAll: {
					_task.Operation = TaskSubtypes.PendingOperation.DeleteAll;
					_task.RequestThreadStop();
					break;
				}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			_dialog.dismiss();
		}
	}
}
