package com.xozm.sdm;

import java.util.List;

import com.xozm.sdm.data.Task;
import com.xozm.sdm.data.TaskSubtypes;
import com.xozm.sdm.data.TaskSubtypes.TaskState;
import com.xozm.sdm.dialogs.TasksActionsDialog;
import com.xozm.sdm.helpers.StringHelper;
import com.xozm.sdm.helpers.Utils;
import com.xozm.smartdownloadmanager.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TasksListAdapter extends BaseAdapter implements OnClickListener {
	public List<Task> Tasks;
	private TasksListActivity _parentActivity;

	public TasksListAdapter(List<Task> tasks, TasksListActivity parentActivity) {
		Tasks = tasks;
		_parentActivity = parentActivity;
	}

	public void AttachTo(ListView lv) {
		lv.setAdapter(this);
	}

	@Override
	public int getCount() {
		return Tasks.size();
	}

	@Override
	public Object getItem(int i) {
		return Tasks.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	private String getTaskStateText(TaskSubtypes.TaskState state) {
		switch (state) {
		case Running:
			return MyApplication.getContext()
					.getString(R.string.status_running);
		case Waiting:
			return MyApplication.getContext()
					.getString(R.string.status_waiting);
		case Stopped:
			return MyApplication.getContext()
					.getString(R.string.status_stopped);
		case StoppedByError:
			return MyApplication.getContext().getString(
					R.string.status_stoppedbyerror);
		case Finished:
			return MyApplication.getContext().getString(
					R.string.status_finished);
		default:
			return "";
		}
	}

	@Override
	public View getView(int index, View view, final ViewGroup parent) {

		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			view = inflater.inflate(R.layout.tasks_list_row, parent, false);

			view.setOnClickListener(this);
			view.setBackgroundResource(android.R.drawable.list_selector_background);
		}

		final Task task = Tasks.get(index);

		try {
			{
				TextView textView = (TextView) view
						.findViewById(R.id.tlr_taskstate);
				textView.setText(getTaskStateText(task.SavingData.State));
			}

			{
				TextView textView = (TextView) view
						.findViewById(R.id.tlr_urlText);
				textView.setText(task.SavingData.URL);
				textView.setSingleLine();
			}

			{
				TextView textView = (TextView) view
						.findViewById(R.id.tlr_localpath);
				textView.setSingleLine();

				String localPath = task.getFullLocalPath();
				if (StringHelper.StringIsEmpty(localPath)) {
					textView.setVisibility(View.GONE);
				} else {
					textView.setText(localPath);
					textView.setVisibility(View.VISIBLE);
				}
			}

			{
				TextView textView = (TextView) view
						.findViewById(R.id.tlr_message);
				textView.setSingleLine();

				if (StringHelper.StringIsEmpty(task.SavingData.Message)) {
					textView.setVisibility(View.GONE);
				} else {
					textView.setText(task.SavingData.Message);
					textView.setVisibility(View.VISIBLE);
				}
			}

			{
				TextView textView = (TextView) view
						.findViewById(R.id.tlr_progress);

				long percent;
				if (task.getSelectedTaskSize() == 0)
					percent = 0;
				else
					percent = task.GetDownloadedSize() * 100
							/ task.getSelectedTaskSize();

				String s;
				if (task.getSelectedTaskSize() > 0) {
					s = "" + percent + "% ("
							+ Utils.formatSize(task.GetDownloadedSize()) + "/"
							+ Utils.formatSize(task.getSelectedTaskSize())
							+ ")";
				} else {
					s = Utils.formatSize(task.GetDownloadedSize());
				}

				if (task.SavingData.State == TaskState.Running) {
					s += "\t"
							+ Utils.formatSize((long) task.SpeedHelper
									.getCurrentSpeed()) + "/s";
				}

				textView.setText(s);
			}

			view.setTag(task.SavingData.Guid);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return view;
	}

	@Override
	public void onClick(View arg0) {
		final View clickedView = arg0;

		onClickThreaded(clickedView);

		// Thread t = new Thread() {
		// @Override
		// public void run() {
		// onClickThreaded(clickedView);
		// }
		// };
		// t.start();
	}

	private void onClickThreaded(View arg0) {
		if (arg0 == null)
			return;

		String taskGuid = (String) arg0.getTag();
		if (StringHelper.StringIsEmpty(taskGuid))
			return;

		TasksActionsDialog.ShowDialog(_parentActivity, taskGuid);
	}
}