package com.xozm.sdm.helpers;

import java.io.File;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.view.Gravity;
import android.widget.Toast;
import java.util.Random;

import com.xozm.sdm.MyApplication;

public class Utils {

	public static Random Random = new Random();

	public static boolean URLGood(String s) {
		if (s.length() == 0)
			return false;

		Pattern p = Pattern.compile("^.+$");
		// Pattern p = Pattern.compile("^(https?|ftp|file)://.+$");
		Matcher m = p.matcher(s);

		return m.matches();
	}

	public static boolean DirectoryGood(String s) {
		if ((s == null) || (s.length() == 0))
			return false;

		File file = new File(s);
		return file != null;
	}

	public static void ShowMessage(String msg) {
		try {
			Toast toast = Toast.makeText(MyApplication.getContext(), msg,
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static String formatSize(long bytes) {
		if (bytes == -1)
			return "N/A";

		String[] s = formatSize(bytes, 0);
		return s[0] + s[1];
	}

	public static String[] formatSize(long bytes, int place) {
		int level = 0;
		float number = bytes;
		String[] unit = { "B", "KB", "MB", "GB", "TB", "PB" };

		while (number >= 1024f) {
			number /= 1024f;
			level++;
		}

		String formatStr = null;
		if (place == 0) {
			formatStr = "###0";
		} else {
			formatStr = "###0.";
			for (int i = 0; i < place; i++) {
				formatStr += "#";
			}
		}

		DecimalFormat nf = new DecimalFormat(formatStr);

		String[] value = new String[2];
		value[0] = nf.format(number);
		value[1] = unit[level];

		return value;
	}

	public static String GetAvailableFileName(String dir, String desiredName,
			String ext) {
		int i = -1;
		if (StringHelper.StringIsEmpty(ext))
			ext = "";
		else {
			if (!ext.startsWith("."))
				ext = "." + ext;
		}

		// если desiredName заканчивается на "[<цифра>]",
		// то продолжим подбирать с этой цифры
		{
			int n1 = desiredName.lastIndexOf('[');
			int n2 = desiredName.lastIndexOf(']');
			if ((n1 > 0) && (n2 > 0) && (n2 > n1)) {
				String ts = desiredName.substring(n1 + 1, n2);
				try {
					i = Integer.parseInt(ts);
					desiredName = desiredName.substring(0, n1);
				} catch (Exception e) {
				}
			}
		}

		while (true) {
			String filePath;
			String fileName;
			if (i < 0)
				fileName = desiredName + ext;
			else
				fileName = desiredName + "[" + i + "]" + ext;

			filePath = dir + File.separator + fileName;

			File f = new File(filePath);
			if (!f.exists())
				return fileName;

			i++;
		}
	}

	public static String getExceptionDescription(Exception ex) {
		if (!StringHelper.StringIsEmpty(ex.getLocalizedMessage()))
			return ex.getLocalizedMessage();

		if (!StringHelper.StringIsEmpty(ex.getMessage()))
			return ex.getMessage();

		return ex.toString();
	}
}
