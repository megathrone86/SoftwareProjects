package com.xozm.sdm.helpers;

import java.net.URLConnection;

import com.xozm.sdm.MyApplication;

public class HttpHelper {
	public static String MakeURLCool(String url) {
		if (!url.toLowerCase(MyApplication.GetLocale()).startsWith("http://"))
			url = "http://" + url;
		return url;
	}

	public static String getExt(URLConnection connection, byte[] buf) {
		String ext = "";

		String contentType = connection.getHeaderField("content-type");
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("html"))
			ext = "html";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("excel"))
			ext = "xls";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("word"))
			ext = "doc";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("powerpoint"))
			ext = "ppt";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("pdf"))
			ext = "pdf";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("jpeg"))
			ext = "jpeg";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("gif"))
			ext = "gif";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("icon"))
			ext = "ico";
		if (contentType.toLowerCase(MyApplication.GetLocale()).contains("zip"))
			ext = "zip";

		if (ext.length() > 0)
			return "." + ext;
		else
			return "";
	}
}
