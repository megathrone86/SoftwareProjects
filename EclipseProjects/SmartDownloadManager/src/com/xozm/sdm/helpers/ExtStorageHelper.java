package com.xozm.sdm.helpers;

import java.io.File;

import android.os.Environment;

import com.xozm.sdm.MyApplication;

public class ExtStorageHelper {
	private static String _appsDataDir = "/data/";

	private static File getExternalStorageRoot() {
		File extFile = Environment.getExternalStorageDirectory();
		if ((extFile == null) || (!extFile.exists()))
			return null;

		File result = new File(extFile.getPath() + _appsDataDir
				+ MyApplication.getContext().getPackageName());
		if (!result.exists())
			result.mkdirs();
		return result;
	}

	public static File getExternalStorageFilesDir() {
		File root = getExternalStorageRoot();
		if (root == null)
			return null;

		File result = new File(root.getPath() + "/files");
		if (!result.exists())
			result.mkdirs();
		return result;
	}

	public static boolean externalStorageConnected() {
		File extFile = Environment.getExternalStorageDirectory();
		return (extFile != null) && extFile.exists();
	}
}
