package com.xozm.sdm.helpers;

import java.util.List;

import com.xozm.sdm.data.Settings;
import com.xozm.sdm.data.SettingsManager;
import com.xozm.sdm.data.Task;
import com.xozm.sdm.data.TasksManager;

//этот класс определяет, на сколько секунд заснуть 
//и какой размер буфера для скачки выбрать 
//(чтобы скорость скачивания соответствовала ограничениям)
public class SpeedSelector {
	private final static int maxBufSize = 8 * 1024;
	private final static int maxSleepTime = 10 * 1000;

	public static int SleepAndGetBufSize(Task task) {
		Settings settings = SettingsManager.Instance.GetSettings();

		int bufSize = maxBufSize;

		if (settings.MaxSpeedKbytes == 0)
			return bufSize;

		// подсчитаем текущую скорость
		float totalSpeedKbytes = 0;
		{
			float totalSpeed = 0;
			List<Task> currentTasks = TasksManager.Instance.GetCurrentTasks();
			for (int i = 0; i < currentTasks.size(); i++) {
				Task t = currentTasks.get(i);
				totalSpeed += t.SpeedHelper.getCurrentSpeed();
			}
			totalSpeedKbytes = totalSpeed / 1024f;
		}

		System.out.println("totalSpeedKbytes = " + totalSpeedKbytes);
		if (totalSpeedKbytes > settings.MaxSpeedKbytes) {
			int sleepTime = (int) (maxSleepTime
					* (totalSpeedKbytes - settings.MaxSpeedKbytes) / totalSpeedKbytes);
			System.out.println("sleepTime = " + sleepTime);
			try {
				Thread.sleep(sleepTime);
			} catch (Exception ex) {
			}
		}
		System.out.println();

		return bufSize;
	}
}
