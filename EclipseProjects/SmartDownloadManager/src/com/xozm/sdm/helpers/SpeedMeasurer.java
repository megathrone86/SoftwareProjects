package com.xozm.sdm.helpers;

import java.util.Vector;

public class SpeedMeasurer {
	public static SpeedMeasurer Instance = new SpeedMeasurer();

	private Vector<StackEntry> vector;

	private final long timeOut = 10 * 1000;
	private final long maxStackSize = 10;

	public SpeedMeasurer() {
		vector = new Vector<StackEntry>();
	}

	private void CheckTimeout() {
		while (vector.size() > maxStackSize)
			vector.remove(0);

		int i = 0;
		while (i < vector.size()) {
			if (vector.get(i).StartTime + timeOut < System.currentTimeMillis()) {
				vector.remove(i);
				continue;
			}

			i++;
		}
	}

	public void AddTraffic(long startTime, int bytesCount) {
		CheckTimeout();

		StackEntry se = new StackEntry();
		se.StartTime = System.currentTimeMillis();
		se.TrafficSize = bytesCount;

		vector.add(se);
	}

	// bytes/second
	public float getCurrentSpeed() {
		CheckTimeout();

		if (vector.size() <= 0)
			return 0;

		float result = 0;
		for (int i = 0; i < vector.size(); i++) {
			result += vector.get(i).TrafficSize;
		}

		return result * 1000.0f
				/ (System.currentTimeMillis() - vector.get(0).StartTime);
	}

	private class StackEntry {
		public long StartTime;
		public int TrafficSize;
	}
}
