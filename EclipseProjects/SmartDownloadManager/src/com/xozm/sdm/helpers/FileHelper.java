package com.xozm.sdm.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileHelper {

	public static String getExtensionFromFileName(String fileName) {
		if (StringHelper.StringIsEmpty(fileName))
			return "";

		int n = fileName.lastIndexOf('.');
		if (n <= 0)
			return "";

		return fileName.substring(n);
	}

	public static String getFileNameWithoutExtension(String fileName) {
		if (StringHelper.StringIsEmpty(fileName))
			return "";

		int n = fileName.lastIndexOf('.');
		if (n <= 0)
			return fileName;

		return fileName.substring(0, n);
	}

	public static void copyFile(File sourceFile, File destFile)
			throws IOException {
		if (!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;
		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();

			// previous code: destination.transferFrom(source, 0,
			// source.size());
			// to avoid infinite loops, should be:
			long count = 0;
			long size = source.size();
			while ((count += destination.transferFrom(source, count, size
					- count)) < size)
				;
		} finally {
			if (source != null) {
				source.close();
			}
			if (destination != null) {
				destination.close();
			}
		}
	}
}
