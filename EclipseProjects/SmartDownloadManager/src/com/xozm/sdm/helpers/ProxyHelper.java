package com.xozm.sdm.helpers;

import java.net.InetSocketAddress;
import java.net.Proxy;

import com.xozm.sdm.data.Settings;
import com.xozm.sdm.data.SettingsManager;

public class ProxyHelper {
	public static Proxy GetProxy() {
		Settings settings = SettingsManager.Instance.GetSettings();

		if (!settings.UseProxy)
			return null;

		Proxy result = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
				settings.ProxyAddress, 8080));

		/*Authenticator authenticator = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return (new PasswordAuthentication("user",
						"password".toCharArray()));
			}
		};
		Authenticator.setDefault(authenticator);*/

		return result;
	}
}
