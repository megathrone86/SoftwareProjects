package com.xozm.sdm.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

import com.xozm.sdm.MyApplication;
import com.xozm.sdm.data.SettingsManager;
import com.xozm.sdm.data.Task;
import com.xozm.sdm.data.TaskSubtypes;
import com.xozm.sdm.data.TasksManager;
import com.xozm.sdm.helpers.FileHelper;
import com.xozm.sdm.helpers.HttpHelper;
import com.xozm.sdm.helpers.ProxyHelper;
import com.xozm.sdm.helpers.SpeedSelector;
import com.xozm.sdm.helpers.StringHelper;
import com.xozm.sdm.helpers.Utils;

public class HttpDownloadThread extends DownloadThread {
	public HttpDownloadThread(Task task) {
		this.task = task;
	}

	@Override
	public void run() {
		Thread.currentThread().setName("HttpDownloadThread");
		MyApplication.SetExceptionHandlerForCurrentThread();

		while (task.SavingData.State != TaskSubtypes.TaskState.Finished) {
			if (shouldBreak())
				break;

			if (task != null) {
				try {
					processTask();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			if (shouldBreak())
				break;
			sleepSimple(1000);
		}

		if (stop)
			task.ProcessPendingOperation();
	}

	private void sleepSimple(long ms) {
		try {
			sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processTask() throws Exception {
		boolean criticalPart = false;
		HttpURLConnection connection = null;

		try {
			{
				boolean locked = task.Lock();
				task.SavingData.Message = "";
				task.Unlock(locked);
			}

			// создадим подключение
			URL url;
			{
				criticalPart = true;
				url = new URL(task.SavingData.URL);
				
				Proxy proxy = ProxyHelper.GetProxy();
				if (proxy == null)
					connection = (HttpURLConnection) url.openConnection();
				else
					connection = (HttpURLConnection) url.openConnection(proxy);
				
				if (connection == null)
					throw new Exception("Несоответствующий тип подключения.");

				connection.setRequestMethod("GET");
				connection
						.setConnectTimeout(SettingsManager.Instance.GetSettings().Timeout);
				connection
						.setReadTimeout(SettingsManager.Instance.GetSettings().Timeout);
				criticalPart = false;
				connection.connect();
			}

			if (shouldBreak())
				return;

			// попробуем получить размер файла
			{
				criticalPart = true;
				if (task.SavingData.TotalTaskSize < 0) {
					int newSize = connection.getContentLength();

					boolean locked = task.Lock();
					task.SavingData.TotalTaskSize = newSize;
					task.Unlock(locked);
				}
				criticalPart = false;
			}

			// сгенерируем имя файла
			{
				criticalPart = true;
				{
					if (StringHelper
							.StringIsEmpty(task.SavingData.LocalFileName)) {
						String newName;
						{
							String newNameWithoutExt = FileHelper
									.getFileNameWithoutExtension(url.getFile());
							String newExt = FileHelper
									.getExtensionFromFileName(url.getFile());

							if (StringHelper.StringIsEmpty(newNameWithoutExt))
								newNameWithoutExt = "download";

							newName = Utils.GetAvailableFileName(
									task.SavingData.LocalDir,
									newNameWithoutExt, newExt);
						}

						{
							boolean locked = task.Lock();
							task.SavingData.LocalFileName = newName;
							task.Unlock(locked);

							TasksManager.Instance.SetSaveNeeded();
						}
					}
				}
				criticalPart = false;
			}

			// приступим к скачиванию
			{
				InputStream in = new BufferedInputStream(
						connection.getInputStream());

				if (shouldBreak())
					return;

				// перейдем на текущую позицию
				long bytesToSkip;
				{
					bytesToSkip = task.GetDownloadedSize();
					while (bytesToSkip > 0) {
						long currentBytesToSkip = bytesToSkip;
						if (currentBytesToSkip > 4 * 1024)
							currentBytesToSkip = 4 * 1024;

						bytesToSkip -= in.skip(currentBytesToSkip);

						if (shouldBreak())
							return;
					}
				}

				// начнем скачивание
				{
					BufferedOutputStream out = null;

					// будем скачивать в цикле по кусочку и записывать в
					// файл, попутно обновляя статус задачи
					boolean firstBlock = true;
					while (true) {
						int bytes_to_read = SpeedSelector
								.SleepAndGetBufSize(task);
						byte[] buf = new byte[bytes_to_read];

						long start = System.currentTimeMillis();
						int len = in.read(buf, 0, bytes_to_read);
						task.SpeedHelper.AddTraffic(start, len);

						// если это скачивание первого блока, попробуем по
						// заголовку определить расширение и переименовать файл
						if ((bytesToSkip == 0)
								&& (firstBlock)
								&& (!task.SavingData.LocalFileName
										.contains("."))) {
							firstBlock = false;

							String fileDir = task.SavingData.LocalDir;
							String fileName = FileHelper
									.getFileNameWithoutExtension(task.SavingData.LocalFileName);
							String fileExt = FileHelper
									.getExtensionFromFileName(fileName);
							if (StringHelper.StringIsEmpty(fileExt))
								fileExt = HttpHelper.getExt(connection, buf);

							boolean locked = task.Lock();
							try {
								task.SavingData.LocalFileName = new File(
										Utils.GetAvailableFileName(fileDir,
												fileName, fileExt)).getName();
							} finally {
								task.Unlock(locked);
							}
							TasksManager.Instance.SetSaveNeeded();
						}

						if (len == -1)
							break;

						if (out == null)
							out = new BufferedOutputStream(
									new FileOutputStream(new File(
											task.getFullLocalPath()), true));

						out.write(buf, 0, len);

						if (shouldBreak())
							return;
					}

					// скачка завершена. закроем поток и поменяем статус
					{
						out.flush();
						out.close();

						if (task.GetDownloadedSize() >= task.SavingData.TotalTaskSize) {
							task.SetState(TaskSubtypes.TaskState.Finished);
							TasksManager.Instance.SetSaveNeeded();
						}
					}
				}
			}
		} catch (Exception ex) {
			task.SavingData.Message = Utils.getExceptionDescription(ex);

			if (criticalPart) {
				task.SetState(TaskSubtypes.TaskState.StoppedByError);
			}

			TasksManager.Instance.SetSaveNeeded();

			ex.printStackTrace();
		} finally {
			if (connection != null)
				connection.disconnect();
		}

		for (int i = 0; i < 20; i++) {
			if (shouldBreak())
				return;
			sleepSimple(500);
		}
	}
}
