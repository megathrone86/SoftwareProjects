package com.xozm.sdm.service;

import com.xozm.sdm.data.Task;

public abstract class DownloadThread extends Thread {
	protected volatile boolean stop = false;
	protected volatile Task task = null;
	
	public void RequestStop() {
		stop = true;		
	}
	
	protected boolean shouldBreak() {
		return stop || Thread.interrupted();		
	}
}
