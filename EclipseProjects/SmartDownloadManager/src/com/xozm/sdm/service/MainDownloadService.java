package com.xozm.sdm.service;

public class MainDownloadService {
	public static MainDownloadService Instance = new MainDownloadService();

	private MainDownloadThread _mainDownloadThread = null;

	public void Start() {
		if (_mainDownloadThread == null)
			_mainDownloadThread = new MainDownloadThread();
		if (!_mainDownloadThread.isAlive())
			_mainDownloadThread.start();
	}

	public void RequestStop() {
		if (_mainDownloadThread != null)
			_mainDownloadThread.stop = true;
	}
}
