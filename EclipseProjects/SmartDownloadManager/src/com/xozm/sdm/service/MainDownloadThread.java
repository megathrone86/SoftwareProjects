package com.xozm.sdm.service;

import java.util.List;

import android.util.Log;

import com.xozm.sdm.MyApplication;
import com.xozm.sdm.data.Settings;
import com.xozm.sdm.data.SettingsManager;
import com.xozm.sdm.data.Task;
import com.xozm.sdm.data.TaskSubtypes;
import com.xozm.sdm.data.TasksManager;

public class MainDownloadThread extends Thread {
	public boolean stop = false;

	private int div = 0;

	@Override
	public void run() {
		Log.w("", "_mainDownloadThread starts");

		this.setName("MainDownloadThread");

		MyApplication.SetExceptionHandlerForCurrentThread();

		while (!stop) {
			try {
				TasksManager.Instance.SaveIfNeeded();
				
				Settings settings = SettingsManager.Instance.GetSettings();

				List<Task> tasks = TasksManager.Instance.GetCurrentTasks();
				// подсчитаем список запущенных задач
				int runningTasks = GetRunningTasksCount(tasks);

				// удалим лишние задачи, если их слишком много,
				// или запустим недостающие, если задач слишком мало
				// и существуют незапущенные
				for (int i = 0; i < tasks.size(); i++) {
					Task task = tasks.get(i);

					if ((runningTasks < settings.MaxTasks)
							&& (task.SavingData.State == TaskSubtypes.TaskState.Waiting)) {
						task.SetState(TaskSubtypes.TaskState.Running);
						runningTasks++;
					}

					if ((runningTasks > settings.MaxTasks)
							&& (task.SavingData.State == TaskSubtypes.TaskState.Running)) {
						task.SetState(TaskSubtypes.TaskState.Waiting);
						runningTasks--;
					}
				}

				// применим наши изменения в статусах задач
				for (int i = 0; i < tasks.size(); i++) {
					tasks.get(i).CheckState();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			try {
				sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			div++;
			//каждые 30 итераций (примерно полминуты) сохраним список задач
			if (div >= 30) {
				boolean locked = TasksManager.Instance.Lock();
				TasksManager.Instance.SetSaveNeeded();
				TasksManager.Instance.Unlock(locked);

				div = 0;
			}
		}

		// завершим дочерние потоки
		List<Task> tasks = TasksManager.Instance.GetCurrentTasks();
		for (int i = 0; i < tasks.size(); i++) {
			Task task = tasks.get(i);
			task.RequestThreadStop();
		}
		
		TasksManager.Instance.SaveIfNeeded();

		Log.w("", "_mainDownloadThread breaks");
	}

	private int GetRunningTasksCount(List<Task> tasks) {
		int result = 0;
		for (int i = 0; i < tasks.size(); i++) {
			Task task = tasks.get(i);

			if (task.SavingData.State == TaskSubtypes.TaskState.Running)
				result++;
		}
		return result;
	}
}
