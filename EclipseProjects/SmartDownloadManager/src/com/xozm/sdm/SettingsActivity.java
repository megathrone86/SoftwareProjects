package com.xozm.sdm;

import com.xozm.sdm.data.Settings;
import com.xozm.sdm.data.SettingsManager;
import com.xozm.sdm.dialogs.SelectFolderDialog;
import com.xozm.sdm.helpers.Utils;
import com.xozm.smartdownloadmanager.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

public class SettingsActivity extends Activity implements OnClickListener,
		OnCheckedChangeListener {

	private CheckBox cbAutoStartNewTasks;
	private EditText txtDownloadDir;
	private EditText txtMaxTasks;
	private EditText txtMaxSpeed;

	private CheckBox cbUseProxy;
	private EditText txtProxyAddress;
	private EditText txtProxyPort;

	private Button btOK;
	private Button btCancel;

	private Settings _settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MyApplication.SettingsActivity = this;

		_settings = SettingsManager.Instance.GetSettings();

		try {
			InitGui();
		} catch (Exception e) {
			Log.w(this.getClass().getName(), e.toString());
			e.printStackTrace();
		}
	}

	private void InitGui() {
		this.setContentView(R.layout.settings_layout);

		{
			cbAutoStartNewTasks = (CheckBox) this
					.findViewById(R.id.s_autostartnewtasks);
			cbAutoStartNewTasks.setChecked(_settings.AutoStartNewTasks);
		}

		{
			txtDownloadDir = (EditText) this.findViewById(R.id.s_downloaddir);
			txtDownloadDir.setOnClickListener(this);
			txtDownloadDir.setInputType(InputType.TYPE_NULL);
			txtDownloadDir.setText(_settings.DownloadDir);
		}

		{
			txtMaxTasks = (EditText) this.findViewById(R.id.s_maxtasks);
			txtMaxTasks.setText(Integer.toString(_settings.MaxTasks));
		}

		{
			txtMaxSpeed = (EditText) this.findViewById(R.id.s_maxspeed);
			txtMaxSpeed.setText(Integer.toString(_settings.MaxSpeedKbytes));
		}

		{
			cbUseProxy = (CheckBox) this.findViewById(R.id.s_useproxy);
			cbUseProxy.setOnCheckedChangeListener(this);

			{
				txtProxyAddress = (EditText) this
						.findViewById(R.id.s_proxyaddress);
				txtProxyAddress.setText(_settings.ProxyAddress);
			}

			{
				txtProxyPort = (EditText) this.findViewById(R.id.s_proxyport);
				txtProxyPort.setText(Integer.toString(_settings.ProxyPort));
			}

			cbUseProxy.setChecked(!_settings.UseProxy);
			cbUseProxy.setChecked(_settings.UseProxy);
		}

		btOK = (Button) this.findViewById(R.id.at_btOK);
		btOK.setOnClickListener(this);

		btCancel = (Button) this.findViewById(R.id.at_btCancel);
		btCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		if (arg0 == btOK) {
			try {
				_settings.AutoStartNewTasks = cbAutoStartNewTasks.isChecked();
				_settings.DownloadDir = txtDownloadDir.getText().toString();
				_settings.MaxTasks = Integer.parseInt(txtMaxTasks.getText()
						.toString());
				_settings.MaxSpeedKbytes = Integer.parseInt(txtMaxSpeed
						.getText().toString());

				_settings.UseProxy = cbUseProxy.isChecked();
				_settings.ProxyAddress = txtProxyAddress.getText().toString();
				_settings.ProxyPort = Integer.parseInt(txtProxyPort.getText()
						.toString());
			} catch (Exception ex) {
				ex.printStackTrace();
				Utils.ShowMessage(ex.getLocalizedMessage());
				return;
			}

			SettingsManager.Instance.SaveSettings();

			finish();
		}

		if (arg0 == btCancel) {
			finish();
		}

		if (arg0 == txtDownloadDir) {
			SelectFolderDialog.ShowDialog(this, txtDownloadDir.getText()
					.toString(), MyApplication.SelectFolderDialogRequest,
					R.drawable.bt_cancel, R.drawable.bt_ok);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ((requestCode == MyApplication.SelectFolderDialogRequest)
				&& (resultCode == RESULT_OK)) {
			txtDownloadDir.setText(data.getStringExtra("currentDir"));
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		if (arg0 == cbUseProxy) {
			txtProxyAddress.setEnabled(arg1);
			txtProxyAddress.setFocusableInTouchMode(arg1);
			txtProxyPort.setEnabled(arg1);
			txtProxyPort.setFocusableInTouchMode(arg1);
		}
	}
}
