package com.xozm.sdm;

import com.xozm.sdm.service.MainDownloadService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MainService extends Service {
	public static MainService Instance = null;
	
	public static final String TAG = "MainService";

	@Override
	public void onCreate() {
		Instance = this;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		MainDownloadService.Instance.Start();
		MyApplication.setServiceContext(this.getApplicationContext());
		return Service.START_STICKY;
	}

	@Override
	public void onDestroy() {
		MainDownloadService.Instance.RequestStop();
	}
}