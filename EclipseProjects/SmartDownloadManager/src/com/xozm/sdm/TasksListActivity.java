package com.xozm.sdm;

import java.util.Timer;
import java.util.TimerTask;

import com.xozm.sdm.data.TasksManager;
import com.xozm.smartdownloadmanager.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class TasksListActivity extends Activity {
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onResume() {
		super.onResume();

		refreshTimer = new Timer();
		refreshTimer.schedule(new RefreshTask(), 1000, 1000);
	}

	@Override
	protected void onPause() {
		super.onPause();

		refreshTimer.cancel();
	}

	private TabHost tabHost = null;
	private ListView list1 = null;
	private TasksListAdapter adapter1 = null;
	private ListView list2 = null;
	private TasksListAdapter adapter2 = null;

	private Button btAdd;
	private Button btOptions;
	private Button btHelp;
	private Button btExit;

	private Timer refreshTimer;

	private class RefreshTask extends TimerTask {
		@Override
		public void run() {
			try {
				// это выполняется в отдельном потоке таймера
				Thread.currentThread().setName("RefreshTask thread");
				MyApplication.TasksListActivity.RefreshTasks();
				Handler.sendEmptyMessage(tabHost.getCurrentTab());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public static class TimerHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			if (MyApplication.TasksListActivity == null)
				return;

			// это выполняется в потоке, в котором был создан TimerHandler (т.е.
			// в потоке активити)
			MyApplication.TasksListActivity.adapter1.notifyDataSetChanged();
			MyApplication.TasksListActivity.UpdateTasksTabIndicator(0);

			MyApplication.TasksListActivity.adapter2.notifyDataSetChanged();
			MyApplication.TasksListActivity.UpdateTasksTabIndicator(1);
		}
	}

	public final TimerHandler Handler = new TimerHandler();

	public void UpdateTasksTabIndicator(int index) {
		String text;
		if (index == 0)
			text = MyApplication.getContext().getString(
					R.string.tab_currenttasks)
					+ " (" + adapter1.getCount() + ")";
		else
			text = MyApplication.getContext().getString(
					R.string.tab_finishedtasks)
					+ " (" + adapter2.getCount() + ")";

		TextView title = (TextView) tabHost.getTabWidget().getChildAt(index)
				.findViewById(android.R.id.title);
		title.setText(text);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MyApplication.SetExceptionHandlerForCurrentThread();

		Thread.currentThread().setName("TasksListActivity thread");

		MyApplication.TasksListActivity = this;
		MyApplication.Start(this.getApplicationContext());

		try {
			InitGui();
		} catch (Exception e) {
			Log.w(this.getClass().getName(), e.toString());
			e.printStackTrace();
		}
	}

	private void InitGui() {
		{
			adapter1 = new TasksListAdapter(
					TasksManager.Instance.GetCurrentTasks(), this);
			adapter2 = new TasksListAdapter(
					TasksManager.Instance.GetFinishedTasks(), this);
		}

		this.setContentView(R.layout.tasks_layout);

		{
			tabHost = (TabHost) this.findViewById(R.id.tabHost);
			tabHost.setup();
			tabHost.setOnTabChangedListener(new OnTabChangeListener() {
				@Override
				public void onTabChanged(String tabId) {
					RefreshTasks();
				}
			});
		}

		// tab 1
		{
			TabSpec tabSpec1 = tabHost.newTabSpec("Tab1");
			tabSpec1.setContent(R.id.t_tab1);
			tabSpec1.setIndicator("");
			tabHost.addTab(tabSpec1);

			list1 = (ListView) this.findViewById(R.id.t_List1);
			list1.setAdapter(adapter1);

			UpdateTasksTabIndicator(0);
		}

		// tab 2
		{
			TabSpec tabSpec2 = tabHost.newTabSpec("Tab2");
			tabSpec2.setContent(R.id.t_tab2);
			tabSpec2.setIndicator("");
			tabHost.addTab(tabSpec2);

			list2 = (ListView) this.findViewById(R.id.t_List2);
			adapter2.AttachTo(list2);

			UpdateTasksTabIndicator(1);
		}

		// main commands
		{
			btAdd = (Button) this.findViewById(R.id.t_btAdd);
			btAdd.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					MyApplication
							.StartAddTaskActivity(MyApplication.TasksListActivity);
				}
			});

			btOptions = (Button) this.findViewById(R.id.t_btOptions);
			btOptions.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					MyApplication
							.StartSettingsActivity(MyApplication.TasksListActivity);
				}
			});

			btHelp = (Button) this.findViewById(R.id.t_btHelp);
			btHelp.setEnabled(false);

			btExit = (Button) this.findViewById(R.id.t_btExit);
			btExit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					MyApplication.Exit();
				}
			});
		}
	}

	private void RefreshTasksThreaded() {
		if (tabHost == null)
			return;

		adapter1.Tasks = TasksManager.Instance.GetCurrentTasks();
		adapter2.Tasks = TasksManager.Instance.GetFinishedTasks();
	}

	private boolean _refreshingTasks = false;

	public void RefreshTasks() {
		if (_refreshingTasks)
			return;

		_refreshingTasks = true;
		try {
			RefreshTasksThreaded();
		} finally {
			_refreshingTasks = false;
		}
	}
}