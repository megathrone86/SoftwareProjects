﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using ships2.GameClasses;

namespace ships2.EngineClasses
{
    public class GameInput
    {
        private Form _form;

        public void AttachTo(Form _form)
        {
            this._form = _form;
        }

        public void MouseUp(Point mp)
        {
            if (GameGlobals.CurrentGameMode == GameMode.PlayerActing)
            {
                if ((GameGlobals.AIBF.SelectedTargets.Count < GameGlobals.PlayerBF.Guns) &&
                    (GameGlobals.AIBF.CellIsShottable(mp.X, mp.Y)))
                {
                    GameGlobals.AIBF.SelectedTargets.Add(new Hit(mp.X, mp.Y, true));                    

                    if ((GameGlobals.AIBF.SelectedTargets.Count >= GameGlobals.PlayerBF.Guns) ||
                        (GameGlobals.AIBF.NoMoreShottableCells))
                    {
                        GameProcess.PlayerFire();                        
                    }                    
                }
            }
        }
    }
}
