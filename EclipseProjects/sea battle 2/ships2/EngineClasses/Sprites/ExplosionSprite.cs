﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using ships2.GameClasses;

namespace ships2.EngineClasses.Sprites
{
    public class ExplosionSprite : BasicSprite
    {
        private static readonly float gravity = 4f;

        private Spark[] sparks;

        public override void Draw(Graphics g, int startX, int startY, int cellSize)
        {
            if (Dead)
                return;

            long TickCount = Environment.TickCount;
            float timeDelta = (TickCount - LastDrawTime) / 1000.0f;               

            foreach (Spark spark in sparks)
            {
                if (!spark.active)
                    continue;

                spark.Move(timeDelta);

                float cpx = startX + spark.X * cellSize;
                float cpy = startY + spark.Y * cellSize;
                g.DrawLine(GamePens.Instance.TracePen, cpx, cpy,
                    cpx + spark.speedX * 2.5f, cpy + spark.speedY * 2.5f);
            }

            LastDrawTime = TickCount;
        }

        public ExplosionSprite(long LifeTime, RectangleF position) :
            base(LifeTime, position)
        {
            sparks = new Spark[15] ;

            for (int i = 0; i<sparks.Length; i++)            
            {
                sparks[i] = new Spark();
                sparks[i].active = true;
                sparks[i].X = this.Position.X + this.Position.Width / 2.0f;
                sparks[i].Y = this.Position.Y + this.Position.Height / 2.0f;
                sparks[i].speedX = (float)(5 - GameGlobals.Random.NextDouble() * 10);
                sparks[i].speedY = -(float)(2 + GameGlobals.Random.NextDouble() * 2);                
            }
        }

        public class Spark
        {
            public bool active;
            public float X, Y;
            public float speedX, speedY;
            public Color color;

            public void Move(float timeDelta)
            {
                if (!active)
                    return;

                X += speedX * timeDelta;
                Y += speedY * timeDelta;
                speedY += gravity * timeDelta;                
            }
        }
    }
}
