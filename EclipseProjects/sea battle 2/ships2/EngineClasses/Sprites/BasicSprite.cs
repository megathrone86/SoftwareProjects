﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ships2.EngineClasses.Sprites
{
    public abstract class BasicSprite
    {
        public RectangleF Position;
        public long StartTime;
        public long EndTime;
        protected long LastDrawTime;

        public abstract void Draw(Graphics g, int startX, int startY, int cellSize);

        public BasicSprite(long LifeTime, RectangleF position)
        {
            StartTime = Environment.TickCount;
            LastDrawTime = StartTime;
            EndTime = StartTime + LifeTime;

            Position = position;
        }

        public bool Dead
        {
            get
            {
                return Environment.TickCount > EndTime;
            }
        }
    }
}
