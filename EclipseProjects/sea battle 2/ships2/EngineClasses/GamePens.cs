﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ships2.EngineClasses
{
    public class GamePens
    {
        public static GamePens Instance = new GamePens();

        protected GamePens()
        {
        }

        public Pen BFRectPen = new Pen(Color.Gray);
        public Pen BFCellPen = new Pen(Color.Aqua);
        public Font IngameFont = new Font("Comic Sans MS", 16, FontStyle.Regular, GraphicsUnit.Pixel);
        public Brush IngameFontBrush = new SolidBrush(Color.Sienna);
        public Brush ShipBrush = new SolidBrush(Color.Thistle);
        public Pen ShipCannonPen = new Pen(Color.SteelBlue);
        public Pen TracePen = new Pen(Color.Red, 2);
        public Brush ShipCannonBrush = new SolidBrush(Color.SteelBlue);
        public Brush SelectionBrush = new SolidBrush(Color.Orange);
        public Brush NewTargetBrush = new SolidBrush(Color.Orchid);
        public Brush ShipWoundCellBrush = new SolidBrush(Color.Red);
        public Brush BFHittedCellBrush = new SolidBrush(Color.Blue);
    }
}
