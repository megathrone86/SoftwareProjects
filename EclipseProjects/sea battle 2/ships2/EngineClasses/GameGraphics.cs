﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using ships2.GameClasses;
using ships2.GuiClasses;

namespace ships2.EngineClasses
{
    public class GameGraphics
    {
        private Form _form;
        private PaintEventHandler _paintHandler;
        private EventHandler _resizeHandler;
        private GameGraphicsFormSizes _formSizes = new GameGraphicsFormSizes();

        public Point CursorToBF(Point p)
        {
            Point result = new Point(p.X, p.Y);
            result.X -= _formSizes.BFRect.Left;
            result.Y -= _formSizes.BFRect.Top;
            result.X = result.X / _formSizes.CellSize;
            result.Y = result.Y / _formSizes.CellSize;

            return result;
        }

        private struct GameGraphicsFormSizes
        {
            public Size ClientSize;
            public int CellSize;
            public Rectangle BFRect;
            public Rectangle BFsRect;
        }

        public void AttachTo(Form _form)
        {
            this._form = _form;

            _paintHandler = new PaintEventHandler(_form_Paint);
            _resizeHandler = new EventHandler(_form_Resize);

            _form.Paint += _paintHandler;
            _form.Resize += _resizeHandler;

            _resizeHandler(_form, null);
        }

        void _form_Resize(object sender, EventArgs e)
        {
            if ((sender == null) || !(sender is Form))
                return;

            Form form = sender as Form;

            _formSizes.ClientSize = form.ClientSize;
            _formSizes.BFsRect.X = 10;
            _formSizes.BFsRect.Y = 5;
            _formSizes.BFsRect.Width = _formSizes.ClientSize.Width -
                _formSizes.BFsRect.Left * 2;
            _formSizes.BFsRect.Height = _formSizes.ClientSize.Height * 60 / 100 -
                _formSizes.BFsRect.Top * 2;

            {
                int cellSizeX = (_formSizes.BFsRect.Width - 2) / GameConfig.BFWidth;
                int cellSizeY = (_formSizes.BFsRect.Height - 2) / GameConfig.BFHeight;

                _formSizes.CellSize = Math.Min(cellSizeX, cellSizeY);
                _formSizes.BFRect.Width = _formSizes.CellSize * GameConfig.BFWidth;
                _formSizes.BFRect.Height = _formSizes.CellSize * GameConfig.BFHeight;

                _formSizes.BFRect.X = _formSizes.BFsRect.Left + 1 +
                    (_formSizes.BFsRect.Width - _formSizes.BFRect.Width) / 2;
                _formSizes.BFRect.Y = _formSizes.BFsRect.Top + 1 +
                    (_formSizes.BFsRect.Height - _formSizes.BFRect.Height) / 2;
            }

            ShipsSelectionHelper.Instance.ResizePanel();
            ShipsSelectionHelper.Instance.SelectionPanel.Left = _formSizes.ClientSize.Width -
                ShipsSelectionHelper.Instance.SelectionPanel.Width - 2;
            ShipsSelectionHelper.Instance.SelectionPanel.Top = _formSizes.BFsRect.Bottom + 1;
            ShipsSelectionHelper.Instance.SelectionPanel.Height = _formSizes.ClientSize.Height -
                ShipsSelectionHelper.Instance.SelectionPanel.Top - 2;
        }

        void _form_Paint(object sender, PaintEventArgs e)
        {
            if (GameGlobals.CurrentGameMode == GameMode.None)
                return;

            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            g.Clear(Color.White);
            g.DrawRectangle(GamePens.Instance.BFRectPen, _formSizes.BFsRect);

            switch (GameGlobals.CurrentGameMode)
            {
                case GameMode.PlacingShips:
                    {
                        GameGlobals.PlayerBF.Draw(g, _formSizes.BFRect.Left,
                            _formSizes.BFRect.Top, _formSizes.CellSize);

                        g.DrawString("На счету: " + GameGlobals.PlayerCash.ToString(),
                            GamePens.Instance.IngameFont, GamePens.Instance.IngameFontBrush,
                            (float)_formSizes.BFsRect.Left, (float)_formSizes.BFsRect.Bottom + 2);
                    }
                    break;
                case GameMode.PlayerActing:
                    {
                        GameGlobals.AIBF.Draw(g, _formSizes.BFRect.Left, _formSizes.BFRect.Top,
                            _formSizes.CellSize);

                        //выделение под курсором                
                        Point? mp = MouseHelper.GetMouseCoordsInField();
                        if (mp.HasValue)
                        {
                            RectangleF rect = new Rectangle(
                                _formSizes.BFRect.Left + mp.Value.X * _formSizes.CellSize,
                                _formSizes.BFRect.Top + mp.Value.Y * _formSizes.CellSize,
                                _formSizes.CellSize, _formSizes.CellSize);
                            g.FillRectangle(GamePens.Instance.SelectionBrush, rect);
                        }

                        int shots = GameGlobals.PlayerBF.Guns -
                            GameGlobals.AIBF.SelectedTargets.Count;
                        int totalShots = GameGlobals.PlayerBF.Guns;

                        g.DrawString("Выстрелов: " + shots.ToString() +
                            "/" + totalShots.ToString(), GamePens.Instance.IngameFont,
                            GamePens.Instance.IngameFontBrush,
                            (float)_formSizes.BFsRect.Left, (float)_formSizes.BFsRect.Bottom + 2);
                    }
                    break;
                case GameMode.PlayerStartFire:
                    {
                        GameGlobals.PlayerBF.Draw(g,
                            _formSizes.BFRect.Left, _formSizes.BFRect.Top,
                            _formSizes.CellSize);
                    }
                    break;
                case GameMode.PlayerEndFire:
                    {
                        GameGlobals.AIBF.Draw(g, _formSizes.BFRect.Left, _formSizes.BFRect.Top,
                            _formSizes.CellSize);
                    }
                    break;
                case GameMode.AIStartFire:
                    {
                        GameGlobals.AIBF.Draw(g, _formSizes.BFRect.Left,
                            _formSizes.BFRect.Top, _formSizes.CellSize);
                    }
                    break;
                case GameMode.AIEndFire:
                    {
                        GameGlobals.PlayerBF.Draw(g,
                            _formSizes.BFRect.Left, _formSizes.BFRect.Top,
                            _formSizes.CellSize);
                    }
                    break;
            }

            g.Flush();
        }
    }
}
