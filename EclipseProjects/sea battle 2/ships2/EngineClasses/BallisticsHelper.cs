﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ships2.EngineClasses
{
    public static class BallisticsHelper
    {
        public static float GetAngle(float sx, float sy, float gravity)
        {
            float tg = sy / sx - gravity * sx / 2;
            float angle = (float)Math.Atan(tg);
            return angle;
        }

        public static float GetVertSpeed(float horSpeed, float sx, float sy, float gravity)
        {
            return sy * horSpeed / sx - gravity * sx / 2 / horSpeed;            
        }       
    }
}
