﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ships2.EngineClasses
{
    public class TimingHelper
    {
        public static void Pause(int interval)
        {
            long pauseEndTime = System.Environment.TickCount + interval;

            while (System.Environment.TickCount < pauseEndTime)
            {
                System.Threading.Thread.Sleep(10);
                System.Windows.Forms.Application.DoEvents();
            }
        }
    }
}
