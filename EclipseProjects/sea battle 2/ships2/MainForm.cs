﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ships2.GameClasses;
using ships2.EngineClasses;
using ships2.GuiClasses;

namespace ships2
{
    public partial class MainForm : Form
    {
        public GameGraphics GameGraphics = new GameGraphics();
        private GameInput gameInput = new GameInput();

        public MainForm()
        {
            InitializeComponent();

            GameGlobals.MainForm = this;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private long LastRefreshTime = 0;
        private void RefreshForm()
        {
            if (System.Environment.TickCount - LastRefreshTime < 15)
                return;

            Refresh();
            LastRefreshTime = System.Environment.TickCount;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            GameClasses.GameGlobals.StartSelection();

            GameGraphics.AttachTo(this);
            gameInput.AttachTo(this);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void MainForm_MouseUp(object sender, MouseEventArgs e)
        {
            Point? mp = MouseHelper.GetMouseCoordsInField();
            if (mp.HasValue)
                gameInput.MouseUp(mp.Value);
        }
    }
}
