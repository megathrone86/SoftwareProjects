﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ships2.GameClasses;
using ships2.EngineClasses;

namespace ships2.GuiClasses
{
    public class ShipsSelectionHelper
    {
        public static ShipsSelectionHelper Instance = new ShipsSelectionHelper();

        public Panel SelectionPanel;
        public Ship CurrentShip;
        private List<Button> ShipButtons = new List<Button>();
        private ToolTip bt_ToolTip;

        protected ShipsSelectionHelper()
        {
            bt_ToolTip = new ToolTip();
            bt_ToolTip.ShowAlways = true;
            bt_ToolTip.AutoPopDelay = 0;
            bt_ToolTip.InitialDelay = 0;
            bt_ToolTip.ReshowDelay = 0;
        }

        public void AttachTo(Form _form)
        {
            if (SelectionPanel == null)
            {
                SelectionPanel = new Panel();
                SelectionPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;                
                SelectionPanel.AutoScroll = true;                
            }

            SelectionPanel.Parent = _form;

            if (ShipButtons.Count > 0)
            {
                foreach (Button bt in ShipButtons)
                    bt.Dispose();
                ShipButtons.Clear();
            }

            bt_ToolTip.RemoveAll();
            foreach (ShipType st in ShipTypesDictionary.ShipTypes)
            {
                Button bt = new Button();

                bt.AutoSize = true;                
                bt.Visible = true;
                bt.Parent = SelectionPanel;
                string text = st.name + "\r\n";
                text += "$" + st.price + "\r\n";
                text += "Размеры: " + st.sx + "x" + st.sy + "\r\n";
                text += "Пушек: " + st.cannons_count;
                bt.Text = text;
                bt.Tag = st;
                bt.Click += new EventHandler(bt_Click);
                bt_ToolTip.SetToolTip(bt, st.description);

                ShipButtons.Add(bt);
            }

            UpdateButtons();
        }        

        void bt_MouseEnter(object sender, EventArgs e)
        {
            ShipType CurrentShipType = (sender as Button).Tag as ShipType;

            bt_ToolTip.Show(CurrentShipType.description, GameGlobals.MainForm);            
        }

        void bt_Click(object sender, EventArgs e)
        {
            ShipType CurrentShipType = (sender as Button).Tag as ShipType;
            if (GameGlobals.PlayerCash < CurrentShipType.price)
                return;

            CurrentShip = new Ship(CurrentShipType);            
            CurrentShip.BF = GameGlobals.PlayerBF;
            CurrentShip.SetRandomCoords();
            GameGlobals.PlayerBF.Ships.Add(CurrentShip);

            GameGlobals.PlayerCash -= CurrentShipType.price;

            UpdateButtons();

            //если больше нельзя выбрать ни одного корабля,
            //начинаем игру
            if (ShipButtons.Find(bt => bt.Enabled) == null)
            {
                AIPlaceShips();
                GameGlobals.MainForm.Invalidate();
                TimingHelper.Pause(1000);
                GameGlobals.StartGame();
            }
        }

        public void ResizePanel()
        {
            int ty = 1;
            foreach (Button bt in ShipButtons)
            {
                bt.Top = ty;
                bt.Left = 1;
                ty = bt.Bottom + 1;
            }
        }

        public void HidePanel()
        {
            SelectionPanel.Hide();
        }

        public void UpdateButtons()
        {
            foreach (Button bt in ShipButtons)
            {
                ShipType CurrentShipType = bt.Tag as ShipType;

                bt.Enabled = GameGlobals.PlayerCash >= CurrentShipType.price;
            }
        }

        public void AIPlaceShips()
        {
            ShipType cheapestShip = ShipTypesDictionary.ShipTypes[0];
            foreach (ShipType st in ShipTypesDictionary.ShipTypes)
            {
                if (st.price < cheapestShip.price)
                    cheapestShip = st;
            }

            while (GameGlobals.AICash>=cheapestShip.price)
            {
                int n = GameGlobals.Random.Next(ShipTypesDictionary.ShipTypes.Count);
                if (GameGlobals.AICash >= ShipTypesDictionary.ShipTypes[n].price)
                {
                    CurrentShip = new Ship(ShipTypesDictionary.ShipTypes[n]);
                    CurrentShip.BF = GameGlobals.AIBF;
                    CurrentShip.SetRandomCoords();
                    GameGlobals.AIBF.Ships.Add(CurrentShip);

                    GameGlobals.AICash -= ShipTypesDictionary.ShipTypes[n].price;
                }
            }
        }
    }
}
