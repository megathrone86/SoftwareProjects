﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace ships2.GuiClasses
{
    class PlayerActingHelper
    {
        public static PlayerActingHelper Instance = new PlayerActingHelper();

        public Panel ButtonsPanel;
        private List<Button> Buttons = new List<Button>();
        private EventHandler MouseHoverEvent;

        public PlayerActingHelper()
        {
            MouseHoverEvent = new EventHandler(_form_MouseHover);
        }

        public void AttachTo(Form _form)
        {
            if (ButtonsPanel == null)
            {
                ButtonsPanel = new Panel();
                ButtonsPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                ButtonsPanel.AutoScroll = true;
                ButtonsPanel.Visible = false;
            }

            ButtonsPanel.Parent = _form;
            _form.MouseHover -= MouseHoverEvent;
            _form.MouseHover += MouseHoverEvent;
        }

        void _form_MouseHover(object sender, EventArgs e)
        {
            
        }

        public void HidePanel()
        {
            ButtonsPanel.Hide();
        }

        public void ResizePanel()
        {
            int tx = 1;
            foreach (Button bt in Buttons)
            {
                bt.Top = 1;
                bt.Left = tx;
                tx = bt.Right + 1;
            }
        }
    }
}
