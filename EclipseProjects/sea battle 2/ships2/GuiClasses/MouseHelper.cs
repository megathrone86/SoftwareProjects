﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using ships2.GameClasses;

namespace ships2.GuiClasses
{
    public class MouseHelper
    {
        public static Point? GetMouseCoordsInField()
        {
            Point localCursorPos = GameGlobals.MainForm.PointToClient(
                System.Windows.Forms.Cursor.Position);

            localCursorPos = GameGlobals.MainForm.GameGraphics.CursorToBF(localCursorPos);

            if ((localCursorPos.X < 0) || (localCursorPos.Y < 0) ||
                (localCursorPos.X >= GameConfig.BFWidth) ||
                (localCursorPos.Y >= GameConfig.BFHeight))
                return null;

            return localCursorPos;;
        }
    }
}
