﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ships2.GameClasses
{
    public enum ShipCellState
    {
        Normal,
        Wound
    }

    public class ShipType
    {
        public string name;
        public string description;
        public int sx;
        public int sy;
        public int cannons_count;
        public int price;
    }

    public static class ShipTypesDictionary
    {
        public static void Init()
        {
            FileStream fs = new FileStream(Path.Combine(Program.AppPath,
                "data\\shipTypes.xml"), FileMode.Open);
            XmlSerializer ser = new XmlSerializer(typeof(List<ShipType>));
            _shipTypes = ser.Deserialize(fs) as List<ShipType>;
            fs.Close();
        }

        private static List<ShipType> _shipTypes;

        public static List<ShipType> ShipTypes
        {
            get
            {
                return _shipTypes;
            }
        }
    }
}
