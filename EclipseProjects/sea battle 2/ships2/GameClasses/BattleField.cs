﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using ships2.EngineClasses;
using ships2.EngineClasses.Sprites;
using ships2.GameClasses.SubClasses;

namespace ships2.GameClasses
{
    public class BattleField
    {
        //здесь хранится инфа о клетках которые уже нельзя выбирать
        private List<Hit> Hits;
        //здесь хранятся корабли расставленные на поле
        public List<Ship> Ships;

        //здесь хранятся цели выбранные игроком
        public List<Hit> SelectedTargets;
        //доп. класс отвечаюший за анимации (полет снарядов и т.п.)
        public BattleFieldAnimation Animation;
        //всякие анимированные спрайты
        public List<BasicSprite> Sprites;

        public void Stop()
        {

        }

        public int Guns
        {
            get
            {
                int result = 0;
                foreach (Ship shp in Ships.ToArray())
                {
                    if (shp.IsDead)
                        continue;

                    result += shp.ShipType.cannons_count;
                }
                return result;
            }
        }

        public BattleField()
        {
            Animation = new BattleFieldAnimation(this);

            Hits = new List<Hit>();
            Ships = new List<Ship>();
            SelectedTargets = new List<Hit>();
            Sprites = new List<BasicSprite>();
        }

        public void Draw(Graphics g, int startX, int startY, int cellSize)
        {
            //сетка
            {
                for (int i = 0; i < GameConfig.BFHeight + 1; i++)
                {
                    g.DrawLine(GamePens.Instance.BFCellPen,
                        startX, startY + i * cellSize,
                        startX + GameConfig.BFWidth * cellSize, startY + i * cellSize);
                }
                for (int i = 0; i < GameConfig.BFWidth + 1; i++)
                {
                    g.DrawLine(GamePens.Instance.BFCellPen,
                        startX + i * cellSize, startY,
                        startX + i * cellSize, startY + GameConfig.BFHeight * cellSize);
                }
            }

            //клетки в которые уже стреляли
            foreach (Hit hit in Hits.ToArray())
            {
                if (!hit.visible)
                    continue;

                float cpx = startX + hit.x * cellSize;
                float cpy = startY + hit.y * cellSize;
                g.FillEllipse(GamePens.Instance.BFHittedCellBrush, cpx, cpy,
                            cellSize, cellSize);
            }

            //корабли
            {
                foreach (Ship ship in Ships.ToArray())
                {
                    ship.Draw(g, startX, startY, cellSize);
                }
            }

            //новые цели
            if (GameGlobals.CurrentGameMode == GameMode.PlayerActing)
            {
                foreach (Hit newTarget in SelectedTargets.ToArray())
                {
                    RectangleF rect = new Rectangle();
                    rect.X = startX + newTarget.x * cellSize;
                    rect.Y = startY + newTarget.y * cellSize;
                    rect.Width = cellSize;
                    rect.Height = cellSize;
                    g.FillRectangle(GamePens.Instance.NewTargetBrush, rect);
                }
            }

            //спрайты
            try
            {
                foreach (BasicSprite sprite in Sprites.ToArray())
                {
                    if (!sprite.Dead)
                        sprite.Draw(g, startX, startY, cellSize);
                }
            }
            catch { }

            //анимация
            Animation.Draw(g, startX, startY, cellSize);

            //чистка
            Sprites.RemoveAll(s => s.Dead);
        }

        public bool CellIsShottable(int x, int y)
        {
            return (Hits.Find(h => h.x == x && h.y == y) == null) &&
                (SelectedTargets.Find(h => h.x == x && h.y == y) == null);
        }

        public void AddHit(int x, int y, bool visible)
        {
            Hit hit = Hits.Find(h => h.x == x && h.y == y);
            if (hit != null)
            {
                hit.visible = visible;
                return;
            }

            if ((x < 0) || (x >= GameConfig.BFWidth) || (y < 0) || (y >= GameConfig.BFHeight))
                return;

            Hits.Add(new Hit(x, y, visible));
        }

        public bool NoMoreShottableCells
        {
            get
            {
                for (int x = 0; x < GameConfig.BFWidth; x++)
                    for (int y = 0; y < GameConfig.BFHeight; y++)
                        if (CellIsShottable(x, y))
                            return false;
                
                return true;
            }
        }
    }

    public class Hit
    {
        public Hit(int x, int y, bool visible)
        {
            this.x = x;
            this.y = y;
            this.visible = visible;
        }

        public bool visible;

        public int x;
        public int y;
    }    
}
