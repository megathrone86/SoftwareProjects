﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace ships2.GameClasses
{
    public static class GameConfig
    {
        public static int StartCash;
        public static int BFWidth;
        public static int BFHeight;

        private static XmlDocument _xmlDoc = new XmlDocument();

        private static string getParam(string paramName)
        {
            XmlNode node = _xmlDoc.SelectSingleNode("//" + paramName);
            if (node != null)
                return node.InnerText;
            else
                return "";
        }

        public static void Load()
        {
            _xmlDoc.Load(Path.Combine(Program.AppPath, "data\\gameConfig.xml"));

            StartCash = int.Parse(getParam("startCash"));
            BFWidth = int.Parse(getParam("bfWidth"));
            BFHeight = int.Parse(getParam("bfHeight"));
        }
    }
}
