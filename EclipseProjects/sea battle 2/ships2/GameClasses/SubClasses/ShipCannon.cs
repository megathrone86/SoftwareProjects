﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ships2.GameClasses.SubClasses
{
    public class ShipCannon
    {
        public float X;
        public float Y;

        public ShipCannon(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
