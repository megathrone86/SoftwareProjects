﻿using System;
using System.Timers;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using ships2.EngineClasses;
using ships2.EngineClasses.Sprites;
using ships2.GameClasses.SubClasses;

namespace ships2.GameClasses.SubClasses
{
    //здесь все координаты относительные (относительно поля боя)
    public class BattleFieldAnimation
    {
        private BattleField parent;
        public List<Trace> Traces = new List<Trace>(100);
        private static readonly float gravity = 5f;
        private Timer animateTimer = new Timer(10);

        public BattleFieldAnimation(BattleField _parent)
        {
            parent = _parent;
            animateTimer.Elapsed += new ElapsedEventHandler(ProcessTraсes);
            animateTimer.Start();
        }

        private long lastAnimateTime = 0;
        void ProcessTraсes(object sender, ElapsedEventArgs e)
        {
            try
            {
                float timeDelta = (System.Environment.TickCount - lastAnimateTime) / 1000.0f;

                for (int i = 0; i < Traces.Count; i++)
                {
                    Trace trace = Traces[i];

                    if ((Traces[i] == null) || (!trace.active))
                        continue;

                    if (trace.X < 0 || trace.X >= GameConfig.BFWidth
                        || trace.Y >= GameConfig.BFHeight)
                        trace.outside = true;

                    MoveTrace(ref trace, timeDelta);

                    Traces[i] = trace;
                }

                lastAnimateTime = System.Environment.TickCount;
            }
            catch { }
        }

        private void MoveTrace(ref Trace trace, float timeDelta)
        {
            if (!trace.active)
                return;

            if (trace.CloseToTarget())
            {
                if ((GameGlobals.CurrentGameMode == GameMode.PlayerEndFire) ||
                (GameGlobals.CurrentGameMode == GameMode.AIEndFire))
                {
                    trace.active = false;

                    float cellX = (float)Math.Truncate(trace.X);
                    float cellY = (float)Math.Truncate(trace.Y);

                    GameProcess.PutHitOnField(parent, cellX, cellY);                    
                }
                else
                {
                    trace.outside = true;
                }
            }
            else
            {
                trace.X += trace.speedX * timeDelta;
                trace.Y += trace.speedY * timeDelta;
                trace.speedY += gravity * timeDelta;
            }
        }

        public void StartAnimateFire(List<Hit> hits, bool leftToRight)
        {
            Traces.Clear();

            int currentHit = 0;
            foreach (Ship ship in parent.Ships.ToArray())
            {
                if (ship.IsDead)
                    continue;

                foreach (ShipCannon cannon in ship.ShipCannons.ToArray())
                {
                    if (currentHit >= parent.Guns)
                        return;
                    if (currentHit >= hits.Count)
                        break;

                    float cpx = ship.X + cannon.X;
                    float cpy = ship.Y + cannon.Y;

                    Trace trace = new Trace()
                    {
                        X = cpx,
                        Y = cpy                        
                    };

                    trace.endX = hits[currentHit].x + 0.5f;
                    trace.endY = hits[currentHit].y + 0.5f;

                    if (leftToRight)
                    {
                        trace.endX += GameConfig.BFWidth;
                        trace.speedX = 4f;
                    }
                    else
                    {
                        trace.endX -= GameConfig.BFWidth;
                        trace.speedX = -4f;
                    }

                    trace.speedY = BallisticsHelper.GetVertSpeed(
                        trace.speedX,
                        trace.endX - trace.X,
                        trace.endY - trace.Y,
                        gravity);
                    
                    Traces.Add(trace);
                    currentHit++;                    
                }
            }

            lastAnimateTime = System.Environment.TickCount;
        }

        public void StartAnimateHit(List<Trace> FlyingTraces, bool leftToRight)
        {
            FlyingTraces.ForEach(trace =>
            {
                Trace newTrace = new Trace();                
                newTrace.Y = trace.Y;                
                newTrace.endY = trace.endY;
                newTrace.speedX = trace.speedX;
                newTrace.speedY = trace.speedY;

                if (leftToRight)
                {
                    newTrace.X = trace.X - GameConfig.BFWidth;
                    newTrace.endX = trace.endX - GameConfig.BFWidth;
                } else
                {
                    newTrace.X = trace.X + GameConfig.BFWidth;
                    newTrace.endX = trace.endX + GameConfig.BFWidth;
                }

                Traces.Add(newTrace);
            });

            lastAnimateTime = System.Environment.TickCount;
        }

        public bool Active
        {
            get
            {
                Trace activeTrace = Traces.Find(trace => trace.active && !trace.outside);
                BasicSprite activeSprite = parent.Sprites.Find(s => !s.Dead && 
                    (s is ExplosionSprite));

                return (activeTrace != null || activeSprite!=null);
            }
        }

        public void Draw(Graphics g, int startX, int startY, int cellSize)
        {
            foreach (Trace trace in Traces.ToArray())
            {
                if (!trace.active || trace.outside)
                    continue;

                float cpx = startX + trace.X * cellSize;
                float cpy = startY + trace.Y * cellSize;
                g.DrawLine(GamePens.Instance.TracePen, cpx, cpy,
                    cpx + trace.speedX * 2.5f, cpy + trace.speedY * 2.5f);
            }
        }
    }

    public class Trace
    {
        public bool active;
        public bool outside;
        public float X, Y;
        public float speedX, speedY;
        public float endX, endY;

        public bool CloseToTarget()
        {
            return Math.Abs(X - endX) < 0.4 &&
                Math.Abs(Y - endY) < 0.4;
        }

        public Trace()
        {
            active = true;
        }
    }
}
