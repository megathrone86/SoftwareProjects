﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ships2.EngineClasses;
using ships2.GuiClasses;

namespace ships2.GameClasses
{
    public static class GameGlobals
    {
        public static MainForm MainForm;
        public static Random Random;
        public static BattleField PlayerBF;
        public static BattleField AIBF;

        public static int PlayerCash;
        public static int AICash;

        public static GameMode CurrentGameMode;

        static GameGlobals()
        {
            Random = new Random((int)System.Environment.TickCount);
        }

        public static void StartSelection()
        {
            CurrentGameMode = GameMode.None;

            GameClasses.GameConfig.Load();
            GameClasses.ShipTypesDictionary.Init();

            PlayerBF = new BattleField();
            AIBF = new BattleField();
            CurrentGameMode = GameMode.PlacingShips;
            PlayerCash = GameConfig.StartCash;
            ShipsSelectionHelper.Instance.AttachTo(MainForm);

            AICash = GameConfig.StartCash;
        }

        public static void StartGame()
        {
            ShipsSelectionHelper.Instance.HidePanel();
            PlayerActingHelper.Instance.AttachTo(MainForm);
            CurrentGameMode = GameMode.PlayerActing;
            GameGlobals.AIBF.SelectedTargets.Clear();
        }
    }

    public enum GameMode
    {
        None,
        PlacingShips,
        PlayerActing,
        PlayerStartFire,
        PlayerEndFire,
        AIStartFire,
        AIEndFire        
    }
}
