﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using ships2.EngineClasses;
using ships2.GameClasses.SubClasses;

namespace ships2.GameClasses
{
    public class Ship
    {
        public ShipType ShipType;
        public BattleField BF;

        public int X;
        public int Y;
        public List<ShipCannon> ShipCannons;

        public ShipCellState[,] Cells;

        private bool is_vertical;
        public bool IsVertical
        {
            get
            {
                return is_vertical;
            }
            set
            {
                is_vertical = value;
                updateCannons();
            }
        }

        public Ship(ShipType shipType)
        {
            ShipType = shipType;
            updateCannons();
            Cells = new ShipCellState[shipType.sx, shipType.sy];

            for (int i = 0; i < shipType.sx; i++)
                for (int j = 0; j < shipType.sy; j++)
                    Cells[i, j] = ShipCellState.Normal;
        }

        public void updateCannons()
        {
            int cannonsCount = ShipType.cannons_count;
            ShipCannons = new List<ShipCannon>(cannonsCount);

            float sy = ShipType.sy;
            float f;

            for (int i = 0; i < cannonsCount; i++)
            {
                if (cannonsCount > 1)
                    f = (float)i / (cannonsCount - 1) * sy * 0.8f + sy / 10f;
                else
                    f = 0.5f * sy;

                if (IsVertical)
                    ShipCannons.Add(new ShipCannon(0.5f * ShipType.sx, (float)f));
                else
                    ShipCannons.Add(new ShipCannon((float)f, 0.5f * ShipType.sx));
            }
        }

        public void Draw(Graphics g, int startX, int startY, int cellSize)
        {

            if ((this.BF == GameGlobals.PlayerBF) || (this.IsDead))
            {
                int sx, sy;
                if (IsVertical)
                {
                    sx = ShipType.sx * cellSize;
                    sy = ShipType.sy * cellSize;
                }
                else
                {
                    sx = ShipType.sy * cellSize;
                    sy = ShipType.sx * cellSize;
                }

                //сам корабль
                g.FillEllipse(GamePens.Instance.ShipBrush, startX + X * cellSize,
                    startY + Y * cellSize, sx, sy);

                //пушки
                foreach (ShipCannon cannon in ShipCannons.ToArray())
                {
                    int cannonOffset = (this.BF == GameGlobals.AIBF) ? -5 : 5;
                    float cpx = startX + X * cellSize + cannon.X * cellSize;
                    float cpy = startY + Y * cellSize + cannon.Y * cellSize;
                    g.DrawLine(GamePens.Instance.ShipCannonPen, cpx, cpy, cpx + cannonOffset, cpy - 5);
                    g.FillEllipse(GamePens.Instance.ShipCannonBrush, cpx - 2, cpy - 2, 4, 4);
                }
            }

            for (int i = 0; i < this.ShipType.sx; i++)
                for (int j = 0; j < this.ShipType.sy; j++)
                {
                    float cpx;
                    float cpy;

                    if (this.is_vertical)
                    {
                        cpx = startX + (X + i) * cellSize;
                        cpy = startY + (Y + j) * cellSize;
                    }
                    else
                    {
                        cpx = startX + (X + j) * cellSize;
                        cpy = startY + (Y + i) * cellSize;
                    }                    

                    if (this.Cells[i, j] == ShipCellState.Wound)
                        g.FillEllipse(GamePens.Instance.ShipWoundCellBrush, cpx, cpy,
                            cellSize, cellSize);
                }            
        }

        public void SetRandomCoords()
        {
            IsVertical = GameGlobals.Random.Next(10) < 5;

            while (true)
            {
                X = GameGlobals.Random.Next(GameConfig.BFWidth);
                Y = GameGlobals.Random.Next(GameConfig.BFHeight);

                if (PositionIsGood)
                    break;
            }
        }

        public int X2
        {
            get
            {
                if (IsVertical)
                    return X + ShipType.sx - 1;
                else
                    return X + ShipType.sy - 1;
            }
        }

        public int Y2
        {
            get
            {
                if (IsVertical)
                    return Y + ShipType.sy - 1;
                else
                    return Y + ShipType.sx - 1;
            }
        }

        public bool PositionIsGood
        {
            get
            {
                if ((X < 0) || (Y < 0) || (X2 >= GameConfig.BFWidth) || (Y2 >= GameConfig.BFHeight))
                    return false;

                foreach (Ship ship in BF.Ships.ToArray())
                {
                    if (ship == this)
                        continue;

                    for (int tx = ship.X - 1; tx <= ship.X2 + 1; tx++)
                        for (int ty = ship.Y - 1; ty <= ship.Y2 + 1; ty++)
                        {
                            if ((tx >= X) && (tx <= X2) && (ty >= Y) && (ty <= Y2))
                                return false;
                        }
                }

                return true;
            }
        }

        public int AliveCells
        {
            get
            {
                int result = 0;
                for (int i = 0; i < this.ShipType.sx; i++)
                    for (int j = 0; j < this.ShipType.sy; j++)
                    {
                        if (this.Cells[i, j] == ShipCellState.Normal)
                            result++;
                    }

                return result;
            }
        }

        public bool IsDead { get { return AliveCells <= 0; } }
    }
}
