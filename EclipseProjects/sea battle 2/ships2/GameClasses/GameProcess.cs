﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using ships2.EngineClasses.Sprites;

namespace ships2.GameClasses
{
    public class GameProcess
    {
        public static void Pause(int msec)
        {
            if (msec <= 0)
                return;

            long ticksEnd = System.Environment.TickCount + msec;

            while (System.Environment.TickCount < ticksEnd)
            {
                System.Threading.Thread.Sleep(10);
                Application.DoEvents();
            }
        }

        public static void PlayerFire()
        {
            //анимация хода игрока
            //запуск
            GameGlobals.CurrentGameMode = GameMode.PlayerStartFire;
            GameGlobals.PlayerBF.Animation.StartAnimateFire(GameGlobals.AIBF.SelectedTargets, true);
            while (GameGlobals.PlayerBF.Animation.Active)
            {
                System.Threading.Thread.Sleep(10);
                Application.DoEvents();
            }            

            //приземление
            GameGlobals.CurrentGameMode = GameMode.PlayerEndFire;
            GameGlobals.AIBF.Animation.StartAnimateHit(GameGlobals.PlayerBF.Animation.Traces, true);
            GameGlobals.PlayerBF.Animation.Traces.Clear();
            while (GameGlobals.AIBF.Animation.Active)
            {
                System.Threading.Thread.Sleep(10);
                Application.DoEvents();
            }

            GameProcess.Pause(1000);

            //передаем ход боту
            AIFire();
        }

        public static void AIFire()
        {
            //выбор целей
            {
                GameGlobals.PlayerBF.SelectedTargets.Clear();
                while ((GameGlobals.PlayerBF.SelectedTargets.Count < GameGlobals.AIBF.Guns) &&
                    !GameGlobals.PlayerBF.NoMoreShottableCells)
                {
                    Hit hit = new Hit(GameGlobals.Random.Next(GameConfig.BFWidth),
                        GameGlobals.Random.Next(GameConfig.BFHeight), true);

                    if (!GameGlobals.PlayerBF.CellIsShottable(hit.x, hit.y))
                        continue;

                    GameGlobals.PlayerBF.SelectedTargets.Add(hit);
                }
            }
            
            //запуск
            GameGlobals.CurrentGameMode = GameMode.AIStartFire;
            GameGlobals.AIBF.Animation.StartAnimateFire(GameGlobals.PlayerBF.SelectedTargets, false);
            while (GameGlobals.AIBF.Animation.Active)
            {
                System.Threading.Thread.Sleep(10);
                Application.DoEvents();
            }

            //приземление
            GameGlobals.CurrentGameMode = GameMode.AIEndFire;
            GameGlobals.PlayerBF.Animation.StartAnimateHit(GameGlobals.AIBF.Animation.Traces, false);
            GameGlobals.AIBF.Animation.Traces.Clear();
            while (GameGlobals.PlayerBF.Animation.Active)
            {
                System.Threading.Thread.Sleep(10);
                Application.DoEvents();
            }

            //передаем ход игрока
            GameGlobals.CurrentGameMode = GameMode.PlayerActing;
            GameGlobals.PlayerBF.SelectedTargets.Clear();
            GameGlobals.AIBF.SelectedTargets.Clear();
        }

        public static void PutHitOnField(BattleField BF, float x, float y)
        {
            ExplosionSprite sprite = new ExplosionSprite(500, new RectangleF(
                x, y, 1f, 1f));
            BF.Sprites.Add(sprite);

            foreach (Ship ship in BF.Ships.ToArray())
            {
                if (ship.IsDead)
                    continue;

                if ((x < ship.X) || (x > ship.X2) || (y < ship.Y) || (y > ship.Y2))
                    continue;

                int shipX = (int)x - ship.X;
                int shipY = (int)y - ship.Y;

                if (!ship.IsVertical)
                {
                    int t = shipX;
                    shipX = shipY;
                    shipY = t;
                }

                ship.Cells[shipX, shipY] = ShipCellState.Wound;
                BF.AddHit((int)x, (int)y, false);

                //если корабль убит, пометим все находящиеся возле него клетки
                if (ship.IsDead)
                {
                    for (int tx = ship.X - 1; tx <= ship.X2 + 1; tx++)                    
                    {
                        BF.AddHit(tx, ship.Y - 1, true);
                        BF.AddHit(tx, ship.Y2 + 1, true);                        
                    }

                    for (int ty = ship.Y - 1; ty <= ship.Y2 + 1; ty++)
                    {
                        BF.AddHit(ship.X - 1, ty, true);
                        BF.AddHit(ship.X2 + 1, ty, true);
                    }
                }

                return;
            }

            BF.AddHit((int)x, (int)y, true);
        }
    }    
}
