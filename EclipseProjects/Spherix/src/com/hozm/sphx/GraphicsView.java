package com.hozm.sphx;

import com.hozm.sphx.gameobjects.*;
import com.hozm.sphx.helpers.*;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.view.MotionEvent;
import android.view.View;

public class GraphicsView extends View {
	private Paint cellsBlockPaint;
	private Paint cellPaint;
	private Paint progressPaint;

	private boolean initialized;

	public GraphicsView(Context context) {
		super(context);

		initialized = false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if ((event.getAction() == MotionEvent.ACTION_UP)
				|| (event.getAction() == MotionEvent.ACTION_DOWN)
				|| (event.getAction() == MotionEvent.ACTION_MOVE))
			InputHelper.Instance.ProcessEvent(event);
		return true;
	}

	public void checkInitialize() {
		if (initialized)
			return;

		cellsBlockPaint = new Paint();
		cellsBlockPaint.setColor(Color.rgb(0, 0, 0));
		cellsBlockPaint.setStyle(Paint.Style.FILL_AND_STROKE);

		cellPaint = new Paint();
		cellPaint.setColor(Color.rgb(160, 160, 40));
		cellPaint.setStyle(Paint.Style.STROKE);

		progressPaint = new Paint();
		progressPaint.setColor(Color.rgb(0, 240, 240));
		progressPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		progressPaint.setTextAlign(Align.RIGHT);
		progressPaint
				.setTextSize(GraphicLayoutHelper.Instance.LogoHeight * 3 / 4);

		initialized = true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (!MyApplication.Started)
			return;

		checkInitialize();

		// ������� ������
		canvas.drawRGB(0, 0, 0);

		// ����
		canvas.drawBitmap(BitmapsHelper.Logo,
				GraphicLayoutHelper.Instance.LogoX,
				GraphicLayoutHelper.Instance.LogoY, null);

		{
			int progressWidth = 0;

			// ��������
			{
				String progress = GameLogic.Instance.GetWinPercent() + "%";
				progressPaint.setTextAlign(Align.RIGHT);
				progressWidth = (int) progressPaint.measureText(progress);
				canvas.drawText(progress,
						GraphicLayoutHelper.Instance.ProgressX,
						GraphicLayoutHelper.Instance.ProgressY, progressPaint);
			}

			// �����
			{
				String time = GameLogic.Instance.GetDiscountTime();
				progressPaint.setTextAlign(Align.CENTER);
				canvas.drawText(time, GraphicLayoutHelper.Instance.DiscountX
						- progressWidth,
						GraphicLayoutHelper.Instance.ProgressY, progressPaint);
			}
		}

		// ������� ����
		canvas.drawRect(GraphicLayoutHelper.Instance.CellsX,
				GraphicLayoutHelper.Instance.CellsY,
				GraphicLayoutHelper.Instance.GetCellsX2(),
				GraphicLayoutHelper.Instance.GetCellsY2(), cellsBlockPaint);

		// ����� �� ����
		{
			for (int i = 1; i < GameLogic.CellsCountX; i++) {
				float x = GraphicLayoutHelper.Instance.CellsX + i
						* (GraphicLayoutHelper.Instance.CellSize + 1);

				canvas.drawLine(x, GraphicLayoutHelper.Instance.CellsY, x,
						GraphicLayoutHelper.Instance.GetCellsY2(), cellPaint);
			}

			for (int i = 1; i < GameLogic.CellsCountY; i++) {
				float y = GraphicLayoutHelper.Instance.CellsY + i
						* (GraphicLayoutHelper.Instance.CellSize + 1);

				canvas.drawLine(GraphicLayoutHelper.Instance.CellsX, y,
						GraphicLayoutHelper.Instance.GetCellsX2(), y, cellPaint);
			}
		}

		// �����
		{
			canvas.drawBitmap(BitmapsHelper.FrameTopHalf1,
					GraphicLayoutHelper.Instance.FrameTopHalf1X,
					GraphicLayoutHelper.Instance.FrameTopHalf1Y, null);
			canvas.drawBitmap(BitmapsHelper.FrameTopHalf2,
					GraphicLayoutHelper.Instance.FrameTopHalf2X,
					GraphicLayoutHelper.Instance.FrameTopHalf2Y, null);

			canvas.drawBitmap(BitmapsHelper.FrameBottomHalf1,
					GraphicLayoutHelper.Instance.FrameBottomHalf1X,
					GraphicLayoutHelper.Instance.FrameBottomHalf1Y, null);
			canvas.drawBitmap(BitmapsHelper.FrameBottomHalf2,
					GraphicLayoutHelper.Instance.FrameBottomHalf2X,
					GraphicLayoutHelper.Instance.FrameBottomHalf2Y, null);

			canvas.drawBitmap(BitmapsHelper.FrameLeftHalf1,
					GraphicLayoutHelper.Instance.FrameLeftHalf1X,
					GraphicLayoutHelper.Instance.FrameLeftHalf1Y, null);
			canvas.drawBitmap(BitmapsHelper.FrameLeftHalf2,
					GraphicLayoutHelper.Instance.FrameLeftHalf2X,
					GraphicLayoutHelper.Instance.FrameLeftHalf2Y, null);

			canvas.drawBitmap(BitmapsHelper.FrameRightHalf1,
					GraphicLayoutHelper.Instance.FrameRightHalf1X,
					GraphicLayoutHelper.Instance.FrameRightHalf1Y, null);
			canvas.drawBitmap(BitmapsHelper.FrameRightHalf2,
					GraphicLayoutHelper.Instance.FrameRightHalf2X,
					GraphicLayoutHelper.Instance.FrameRightHalf2Y, null);
		}

		// ������
		if (Field.Instance != null) {
			for (int i = 0; i < Field.Instance.Balls.size(); i++) {
				Ball ball = Field.Instance.Balls.get(i);
				float bx = GraphicLayoutHelper.Instance.CellsX + ball.X
						* (GraphicLayoutHelper.Instance.CellSize + 1);
				float by = GraphicLayoutHelper.Instance.CellsY + ball.Y
						* (GraphicLayoutHelper.Instance.CellSize + 1);

				drawBall(canvas, ball, bx, by);
			}
		}

		//�����-����� ��������� ����
		if (GameLogic.Instance.GameFinished()) {
			int x = (GraphicLayoutHelper.Instance.ScreenSizeX - BitmapsHelper.WinScreen
					.getWidth()) / 2;
			int y = (GraphicLayoutHelper.Instance.ScreenSizeY - BitmapsHelper.WinScreen
					.getHeight()) / 2;

			canvas.drawBitmap(BitmapsHelper.WinScreen, x, y, null);
		}
	}

	private void drawBall(Canvas canvas, Ball ball, float x, float y) {
		int offset_x = 0;
		int offset_y = 0;

		// ����� ���������
		if (ball.MovingInfo.getMoving()) {
			long dTime = System.currentTimeMillis()
					- ball.MovingInfo.AnimationStart;
			int dx = (ball.MovingInfo.NewBallX - ball.X)
					* (GraphicLayoutHelper.Instance.CellSize + 1);
			int dy = (ball.MovingInfo.NewBallY - ball.Y)
					* (GraphicLayoutHelper.Instance.CellSize + 1);

			float f = (float) dTime / BallMovingInfo.MoveAnimationPeriod;
			if (f > 1f)
				f = 1f;

			offset_x = (int) (dx * f);
			offset_y = (int) (dy * f);

			if (dTime > BallMovingInfo.MoveAnimationPeriod) {
				// �������� ����������� ���������
				ball.X = ball.MovingInfo.NewBallX;
				ball.Y = ball.MovingInfo.NewBallY;
				ball.MovingInfo.setMoving(false);
				ball.HoverInfo.setHovered(false);
				GameLogic.Instance.RecalcBalls();
			}
		}

		canvas.drawBitmap(ball.GetBitmap(), x + offset_x, y + offset_y, null);
	}
}
