package com.hozm.sphx.gameobjects;

import android.graphics.Bitmap;

import com.hozm.sphx.helpers.BitmapsHelper;

public class Ball {
	public static final int BallTypesCount = 4;

	public int X;
	public int Y;
	public BallType Type;

	public BallMovingInfo MovingInfo;
	public BallHoverInfo HoverInfo;

	public Ball(int x, int y, BallType type) {
		X = x;
		Y = y;
		Type = type;

		MovingInfo = new BallMovingInfo();
		HoverInfo = new BallHoverInfo();
	}

	public enum BallType {
		Red, Green, Yellow, Blue
	}

	public Bitmap GetBitmap() {
		switch (Type) {
		case Blue: {
			if (HoverInfo.getHovered())
				return BitmapsHelper.BallHoverBlue;
			else
				return BitmapsHelper.BallBlue;
		}
		case Green: {
			if (HoverInfo.getHovered())
				return BitmapsHelper.BallHoverGreen;
			else
				return BitmapsHelper.BallGreen;
		}
		case Red: {
			if (HoverInfo.getHovered())
				return BitmapsHelper.BallHoverRed;
			else
				return BitmapsHelper.BallRed;
		}
		case Yellow: {
			if (HoverInfo.getHovered())
				return BitmapsHelper.BallHoverYellow;
			else
				return BitmapsHelper.BallYellow;
		}
		default:
			return null;
		}
	}
}
