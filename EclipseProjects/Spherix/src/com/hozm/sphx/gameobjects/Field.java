package com.hozm.sphx.gameobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.hozm.sphx.gameobjects.Ball.BallType;
import com.hozm.sphx.helpers.GraphicLayoutHelper;

public class Field {
	public static Field Instance;

	public List<Ball> Balls;
	private Random random;

	public int MovingBalls;
	public int HoveredBalls;		

	public Field() {
		Balls = new ArrayList<Ball>();

		random = new Random();
		random.setSeed(System.currentTimeMillis());

		MovingBalls = 0;
		HoveredBalls = 0;
	}

	private BallType getCurrentBallType(int num) {
		switch (num % Ball.BallTypesCount) {
		case 1:
			return BallType.Green;
		case 2:
			return BallType.Red;
		case 3:
			return BallType.Yellow;
		default:
			return BallType.Blue;
		}
	}

	public void Fill() {
		int ballsCount = GameLogic.CellsCountX * GameLogic.CellsCountY
				- GameLogic.FreeCells;

		for (int i = 0; i < ballsCount; i++) {
			Ball.BallType currentType = getCurrentBallType(i
					% Ball.BallTypesCount);

			while (true) {
				int x = Math.abs(random.nextInt() % GameLogic.CellsCountX);
				int y = Math.abs(random.nextInt() % GameLogic.CellsCountY);

				Ball ball = GetBall(x, y);
				if (ball == null) {
					ball = new Ball(x, y, currentType);
					Balls.add(ball);
					break;
				}
			}
		}
	}

	public Ball GetBall(int x, int y) {
		for (int i = 0; i < Balls.size(); i++) {
			if ((Balls.get(i).X == x) && (Balls.get(i).Y == y))
				return Balls.get(i);
		}
		return null;
	}

	public Ball GetBall(float x, float y) {
		if ((x < 0) || (y < 0) || (x > GraphicLayoutHelper.Instance.CellsSizeX)
				|| (y > GraphicLayoutHelper.Instance.CellsSizeY))
			return null;

		int cx = (int) (x / (GraphicLayoutHelper.Instance.CellSize + 1));
		int cy = (int) (y / (GraphicLayoutHelper.Instance.CellSize + 1));

		return GetBall(cx, cy);
	}
}
