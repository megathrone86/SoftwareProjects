package com.hozm.sphx.gameobjects;

public class BallMovingInfo {
	public static final long MoveAnimationPeriod = 100;
	
	private boolean moving;
	public int NewBallX;
	public int NewBallY;
	public long AnimationStart;

	public boolean getMoving() {
		return moving;
	}

	public void setMoving(boolean value) {
		if (value == moving)
			return;

		moving = value;
		if (moving)
			Field.Instance.MovingBalls++;
		else
			Field.Instance.MovingBalls--;
	}

	public BallMovingInfo() {
		moving = false;
	}
}