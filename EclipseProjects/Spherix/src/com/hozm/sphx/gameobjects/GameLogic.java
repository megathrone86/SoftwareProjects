package com.hozm.sphx.gameobjects;

public class GameLogic {
	public static int CellsCountX = 12;
	public static int CellsCountY = 8;
	public static int FreeCells = 4;
	
	public static GameLogic Instance = new GameLogic();

	private int TotalBalls;
	private int GoodBalls;

	public int GetWinPercent() {
		return 100 * GoodBalls / TotalBalls;
	}

	public boolean GameFinished() {
		return GoodBalls == TotalBalls;
	}

	public long PlayTimeSeconds;
	public int PlayTimeMilliseconds;

	public void StartCount() {
		PlayTimeSeconds = 0;
		PlayTimeMilliseconds = 0;
	}

	public boolean AddMillisecondsToCount(int msecs) {
		if (GameFinished())
			return false;
		
		PlayTimeMilliseconds += msecs;
		if (PlayTimeMilliseconds > 1000) {
			PlayTimeSeconds += PlayTimeMilliseconds / 1000;
			PlayTimeMilliseconds = PlayTimeMilliseconds % 1000;
			return true;
		}
		return false;
	}

	public String GetDiscountTime() {
		long longVal = PlayTimeSeconds;
		int hours = (int) longVal / 3600;
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		return String.format("%02d:%02d:%02d", hours, mins, secs);
	}

	public void RecalcBalls() {
		TotalBalls = Field.Instance.Balls.size();

		GoodBalls = 0;
		for (int i = 0; i < Field.Instance.Balls.size(); i++) {
			Ball ball = Field.Instance.Balls.get(i);

			switch (ball.Type) {
			case Green: {
				if ((ball.X < GameLogic.CellsCountX / 2)
						&& (ball.Y < GameLogic.CellsCountY / 2))
					GoodBalls++;
				break;
			}
			case Blue: {
				if ((ball.X >= GameLogic.CellsCountX / 2)
						&& (ball.Y >= GameLogic.CellsCountY / 2))
					GoodBalls++;
				break;
			}
			case Red: {
				if ((ball.X >= GameLogic.CellsCountX / 2)
						&& (ball.Y < GameLogic.CellsCountY / 2))
					GoodBalls++;
				break;
			}
			case Yellow: {
				if ((ball.X < GameLogic.CellsCountX / 2)
						&& (ball.Y >= GameLogic.CellsCountY / 2))
					GoodBalls++;
				break;
			}
			}
		}
	}
}
