package com.hozm.sphx.gameobjects;

public class BallHoverInfo {
	private boolean hovered;

	public boolean getHovered() {
		return hovered;
	}

	public void setHovered(boolean value) {
		if (value == hovered)
			return;

		hovered = value;
		if (hovered)
			Field.Instance.HoveredBalls++;
		else
			Field.Instance.HoveredBalls--;
	}

	public BallHoverInfo() {
		hovered = false;
	}
}