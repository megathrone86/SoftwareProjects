package com.hozm.sphx;

import com.hozm.sphx.gameobjects.GameLogic;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InputStringDialog implements OnClickListener {
	private Activity parentActivity;
	private DialogListener dialogListener;
	private AlertDialog.Builder alert;

	private EditText inputCellsCountX;
	private EditText inputCellsCountY;
	private EditText inputFreeCells;

	public InputStringDialog(Activity parentActivity) {
		this.parentActivity = parentActivity;
		this.dialogListener = null;
	}

	public void SetDialogListener(DialogListener dialogListener) {
		this.dialogListener = dialogListener;
	}

	public void ShowDialog(String message) {
		alert = new AlertDialog.Builder(parentActivity);
		alert.setMessage(message);

		LinearLayout layout = new LinearLayout(parentActivity);
		layout.setOrientation(LinearLayout.VERTICAL);

		{
			LinearLayout l2 = new LinearLayout(parentActivity);
			l2.setOrientation(LinearLayout.HORIZONTAL);

			TextView t = new TextView(parentActivity);
			t.setText("������ ����:");
			l2.addView(t);

			inputCellsCountX = new EditText(parentActivity);
			inputCellsCountX.setSingleLine();
			inputCellsCountX.setText(Integer.toString(GameLogic.CellsCountX));
			l2.addView(inputCellsCountX);

			layout.addView(l2);
		}

		{
			LinearLayout l2 = new LinearLayout(parentActivity);
			l2.setOrientation(LinearLayout.HORIZONTAL);

			TextView t = new TextView(parentActivity);
			t.setText("������ ����:");
			l2.addView(t);

			inputCellsCountY = new EditText(parentActivity);
			inputCellsCountY.setSingleLine();
			inputCellsCountY.setText(Integer.toString(GameLogic.CellsCountY));
			l2.addView(inputCellsCountY);

			layout.addView(l2);
		}

		{
			LinearLayout l2 = new LinearLayout(parentActivity);
			l2.setOrientation(LinearLayout.HORIZONTAL);

			TextView t = new TextView(parentActivity);
			t.setText("������ ������:");
			l2.addView(t);

			inputFreeCells = new EditText(parentActivity);
			inputFreeCells.setSingleLine();
			inputFreeCells.setText(Integer.toString(GameLogic.FreeCells));
			l2.addView(inputFreeCells);

			layout.addView(l2);
		}

		alert.setView(layout);
		alert.setPositiveButton("OK", this);

		parentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				alert.show();
			}
		});
	}

	private void ApplyChanges() {
		try {
			GameLogic.CellsCountX = Integer.parseInt(inputCellsCountX.getText()
					.toString());
			GameLogic.CellsCountY = Integer.parseInt(inputCellsCountY.getText()
					.toString());
			GameLogic.FreeCells = Integer.parseInt(inputFreeCells.getText()
					.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		if ((arg1 == DialogInterface.BUTTON_POSITIVE)
				&& (dialogListener != null)) {
			ApplyChanges();
			dialogListener.DialogClosed(this, false);
		}

		if ((arg1 == DialogInterface.BUTTON_NEGATIVE)
				&& (dialogListener != null))
			dialogListener.DialogClosed(this, true);
	}
}
