package com.hozm.sphx.helpers;

import com.hozm.sphx.MyApplication;
import com.hozm.sphx.gameobjects.GameLogic;

import android.content.Context;
import android.view.Display;
import android.view.WindowManager;

public class GraphicLayoutHelper {
	public static GraphicLayoutHelper Instance = new GraphicLayoutHelper();

	public int ScreenSizeX;
	public int ScreenSizeY;

	public int HeaderHeight;

	public int LogoHeight;
	public int LogoWidth;
	public int LogoX;
	public int LogoY;
	public int DiscountX;
	public int ProgressX;
	public int ProgressY;

	public int CellSize;
	public int CellsX;
	public int CellsY;
	public int CellsSizeX;
	public int CellsSizeY;

	public int FrameTopHalf1X;
	public int FrameTopHalf1Y;
	public int FrameTopHalf2X;
	public int FrameTopHalf2Y;

	public int FrameBottomHalf1X;
	public int FrameBottomHalf1Y;
	public int FrameBottomHalf2X;
	public int FrameBottomHalf2Y;

	public int FrameLeftHalf1X;
	public int FrameLeftHalf1Y;
	public int FrameLeftHalf2X;
	public int FrameLeftHalf2Y;

	public int FrameRightHalf1X;
	public int FrameRightHalf1Y;
	public int FrameRightHalf2X;
	public int FrameRightHalf2Y;

	public int GetCellsX2() {
		return CellsX + CellsSizeX;
	}

	public int GetCellsY2() {
		return CellsY + CellsSizeY;
	}

	protected GraphicLayoutHelper() {		
	}

	public void CalcSizes() {
		WindowManager wm = (WindowManager) MyApplication.getContext()
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		ScreenSizeX = display.getWidth();
		ScreenSizeY = display.getHeight();

		{
			HeaderHeight = ScreenSizeY / 12;

			LogoHeight = (int) (HeaderHeight * 0.75);
			LogoWidth = LogoHeight * 4;

			LogoX = 5;
			LogoY = (HeaderHeight - LogoHeight) / 2;
		}

		{
			int cellSizeX = ScreenSizeX / (GameLogic.CellsCountX + 1);
			int cellSizeY = (ScreenSizeY - HeaderHeight)
					/ (GameLogic.CellsCountY + 1);
			if (cellSizeX < cellSizeY)
				CellSize = cellSizeX;
			else
				CellSize = cellSizeY;

			CellsSizeX = (CellSize + 1) * GameLogic.CellsCountX;
			CellsSizeY = (CellSize + 1) * GameLogic.CellsCountY;

			CellsX = (ScreenSizeX - CellsSizeX) / 2;
			CellsY = HeaderHeight + CellSize / 2;
		}

		{
			FrameTopHalf1X = CellsX - CellSize / 2;
			FrameTopHalf1Y = CellsY - CellSize / 2;
			FrameTopHalf2X = CellsX + CellsSizeX / 2;
			FrameTopHalf2Y = FrameTopHalf1Y;

			FrameBottomHalf1X = CellsX - CellSize / 2;
			FrameBottomHalf1Y = CellsY + CellsSizeY;
			FrameBottomHalf2X = CellsX + CellsSizeX / 2;
			FrameBottomHalf2Y = FrameBottomHalf1Y;

			FrameLeftHalf1X = CellsX - CellSize / 2;
			FrameLeftHalf1Y = CellsY;
			FrameLeftHalf2X = FrameLeftHalf1X;
			FrameLeftHalf2Y = CellsY + CellsSizeY / 2;

			FrameRightHalf1X = CellsX + CellsSizeX;
			FrameRightHalf1Y = CellsY;
			FrameRightHalf2X = FrameRightHalf1X;
			FrameRightHalf2Y = CellsY + CellsSizeY / 2;
		}

		{
			LogoX = FrameLeftHalf1X;

			ProgressX = FrameRightHalf1X + CellSize / 2;
			ProgressY = (int) (LogoY + LogoHeight * 3.5 / 4);

			DiscountX = (FrameRightHalf1X + GraphicLayoutHelper.Instance.LogoX + LogoWidth) / 2;
		}
	}
}
