package com.hozm.sphx.helpers;

import java.io.InputStream;

import com.hozm.sphx.MyApplication;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapsHelper {
	public static Bitmap Logo;
	public static Bitmap WinScreen;

	public static Bitmap FrameTopHalf1;
	public static Bitmap FrameTopHalf2;
	public static Bitmap FrameBottomHalf1;
	public static Bitmap FrameBottomHalf2;
	public static Bitmap FrameLeftHalf1;
	public static Bitmap FrameLeftHalf2;
	public static Bitmap FrameRightHalf1;
	public static Bitmap FrameRightHalf2;

	public static Bitmap BallBlue;
	public static Bitmap BallYellow;
	public static Bitmap BallGreen;
	public static Bitmap BallRed;

	public static Bitmap BallHoverBlue;
	public static Bitmap BallHoverYellow;
	public static Bitmap BallHoverGreen;
	public static Bitmap BallHoverRed;

	private static Bitmap LoadBitmapFromAssets(String filePath) {
		try {
			AssetManager assetManager = MyApplication.MainActivity.getAssets();
			InputStream istr;
			istr = assetManager.open(filePath);
			Bitmap bitmap = BitmapFactory.decodeStream(istr);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void Init() {
		{
			Logo = LoadBitmapFromAssets("images/logo.png");
			Logo = resizeBitmap(Logo, GraphicLayoutHelper.Instance.LogoWidth,
					GraphicLayoutHelper.Instance.LogoHeight);
		}

		{
			WinScreen = LoadBitmapFromAssets("images/winscreen.png");
			WinScreen = resizeBitmapByWidth(WinScreen,
					GraphicLayoutHelper.Instance.ScreenSizeX / 2);
		}

		{
			FrameTopHalf1 = LoadBitmapFromAssets("images/frame/frame_top_half1.png");
			FrameTopHalf1 = resizeBitmap(
					FrameTopHalf1,
					(GraphicLayoutHelper.Instance.CellsSizeX + GraphicLayoutHelper.Instance.CellSize) / 2f + 1,
					GraphicLayoutHelper.Instance.CellSize / 2f);

			FrameTopHalf2 = LoadBitmapFromAssets("images/frame/frame_top_half2.png");
			FrameTopHalf2 = resizeBitmap(
					FrameTopHalf2,
					(GraphicLayoutHelper.Instance.CellsSizeX + GraphicLayoutHelper.Instance.CellSize) / 2f,
					GraphicLayoutHelper.Instance.CellSize / 2f);
		}

		{
			FrameBottomHalf1 = LoadBitmapFromAssets("images/frame/frame_bottom_half1.png");
			FrameBottomHalf1 = resizeBitmap(
					FrameBottomHalf1,
					(GraphicLayoutHelper.Instance.CellsSizeX + GraphicLayoutHelper.Instance.CellSize) / 2f + 1,
					GraphicLayoutHelper.Instance.CellSize / 2f);

			FrameBottomHalf2 = LoadBitmapFromAssets("images/frame/frame_bottom_half2.png");
			FrameBottomHalf2 = resizeBitmap(
					FrameBottomHalf2,
					(GraphicLayoutHelper.Instance.CellsSizeX + GraphicLayoutHelper.Instance.CellSize) / 2f,
					GraphicLayoutHelper.Instance.CellSize / 2f);
		}

		{
			FrameLeftHalf1 = LoadBitmapFromAssets("images/frame/frame_left_half1.png");
			FrameLeftHalf1 = resizeBitmap(FrameLeftHalf1,
					GraphicLayoutHelper.Instance.CellSize / 2f,
					(GraphicLayoutHelper.Instance.CellsSizeY) / 2f + 1);

			FrameLeftHalf2 = LoadBitmapFromAssets("images/frame/frame_left_half2.png");
			FrameLeftHalf2 = resizeBitmap(FrameLeftHalf2,
					GraphicLayoutHelper.Instance.CellSize / 2f,
					(GraphicLayoutHelper.Instance.CellsSizeY) / 2f);
		}

		{

			FrameRightHalf1 = LoadBitmapFromAssets("images/frame/frame_right_half1.png");
			FrameRightHalf1 = resizeBitmap(FrameRightHalf1,
					GraphicLayoutHelper.Instance.CellSize / 2f,
					(GraphicLayoutHelper.Instance.CellsSizeY) / 2f + 1);

			FrameRightHalf2 = LoadBitmapFromAssets("images/frame/frame_right_half2.png");
			FrameRightHalf2 = resizeBitmap(FrameRightHalf2,
					GraphicLayoutHelper.Instance.CellSize / 2f,
					(GraphicLayoutHelper.Instance.CellsSizeY) / 2f);
		}

		{
			BallBlue = LoadBitmapFromAssets("images/balls/normal/ball_blue.png");
			BallBlue = resizeBitmap(BallBlue,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);

			BallRed = LoadBitmapFromAssets("images/balls/normal/ball_red.png");
			BallRed = resizeBitmap(BallRed,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);

			BallYellow = LoadBitmapFromAssets("images/balls/normal/ball_yellow.png");
			BallYellow = resizeBitmap(BallYellow,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);

			BallGreen = LoadBitmapFromAssets("images/balls/normal/ball_green.png");
			BallGreen = resizeBitmap(BallGreen,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);
		}

		{
			BallHoverBlue = LoadBitmapFromAssets("images/balls/hover/ball_blue.png");
			BallHoverBlue = resizeBitmap(BallHoverBlue,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);

			BallHoverRed = LoadBitmapFromAssets("images/balls/hover/ball_red.png");
			BallHoverRed = resizeBitmap(BallHoverRed,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);

			BallHoverYellow = LoadBitmapFromAssets("images/balls/hover/ball_yellow.png");
			BallHoverYellow = resizeBitmap(BallHoverYellow,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);

			BallHoverGreen = LoadBitmapFromAssets("images/balls/hover/ball_green.png");
			BallHoverGreen = resizeBitmap(BallHoverGreen,
					GraphicLayoutHelper.Instance.CellSize,
					GraphicLayoutHelper.Instance.CellSize);
		}
	}

	private static Bitmap resizeBitmap(Bitmap bitmap, float m) {
		float newSizeX = (bitmap.getWidth() * m);
		float newSizeY = (bitmap.getHeight() * m);

		return Bitmap.createScaledBitmap(bitmap, (int) newSizeX,
				(int) newSizeY, false);
	}

	private static Bitmap resizeBitmapByWidth(Bitmap bitmap, float newWidth) {
		float m = newWidth / bitmap.getWidth();
		return resizeBitmap(bitmap, m);
	}

	@SuppressWarnings("unused")
	private static Bitmap resizeBitmapByHeight(Bitmap bitmap, float newHeight) {
		float m = newHeight / bitmap.getHeight();
		return resizeBitmap(bitmap, m);
	}

	private static Bitmap resizeBitmap(Bitmap bitmap, float newWidth,
			float newHeight) {
		return Bitmap.createScaledBitmap(bitmap, (int) newWidth,
				(int) newHeight, false);
	}
}
