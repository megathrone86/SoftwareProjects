package com.hozm.sphx.helpers;

import java.util.ArrayList;
import java.util.List;

import com.hozm.sphx.MyApplication;
import com.hozm.sphx.gameobjects.Ball;
import com.hozm.sphx.gameobjects.Field;
import com.hozm.sphx.gameobjects.GameLogic;

import android.view.MotionEvent;

public class InputHelper {
	private static final int MinTraceLength = 10;

	public static InputHelper Instance = new InputHelper();

	private List<Trace> traces = new ArrayList<Trace>();

	public void ProcessEvent(MotionEvent event) {
		try {
			if (GameLogic.Instance.GameFinished())
				return;

			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				int index = event.getActionIndex();

				Ball tBall = null;
				{
					float x = event.getX(index)
							- GraphicLayoutHelper.Instance.CellsX;
					float y = event.getY(index)
							- GraphicLayoutHelper.Instance.CellsY;

					tBall = Field.Instance.GetBall(x, y);
					if ((tBall == null) || (tBall.MovingInfo.getMoving()))
						return;
					tBall.HoverInfo.setHovered(true);
				}

				Trace trace = new Trace(event.getPointerId(index));
				trace.SelectedBall = tBall;
				trace.X1 = (int) event.getX(index);
				trace.Y1 = (int) event.getY(index);
				traces.add(trace);
			}

			if (event.getAction() == MotionEvent.ACTION_UP) {
				int index = event.getActionIndex();
				Trace trace = getTraceByPointerId(event.getPointerId(index));
				trace.X2 = (int) event.getX(index);
				trace.Y2 = (int) event.getY(index);
				traces.remove(trace);

				processTrace(trace);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void processTrace(Trace trace) {
		Ball selectedBall = trace.SelectedBall;

		boolean moveRecognized = false;
		int cx = selectedBall.X, cy = selectedBall.Y;
		{
			float dx = trace.X2 - trace.X1;
			float dy = trace.Y2 - trace.Y1;
			// ������� ��� )
			if (dx == 0)
				dx = 0.01f;
			if (dy == 0)
				dy = 0.01f;

			if ((Math.abs(dx) >= MinTraceLength)
					|| (Math.abs(dy) >= MinTraceLength)) {

				float f = Math.abs(dx / dy);
				if (f < 0.8) { // dx<dy
					if (dy > 0)
						cy = selectedBall.Y + 1;
					else
						cy = selectedBall.Y - 1;
					moveRecognized = true;
				}
				if (f > 1.25) { // dx>dy
					if (dx > 0)
						cx = selectedBall.X + 1;
					else
						cx = selectedBall.X - 1;
					moveRecognized = true;
				}

				// �������� �� ����� �� ������� ����
				if ((cx < 0) || (cx >= GameLogic.CellsCountX) || (cy < 0)
						|| (cy >= GameLogic.CellsCountY)) {
					cx = selectedBall.X;
					cy = selectedBall.Y;
					moveRecognized = false;
				}
			}
		}

		if (moveRecognized) {
			// �������� ����������
			Ball tBall = Field.Instance.GetBall(cx, cy);
			if (tBall != null) {
				// � �������� ����� �������� ����� ������ ���
				selectedBall.HoverInfo.setHovered(false);

				// TODO: ������� �������� ��� ��� ��� ������ ��
				// ������ � ����������� �������
			} else {
				// �������� ����� �������� �����
				// ����� ������� ���
				selectedBall.MovingInfo.AnimationStart = System
						.currentTimeMillis();
				selectedBall.MovingInfo.setMoving(true);
				selectedBall.MovingInfo.NewBallX = cx;
				selectedBall.MovingInfo.NewBallY = cy;
			}
		} else {
			// �������� �� ����������. ������� ���
			selectedBall.HoverInfo.setHovered(false);
		}

		MyApplication.MainActivity.Refresh();
	}

	private class Trace {
		public int Id;
		public int X1;
		public int Y1;
		public int X2;
		public int Y2;

		public Ball SelectedBall;

		public Trace(int id) {
			Id = id;
		}
	}

	private Trace getTraceByPointerId(int pointerId) {
		if (pointerId < 0)
			return null;

		for (int i = 0; i < traces.size(); i++) {
			Trace trace = traces.get(i);
			if (trace.Id == pointerId) {
				return trace;
			}
		}

		return null;
	}
}
