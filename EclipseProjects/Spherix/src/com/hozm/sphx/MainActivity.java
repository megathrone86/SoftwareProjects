package com.hozm.sphx;

import java.util.Timer;
import java.util.TimerTask;

import com.hozm.sphx.gameobjects.Field;
import com.hozm.sphx.gameobjects.GameLogic;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.app.Activity;

public class MainActivity extends Activity implements DialogListener {
	private static int MainTimerInterval = 35;

	private Timer _timer;
	private GraphicsView _myview;

	public MainActivity() {
		MyApplication.MainActivity = this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		_myview = new GraphicsView(this);
		setContentView(_myview);

		InputStringDialog dlg = new InputStringDialog(this);
		dlg.SetDialogListener(this);
		dlg.ShowDialog("������� �������������� ���������");
	}

	public void Refresh() {
		// ���� ������������� �� ���������
		// �� ����� ������ ������
		if (!MyApplication.Started)
			return;

		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				_myview.invalidate();
			}
		});
	}

	// ������������� ���������
	public void ApplicationStarted() {
		Refresh();
	}

	private void startTimer() {
		_timer = new Timer();
		_timer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					if (!MyApplication.Started || (Field.Instance == null))
						return;

					if (GameLogic.Instance
							.AddMillisecondsToCount(MainTimerInterval)) {
						Refresh();
						return;
					}

					if ((Field.Instance.MovingBalls <= 0)
							&& (Field.Instance.HoveredBalls <= 0))
						return;

					Refresh();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}, MainTimerInterval, MainTimerInterval);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();

		_timer.cancel();
	}

	@Override
	public void DialogClosed(Object sender, boolean cancelled) {
		MyApplication.Start();
		startTimer();
	}
}
