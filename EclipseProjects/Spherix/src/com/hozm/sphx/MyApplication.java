package com.hozm.sphx;

import com.hozm.sphx.gameobjects.*;
import com.hozm.sphx.helpers.BitmapsHelper;
import com.hozm.sphx.helpers.GraphicLayoutHelper;

import android.content.Context;

public class MyApplication {
	public static MainActivity MainActivity;

	public static Context getContext() {
		if (MainActivity == null)
			return null;
		return MainActivity.getApplicationContext();
	}

	public static boolean Started = false;

	public static void Start() {		
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					StartThreaded();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		t.start();
	}

	private static void StartThreaded() {
		GraphicLayoutHelper.Instance.CalcSizes();
		BitmapsHelper.Init();
		CreateField();
		GameLogic.Instance.RecalcBalls();
		GameLogic.Instance.StartCount();

		Started = true;		
		MainActivity.ApplicationStarted();
	}

	public static void CreateField() {
		Field.Instance = new Field();
		Field.Instance.Fill();
	}
}
