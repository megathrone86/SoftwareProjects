﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Diagnostics;

namespace NetCoreHelpers
{
    public class ActionValidator
    {
        public static ActionValidator Instance = new ActionValidator();

        public TimeSpan MinRequestTimeSpan { get; set; } = TimeSpan.FromSeconds(1);

        Dictionary<string, DateTime> log = new Dictionary<string, DateTime>();

        public bool Validate(IPAddress remoteIpAddress)
        {
            ClearOld();

            var address = remoteIpAddress.ToString();
            try
            {
                if (log.ContainsKey(address))
                {
                    var lastTime = log[address];
                    var diff = DateTime.Now - lastTime;
                    var r = diff > MinRequestTimeSpan;
                    if (!r)
                        Debug.WriteLine($"Слишком частые запросы от клиента {address}: интервал равен {diff}");
                    return r;
                } else
                    return true;
            } finally
            {
                log[address] = DateTime.Now;
            }
        }

        private void ClearOld()
        {
            var ts = TimeSpan.FromHours(1);
            var list = log.Where(l => DateTime.Now - l.Value > ts).ToList();
            list.ForEach(l => log.Remove(l.Key));
        }
    }
}
