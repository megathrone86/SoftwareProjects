﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using System.Text;
using Microsoft.Extensions.FileProviders;

namespace NetCoreHelpers
{
    public class ImagesInserter
    {
        public static IHtmlContent InsertImages(string dir, Func<string, string> insertAction)
        {
            StringBuilder result = new StringBuilder();

            var provider = new PhysicalFileProvider(FileHelper.AppRoot);
            var contents = provider.GetDirectoryContents(dir.Replace("~", ""));
            var files = contents.OrderBy(f => f.Name).ToList();

            foreach (var file in files)
            {
                if (!file.IsDirectory || IsImage(file.Name) && insertAction != null)
                {
                    string fileWebPath = dir + '/' + file.Name;
                    var imgCode = insertAction?.Invoke(fileWebPath);
                    result.AppendLine(imgCode);
                }
            }

            return new HtmlString(result.ToString());
        }

        private static bool IsImage(string file)
        {
            string ext = Path.GetExtension(file).ToLower();
            return ext == ".jpg" || ext == ".jpeg" || ext == ".jfif" || ext == ".png";
        }
    }
}