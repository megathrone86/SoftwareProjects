﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreHelpers
{
    public class FileHelper
    {
        public static string LocalFolderName { get { return "LocalFilesDB"; } }
        public static string LocalFolderPath { get { return AppRoot + "\\" + LocalFolderName; } }

        static string appRoot = null;
        public static string AppRoot
        {
            get
            {
                if (appRoot == null)
                    throw new Exception("NetCoreHelpers.FileHelper is not initialized. Call Init() on the app starting.");
                return appRoot;
            }
            private set
            {
                appRoot = value;
            }
        }

        public static void Init(IHostingEnvironment env)
        {
            AppRoot = env.WebRootPath;
        }
    }
}