﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NetCoreHelpers
{
    public class CaptchaHelper
    {
        public static string SecretKey { get; set; }
        const string serviceUri = @"https://www.google.com/recaptcha/api/siteverify";

        public bool IsValid { get; private set; }

        public CaptchaHelper(string response)
        {
            IsValid = false;
            ValidateRecaptcha(response).Wait();
        }

        private async Task ValidateRecaptcha(string response)
        {
            using (var webClient = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("secret", SecretKey),
                    new KeyValuePair<string, string>("response", response)
                });
                HttpResponseMessage serverResponse = await webClient.PostAsync(serviceUri, content);
                string json = await serverResponse.Content.ReadAsStringAsync();
                var reCaptchaResponse = JsonConvert.DeserializeObject<ReCaptchaResponse>(json);
                if (reCaptchaResponse != null && reCaptchaResponse.success)
                    IsValid = true;
            }
        }

        public class ReCaptchaResponse
        {
            public bool success { get; set; }
            public string challenge_ts { get; set; }
            public string hostname { get; set; }
            public string[] errorcodes { get; set; }
        }
    }
}
