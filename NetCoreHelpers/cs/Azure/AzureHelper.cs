﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreHelpers.Azure
{
    public class AzureHelper
    {
        const char Separator = ';';

        public static AzureId ConvertId(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            var ts = id.Split(Separator);

            if (ts.Length != 2 || string.IsNullOrEmpty(ts[0]) || string.IsNullOrEmpty(ts[1]))
                throw new ArgumentException();

            return new AzureId() { PartitionKey = ts[0], RowKey = ts[1] };
        }

        public static string ConvertId(string partitionKey, string rowKey)
        {
            if (string.IsNullOrEmpty(partitionKey) || string.IsNullOrEmpty(rowKey))
                throw new ArgumentException();

            if (partitionKey.Contains(Separator) || rowKey.Contains(Separator))
                throw new ArgumentException();
            return $"{partitionKey}{Separator}{rowKey}";
        }

        public static T SetIdIfNotNull<T>(T r, AzureId id) where T : BaseEntity<T>
        {
            if (id != null)
            {
                r.PartitionKey = id.PartitionKey;
                r.RowKey = id.RowKey;
            }

            return r;
        }

        public static CloudTable GetOrCreateTable(CloudTableClient client, string tableName)
        {
            var result = client.GetTableReference(tableName);
            result.CreateIfNotExistsAsync().Wait();
            return result;
        }
    }

    public class AzureId
    {
        public string PartitionKey { get; internal set; }
        public string RowKey { get; internal set; }
    }
}