﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreHelpers.Azure
{
    public class BaseEntity<T> : TableEntity where T : class
    {
        public bool IsDeleted { get; set; }

        protected virtual string DefaultPartitionKey { get { return "0"; } }

        public BaseEntity()
        {
            PartitionKey = DefaultPartitionKey;
            Timestamp = DateTime.Now;
            GenerateRowKey();
        }

        public BaseEntity<T> GenerateRowKey()
        {
            RowKey = Guid.NewGuid().ToString("N");
            return this as BaseEntity<T>;
        }
    }
}
