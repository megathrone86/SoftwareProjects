﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace parser.Helpers
{
    public class WebHelper
    {
        public static HttpWebRequest CreateRequest(string url, CookieContainer container)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ProtocolVersion = HttpVersion.Version10;
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.1.4322; .NET CLR 2.0.40607)";

            if (container != null)
                request.CookieContainer = container;
            return request;
        }

        public static void SetRequestData(HttpWebRequest request, string requestData)
        {
            byte[] postBytes = Encoding.UTF8.GetBytes(requestData);
            request.ContentLength = postBytes.Length;

            StreamWriter sw = new StreamWriter(request.GetRequestStream());
            sw.Write(requestData);
            sw.Flush();
            sw.Close();
        }

        public static byte[] ReadFromResponse(HttpWebResponse response)
        {
            Stream resStream = response.GetResponseStream();
            return readFromStream(resStream);
        }

        public static byte[] readFromStream(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, read);
                return ms.ToArray();
            }
        }

        public static string GetHeadersFromResponse(HttpWebResponse response)
        {
            string result = "";

            foreach (string key in response.Headers.AllKeys)
            {
                result += key + " = " + response.Headers[key] + "\r\n";
            }

            return result;
        }

        public static void WriteResponseInfo(HttpWebResponse response, string filePath)
        {
            WriteStatus(response, filePath + ".status.txt");
            WriteHeaders(response, filePath + ".headers.txt");
            WriteResponseBody(response, filePath + ".body.dat");
        }

        public static void WriteStatus(HttpWebResponse response, string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);
            File.WriteAllText(filePath, response.StatusCode.ToString());
        }

        public static void WriteHeaders(HttpWebResponse response, string filePath)
        {
            string headers = WebHelper.GetHeadersFromResponse(response);
            if (File.Exists(filePath))
                File.Delete(filePath);
            File.WriteAllText(filePath, headers);
        }

        public static void WriteResponseBody(HttpWebResponse response, string filePath)
        {
            byte[] buf = WebHelper.ReadFromResponse(response);
            if (File.Exists(filePath))
                File.Delete(filePath);
            File.WriteAllBytes(filePath, buf);
        }
    }
}
