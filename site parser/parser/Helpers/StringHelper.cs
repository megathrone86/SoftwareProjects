﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace parser.Helpers
{
    public class StringHelper
    {
        public static string DecodeString(string s)
        {
            string result = "";

            for (int i = 0; i < s.Length; i++)
            {
                if ((s[i] == '\\') && (s[i + 1] == 'u'))
                {
                    string hex_code = string.Concat(s[i + 2], s[i + 3], s[i + 4], s[i + 5]);
                    int a = int.Parse(hex_code, NumberStyles.AllowHexSpecifier);
                    result += (char)a;

                    i += 5;
                }
                else
                    result += s[i];
            }

            return result;
        }

        public static string EncodeString(string s)
        {
            string result = "";

            for (int i = 0; i < s.Length; i++)
            {
                int a = (int)s[i];
                string hex_code = a.ToString("x4");
                result += @"\u" + hex_code;
            }

            return result;
        }
    }
}
