﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace parser.Helpers
{
    public class ConfigHelper
    {
        public static ConfigHelper Instance = new ConfigHelper();

        public string LastRequest;

        public bool DoSleep;

        protected string _filePath
        {
            get
            {
                return Path.Combine(DirectoryHelper.CurrentDir, "config.cfg");
            }
        }

        public void Load()
        {
            LastRequest = "";
            DoSleep = true;

            try
            {
                string[] lines = File.ReadAllLines(_filePath);
                LastRequest = lines[0];
                DoSleep = lines[0].Trim().ToLower() == "true";
            }
            catch
            {
            }
        }

        public void Save()
        {
            try
            {
                string fileBody = "";
                fileBody += LastRequest + "\r\n";
                fileBody += DoSleep.ToString() + "\r\n";

                File.WriteAllText(_filePath, fileBody);
            }
            catch
            {
            }
        }
    }
}
