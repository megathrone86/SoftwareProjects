﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using parser.Helpers;
using System.IO;
using System.Threading;

namespace parser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            panel1.Left = 2;
            panel1.Top = 2;

            webBrowser1.Left = 2;
            webBrowser1.Top = panel1.Bottom + 2;
            webBrowser1.Width = ClientSize.Width - 4;
            webBrowser1.Height = ClientSize.Height - webBrowser1.Top - 2;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Form1_Resize(null, null);

            ConfigHelper.Instance.Load();

            textBox1.Text = ConfigHelper.Instance.LastRequest;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigHelper.Instance.LastRequest = textBox1.Text.Trim();
            ConfigHelper.Instance.Save();

            try
            {
                progressBar1.Value = 0;
                progressBar1.Visible = true;

                try
                {
                    TotalParser.Instance.StartReadAllSites(ConfigHelper.Instance.LastRequest);

                    while (true)
                    {
                        Thread.Sleep(10);

                        int currentProgress = TotalParser.Instance.GetCurrentProgress();
                        progressBar1.Value = currentProgress;

                        Application.DoEvents();

                        if (currentProgress == 100)
                            break;
                    }
                }
                finally
                {
                    progressBar1.Visible = false;
                }

                string tempResultPath = Path.Combine(DirectoryHelper.TempDir, "temp_result.htm");
                File.WriteAllText(tempResultPath, TotalParser.Instance.GetResult(
                    ConfigHelper.Instance.LastRequest));

                webBrowser1.Navigate(tempResultPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла ошибка: " + ex.Message +
                    "\r\n" + ex.StackTrace.ToString());
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            ConfigHelper.Instance.Save();
        }
    }
}