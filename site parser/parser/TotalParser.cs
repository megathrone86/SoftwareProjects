﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

using parser.Helpers;
using parser.Parsers;

namespace parser
{
    public class TotalParser
    {
        public static TotalParser Instance = new TotalParser();

        protected ParserArmtek _parserArmtek;
        protected ParserForumAuto _parserForumAuto;

        protected TotalParser()
        {
            _parserArmtek = new ParserArmtek()
            {
                Name = "shop.armtek.ru",
                AuthUrl = "http://shop.armtek.ru/authorization",
                DisconnectUrl = "http://shop.armtek.ru/authorization/logout",
                SelectModeUrl = "http://shop.armtek.ru/setvkorg",
                RequestUrl = "http://shop.armtek.ru/search",
                Login = "bkspare.parts@gmail.com",
                Password = "fyjdsyrji"
            };

            _parserForumAuto = new ParserForumAuto()
            {
                Name = "forum-auto.ru",
                AuthUrl = "http://itrade.forum-auto.ru/shop/index.html",
                RequestUrl = "http://itrade.forum-auto.ru/shop/index.html",
                DisconnectUrl = "http://itrade.forum-auto.ru/shop/index.html",
                Login = "bkspare.parts@gmail.com",
                Password = "d1c5286f64cd8884"
            };
        }

        public static readonly string MainFontStart = "<u><font color=\"blue\" size=\"5px\">";
        public static readonly string MainFontEnd = "</font></u>";

        public static readonly string FontStart = "<font color=\"blue\">";
        public static readonly string FontEnd = "</font>";

        public void StartReadAllSites(string partNumber)
        {
            if (_parserArmtek!=null)
                _parserArmtek.SearchAsync(partNumber);
            
            if (_parserForumAuto!=null)
                _parserForumAuto.SearchAsync(partNumber);            
        }

        public int GetCurrentProgress()
        {
            int n = 0;
            int div = 0;

            {
                if (_parserArmtek != null)
                {
                    n += _parserArmtek.Progress;
                    div++;
                }

                if (_parserForumAuto != null)
                {
                    n += _parserForumAuto.Progress;
                    div++;
                }
            }

            if (div > 0)
                return n / div;
            else
                return 0;
        }

        public string GetResult(string partNumber)
        {
            StringBuilder sbResult = new StringBuilder();

            sbResult.Append("<meta http-equiv=\"Content-Type\" " +
             "content=\"text/html; charset=utf-8\" />\r\n");

            sbResult.Append("<body>\r\n");
            sbResult.Append("<p>Поиск по словосочетанию \"<b>" + partNumber + "</b>\" (запущен " +
                DateTime.Now.ToString() + "):</p>\r\n");

            if (_parserArmtek!=null)
            {
                sbResult.Append(string.Format(
                    "<br>\r\n{1}Результаты с сайта {0}{2}\r\n",
                    _parserArmtek.Name, MainFontStart, MainFontEnd));
                sbResult.Append("<p>");
                sbResult.Append(_parserArmtek.SearchResult);
                sbResult.Append("</p>\r\n");
            }

            if (_parserForumAuto!=null)
            {
                sbResult.Append(string.Format(
                    "<br>\r\n{1}Результаты с сайта {0}{2}\r\n",
                    _parserForumAuto.Name, MainFontStart, MainFontEnd));
                sbResult.Append("<p>");
                sbResult.Append(_parserForumAuto.SearchResult);
                sbResult.Append("</p>\r\n");
            }

            sbResult.Append("</body>");

            return sbResult.ToString();
        }
    }
}
