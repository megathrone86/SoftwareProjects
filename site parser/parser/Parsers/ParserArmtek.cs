﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using parser.Helpers;
using System.Net;
using System.Threading;

namespace parser.Parsers
{
    public class ParserArmtek : BasicParser
    {
        public string SelectModeUrl;

        public override void AddHeadersToRequest(HttpWebRequest request)
        {
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.Accept = "application/json";
        }

        public override string GetAuthRequestData()
        {
            return String.Format("login={0}&pass={1}",
                    EncodeValue(Login), EncodeValue(Password));
        }

        public override string GetSearchRequestData(string partNumber)
        {
            return String.Format("searchQuery={0}&searchType=0", EncodeValue(partNumber));
        }

        public override void GetResult(string partNumber)
        {
            if (!Directory.Exists(DirectoryHelper.GetTempFileName(Name)))
                Directory.CreateDirectory(DirectoryHelper.GetTempFileName(Name));

            //легковая
            {
                Authorize("response1");
                Progress = 5;
                GetResults(partNumber, "4000", "response2", "response3");
                Progress = 35;
                Logoff("response4");
                Progress = 45;
            }

            //грузовая
            {
                Authorize("response5");
                Progress = 55;
                GetResults(partNumber, "5000", "response6", "response7");
                Progress = 85;
                Logoff("response8");
                Progress = 95;
            }

            SearchResult.Append(string.Format(
                    "<p>{0}Легковая программа:{1}</p>\r\n",
                    TotalParser.FontStart, TotalParser.FontEnd));
            ParseResult(DirectoryHelper.GetTempFileName(Name,
                "response3.body.dat"), SearchResult);

            SearchResult.Append(string.Format(
                    "<p>{0}Грузовая программа:{1}</p>\r\n",
                    TotalParser.FontStart, TotalParser.FontEnd));
            ParseResult(DirectoryHelper.GetTempFileName(Name,
                "response7.body.dat"), SearchResult);

            Progress = 100;
        }

        //4000 - легковая программа г. Москва
        //5000 - грузовая программа г. Москва
        protected void GetResults(string partNumber, string vkorg, string name1, string name2)
        {
            if (ConfigHelper.Instance.DoSleep)
                Thread.Sleep(100 + RandomHelper.Random.Next(1000));

            //выбор режима
            {
                HttpWebRequest request = CreateRequest(SelectModeUrl);                
                WebHelper.SetRequestData(request, "VKORG=" + vkorg);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                WebHelper.WriteResponseInfo(response, DirectoryHelper.GetTempFileName(
                    Name, name1));
            }

            if (ConfigHelper.Instance.DoSleep)
                Thread.Sleep(100 + RandomHelper.Random.Next(1000));

            Search(name2, partNumber);            
        }

        protected static void ParseResult(string fileName, StringBuilder sbResult)
        {
            string fileText = File.ReadAllText(fileName, Encoding.GetEncoding(1251));

            int pos = fileText.IndexOf("Показывать на странице:");
            if (pos < 0)
                throw new Exception("Ошибка при разборе результатов");

            pos = fileText.IndexOf("<table", pos);
            if (pos < 0)
                throw new Exception("Ошибка при разборе результатов");

            int pos2 = fileText.IndexOf("</table>", pos+1) + 
                "</table>".Length;
            if (pos2 < 0)
                throw new Exception("Ошибка при разборе результатов");

            string s = fileText.Substring(pos, pos2 - pos);
            sbResult.Append(s);            
        }
    }
}
