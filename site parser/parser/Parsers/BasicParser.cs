﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using parser.Helpers;

namespace parser.Parsers
{
    public abstract class BasicParser
    {
        public string Name;
        public string AuthUrl;
        public string DisconnectUrl;

        public string RequestUrl;
        public string Login;
        public string Password;

        public DateTime LastConnect;

        protected CookieContainer _container = new CookieContainer();

        protected virtual string EncodeValue(string s)
        {
            string result = s.Replace("@", "%40");
            return result;
        }

        public virtual void AddHeadersToRequest(HttpWebRequest request)
        {
        }

        public virtual void AddHeadersToConnectRequest(HttpWebRequest request)
        {            
        }        

        public virtual string GetAuthRequestData()
        {
            return null;
        }

        public abstract string GetSearchRequestData(string partNumber);

        public virtual string GetDisconnectRequestData()
        {
            return null;
        }

        public abstract void GetResult(string partNumber);

        public virtual HttpWebRequest CreateRequest(string url)
        {
            HttpWebRequest request = WebHelper.CreateRequest(url, _container);
            AddHeadersToRequest(request);
            return request;
        }

        public virtual void Authorize(string logName)
        {
            HttpWebRequest request = CreateRequest(AuthUrl);
            AddHeadersToConnectRequest(request);
            {
                string data = GetAuthRequestData();
                if (!string.IsNullOrEmpty(data))
                    WebHelper.SetRequestData(request, data);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            WebHelper.WriteResponseInfo(response, DirectoryHelper.GetTempFileName(
                Name, logName));
        }

        public virtual void Logoff(string logName)
        {
            HttpWebRequest request = CreateRequest(DisconnectUrl);
            string data = GetDisconnectRequestData();
            if (!string.IsNullOrEmpty(data))
                WebHelper.SetRequestData(request, data);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            WebHelper.WriteResponseInfo(response, DirectoryHelper.GetTempFileName(
                Name, logName));
        }

        public virtual void Search(string logName, string partNumber)
        {
            HttpWebRequest request = CreateRequest(RequestUrl);
            string data = GetSearchRequestData(partNumber);
            if (!string.IsNullOrEmpty(data))
                WebHelper.SetRequestData(request, data);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            WebHelper.WriteResponseInfo(response, DirectoryHelper.GetTempFileName(
                Name, logName));
        }

        protected string _partNumber;
        public StringBuilder SearchResult;
        public int Progress;

        public static string DeleteTagsFromString(string s, string tagName)
        {
            while (true)
            {
                int n = s.IndexOf("<" + tagName);
                if (n < 0)
                    return s;

                int n2 = s.IndexOf('>', n);
                if (n2 < 0)
                    return s;

                s = s.Substring(0, n) + s.Substring(n2 + 1);
            }
        }

        public void SearchAsync(string partNumber)
        {
            _partNumber = partNumber;
            Progress = 0;
            SearchResult = new StringBuilder();

            Thread t = new Thread(new ThreadStart(SearchThreaded));
            t.Start();
        }

        protected void SearchThreaded()
        {
            try
            {
                GetResult(_partNumber);
            }
            catch (Exception ex)
            {
                SearchResult.Append("<br>Ошибка: " + ex.Message + "<br>\r\n" +
                    ex.StackTrace.ToString().Replace("\r\n", "<br>"));
                Progress = 100;                
            }
        }
    }
}
