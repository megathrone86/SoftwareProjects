﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using parser.Helpers;
using System.Net;
using System.Threading;

namespace parser.Parsers
{
    public class ParserForumAuto : BasicParser
    {
        public override void AddHeadersToRequest(HttpWebRequest request)
        {
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.Headers.Add("Accept-Language", "ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Headers.Add("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.7");
            request.KeepAlive = true;
            request.Referer = "http://itrade.forum-auto.ru/shop/index.html";
            request.ContentType = "application/x-www-form-urlencoded";
        }

        public override string GetAuthRequestData()
        {
            return String.Format("login={0}&password={1}&enter=%C2%EE%E9%F2%E8",
                EncodeValue(Login), EncodeValue(Password));
        }

        public override string GetSearchRequestData(string partNumber)
        {
            string like = "off";
            if (partNumber.Length >= 4)
                like = "on";

            return String.Format("jspt=0&cat_num={0}&search=%CD%E0%E9%F2%E8&like={1}&br_num=",
                EncodeValue(partNumber), EncodeValue(like));
        }

        public override string GetDisconnectRequestData()
        {
            return String.Format("exit=%C2%FB%E9%F2%E8");
        }

        public override void GetResult(string partNumber)
        {
            if (!Directory.Exists(DirectoryHelper.GetTempFileName(Name)))
                Directory.CreateDirectory(DirectoryHelper.GetTempFileName(Name));

            //легковая
            {
                Authorize("response1");
                Progress = 15;
                GetResults(partNumber, "response2");
                Progress = 80;
                Logoff("response3");
                Progress = 95;
            }

            //грузовая    
            {
            }

            ParseResult(DirectoryHelper.GetTempFileName(Name,
                "response2.body.dat"), SearchResult);

            Progress = 100;
        }

        protected void GetResults(string partNumber, string name)
        {
            if (ConfigHelper.Instance.DoSleep)
                Thread.Sleep(100 + RandomHelper.Random.Next(1000));

            Search(name, partNumber);
        }

        protected static void ParseResult(string fileName, StringBuilder sbResult)
        {
            string fileText = File.ReadAllText(fileName, Encoding.GetEncoding(1251));

            if (fileText.Contains("К сожалению, товара с таким артикулом в каталоге нет."))
            {
                sbResult.Append("К сожалению, товара с таким артикулом в каталоге нет.");
                return;
            }

            int pos = fileText.IndexOf(
                "<table border=0 cellpadding=0 cellspacing=0 width=100% id=\"search\" name=\"tab\">");
            if (pos < 0)
                throw new Exception("Ошибка при разборе результатов");

            int pos2 = fileText.IndexOf("</table>", pos + 1) +
                "</table>".Length;
            if (pos2 < 0)
                throw new Exception("Ошибка при разборе результатов");

            string s = fileText.Substring(pos, pos2 - pos);

            s = DeleteTagsFromString(s, "input");
            s = DeleteTagsFromString(s, "img");
            s = s.Replace("<th class=th2 width=50px align=right>Кол-во</th>", "");

            sbResult.Append(s);
        }
    }
}
