﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using parser.Helpers;

namespace parser
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //DirectoryHelper.ClearTempDir();

            Application.Run(new Form1());
        }
    }
}
