<!-- NOTE: If you need to make changes to this file, copy it to your current theme's main
	directory so your changes won't be overwritten when the plugin is upgraded. -->

<!-- Start of Post Wrap -->
<div class="post hentry ivycat-post">
	<!-- This is the output of the post TITLE -->
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

	<!-- This is the output of the EXCERPT -->
	<div class="entry-summary">
		<?php the_excerpt(50); ?>
	</div>

	<!-- This is the output of the META information -->
	<div class="entry-utility">
		<span class="comments-link"><?php comments_popup_link( __( 'Комментировать', 'twentyten' ), __( '1 комментарий', 'twentyten' ), __( '% комментариев', 'twentyten' ) ); ?></span>
		<?php edit_post_link( __( 'Изменить', 'twentyten' ), '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
	</div>
</div>
<!-- // End of Post Wrap -->
