﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace MainService.Helpers
{
    public class DirectoryHelper
    {
        public static readonly string CurrentDir =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public static string TempDir { get { return Path.Combine(CurrentDir, "Temp"); } }

        public static void ClearTempDir()
        {
            try
            {
                if (Directory.Exists(TempDir))
                    Directory.Delete(TempDir, true);
                Directory.CreateDirectory(TempDir);
            }
            catch { }
        }

        public static string GetTempFileName(string fileName)
        {
            return Path.Combine(DirectoryHelper.TempDir, fileName);
        }

        public static string GetTempFileName(string subdir, string fileName)
        {
            return Path.Combine(DirectoryHelper.TempDir, Path.Combine(subdir, fileName));
        }
    }
}
