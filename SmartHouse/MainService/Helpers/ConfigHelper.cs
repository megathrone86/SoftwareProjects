﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MainService.Helpers
{
    public class ConfigHelper
    {
        public static ConfigHelper Instance = new ConfigHelper();

        protected const string _configName = "config.xml";
        
        protected ConfigHelper()
        {
        }

        public void Load()
        {
            string configPath = Path.Combine(DirectoryHelper.CurrentDir, _configName);

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(configPath);

            }
            catch (Exception ex)
            {
                ExceptionHelper.ShowError("Произошла ошибка при загрузке конфига", ex);
            }
        }
    }
}
