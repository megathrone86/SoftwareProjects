﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MainService.Helpers
{
    public class ExceptionHelper
    {
        public static string GetExceptionDescription(Exception ex)
        {
            string result = "=============\r\nТип исключения: " + ex.GetType().Name + "\r\nСообщение: " +
                ex.Message + "\r\nТрассировка стека:\r\n" + ex.StackTrace.ToString();

            if (ex.InnerException != null)
                result = result + "\r\n\r\n" +
                    GetExceptionDescription(ex.InnerException);

            return result;
        }

        public static void ShowError(string message, Exception ex)
        {
            MessageBox.Show(message + "\r\n" + GetExceptionDescription(ex));
        }
    }
}
