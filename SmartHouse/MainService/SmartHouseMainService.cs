﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MainService.Helpers;

namespace MainService
{
    public class SmartHouseMainService
    {
        public static SmartHouseMainService Instance = new SmartHouseMainService();

        public void Start()
        {
            ConfigHelper.Instance.Load();
        }
    }
}
