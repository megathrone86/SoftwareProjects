﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace VKMusicCacheViewer {
    //C:\Users\khozeev.vladimir\AppData\Local\Packages\C6965DD5.VK_v422avzh127ra\LocalState\audios
    public class VKCacheHelper {
        public struct MusicFileInfo {
            public string SongName;
            public string ArtistName;
            public string FullPath;
            public DateTime Date;
            public int Duration;

            public override string ToString() {
                return $"{ArtistName} - {SongName}";
            }
        }

        [JsonObject(MemberSerialization.OptIn)]
        struct JsonMusicFileInfo {
            [JsonProperty("id")]
            public string id { get; set; }

            [JsonProperty("artist")]
            public string artist { get; set; }

            [JsonProperty("title")]
            public string title { get; set; }

            [JsonProperty("duration")]
            public int duration { get; set; }

            [JsonProperty("date")]
            public string date { get; set; }
        }

        public static VKCacheHelper Instance = new VKCacheHelper();

        public string GetMusicDir() {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string packages = Path.Combine(appData, "Packages");
            var dirs = Directory.GetDirectories(packages).ToList<string>();
            string vkDir = dirs.Find(s => Path.GetFileName(s).StartsWith("C6965DD5.VK_"));
            if(string.IsNullOrEmpty(vkDir)) {
                throw new Exception("Не найден каталог приложения VK.");
            }
            string musicDir = Path.Combine(vkDir, "LocalState", "audios");
            return musicDir;
        }

        public List<MusicFileInfo> GetFiles(string suffix) {
            string musicDir = GetMusicDir();
            if(string.IsNullOrEmpty(musicDir))
                throw new Exception("Не определен путь к кэшу.");
            if(!Directory.Exists(musicDir))
                throw new Exception("Папка с кэшем не существует.");

            List<string> infos = new List<string>(Directory.GetFiles(musicDir, "*" + suffix));
            var result = new List<MusicFileInfo>(infos.Count);
            foreach(string file in infos) {
                result.Add(GetMusicFileInfo(file, suffix));
            }
            result.Sort((x, y) => { return -x.Date.CompareTo(y.Date); });

            return result;
        }

        private MusicFileInfo GetMusicFileInfo(string file, string suffix) {
            FileInfo fi = new FileInfo(file);

            string js = File.ReadAllText(file);
            JsonMusicFileInfo obj = JsonConvert.DeserializeObject<JsonMusicFileInfo>(js);
            return new MusicFileInfo() {
                ArtistName = obj.artist,
                FullPath = file.Substring(0, file.Length - suffix.Length),
                SongName = obj.title,
                Duration = obj.duration,
                Date = fi.LastWriteTime
            };
        }

        private DateTime? TryParseDate(string date) {
            long millisec;
            if(!long.TryParse(date, out millisec))
                return null;
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-GB");
            DateTime dt = DateTime.Parse("1970,1,1", ci);
            dt = dt.AddSeconds(millisec);
            return dt;
        }
    }
}