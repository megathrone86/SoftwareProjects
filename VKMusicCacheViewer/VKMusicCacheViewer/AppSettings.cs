﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace VKMusicCacheViewer {

    [JsonObject(MemberSerialization.OptIn)]
    public class AppSettings {
        private const string FileName = "config.json";

        [JsonProperty("ExportDirs")]
        public List<string> ExportDirs { get; set; } = new List<string>();

        public static AppSettings Instance { get; private set; }

        public AppSettings() {
        }

        public static void Load() {
            if(File.Exists(FileName)) {
                try {
                    string js = File.ReadAllText(FileName);
                    Instance = JsonConvert.DeserializeObject<AppSettings>(js);
                } catch(Exception ex) {
                    Console.WriteLine(ex);
                    Instance = new AppSettings();
                    MessageBox.Show(ex.Message, "Ошибка загрузки конфига");
                }
            } else {
                Instance = new AppSettings();
            }
        }

        public static void Save() {
            string js = JsonConvert.SerializeObject(Instance);
            File.WriteAllText(FileName, js);
        }

        public void AddExportDir(string dir) {
            ExportDirs.RemoveAll(d => d == dir);
            ExportDirs.Add(dir);

            if(ExportDirs.Count > 10)
                ExportDirs.RemoveRange(0, ExportDirs.Count - 10);

            Save();
        }
    }
}
