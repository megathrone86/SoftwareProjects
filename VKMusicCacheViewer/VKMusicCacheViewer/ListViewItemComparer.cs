﻿using System;
using System.Collections;

namespace VKMusicCacheViewer {
    internal class ListViewItemComparer : IComparer {
        private int column;

        public ListViewItemComparer(int column) {
            this.column = column;
        }

        public int Compare(object x, object y) {
            if(x == null && y == null)
                return 0;
            if(x == null && y != null)
                return 1;
            if(x != null && y == null)
                return -1;
            return x.ToString().CompareTo(y.ToString());
        }
    }
}