﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VKMusicCacheViewer {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            listView1.Columns.Add("Artist");
            listView1.Columns.Add("Track");
            listView1.Columns.Add("Creation Date");
            listView1.Columns.Add("Duration");

            OnSizeChanged();
            RefreshItems();
        }

        public void RefreshItems() {
            try {
                listView1.Items.Clear();

                var rawItems = VKCacheHelper.Instance.GetFiles("_info");

                List<ListViewItem> items = new List<ListViewItem>(rawItems.Count);
                foreach (var rawItem in rawItems) {
                    ListViewItem item = new ListViewItem(new string[] {
                        rawItem.ArtistName,
                        rawItem.SongName,
                        rawItem.Date.ToString(),
                        rawItem.Duration.ToString()
                    }) {
                        Tag = rawItem
                    };
                    items.Add(item);
                }

                listView1.Items.AddRange(items.ToArray());
            } catch (Exception ex) {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Ошибка при обращении к кэшу");
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            RefreshItems();
        }

        private void Form1_SizeChanged(object sender, EventArgs e) {
            OnSizeChanged();
        }

        void OnSizeChanged() {
            button1.Left = ClientSize.Width - button1.Width - 5;
            button2.Left = button1.Left - button2.Width - 5;
            button3.Left = button2.Left - button3.Width - 5;
            listView1.Width = button1.Right - listView1.Left;
            listView1.Height = ClientSize.Height - listView1.Top - 5;

            int colWidth = listView1.Width / listView1.Columns.Count - 20;
            if (colWidth < 50)
                colWidth = 50;

            for (int i = 0; i < listView1.Columns.Count; i++) {
                listView1.Columns[i].Width = colWidth;
            }
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e) {
            listView1.ListViewItemSorter = new ListViewItemComparer(e.Column);
            listView1.Sort();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete) {
                if (listView1.SelectedItems.Count > 0 && MessageBox.Show("Вы уверены, что хотите удалить выбранные файлы?", "", MessageBoxButtons.OKCancel) == DialogResult.OK) {
                    DeleteFiles(listView1.SelectedItems);
                }
            }
            if (e.KeyCode == Keys.Enter) {
                if (listView1.SelectedItems.Count >= 1) {
                    var rawItem = GetMusicFileInfo(listView1.SelectedItems[0]);
                    RunSong(rawItem);
                }
            }
        }

        private void DeleteFiles(ListView.SelectedListViewItemCollection items) {
            try {
                foreach (var item in items) {
                    var rawItem = GetMusicFileInfo(item);
                    File.Delete(rawItem.FullPath);
                    File.Delete(rawItem.FullPath + "_info");
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Ошибка удаления");
            } finally {
                RefreshItems();
            }
        }

        private VKCacheHelper.MusicFileInfo GetMusicFileInfo(object o) {
            return (VKCacheHelper.MusicFileInfo)(o as ListViewItem).Tag;
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e) {
            if (listView1.SelectedItems.Count == 1) {
                var rawItem = GetMusicFileInfo(listView1.SelectedItems[0]);
                RunSong(rawItem);
            }
        }

        string GetSafePath(string path) {
            var invalidChars = Path.GetInvalidFileNameChars();
            var sb = new StringBuilder();
            foreach (char c in path) {
                if (!invalidChars.Contains(c))
                    sb.Append(c);
            }
            var result = sb.ToString();
            if (result.Length > 200)
                result = result.Substring(0, 200);
            return result;
        }

        private void RunSong(VKCacheHelper.MusicFileInfo rawItem) {
            try {
                string tempDir = Path.GetTempPath();
                string tempFilePath = Path.Combine(tempDir, GetSafePath(rawItem.ToString() + ".mp3"));
                File.Copy(rawItem.FullPath, tempFilePath, true);
                Process.Start(tempFilePath);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Ошибка воспроизведения");
            }
        }

        private void ExportFiles(ListView.SelectedListViewItemCollection items, string selectedPath) {
            AppSettings.Instance.AddExportDir(selectedPath);

            try {
                foreach (var item in items) {
                    var rawItem = GetMusicFileInfo(item);
                    string filePath = Path.Combine(selectedPath, GetSafePath(rawItem.ToString() + ".mp3"));
                    try {
                        File.Copy(rawItem.FullPath, filePath, true);
                    } catch (Exception ex) {
                        Console.WriteLine(ex);
                        MessageBox.Show(filePath + "\r\n" + ex.Message, "Ошибка сохранения.");
                    }
                }

                MessageBox.Show("Файлы успешно сохранены.");
            } catch (Exception ex) {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Ошибка сохранения");
            }
        }

        private void button2_MouseUp(object sender, MouseEventArgs e) {
            if (listView1.SelectedItems.Count > 0) {
                if (e.Button == MouseButtons.Left ||
                    e.Button == MouseButtons.Right && AppSettings.Instance.ExportDirs.Count <= 0) {
                    FolderBrowserDialog dlg = new FolderBrowserDialog();
                    if (dlg.ShowDialog() != DialogResult.OK)
                        return;

                    ExportFiles(listView1.SelectedItems, dlg.SelectedPath);
                } else {
                    ContextMenu menu = new ContextMenu();
                    foreach (string dir in AppSettings.Instance.ExportDirs) {
                        menu.MenuItems.Add($"Экспортировать в {dir}", (s, a) => { ExportFiles(listView1.SelectedItems, dir); });
                    }
                    menu.Show(button2, new Point(0, 0));
                }
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            var rawItem = GetMusicFileInfo(listView1.SelectedItems[0]);
            RunSong(rawItem);
        }
    }
}
