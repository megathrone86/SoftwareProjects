package ru.korpse.autoservice.core.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.korpse.autoservice.entities.Message;
import ru.korpse.autoservice.entities.Request;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.enums.MessageDirection;
import ru.korpse.autoservice.utils.IntegrationTest;

public class MessageDaoTest extends IntegrationTest {

	@Autowired
	private MessageDao dao;
	@Autowired
	private WorkflowDao workflowDao;
	@Autowired
	private UserDao userDao;

	@Test
	public void attachMessageTest() {
		Message message0 = new Message();
		message0.setDirection(MessageDirection.OPERATOR_TO_USER);
		message0.setContent("content0");

		Message message1 = new Message();
		message1.setDirection(MessageDirection.USER_TO_OPERATOR);
		message1.setContent("content1");

		Message message2 = new Message();
		message2.setDirection(MessageDirection.USER_TO_OPERATOR);
		message2.setContent("content2");

		Workflow workflow = new Workflow();
		Request request = new Request();
		request.setUser("userid");
		workflow.setRequest(new Request());

		User user = userDao.createByMid("000");

		workflowDao.save(workflow, user);

		dao.attachMessage(message0, workflow.getId().getId());
		dao.attachMessage(message1, workflow.getId().getId());
		dao.attachMessage(message2, workflow.getId().getId());

		List<Message> messages = dao.getMessages(workflow.getId().getId());
		assertNotNull(messages);

		workflowDao.delete(workflow);
		
		userDao.delete(user);

	}
}
