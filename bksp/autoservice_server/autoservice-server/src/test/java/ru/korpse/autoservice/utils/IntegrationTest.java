package ru.korpse.autoservice.utils;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:beans.xml")
public abstract class IntegrationTest extends Assert {

	private final LocalServiceTestHelper helper;
	
	public IntegrationTest() {
		LocalDatastoreServiceTestConfig config = new LocalDatastoreServiceTestConfig();
		config.setDefaultHighRepJobPolicyUnappliedJobPercentage(20);
		helper = new LocalServiceTestHelper(config);
	}

	@Before
	public void setUp() {
		helper.setUp();
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}
}
