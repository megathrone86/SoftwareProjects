package ru.korpse.autoservice.core.dao;

import java.io.IOException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.korpse.autoservice.core.helper.OAuthServiceHelper;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.dto.UserInfo;
import ru.korpse.autoservice.utils.IntegrationTest;
import ru.korpse.autoservice.utils.ObjectMapper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class UserDaoTest extends IntegrationTest {

	@Autowired
	private UserDao dao;

	@Autowired
	private ObjectMapper mapper;

	@Test
	public void createByResponseIfNotExistsTest()
			throws JsonParseException, JsonMappingException, IOException {
		UserInfo userInfo = new UserInfo();
		userInfo.setUid("205387401");
		userInfo.setName("Tom Cruise");
		userInfo.setProvider(OAuthServiceHelper.VK);
		dao.createByUserInfoIfNotExists(userInfo);
		User user = dao.get("205387401");
		assertEquals("205387401", user.getMid());
		assertEquals("Tom Cruise", user.getName());
		assertEquals(OAuthServiceHelper.VK, user.getProvider());
		dao.delete(user);
	}

}
