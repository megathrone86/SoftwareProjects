package ru.korpse.autoservice.core.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.korpse.autoservice.entities.Request;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.dto.Spare;
import ru.korpse.autoservice.entities.enums.WorkflowStatus;
import ru.korpse.autoservice.utils.IntegrationTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.repackaged.com.google.common.collect.Lists;

public class WorkflowDaoTest extends IntegrationTest {

	@Autowired
	private WorkflowDao dao;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserDao userDao;

	@Test
	public void testSaveGetDelete() throws JsonGenerationException,
			JsonMappingException, IOException {
		Workflow workflow = new Workflow();
		Request request = new Request();
		request.setUser("userid");
		workflow.setDateOpen(DateTime.now(DateTimeZone.forID(System
				.getProperty("dateTimeZoneId"))));

		User user = userDao.createByMid("000");

		dao.save(workflow, user);
		objectMapper.writeValueAsString(workflow);
		Workflow workflow1 = dao.get(workflow.getId().getId());
		assertEquals(workflow, workflow1);

		dao.delete(workflow);
		userDao.delete(user);
	}

	@Test
	public void testQuerying() throws InterruptedException {
		Workflow workflow0 = new Workflow();
		workflow0.setStatus(WorkflowStatus.SENT);
		Workflow workflow1 = new Workflow();
		workflow1.setStatus(WorkflowStatus.SENT);
		Workflow workflow2 = new Workflow();
		workflow2.setStatus(WorkflowStatus.SENT);
		workflow2.setClosed(true);
		Workflow workflow3 = new Workflow();
		workflow3.setStatus(WorkflowStatus.NOT_SENT);

		User user = userDao.createByMid("000");

		dao.save(workflow0, user);
		dao.save(workflow1, user);
		dao.save(workflow2, user);
		dao.save(workflow3, user);

		List<Workflow> result = dao.getActive();
		assertNotNull(result);

		dao.delete(workflow0);
		dao.delete(workflow1);
		dao.delete(workflow2);
		dao.delete(workflow3);

		userDao.delete(user);
	}

	@Test
	public void getByUserIdTest() {

		Workflow workflow0 = new Workflow();
		Workflow workflow1 = new Workflow();
		Workflow workflow2 = new Workflow();
		Workflow workflow3 = new Workflow();

		User user0 = userDao.createByMid("000");
		User user1 = userDao.createByMid("001");

		dao.save(workflow0, user0);
		dao.save(workflow1, user0);
		dao.save(workflow2, user0);
		dao.save(workflow3, user1);

		List<Workflow> result = dao.getByUserId("000");
		assertTrue(result.size() > 0);

		dao.delete(workflow0);
		dao.delete(workflow1);
		dao.delete(workflow2);
		dao.delete(workflow3);

		userDao.delete(user0);
		userDao.delete(user1);
	}

	@Test
	public void testDeleteSpares() {
		Workflow workflow = new Workflow();
		workflow.setSparesMap(new HashMap<String, List<Spare>>());
		workflow.getSparesMap().put(
				"part000",
				Lists.newArrayList(new Spare[] {
							new Spare("spareName000", DateTime.now(), BigDecimal.ONE, "part000", 2, false),
							new Spare("spareName001", DateTime.now(), BigDecimal.ONE, "part000", 3, false)
						}));
		User user0 = userDao.createByMid("000");
		dao.save(workflow, user0);
		Map<String, List<Integer>> spareIdMap = new HashMap<String, List<Integer>>();
		spareIdMap.put("part000", Lists.newArrayList(new Integer[] {0}));
		dao.removeSpares(workflow.getId().getId(), spareIdMap);
		Workflow workflowDest = dao.get(workflow.getId().getId());
		assertEquals(1, workflowDest.getSparesMap().get("part000").size());
	}
	
	@Test
	public void testSelectSpares() {
		Workflow workflow = new Workflow();
		workflow.setSparesMap(new HashMap<String, List<Spare>>());
		workflow.getSparesMap().put(
				"part000",
				Lists.newArrayList(new Spare[] {
							new Spare("spareName000", DateTime.now(), BigDecimal.ONE, "part000", 2, false)
						}));
		workflow.getSparesMap().put(
				"part001",
				Lists.newArrayList(new Spare[] {
						new Spare("spareName001", DateTime.now(), BigDecimal.ONE, "part001", 3, false)
					}));
		User user0 = userDao.createByMid("000");
		dao.save(workflow, user0);
		Map<String, List<Integer>> spareIdMap = new HashMap<String, List<Integer>>();
		spareIdMap.put("part001", Lists.newArrayList(new Integer[] {0}));
		dao.selectSpares(workflow.getId().getId(), spareIdMap);
		Workflow workflowDest = dao.get(workflow.getId().getId());
		assertTrue(workflowDest.getSparesMap().get("part001").get(0).isSelected());
	}
}