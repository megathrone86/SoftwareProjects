package ru.korpse.autoservice.controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import lombok.extern.log4j.Log4j;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import ru.korpse.autoservice.core.controllers.WorkflowController;
import ru.korpse.autoservice.core.dao.MessageDao;
import ru.korpse.autoservice.core.dao.WorkflowDao;
import ru.korpse.autoservice.core.service.AuthenticationService;
import ru.korpse.autoservice.core.service.CheckWorkflowException;
import ru.korpse.autoservice.core.service.WorkflowCheckService;
import ru.korpse.autoservice.entities.Message;
import ru.korpse.autoservice.entities.Request;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.dto.Spare;
import ru.korpse.autoservice.entities.enums.MessageDirection;
import ru.korpse.autoservice.entities.enums.UserRole;
import ru.korpse.autoservice.utils.IntegrationTest;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Sets;

@Log4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class WorkflowControllerTest extends IntegrationTest {

	@Autowired
	private WorkflowController workflowController;

	@Autowired
	private WorkflowDao workflowDao;

	@Autowired
	private MessageDao messageDao;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private WorkflowController controller;
	
	@Test
	public void attachMessageUserFailTest() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_USER));
		when(authenticationService.getCurrentUser()).thenReturn(user);

		Workflow workflow = new Workflow();
		workflow.setId(KeyFactory.createKey(Workflow.class.getSimpleName(), 1));
		workflow.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), user.getMid()));
		when(workflowDao.get(1)).thenReturn(workflow);

		{ //юзер отправляет сообщение от имени оператора
			Message message = new Message();
			message.setDirection(MessageDirection.OPERATOR_TO_USER);
	
			controller.attachMessage(message, 1l);
			verify(messageDao, never()).attachMessage(any(Message.class), any(Long.class));
		}
		
		{ //юзер отправляет сообщение с запчастями
			Message message = new Message();
			message.setDirection(MessageDirection.USER_TO_OPERATOR);
			List<Spare> spares = new ArrayList<Spare>();
			spares.add(new Spare());
			message.setSpares(spares);
	
			controller.attachMessage(message, 1l);
			verify(messageDao, never()).attachMessage(any(Message.class), any(Long.class));
		}
		
		{ //юзер отправляет сообщение без текста
			Message message = new Message();
			message.setDirection(MessageDirection.USER_TO_OPERATOR);
	
			controller.attachMessage(message, 1l);
			verify(messageDao, never()).attachMessage(any(Message.class), any(Long.class));
		}
	}
	
	@Test
	public void attachMessageUserTest1() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_USER));
		when(authenticationService.getCurrentUser()).thenReturn(user);

		Workflow workflow = new Workflow();
		workflow.setId(KeyFactory.createKey(Workflow.class.getSimpleName(), 1));
		workflow.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), user.getMid()));
		when(workflowDao.get(1)).thenReturn(workflow);

		//юзер отправляет сообщение от себя, список запчастей пуст
		Message message = new Message();
		message.setDirection(MessageDirection.USER_TO_OPERATOR);
		message.setContent("fuck");
		List<Spare> spares = new ArrayList<Spare>();
		message.setSpares(spares);

		controller.attachMessage(message, 1l);
		verify(messageDao).attachMessage(eq(message), eq(1l));
	}
	
	@Test
	public void attachMessageUserTest2() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_USER));
		when(authenticationService.getCurrentUser()).thenReturn(user);

		Workflow workflow = new Workflow();
		workflow.setId(KeyFactory.createKey(Workflow.class.getSimpleName(), 1));
		workflow.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), user.getMid()));
		when(workflowDao.get(1)).thenReturn(workflow);

		//юзер отправляет сообщение от себя, список запчастей = null
		Message message = new Message();
		message.setDirection(MessageDirection.USER_TO_OPERATOR);
		message.setContent("fuck");

		controller.attachMessage(message, 1l);
		verify(messageDao).attachMessage(eq(message), eq(1l));
	}

	@Test
	public void attachMessageOperatorTest1() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_OPERATOR));
		when(authenticationService.getCurrentUser()).thenReturn(user);

		Workflow workflow = new Workflow();
		workflow.setId(KeyFactory.createKey(Workflow.class.getSimpleName(), 1));
		workflow.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), user.getMid()));
		when(workflowDao.get(1)).thenReturn(workflow);

		{ //оператор отправляет сообщение с запчастями
			Message message = new Message();
			message.setDirection(MessageDirection.OPERATOR_TO_USER);
			message.setContent("fuck");
			List<Spare> spares = new ArrayList<Spare>();
			spares.add(new Spare());
			message.setSpares(spares);
	
			controller.attachMessage(message, 1l);
			verify(messageDao).attachMessage(eq(message), eq(1l));
		}
	}
	
	@Test
	public void attachMessageOperatorTest2() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_OPERATOR));
		when(authenticationService.getCurrentUser()).thenReturn(user);

		Workflow workflow = new Workflow();
		workflow.setId(KeyFactory.createKey(Workflow.class.getSimpleName(), 1));
		workflow.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), user.getMid()));
		when(workflowDao.get(1)).thenReturn(workflow);

		{ //оператор отправляет сообщение без запчастей
			Message message = new Message();
			message.setDirection(MessageDirection.OPERATOR_TO_USER);
			message.setContent("fuck");
	
			controller.attachMessage(message, 1l);
			verify(messageDao).attachMessage(eq(message), eq(1l));
		}
	}

	@Test
	public void attachMessageFailTest() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_USER));
		when(authenticationService.getCurrentUser()).thenReturn(user);

		Workflow workflow = new Workflow();
		workflow.setId(KeyFactory.createKey(Workflow.class.getSimpleName(), 1));
		workflow.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), "useridbad"));
		when(workflowDao.get(1)).thenReturn(workflow);

		Message message = new Message();

		controller.attachMessage(message, 1l);
		verify(messageDao, never()).attachMessage(any(Message.class), any(Long.class));
	}

	private Request createValidRequest() {
		Request request = new Request();
		request.setVinNumber("TESTVIN123");
		request.setBodyNumber("TESTBODYNUMBER456");
		request.setCarYear(1999);
		request.setCarBrandName("DeLorean");
		request.setCarModel("DMC-12");
		request.setCarEngineModel("V6 2849");
		request.setContactName("Эмметт Браун");
		request.setContactAddress("USA, Valley Hill");
		request.setContactPhone("9203331914");

		ArrayList<String> partNames = new ArrayList<String>();
		partNames.add("Энергетический флюктуатор");
		request.setPartNames(partNames);

		return request;
	}

	private User createTestUser() {
		User user = new User();
		user.setMid("user_id000");
		user.setName("user000");
		user.setRoles(Sets.newHashSet(UserRole.ROLE_USER));
		return user;
	}

	@Test
	public void saveTest() {
		User user = createTestUser();
		when(authenticationService.getCurrentUser()).thenReturn(user);

		try {
			Workflow workflow = new Workflow();
			workflow.setRequest(null);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "request");
		}

		try {
			Workflow workflow = new Workflow();
			workflow.setRequest(new Request());
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 10);
		}

		try {
			Workflow workflow = new Workflow();
			workflow.setRequest(createValidRequest());
			controller.save(workflow);
			verify(workflowDao).save(refEq(workflow), refEq(user));
		} catch (CheckWorkflowException ex) {
			ex.printCheckResult();
			fail("Didn't expected an exception to be thrown");
		}
	}

	@Test
	public void saveVinAndBodyTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setVinNumber("asd");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "vinNumber");
		}

		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setBodyNumber("asd");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "bodyNumber");
		}

		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setVinNumber("");
			request.setBodyNumber("");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 2);
			assertEquals(ex.getCheckResult().get(0)[0], "vinNumber");
			assertEquals(ex.getCheckResult().get(1)[0], "bodyNumber");
		}
	}

	@Test
	public void saveCarYearTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setCarYear(666);
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "carYear");
		}
	}

	@Test
	public void saveCarBrandNameTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setCarBrandName("");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "carBrandName");
		}
	}

	@Test
	public void saveCarModelTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setCarModel("");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "carModel");
		}
	}

	@Test
	public void saveCarEngineModelTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setCarEngineModel("");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "carEngineModel");
		}
	}

	@Test
	public void saveContactNameTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setContactName("");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "contactName");
		}
	}
	
	@Test
	public void saveContactAddressTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setContactAddress("");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "contactAddress");
		}
	}

	@Test
	public void saveContactPhoneTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setContactPhone("11");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "contactPhone");
		}
	}

	@Test
	public void saveEmailTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setContactEmail("fuck you");
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "contactEmail");
		}
	}

	@Test
	public void savePartNamesTest() {
		try {
			Workflow workflow = new Workflow();
			Request request = createValidRequest();
			request.setPartNames(null);
			workflow.setRequest(request);
			controller.save(workflow);
			fail("Expected an exception to be thrown");
		} catch (CheckWorkflowException ex) {
			assertEquals(ex.getCheckResult().size(), 1);
			assertEquals(ex.getCheckResult().get(0)[0], "partNames");
		}
	}

	@Configuration
	static class Context {

		@Bean
		public AuthenticationService authenticationService() {
			return mock(AuthenticationService.class);
		}

		@Bean
		public MessageDao messageDao() {
			return mock(MessageDao.class);
		}

		@Bean
		public WorkflowDao workflowDao() {
			return mock(WorkflowDao.class);
		}

		@Bean
		public WorkflowController workflowController() {
			return new WorkflowController();
		}
	}
}