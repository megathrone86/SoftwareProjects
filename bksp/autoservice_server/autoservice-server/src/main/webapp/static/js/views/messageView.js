/**
 * Message view
 */
(function () {
    
    app.MessageView = Backbone.View.extend({
        tagName: "div",
        
        template: _.template( $('#message-template').html() ),
        
        events: {
        },
        
        initialize: function () {
            this.listenTo(this, 'change', this.render);
        },
        
        render: function () {
            this.$el.html( this.template( this.model.toJSON() ) );
            this.$el.addClass("row");
            return this;
        },

    });
    
})();
