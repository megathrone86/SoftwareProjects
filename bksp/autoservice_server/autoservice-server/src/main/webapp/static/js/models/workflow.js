/**
 * Workflow model
 */
(function() {

	app.Workflow = Backbone.Model.extend({
		idAttribute: "id.id",
		urlRoot : GLOBAL_URL + "/workflow/",
		url : function () {
			if (this.get("id")) {
				return this.urlRoot + this.get("id").id;
			}
			return this.urlRoot;
		},
		defaults : {
			request : null,
			closed : false,
			dateOpen : 0,
			sparesMap : {}
		},
		initialize : function() {
		},

		// возвращает список неправильных полей в формате {name,
		// message}
		validate : function(attrs, options) {
			var request = this.get("request");
			var fields = [];

			// vinNumber, bodyNumber
			if (!request.vinNumber && !request.bodyNumber) {
				fields.push({
					name : "vinNumber",
					message : "Нужно указать VIN или номер кузова."
				});
				fields.push({
					name : "bodyNumber",
					message : "Нужно указать VIN или номер кузова."
				});
			} else {
				if (request.vinNumber && !/^.{10,20}$/.test(request.vinNumber)) {
					fields.push({
						name : "vinNumber",
						message : "Номер VIN состоит из латинских символов и цифр и имеет длину от 10 до 20 символов."
					});
				}

				if (request.bodyNumber && !/^.{10,20}$/.test(request.bodyNumber)) {
					fields.push({
						name : "bodyNumber",
						message : "Номер кузова состоит из латинских символов и цифр и имеет длину от 10 до 20 символов."
					});
				}
			}

			// carYear
			if (request.carYear) {
				var carYear = parseInt(request.carYear);
				if (isNaN(carYear) || carYear < 1900 || carYear > new Date().getFullYear()) {
					fields.push({
						name : "carYear",
						message : "Некорректный год выпуска."
					});
				}
			} else {
				fields.push({
					name : "carYear",
					message : "Это обязательное поле."
				});
			}

			// carBrandName
			if (request.carBrandName) {
				if (request.carBrandName.length > 150) {
					fields.push({
						name : "carBrandName",
						message : "Слишком длинная строка."
					});
				}
			} else {
				fields.push({
					name : "carBrandName",
					message : "Это обязательное поле."
				});
			}

			// carModel
			if (request.carModel) {
				if (request.carModel.length > 100) {
					fields.push({
						name : "carModel",
						message : "Слишком длинная строка."
					});
				}
			} else {
				fields.push({
					name : "carModel",
					message : "Это обязательное поле."
				});
			}

			// carEngineModel
			if (request.carEngineModel) {
				if (request.carEngineModel.length > 100) {
					fields.push({
						name : "carEngineModel",
						message : "Слишком длинная строка."
					});
				}
			} else {
				fields.push({
					name : "carEngineModel",
					message : "Это обязательное поле."
				});
			}

			// carBodyType, carFuelType, carDriveType, carGearboxType - не проверяем

			// contactName
			if (request.contactName) {
				if (request.contactName.length > 100) {
					fields.push({
						name : "contactName",
						message : "Слишком длинная строка."
					});
				}
			} else {
				fields.push({
					name : "contactName",
					message : "Это обязательное поле."
				});
			}
			
			// contactAddress
			if (request.contactAddress) {
				if (request.contactAddress.length > 200) {
					fields.push({
						name : "contactAddress",
						message : "Слишком длинная строка."
					});
				}
			} else {
				fields.push({
					name : "contactAddress",
					message : "Это обязательное поле."
				});
			}

			// contactPhone
			if (request.contactPhone) {
				if (!/^\d{8,12}$/.test(request.contactPhone)) {
					fields.push({
						name : "contactPhone",
						message : "Неправильно указан контактный телефон."
					});
				}
			} else {
				fields.push({
					name : "contactPhone",
					message : "Это обязательное поле."
				});
			}

			// contactEmail
			if (request.contactEmail && !/^\S+@\S+\.\S+$/.test(request.contactEmail)) {
				fields.push({
					name : "contactEmail",
					message : "Неправильно указан email."
				});
			}

			// description
			if (request.description && request.description.length > 150) {
				fields.push({
					name : "description",
					message : "Слишком длинная строка."
				});
			}

			// partNames
			if (!request.partNames) {
				fields.push({
					name : "partNames",
					message : "Нужно указать хотя бы одну запчасть."
				});
			} else {
				var ok = false;
				for (i in request.partNames) {
					if (jQuery.trim(request.partNames[i]).length > 0) {
						ok = true;
						break;
					}
				}

				if (!ok) {
					fields.push({
						name : "partNames",
						message : "Нужно указать хотя бы одно наименование детали."
					});
				}
			}

			if (fields.length)
				return fields;
		}
	});
}());
