/**
 * Workflows collection
 */

(function () {
    app.Messages = Backbone.Collection.extend({
        model: app.Message,
        workflowId: {},
        url: function () { return GLOBAL_URL + "/workflow/" + app.selectedWorkflowId + "/messages/"; },
        comparator: function (message) {
        	return -message.get("date");
        }
    });
}());