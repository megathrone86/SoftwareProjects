/**
 * Message model
 */
(function () {
    
    app.Message = Backbone.Model.extend({
    	idAttribute: "id.id",
        urlRoot: function () { return GLOBAL_URL + "/workflow/" + app.selectedWorkflowId + "/messages/"; },
        defaults: {
            direction: "",
            content: "",
            date: 0
        },
        initialize: function () {

        },
        validate: function(attrs, options) {
        	if ((!attrs.spares || attrs.spares.length == 0)
        			&& (!attrs.content || $.trim(attrs.content).length == 0)) {
        		console.log("message validation: error");
        		return "message";
        	}
        	console.log("message validation: OK");
        }
    });
}());
