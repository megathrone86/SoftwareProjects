/**
 * Workflows view
 */
(function() {

	app.WorkflowsView = Backbone.View.extend({
		el : "#workflows",

		events : {
			"submit #workflowAddForm" : "submit",
			"reset #workflowAddForm" : "reset"
		},

		model : app.workflows,

		initialize : function() {
			this.listenTo(this.model, "sync", this.render);
			//this.listenTo(this.model, "add", this.render);
		},

		render : function() {
			var $el = this.$el.children("#workflowsList");
			$el.children(".workflowItem").remove();
			this.model.each(function(item) {
				var view = new app.WorkflowView({
					model : item
				});
				$el.append(view.render().el);
			});

			return this;
		},

		reset : function(e) {
			var divs = this.$('#workflowAddForm .form-group');
			divs.removeClass('has-error');
			var labels = this.$('#workflowAddForm .control-label');
			labels.hide();
		},

		submit : function(e) {
			e.preventDefault();

			var workflowsView = this;

			var request = Backbone.Syphon.serialize(this);
			request.partNames = request.partNames.split("\n");
			var newWorkflow = new app.Workflow({
				request : request,
				status : "NOT_SENT",
				dateOpen: moment.tz("Europe/Moscow").valueOf()
			});
			newWorkflow.on("invalid", function(model, error) {
				workflowsView.invalid(model, error);
			});

			newWorkflow.save(null, {
				success : function(model, response) {
					app.workflows.add(newWorkflow);
					workflowsView.$('#workflowAddForm')[0].reset();
					$('#workflowAdd').modal('hide');
					return false;
				},
				error : function(model, response) {
					console.log("error");
				}
			});
		},

		invalid : function(model, response) {
			var workflowsView = this;

			this.reset();

			response.forEach(function(entry) {
				var input = workflowsView.$('#' + entry.name);
				var div = input.closest('.form-group');
				div.removeClass('has-success').addClass('has-error');

				var label = div.find('.control-label');
				label.text(entry.message);
				label.show();
			});
		}
	});
})();
