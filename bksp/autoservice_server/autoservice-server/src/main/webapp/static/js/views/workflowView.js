/**
 * Workflow view
 */
(function() {

	app.WorkflowView = Backbone.View.extend({
		tagName : "a",

		template : _.template($('#workflow-template').html()),

		requestTemplate : _.template($("#request-template").html()),

		events : {
			"click" : "click",
			"click #sendWorkflow" : "doSend"
		},

		initialize : function() {
			this.listenTo(this.model, "change", this.render);
		},

		render : function() {
			this.$el.attr("href", "#");
			this.$el.addClass("list-group-item workflowItem");
			this.$el.html(this.template(this.model.toJSON()));
			if (app.selectedWorkflowId && this.model.get("id").id == app.selectedWorkflowId) {
				this.$el.addClass("active");
			}
			return this;
		},
		
		click : function () {
			$(".workflowItem").removeClass("active");
			app.selectedWorkflowId = this.model.get("id").id;
			this.showDetails();
		},

		showDetails : function() {
			var jsonModel = this.model.toJSON();
			$("#requestInfo").off("needToUpdate");
			var workflowView = this;
			$("#requestInfo").on("needToUpdate", function () {
				workflowView.model.fetch({
					success : function () {
						workflowView.showDetails();
					},
					error : function () {
						console.log("workflow fetch error");
					}
				});
			});
			$("#requestInfo").html(this.requestTemplate(jsonModel));
			this.$el.addClass("active");
			$("#messages").removeClass("hidden");
			if (this.model.get("status") != "SENT") {
				$("#addPartsButton").addClass("hidden");
			} else {
				$("#addPartsButton").removeClass("hidden");
			}
			app.sourceNames = jsonModel.request.partNames;
			app.messages = new app.Messages();
			app.messages.fetch();
			if (app.messagesView) {
				app.messagesView.stopListening();
				app.messagesView.undelegateEvents();
			}
			app.messagesView = new app.MessagesView({
				model : app.messages
			});
			
			//spare list editor
			(function () {
				
				var workflowSparesToDelete = {};
			
				$(".spareDeleteBtn").on("click", function () {
					var num = $(this).data("num");
					var original = $(this).data("original");
					if (!workflowSparesToDelete[original] || workflowSparesToDelete[original].indexOf(num) < 0) {
						if (!workflowSparesToDelete[original]) {
							workflowSparesToDelete[original] = [];
						}
						workflowSparesToDelete[original].push(num);
						$(this).parents("tr.spareRow").addClass("danger");
						$(this).html('<span class="glyphicon glyphicon-arrow-left"></span>')
						$(this).removeClass("btn-danger");
						$(this).addClass("btn-default");
					} else {
						workflowSparesToDelete[original].splice(workflowSparesToDelete[original].indexOf(num), 1);
						if (workflowSparesToDelete[original].length == 0) {
							delete workflowSparesToDelete[original];
						}
						$(this).parents("tr.spareRow").removeClass("danger");
						$(this).html('<span class="glyphicon glyphicon-remove"></span>')
						$(this).removeClass("btn-default");
						$(this).addClass("btn-danger");
					}
					if (_.size(workflowSparesToDelete) > 0) {
						$("#deleteSparesBtn").removeClass("hidden");
					} else {
						$("#deleteSparesBtn").addClass("hidden");
					}
				});
				
				$("#deleteSparesBtn").on("click", function () {
					if (_.size(workflowSparesToDelete) > 0) {
						$.ajax({
							url : GLOBAL_URL + "/workflow/" + app.selectedWorkflowId + "/removeSpares",
							data: JSON.stringify(workflowSparesToDelete),
					        contentType: "application/json; charset=utf-8",
					        dataType: "json",
							type : "POST",
							success : function(data, textStatus, xhr) {
								console.log("success");
								$("#wfSparesTable tbody tr.spareRow.danger").remove();
								workflowView.model.fetch({
									success : function () {
										workflowView.showDetails();
									},
									error : function () {
										console.log("workflow fetch error");
									}
								});
							}
						});
					}
				});
			})();
			
			//spares selector
			(function () {
				var workflowSparesToSelect = {};
				
				for (var original in jsonModel.sparesMap) {
					for (var num in jsonModel.sparesMap[original]) {
						if (jsonModel.sparesMap[original][num].selected) {
							if (!workflowSparesToSelect[original]) {
								workflowSparesToSelect[original] = [];
							}
							workflowSparesToSelect[original].push(parseInt(num));
						}
					}
				}
				
				$(".spareCheck").on("change", function () {
					var num = $(this).data("num");
					var original = $(this).data("original");
					if (!workflowSparesToSelect[original] || workflowSparesToSelect[original].indexOf(num) < 0
							&& $(this).is(":checked")) {
						if (!workflowSparesToSelect[original]) {
							workflowSparesToSelect[original] = [];
						}
						workflowSparesToSelect[original].push(num);
						$(this).parents("tr.spareRow").addClass("success");
						workflowView.model.get("sparesMap")[original][num].selected = true;
					} else {
						workflowSparesToSelect[original].splice(workflowSparesToSelect[original].indexOf(num), 1);
						if (workflowSparesToSelect[original].length == 0) {
							delete workflowSparesToSelect[original];
						}
						$(this).parents("tr.spareRow").removeClass("success");
						workflowView.model.get("sparesMap")[original][num].selected = false;
					}
					$("#saveSelectedSparesBtn").removeClass("dontshow");
				});
				
				$("#saveSelectedSparesBtn").on("click", function () {
					$.ajax({
						url : GLOBAL_URL + "/workflow/" + app.selectedWorkflowId + "/selectSpares",
						data: JSON.stringify(workflowSparesToSelect),
				        contentType: "application/json; charset=utf-8",
				        dataType: "json",
						type : "POST",
						success : function(data, textStatus, xhr) {
							console.log("success");
							if (_.size(workflowSparesToSelect) > 0) {
								workflowView.model.set("status", "PROCESSING");
							}
							view.showDetails();
						}
					});
				});
			})();
			
			var view = this;
			$("#submitList").on("click", function () {
				view.doApplySpareList();
			});
			$("#rollbackList").on("click", function () {
				view.doRollbackSpareList();
			});
		},
		
		doApplySpareList : function() {
			var view = this;
			var model = this.model;
			$.ajax({
				url : GLOBAL_URL + "/workflow/" + model.get("id").id + "/submit",
				type : "POST",
				success : function(data, textStatus, xhr) {
					console.log("success");
					model.set("status", "FEEDBACK");
					view.showDetails();
				}
			});
		},
		
		doRollbackSpareList : function() {
			var view = this;
			var model = this.model;
			$.ajax({
				url : GLOBAL_URL + "/workflow/" + model.get("id").id + "/submit",
				type : "POST",
				success : function(data, textStatus, xhr) {
					console.log("success");
					model.set("status", "FEEDBACK");
					view.showDetails();
				}
			});
		},

		doSend : function(e) {
			var view = this;
			var model = this.model;
			$.ajax({
				url : GLOBAL_URL + "/workflow/" + model.get("id").id + "/send",
				type : "POST",
				success : function(data, textStatus, xhr) {
					console.log("success");
					model.set("status", "SENT");
					view.showDetails();
				}
			});
			e.stopPropagation();
		}
	});

})();
