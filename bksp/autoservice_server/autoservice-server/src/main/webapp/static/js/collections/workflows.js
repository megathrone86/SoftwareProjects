/**
 * Workflows collection
 */

(function() {
	var Workflows = Backbone.Collection.extend({
		model : app.Workflow,
        comparator: function (message) {
        	return -message.get("dateOpen");
        }
	});

	app.workflows = new Workflows();
	app.workflows.url = (PAGE_MODE == "OPERATOR"
		? GLOBAL_URL + "/workflow/active/"
		: GLOBAL_URL + "/workflow/my/");
	
	app.workflows.fetch({
		success : function () {
			new app.WorkflowsView();
		}
	});
}());