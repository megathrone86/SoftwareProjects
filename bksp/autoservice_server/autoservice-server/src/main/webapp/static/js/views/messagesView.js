/**
 * Messages view
 */
(function() {

	app.MessagesView = Backbone.View.extend({
		el : "#messages",

		events : {
			"submit #messageForm" : "save"
		},

		initialize : function() {
			this.listenTo(this.model, "sync", this.render);
			this.listenTo(this.model, "add", this.render);
		},

		render : function() {
			this.$el.children(".row").remove();
			var $el = this.$el;
			this.model.each(function(item) {
				var view = new app.MessageView({
					model : item
				});
				$el.append(view.render().el);
			});

			return this;
		},
		
		save : function (e) {
			var message = new app.Message(Backbone.Syphon.serialize(this));
			message.set("date", moment.tz("Europe/Moscow").valueOf());
			if (app.partsToSave.length > 0) {
				message.set("spares", app.partsToSave);
			}
			if (message.isValid()) {
				message.save(null, {
					success : function (model, response) {
						app.partsToSave = [];
						parts = [];
						$("#addPartsButton").text("Создать список запчастей");
						$("#requestInfo").trigger("needToUpdate");
					},
					error : function (model, response) {
						console.log("message save error");
					}
				});
				this.$("#content").val("");
			}
			e.preventDefault();
			return false;
		}

	});

})();
