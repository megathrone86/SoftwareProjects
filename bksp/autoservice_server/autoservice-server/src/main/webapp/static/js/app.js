/**
 * Enter-point of app
 */
var GLOBAL_URL = "/data";
var DATE_FORMAT = "YYYY-MM-DD HH:mm:ss.SSS";
var app = {};

_.templateSettings = {
	interpolate : /\<\@\=(.+?)\@\>/gim,
	evaluate : /\<\@(.+?)\@\>/gim,
	escape : /\<\@\-(.+?)\@\>/gim
};

app.statuses = 	{
	"NOT_SENT": "Не отправлена",
	"SENT": "Отправлена",
	"PROCESSING": "В обработке",
	"FEEDBACK": "Обратная связь",
	"PAY": "Ожидание оплаты",
	"DELIVERY": "Ожидание доставки",
	"RECIEVING": "Ожидание получения",
	"DONE": "Завершена",
	"CANCELED": "Отменена",
}
app.partsToSave = [];