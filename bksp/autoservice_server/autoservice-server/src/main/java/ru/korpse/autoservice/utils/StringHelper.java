package ru.korpse.autoservice.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringHelper {
	public static boolean stringIsNullOrEmpty(CharSequence s) {
		return s == null || (s.length() == 0);
	}

	public static boolean regexMatch(CharSequence regexString, CharSequence string) {
		Matcher m = Pattern.compile((String) regexString).matcher(string);
		return m.matches();
	}
}
