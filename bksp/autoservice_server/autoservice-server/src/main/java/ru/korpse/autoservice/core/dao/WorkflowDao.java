package ru.korpse.autoservice.core.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import lombok.extern.log4j.Log4j;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.korpse.autoservice.entities.Message;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.dto.Spare;
import ru.korpse.autoservice.entities.enums.WorkflowStatus;
import ru.korpse.autoservice.utils.PMF;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.repackaged.com.google.common.base.Predicate;
import com.google.appengine.repackaged.com.google.common.collect.Collections2;

@Log4j
@Repository
public class WorkflowDao {
	@Autowired
	private MessageDao messageDao;

	public Workflow get(long id) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			if (id <= 0) {
				return null;
			}
			Workflow result = pm.getObjectById(Workflow.class, id);
			return pm.detachCopy(result);
		} finally {
			pm.close();
		}
	}

	public List<Workflow> getActive() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query q = pm.newQuery(Workflow.class);
			q.setFilter("closed == false");
			q.setOrdering("dateOpen desc");
			@SuppressWarnings("unchecked")
			List<Workflow> result = (List<Workflow>) q.execute();
			// where status is not NOT_SENT
			result = new ArrayList<Workflow>(Collections2.filter(result, new Predicate<Workflow>() {
				@Override
				public boolean apply(Workflow item) {
					return item.getStatus() != WorkflowStatus.NOT_SENT;
				}
			}));
			return result;
		} finally {
			pm.close();
		}
	}
	
	public List<Workflow> getByUserId(String mid) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query q = pm.newQuery(Workflow.class);
			q.setFilter("userKey == param");
			q.declareParameters(Key.class.getName() + " param");
			Key userKey = KeyFactory.createKey(
					User.class.getSimpleName(), mid);

			@SuppressWarnings("unchecked")
			List<Workflow> workflows = (List<Workflow>) q.execute(userKey);
			return workflows;
		} finally {
			pm.close();
		}
	}

	public void save(Workflow item, User user) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		item.setDateOpen(DateTime.now(DateTimeZone.forID(System.getProperty("dateTimeZoneId"))));
		item.setUserKey(KeyFactory.createKey(User.class.getSimpleName(), user.getMid()));
		if (item.getStatus() == null) {
			item.setStatus(WorkflowStatus.NOT_SENT);
		}
		try {
			pm.makePersistent(item);
		} finally {
			pm.close();
		}
	}

	public void delete(Workflow item) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query q = pm.newQuery("select id from " + Message.class.getName());
			q.setFilter("workflowKey == param");
			q.declareParameters(Key.class.getName() + " param");
			Key workflowKey = item.getId();

			@SuppressWarnings("unchecked")
			List<Key> messages = (List<Key>) q.execute(workflowKey);
			for (Key messageId : messages) {
				pm.deletePersistent(pm.getObjectById(Message.class, messageId));
			}
			pm.deletePersistent(pm.getObjectById(Workflow.class, workflowKey));
		} finally {
			pm.close();
		}
	}

	public void changeStatus(long id, WorkflowStatus status) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Workflow workflow = pm.getObjectById(Workflow.class, id);
			workflow.setStatus(status);
			pm.makePersistent(workflow);
		} finally {
			pm.close();
		}
	}

	public void removeSpares(long id, Map<String, List<Integer>> spareIdMap) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			Workflow workflow = pm.getObjectById(Workflow.class, id);
			for (String original : spareIdMap.keySet()) {
				for (Integer num : spareIdMap.get(original)) {
					workflow.getSparesMap().get(original).remove(num.intValue());
					if (workflow.getSparesMap().get(original).size() == 0) {
						workflow.getSparesMap().remove(original);
					}
				}
			}
			pm.makePersistent(workflow);
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
				log.error("Workflow save failed");
			}
			pm.close();
		}
	}
	//TODO: test me
	public void selectSpares(long id, Map<String, List<Integer>> spareIdMap) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			Workflow workflow = pm.getObjectById(Workflow.class, id);
			for (String original : workflow.getSparesMap().keySet()) {
				for (Spare spare : workflow.getSparesMap().get(original)) {
					spare.setSelected(false);
				}
			}
			for (String original : spareIdMap.keySet()) {
				for (Integer num : spareIdMap.get(original)) {
					workflow.getSparesMap().get(original).get(num.intValue()).setSelected(true);
				}
			}
			if (spareIdMap.size() > 0) {
				workflow.setStatus(WorkflowStatus.PROCESSING);
			}
			pm.makePersistent(workflow);
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
				log.error("Select workflow spares failed");
			}
			pm.close();
		}
	}
}
