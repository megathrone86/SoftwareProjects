package ru.korpse.autoservice.entities;

import java.io.Serializable;
import java.util.List;

import ru.korpse.autoservice.entities.enums.CarBodyType;
import ru.korpse.autoservice.entities.enums.CarDriveType;
import ru.korpse.autoservice.entities.enums.CarFuelType;
import ru.korpse.autoservice.entities.enums.CarGearboxType;
import ru.korpse.autoservice.entities.enums.SteeringWheelType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request implements Serializable {
	private static final long serialVersionUID = 4686785323093480457L;
	private String user;

	// Наименование детали
	private List<String> partNames;

	// Номер VIN
	private String vinNumber;

	// Номер кузова
	public String bodyNumber;

	// Год выпуска
	private int carYear;

	// Марка машины
	private String carBrandName;

	// Модель машины
	private String carModel;

	// Модель и объем двигателя
	private String carEngineModel;

	// Кузов
	private CarBodyType carBodyType;

	// Топливо
	private CarFuelType carFuelType;
	
	// КПП
	private CarGearboxType carGearboxType;

	// Привод
	private CarDriveType carDriveType;

	// Руль
	private SteeringWheelType steeringWheelType;

	// Контактная информация: имя
	private String contactName;
	
	// Контактная информация: адрес
	private String contactAddress;

	// Контактная информация: телефон
	private String contactPhone;

	// Контактная информация: email
	private String contactEmail;

	// Хочу получать информацию об акциях, мероприятиях и других важных событиях
	// компании BK Spare Parts
	private boolean subscribe;

	// Примечания
	private String description;
}
