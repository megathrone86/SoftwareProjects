package ru.korpse.autoservice.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.scribe.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import ru.korpse.autoservice.core.helper.OAuthServiceHelper;
import ru.korpse.autoservice.entities.dto.UserInfo;

public class AutoservicePreAuthenticatedProcessingFilter extends
		AbstractPreAuthenticatedProcessingFilter {
	
	@Autowired
	private OAuthServiceHelper oauthServiceHelper;
	
	private UserInfo getUserInfo(HttpServletRequest request) {
		Token token = (Token) request.getSession().getAttribute("accessToken");
		String provider = (String) request.getSession().getAttribute("provider");
		UserInfo userInfo = oauthServiceHelper.getAndParseUserInfo(token, request, provider);
		return userInfo;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		UserInfo userInfo = getUserInfo(request);
		if (userInfo == null) {
			return null;
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("name", userInfo.getUid());
		return result;
	}

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		UserInfo userInfo = getUserInfo(request);
		if (userInfo == null) {
			return null;
		}
		return userInfo.getUid();
	}

}
