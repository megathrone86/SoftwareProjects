package ru.korpse.autoservice.entities.enums;

public enum CarBodyType {
	// Cедан
	SEDAN,
	// Хетчбек
	HATCHBACK,
	// Универсал
	UNIVERSAL,
	// Купе
	COUPE,
	// Фастбэк
	FASTBACK
}
