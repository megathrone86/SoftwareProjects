package ru.korpse.autoservice.entities.enums;

public enum CarGearboxType {
	// Ручная
	MANUAL,
	// Автоматическая
	AUTO,
	// Робот
	ROBOT,
	// Вариатор
	VARIATOR
}
