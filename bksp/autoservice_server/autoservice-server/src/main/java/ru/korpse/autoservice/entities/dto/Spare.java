package ru.korpse.autoservice.entities.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.joda.time.DateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Spare implements Serializable {
	private static final long serialVersionUID = -301754430947293559L;
	private String name;
	private DateTime dateOfDelivery;
	private BigDecimal price;
	private String sourceName;
	private int quantity;
	private boolean selected;
}
