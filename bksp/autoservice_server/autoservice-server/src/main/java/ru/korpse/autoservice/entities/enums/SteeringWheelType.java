package ru.korpse.autoservice.entities.enums;

public enum SteeringWheelType {
	LEFT,
	RIGHT
}
