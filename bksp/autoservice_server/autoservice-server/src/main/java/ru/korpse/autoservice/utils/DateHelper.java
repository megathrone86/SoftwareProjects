package ru.korpse.autoservice.utils;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {
	public static Date getCurrentTime() {
		return Calendar.getInstance().getTime();
	}

	public static int getCurrentYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}
}
