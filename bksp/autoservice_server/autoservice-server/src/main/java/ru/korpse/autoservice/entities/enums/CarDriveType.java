package ru.korpse.autoservice.entities.enums;

public enum CarDriveType {
	// Передний
	FRONT_WHEEL,
	// Полный
	FULL,
	// Задний
	REAR
}
