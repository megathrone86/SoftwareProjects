package ru.korpse.autoservice.entities;

import java.util.List;
import java.util.Map;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.joda.time.DateTime;

import ru.korpse.autoservice.entities.dto.Spare;
import ru.korpse.autoservice.entities.enums.WorkflowStatus;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(detachable = "true")
@Data
public class Workflow {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@JsonIgnore
	@Persistent
	@Extension(vendorName = "datanucleus", key = "User", value = "true")
	private Key userKey;
	
	@Persistent(serialized = "true", defaultFetchGroup = "true")
	private Request request;

	@Persistent
	private boolean closed;
	
	@Persistent
	private DateTime dateOpen;
	
	@Persistent
	private WorkflowStatus status;
	
	@Persistent(serialized = "true", defaultFetchGroup = "true")
	private Map<String, List<Spare>> sparesMap;
}
