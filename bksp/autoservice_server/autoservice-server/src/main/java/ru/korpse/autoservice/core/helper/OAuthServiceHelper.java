package ru.korpse.autoservice.core.helper;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.log4j.Log4j;

import org.apache.commons.lang.StringUtils;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.VkontakteApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ru.korpse.autoservice.entities.dto.UserInfo;
import ru.korpse.autoservice.utils.Google2Api;
import ru.korpse.autoservice.utils.ObjectMapper;

@Repository
@Log4j
public class OAuthServiceHelper {
	
	public static final String VK = "vk";
	public static final String GOOGLE = "google";
	public static final Set<String> allowedApis = new HashSet<String>();
	
	static {
		allowedApis.add(VK);
		allowedApis.add(GOOGLE);
	}

	private final String VK_SCOPE = "friends,wall,offline";
	private final String VK_USER_INFO_URL = "https://api.vk.com/method/users.get";
	private @Value("${vkAppSecretKey}")
	String vkAppSecretKey;
	private @Value("${vkAppKey}")
	String vkAppKey;

	private final String GOOGLE_SCOPE = "https://www.googleapis.com/auth/plus.login";
	private final String GOOGLE_USER_INFO_URL = "https://www.googleapis.com/plus/v1/people/me";
	private @Value("${googleAppSecretKey}")
	String googleAppSecretKey;
	private @Value("${googleAppKey}")
	String googleAppKey;

	@Autowired
	private ObjectMapper mapper;
	
	private OAuthService vkService;

	private OAuthService googleService;

	private String getCallbackUrl(HttpServletRequest req, String provider) {
	    String callbackUrl = req.getScheme() + "://" + req.getServerName()
	    		+ (req.getServerPort() == 80 ? "" : ":" + req.getServerPort()) 
	    		+ "/auth/code?provider=" + provider;
	    return callbackUrl;
	}
	
	private OAuthService getVkService(HttpServletRequest req) {
		if (vkService == null) {
			String callbackUrl = getCallbackUrl(req, VK);
		    OAuthService service = new ServiceBuilder().provider(VkontakteApi.class)
					.apiKey(vkAppKey).apiSecret(vkAppSecretKey)
					.scope(VK_SCOPE)
					.callback(callbackUrl).build();
		    vkService = service;
			return service;
		} else {
			return vkService;
		}
	}

	private OAuthService getGoogleService(HttpServletRequest req) {
		if (googleService == null) {
			String callbackUrl = getCallbackUrl(req, GOOGLE);
		    OAuthService service = new ServiceBuilder().provider(Google2Api.class)
					.apiKey(googleAppKey).apiSecret(googleAppSecretKey)
					.scope(GOOGLE_SCOPE)
					.callback(callbackUrl).build();
		    googleService = service;
			return service;
		} else {
			return googleService;
		}
	}
	public OAuthService getService(HttpServletRequest req, String provider) {
		if (provider.equals(VK)) {
			return getVkService(req); 
		} else if (provider.equals(GOOGLE)) {
			return getGoogleService(req);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	private UserInfo getAndParseVkUserInfo(Token token, HttpServletRequest req) {
		OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, VK_USER_INFO_URL);
		getService(req, VK).signRequest(token, oauthRequest);
		Response response = oauthRequest.send();
		
		log.debug(response.getBody());

		if (response.getCode() != 200) {
			return null;
		}
		
		Map<String, Object> responseBody = null;
		try {
			responseBody = mapper.readValue(response.getBody(), Map.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		UserInfo result = new UserInfo();
		String uid = null;
		String name = null;

		Map<String, Object> userInfo = ((List<Map<String, Object>>) responseBody.get("response")).get(0);
		uid = userInfo.get("uid").toString();
		name = userInfo.get("first_name") + " " + userInfo.get("last_name");

		result.setUid(uid);
		result.setName(name);
		result.setProvider(VK);
		return result;

	}
	
	@SuppressWarnings("unchecked")
	private UserInfo getAndParseGoogleUserInfo(Token token, HttpServletRequest req) {
		OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, GOOGLE_USER_INFO_URL);
		getService(req, GOOGLE).signRequest(token, oauthRequest);
		Response response = oauthRequest.send();
		
		log.debug(response.getBody());

		if (response.getCode() != 200) {
			return null;
		}
		
		Map<String, Object> responseBody = null;
		try {
			responseBody = mapper.readValue(response.getBody(), Map.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		UserInfo result = new UserInfo();
		String uid = null;
		String name = null;

		Map<String, Object> userInfo = (Map<String, Object>) responseBody;
		uid = userInfo.get("id").toString();
		name = userInfo.get("displayName").toString();

		result.setUid(uid);
		result.setName(name);
		result.setProvider(GOOGLE);
		return result;

	}

	public UserInfo getAndParseUserInfo(Token token, HttpServletRequest req, String provider) {
		if (token == null || StringUtils.isEmpty(provider) || !allowedApis.contains(provider)) {
			return null;
		}
		
		if (provider.equals(VK)) {
			return getAndParseVkUserInfo(token, req);
		} else if (provider.equals(GOOGLE)) {
			return getAndParseGoogleUserInfo(token, req);
		}
		
		return null;
	}
}
