package ru.korpse.autoservice.entities.enums;

public enum MessageDirection {
	OPERATOR_TO_USER,
	USER_TO_OPERATOR,
	SYSTEM_TO_ALL
}
