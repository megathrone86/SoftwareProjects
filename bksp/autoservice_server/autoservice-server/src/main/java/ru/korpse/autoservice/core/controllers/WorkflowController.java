package ru.korpse.autoservice.core.controllers;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.korpse.autoservice.core.dao.MessageDao;
import ru.korpse.autoservice.core.dao.WorkflowDao;
import ru.korpse.autoservice.core.helper.SystemMessageHelper;
import ru.korpse.autoservice.core.service.AuthenticationService;
import ru.korpse.autoservice.core.service.CheckWorkflowException;
import ru.korpse.autoservice.core.service.WorkflowCheckService;
import ru.korpse.autoservice.entities.Message;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.enums.MessageDirection;
import ru.korpse.autoservice.entities.enums.UserRole;
import ru.korpse.autoservice.entities.enums.WorkflowStatus;

@Log4j
@Controller
@RequestMapping(value = "/data/workflow/")
public class WorkflowController {

	@Autowired
	private WorkflowDao dao;

	@Autowired
	private MessageDao messageDao;

	@Autowired
	private AuthenticationService authenticationService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> save(@RequestBody Workflow item) throws CheckWorkflowException {
		try
		{
			User user = authenticationService.getCurrentUser();
	
			WorkflowCheckService service = new WorkflowCheckService();
			service.check(item);
			
			// сохраняем только новые workflow
			if (item.getId() != null) {
				item.setId(null);
			}
	
			dao.save(item, user);
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("id", item.getId());
			return result;
		} catch (Exception e) {
			log.debug("workflow save error");
			log.debug(e);
			throw e;
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	@ResponseBody
	public Workflow get(@PathVariable("id") long id) {
		User user = authenticationService.getCurrentUser();
		Workflow workflow = dao.get(id);
		if (user.getRoles().contains(UserRole.ROLE_OPERATOR)
				|| user.getMid().equals(workflow.getUserKey().getName())) {
			return workflow;
		} else {
			throw new AccessDeniedException("this workflow is private");
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/active/")
	@ResponseBody
	@Secured("ROLE_OPERATOR")
	public List<Workflow> getActive() {
		return dao.getActive();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/my/")
	@ResponseBody
	public List<Workflow> getMine() {
		User user = authenticationService.getCurrentUser();
		return dao.getByUserId(user.getMid());
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	@ResponseBody
	@Secured("ROLE_OPERATOR")
	public void delete(@PathVariable("id") long id) {
		dao.delete(dao.get(id));
	}

	private boolean canAttachMessage(Message message, Workflow workflow, User user) {
		if (workflow.getStatus() == WorkflowStatus.NOT_SENT &&
				message.getDirection() != MessageDirection.SYSTEM_TO_ALL) {
			return false;
		}
		
		if (message.getDirection() == MessageDirection.OPERATOR_TO_USER &&
				!user.getRoles().contains(UserRole.ROLE_OPERATOR)) {
			return false;
		}
		
		if (message.getDirection() == MessageDirection.USER_TO_OPERATOR &&
				!user.getRoles().contains(UserRole.ROLE_USER)) {
			return false;
		}

		if (message.getDirection() == MessageDirection.USER_TO_OPERATOR && message.getSpares() != null) {
			return false;
		}
		
		if (message.getDirection() == MessageDirection.SYSTEM_TO_ALL && message.getSpares() != null) {
			return false;
		}
		
		if ((message.getContent() == null || message.getContent().trim().length() <=0) &&
				message.getSpares() == null)
			return false;

		return true;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{workflowId}/messages/")
	@ResponseBody
	public Map<String, Object> attachMessage(@RequestBody Message message,
			@PathVariable("workflowId") long workflowId) {
		if (message.getSpares() != null && message.getSpares().size() == 0) {
			message.setSpares(null);
		}

		User user = authenticationService.getCurrentUser();
		Workflow workflow = dao.get(workflowId);

		if (canAttachMessage(message, workflow, user)) {
			messageDao.attachMessage(message, workflowId);
		} else {
			log.debug("cannot attach message");
			log.debug(message);
			log.debug(workflow);
			log.debug(user);
		}

		return new HashMap<String, Object>();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{workflowId}/messages/")
	@ResponseBody
	public List<Message> getMessages(@PathVariable("workflowId") long workflowId) {
		List<Message> result = messageDao.getMessages(workflowId);

		Collections.sort(result, new Comparator<Message>() {
			@Override
			public int compare(Message item0, Message item1) {
				if (item0.getDate() == null && item1.getDate() == null) {
					return 0;
				} else if (item0.getDate() == null) {
					return 1;
				} else if (item1.getDate() == null) {
					return -1;
				}
				return -item0.getDate().compareTo(item1.getDate());
			}
		});

		return result;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{workflowId}/send")
	@ResponseBody
	public void send(@PathVariable("workflowId") long id) {
		User user = authenticationService.getCurrentUser();
		Workflow workflow = dao.get(id);
		if (user.getRoles().contains(UserRole.ROLE_OPERATOR)
				|| user.getMid().equals(workflow.getUserKey().getName())) {
			dao.changeStatus(id, WorkflowStatus.SENT);
			SystemMessageHelper.AttachMessage(this, workflow, "Заявка отправлена.");
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/{workflowId}/submit")
	@ResponseBody
	@Secured("ROLE_OPERATOR")
	public void submit(@PathVariable("workflowId") long id) {
		Workflow workflow = dao.get(id);
		if (workflow.getSparesMap().size() > 0
				&& (workflow.getStatus() == WorkflowStatus.SENT || workflow.getStatus() == WorkflowStatus.PROCESSING)) {
			dao.changeStatus(id, WorkflowStatus.FEEDBACK);
			SystemMessageHelper.AttachMessage(this, workflow, "Список деталей подтвержден.");
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/{workflowId}/removeSpares")
	@ResponseBody
	@Secured("ROLE_OPERATOR")
	public Map<String, String> removeSpares(@PathVariable("workflowId") long id, @RequestBody Map<String, List<Integer>> spareIdMap) {
		dao.removeSpares(id, spareIdMap);
		return new HashMap<String, String>();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{workflowId}/selectSpares")
	@ResponseBody
	public Map<String, String> selectSpares(@PathVariable("workflowId") long id, @RequestBody Map<String, List<Integer>> spareIdMap) {
		User user = authenticationService.getCurrentUser();
		Workflow workflow = dao.get(id);
		if (user.getMid().equals(workflow.getUserKey().getName()) && workflow.getStatus() == WorkflowStatus.FEEDBACK) {
			dao.selectSpares(id, spareIdMap);
			return new HashMap<String, String>();
		} else {
			throw new AccessDeniedException("Access to this workflow is denied for user " + user.getMid());
		}
	}
}
