package ru.korpse.autoservice.core.dao;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.dto.UserInfo;
import ru.korpse.autoservice.entities.enums.UserRole;
import ru.korpse.autoservice.utils.PMF;

@Repository
public class UserDao {
	
	private @Value("${defaultOperatorIds}") String defaultOperatorIds;
	
	private void setRoles(User user) {
		user.getRoles().add(UserRole.ROLE_USER);
		String[] ids = defaultOperatorIds.split(",");
		for (String id : ids) {
			if (id.equals(user.getMid())) {
				user.getRoles().add(UserRole.ROLE_OPERATOR);
			}
		}
	}

	public User createByMid(String mid) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			User user = new User();
			user.setMid(mid);
			setRoles(user);
			pm.makePersistent(user);
			return user;
		} finally {
			pm.close();
		}

	}
	
	public void createByUserInfoIfNotExists(UserInfo userInfo) {
		if (userInfo == null) {
			return;
		}
		if (get(userInfo.getUid()) != null) {
			return;
		}
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			User user = new User();
			user.setMid(userInfo.getUid());
			user.setProvider(userInfo.getProvider());
			setRoles(user);
			user.setName(userInfo.getName());
			pm.makePersistent(user);
		} finally {
			pm.close();
		}

	}

	public User get(String mid) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			if (mid == null) {
				return null;
			}
			User result = null;
			User detachedResult;
			
			try {
				result = pm.getObjectById(User.class, mid);
			} catch (JDOObjectNotFoundException ex) {
				return null;
			}

			detachedResult = pm.detachCopy(result);
			return detachedResult;
		} finally {
			pm.close();
		}
	}
	
	public void delete(User user) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.deletePersistent(user);
		} finally {
			pm.close();
		}
	}

}
