package ru.korpse.autoservice.entities.enums;

public enum WorkflowStatus {
	// Не отправлен
	NOT_SENT,
	// Отправлен
	SENT,
	// В обработке
	PROCESSING,
	// Обратная связь
	FEEDBACK,
	// Ожидание оплаты
	PAY,
	// Ожидание доставки
	DELIVERY,
	// Ожидание получения
	RECIEVING,
	// Завершен
	DONE,
	// Отменен
	CANCELED
}
