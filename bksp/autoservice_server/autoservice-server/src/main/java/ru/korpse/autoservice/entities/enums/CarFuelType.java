package ru.korpse.autoservice.entities.enums;

public enum CarFuelType {
	// Бензин
	GASOLINE,
	// Дизель
	DIESEL,
	// Газ
	GAS
}
