package ru.korpse.autoservice.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import ru.korpse.autoservice.core.dao.UserDao;
import ru.korpse.autoservice.core.helper.OAuthServiceHelper;

public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private OAuthServiceHelper helper;
	
	@Override
	public UserDetails loadUserByUsername(String name)
			throws UsernameNotFoundException {
		return userDao.get(name);
	}
}
