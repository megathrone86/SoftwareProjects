package ru.korpse.autoservice.core.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.korpse.autoservice.core.dao.UserDao;
import ru.korpse.autoservice.core.service.AuthenticationService;
import ru.korpse.autoservice.entities.User;
import ru.korpse.autoservice.entities.enums.UserRole;

@Controller
@RequestMapping(value = "/", method = RequestMethod.GET)
public class FirstController {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String login() {
		User user = authenticationService.getCurrentUser();
		if (user != null) {
			return "redirect:client";
		}
		return "login";
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout() {
		return "redirect:/j_spring_security_logout";
	}

	@RequestMapping(value = "client", method = RequestMethod.GET)
	public ModelAndView index() {
		User user = authenticationService.getCurrentUser();
		ModelAndView mav = new ModelAndView("client").addObject("displayName", user.getName());
		return mav;
	}
	
}
