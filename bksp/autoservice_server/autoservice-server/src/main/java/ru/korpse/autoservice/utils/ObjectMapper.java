package ru.korpse.autoservice.utils;

import com.fasterxml.jackson.datatype.joda.JodaModule;

public class ObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {

	private static final long serialVersionUID = 624091526969564455L;

	public ObjectMapper() {
		super();
		this.registerModule(new JodaModule());
//		this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}

}
