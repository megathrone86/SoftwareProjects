package ru.korpse.autoservice.core.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ru.korpse.autoservice.entities.Message;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.dto.Spare;
import ru.korpse.autoservice.utils.PMF;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@Repository
public class MessageDao {
	
	private @Value("${dateTimeZoneId}") String dateTimeZoneId;
	
	PersistenceManager pm = PMF.get().getPersistenceManager();

	public void save(Message message) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistent(message);
		} finally {
			pm.close();
		}
	}

	public void attachMessage(Message message, long workflowId) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			//Добавляем пришедшие детали к workflow если они есть
			Key workflowKey = KeyFactory.createKey(
					Workflow.class.getSimpleName(), workflowId);
			Workflow workflow = pm.getObjectById(Workflow.class, workflowKey);
			if (message.getSpares() != null) {
				if (workflow.getSparesMap() == null) {
					workflow.setSparesMap(new HashMap<String, List<Spare>>());
				}
				for (Spare spare : message.getSpares()) {
					if (workflow.getSparesMap().get(spare.getSourceName()) == null) {
						workflow.getSparesMap().put(spare.getSourceName(), new ArrayList<Spare>());
					}
					workflow.getSparesMap().get(spare.getSourceName()).add(spare);
				}
			}
			pm.makePersistent(workflow);
			
			message.setWorkflowKey(workflowKey);
			message.setDate(DateTime.now(DateTimeZone.forID(dateTimeZoneId)));
			pm.makePersistent(message);
			tx.commit();
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
			pm.close();
		}
	}

	public void delete(Message message) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.deletePersistent(message);
		} finally {
			pm.close();
		}
	}

	public List<Message> getMessages(long workflowId) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query q = pm.newQuery(Message.class);
			q.setFilter("workflowKey == param");
			q.declareParameters(Key.class.getName() + " param");
			Key workflowKey = KeyFactory.createKey(
					Workflow.class.getSimpleName(), workflowId);

			@SuppressWarnings("unchecked")
			List<Message> messages = (List<Message>) q.execute(workflowKey);
			return messages;
		} finally {
			pm.close();
		}
	}
}
