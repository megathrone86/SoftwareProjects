package ru.korpse.autoservice.core.service;

import java.util.ArrayList;

import lombok.extern.log4j.Log4j;

@Log4j
public class CheckWorkflowException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -805674777184300683L;

	private ArrayList<String[]> checkResult;

	public ArrayList<String[]> getCheckResult() {
		return checkResult;
	}

	public CheckWorkflowException(ArrayList<String[]> checkResult) {
		this.checkResult = checkResult;
	}

	public void printCheckResult() {
		log.debug("CheckWorkflowException printCheckResult");
		for (String[] s : getCheckResult()) {
			log.debug(s[0] + " : " + s[1]);
		}
	}
}
