package ru.korpse.autoservice.entities.enums;

public enum UserRole {
	ROLE_USER,
	ROLE_OPERATOR
}
