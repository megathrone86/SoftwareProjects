package ru.korpse.autoservice.entities;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import lombok.Data;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import ru.korpse.autoservice.entities.enums.UserRole;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

@PersistenceCapable(detachable = "true")
@Data
public class User implements UserDetails {

	private static final long serialVersionUID = 3555116371833071409L;

	@PrimaryKey
	@Persistent
	private String mid;
	
	@Persistent(serialized = "true")
	private Set<UserRole> roles = new HashSet<UserRole>();
	
	@Persistent
	private String name;
	
	@Persistent
	private String provider;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Lists.transform(Lists.newArrayList(this.roles),
				new Function<UserRole, GrantedAuthority> () {

			@Override
			public GrantedAuthority apply(UserRole role) {
				return new SimpleGrantedAuthority(role.toString());
			}
			
		});
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		return mid;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
}
