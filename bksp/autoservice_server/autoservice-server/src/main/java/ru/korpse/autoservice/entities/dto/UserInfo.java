package ru.korpse.autoservice.entities.dto;

import lombok.Data;

@Data
public class UserInfo {
	private String uid;
	private String name;
	private String provider;
}
