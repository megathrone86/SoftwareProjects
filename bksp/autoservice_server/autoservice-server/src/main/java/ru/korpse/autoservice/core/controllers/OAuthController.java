package ru.korpse.autoservice.core.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.log4j.Log4j;

import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ru.korpse.autoservice.core.dao.UserDao;
import ru.korpse.autoservice.core.helper.OAuthServiceHelper;
import ru.korpse.autoservice.entities.dto.UserInfo;

@RequestMapping(value = "/auth/")
@Controller
@Log4j
public class OAuthController {

	@Autowired
	private OAuthServiceHelper serviceHelper;
	
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(method = RequestMethod.GET)
	public String doAuth(HttpServletRequest request, @RequestParam("provider") String provider) {
		if (!OAuthServiceHelper.allowedApis.contains(provider)) {
			return "redirect:/";
		}
		String authorizationUrl = serviceHelper.getService(request, provider).getAuthorizationUrl(null);
		return "redirect:" + authorizationUrl;
	}
	
	@RequestMapping(value = "code", method = RequestMethod.GET)
	public String authCode(@RequestParam("code") String code,
			@RequestParam("provider") String provider,
			HttpServletRequest request) throws IOException {
		if (!OAuthServiceHelper.allowedApis.contains(provider)) {
			return "redirect:/";
		}

		Verifier verifier = new Verifier(code);
		Token accessToken = serviceHelper.getService(request, provider).getAccessToken(null, verifier);
		if (accessToken == null) {
			return "redirect:/";
		}
		log.debug("Access token: " + accessToken.getToken());
		UserInfo userInfoResponse = serviceHelper.getAndParseUserInfo(accessToken, request, provider);
		userDao.createByUserInfoIfNotExists(userInfoResponse);

		request.getSession().setAttribute("accessToken", accessToken);
		request.getSession().setAttribute("provider", provider);
		
		return "redirect:/client";
	}
	

}
