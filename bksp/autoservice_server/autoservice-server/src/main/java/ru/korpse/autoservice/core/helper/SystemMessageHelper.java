package ru.korpse.autoservice.core.helper;

import lombok.extern.log4j.Log4j;
import ru.korpse.autoservice.core.controllers.WorkflowController;
import ru.korpse.autoservice.entities.Message;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.entities.enums.MessageDirection;

@Log4j
public class SystemMessageHelper {

	public SystemMessageHelper() {		
	}
	
	public static void AttachMessage(WorkflowController workflowController, Workflow workflow, String content) {
		try
		{
			Message message = new Message();
			message.setDirection(MessageDirection.SYSTEM_TO_ALL);
			message.setContent(content);
			
			workflowController.attachMessage(message, workflow.getId().getId());
		} catch (Exception e) {
			log.debug("SystemMessageHelper.AttachMessage error");
			log.debug(e);			
		}
	}
}
 