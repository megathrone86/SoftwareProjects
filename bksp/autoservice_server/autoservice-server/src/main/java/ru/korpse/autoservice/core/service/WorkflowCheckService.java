package ru.korpse.autoservice.core.service;

import java.util.ArrayList;
import java.util.Date;

import lombok.extern.log4j.Log4j;
import ru.korpse.autoservice.entities.Request;
import ru.korpse.autoservice.entities.Workflow;
import ru.korpse.autoservice.utils.DateHelper;
import ru.korpse.autoservice.utils.StringHelper;

//@Log4j
public class WorkflowCheckService {
	public void check(Workflow item) throws CheckWorkflowException {
		try {
			ArrayList<String[]> checkResult = checkRequest(item.getRequest());

			//for (String[] s : checkResult) {
			//	log.debug(s[0] + " : " + s[1]);
			//}

			if (checkResult.size() > 0) {
				throw new CheckWorkflowException(checkResult);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public ArrayList<String[]> checkRequest(Request request) {
		ArrayList<String[]> fields = new ArrayList<>();
		
		if (request == null) {
			fields.add(new String[] { "request", "null request" });
			return fields;
		}

		// vinNumber, bodyNumber
		if (StringHelper.stringIsNullOrEmpty(request.getVinNumber())
				&& StringHelper.stringIsNullOrEmpty(request.getBodyNumber())) {
			fields.add(new String[] { "vinNumber", "Нужно указать VIN или номер кузова." });
			fields.add(new String[] { "bodyNumber", "Нужно указать VIN или номер кузова." });
		} else {
			if (!StringHelper.stringIsNullOrEmpty(request.getVinNumber())
					&& !StringHelper.regexMatch("^.{10,20}$", request.getVinNumber())) {
				fields.add(new String[] { "vinNumber",
						"Номер VIN состоит из латинских символов и цифр и имеет длину от 10 до 20 символов." });
			}

			if (!StringHelper.stringIsNullOrEmpty(request.getBodyNumber())
					&& !StringHelper.regexMatch("^.{10,20}$", request.getBodyNumber())) {
				fields.add(new String[] { "bodyNumber",
						"Номер кузова состоит из латинских символов и цифр и имеет длину от 10 до 20 символов." });
			}
		}

		// carYear
		if (request.getCarYear() < 1900 || request.getCarYear() > DateHelper.getCurrentYear()) {
			fields.add(new String[] { "carYear", "Некорректный год выпуска." });
		}

		// carBrandName
		if (!StringHelper.stringIsNullOrEmpty(request.getCarBrandName())) {
			if (request.getCarBrandName().length() > 150) {
				fields.add(new String[] { "carBrandName", "Слишком длинная строка." });
			}
		} else {
			fields.add(new String[] { "carBrandName", "Это обязательное поле." });
		}

		// carModel
		if (!StringHelper.stringIsNullOrEmpty(request.getCarModel())) {
			if (request.getCarModel().length() > 100) {
				fields.add(new String[] { "carModel", "Слишком длинная строка." });
			}
		} else {
			fields.add(new String[] { "carModel", "Это обязательное поле." });
		}

		// carEngineModel
		if (!StringHelper.stringIsNullOrEmpty(request.getCarEngineModel())) {
			if (request.getCarEngineModel().length() > 100) {
				fields.add(new String[] { "carEngineModel", "Слишком длинная строка." });
			}
		} else {
			fields.add(new String[] { "carEngineModel", "Это обязательное поле." });
		}

		// carBodyType, carFuelType, carDriveType, carGearboxType - не проверяем

		// contactName
		if (!StringHelper.stringIsNullOrEmpty(request.getContactName())) {
			if (request.getContactName().length() > 100) {
				fields.add(new String[] { "contactName", "Слишком длинная строка." });
			}
		} else {
			fields.add(new String[] { "contactName", "Это обязательное поле." });
		}
		
		// contactAddress
		if (!StringHelper.stringIsNullOrEmpty(request.getContactAddress())) {
			if (request.getContactAddress().length() > 100) {
				fields.add(new String[] { "contactAddress", "Слишком длинная строка." });
			}
		} else {
			fields.add(new String[] { "contactAddress", "Это обязательное поле." });
		}

		// contactPhone
		if (!StringHelper.stringIsNullOrEmpty(request.getContactPhone())) {
			if (!StringHelper.regexMatch("^\\d{8,12}$", request.getContactPhone())) {
				fields.add(new String[] { "contactPhone", "Неправильно указан контактный телефон." });
			}
		} else {
			fields.add(new String[] { "contactPhone", "Это обязательное поле." });
		}

		// contactEmail
		if (!StringHelper.stringIsNullOrEmpty(request.getContactEmail())
				&& !StringHelper.regexMatch("^\\S+@\\S+\\.\\S+$", request.getContactEmail())) {
			fields.add(new String[] { "contactEmail", "Неправильно указан email." });
		}

		// description
		if (!StringHelper.stringIsNullOrEmpty(request.getDescription())
				&& request.getDescription().length() > 150) {
			fields.add(new String[] { "description", "Слишком длинная строка." });
		}

		// partNames
		if (request.getPartNames() != null) {
			boolean ok = false;
			for (String s : request.getPartNames()) {
				if (s.trim().length() > 0) {
					ok = true;
					break;
				}
			}

			if (!ok) {
				fields.add(new String[] { "partNames", "Нужно указать хотя бы одну запчасть." });
			}
		} else {
			fields.add(new String[] { "partNames", "Нужно указать хотя бы одну запчасть." });
		}

		return fields;
	}
}
