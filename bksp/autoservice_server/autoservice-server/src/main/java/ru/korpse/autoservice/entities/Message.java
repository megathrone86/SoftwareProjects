package ru.korpse.autoservice.entities;

import java.util.List;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.joda.time.DateTime;

import ru.korpse.autoservice.entities.dto.Spare;
import ru.korpse.autoservice.entities.enums.MessageDirection;

import com.google.appengine.api.datastore.Key;

@Data
@PersistenceCapable(detachable = "true")
public class Message {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@JsonIgnore
	@Persistent
	@Extension(vendorName = "datanucleus", key = "Workflow", value = "true")
	private Key workflowKey;

	@Persistent
	private MessageDirection direction;

	@Persistent
	private String content;
	
	@Persistent
	private DateTime date;
	
	@Persistent(serialized = "true", defaultFetchGroup = "true")
	private List<Spare> spares;
}
