﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BKSP_Manager_ARM.Data;
using System.IO;

namespace BKSP_Manager_ARM
{
    class Global
    {
        public static readonly string AppPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public static string GetWorkflowsURL()
        {
            return "http://automobileserv.appspot.com/workflow/";
        }

        public static string GetWorkflowURL(Workflow workflow)
        {
            return GetWorkflowsURL() + workflow.ID.ToString() + "/";
        }

        public static string GetWorkflowMessagesURL(Workflow workflow)
        {
            return GetWorkflowsURL() + workflow.ID.ToString() + "/messages/";
        }
    }
}
