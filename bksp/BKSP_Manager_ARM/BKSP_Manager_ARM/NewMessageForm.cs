﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BKSP_Manager_ARM.Data;
using AstralOnline.Utils.Errors;

namespace BKSP_Manager_ARM
{
    public partial class NewMessageForm : Form
    {
        private WorkflowsForm _parent;

        public NewMessageForm(WorkflowsForm parent)
        {
            _parent = parent;

            InitializeComponent();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
                return;

            try
            {
                Workflow workflow = _parent.GetSelection();

                WorkflowMessage message = new WorkflowMessage()
                {
                    MessageData = textBox1.Text,
                    Direction = "OPERATOR_TO_USER"
                };

                MessageAccessor.Instance.WriteMessage(workflow, message);

                _parent.RefreshData();

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка отправки сообщения. " + ErrorHandler.GetExceptionDescription(ex));
            }
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            Form2_Resize(null, null);
        }

        private void Form2_Resize(object sender, EventArgs e)
        {
            btCancel.Left = ClientSize.Width - btCancel.Width - 2;
            btCancel.Top = ClientSize.Height - btCancel.Height - 2;

            btOK.Left = btCancel.Left - btOK.Width - 2;
            btOK.Top = ClientSize.Height - btCancel.Height - 2;

            textBox1.Left = 2;
            textBox1.Width = ClientSize.Width - 4;
            textBox1.Top = 2;
            textBox1.Height = btOK.Top - 6;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Enter)
                btOK.PerformClick();
        }
    }
}
