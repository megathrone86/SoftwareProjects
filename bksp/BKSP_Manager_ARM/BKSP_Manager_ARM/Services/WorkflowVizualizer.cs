﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BKSP_Manager_ARM.Data;

namespace BKSP_Manager_ARM.Services
{
    class WorkflowVizualizer
    {
        private Workflow _workflow;
        private StringBuilder _result;

        public WorkflowVizualizer(Workflow workflow)
        {
            _workflow = workflow;
        }

        private string GetCarYear(int year)
        {
            return (year > 1900) ? year.ToString() : "";
        }

        private void CreateRequestInfo()
        {
            _result.AppendLine("<p><b>Контактная информация:</b><br>");
            _result.AppendLine(string.Format("Имя: {0}<br>", _workflow.Request.ContactName));
            _result.AppendLine(string.Format("Телефон: {0}<br>", _workflow.Request.ContactPhone));
            _result.AppendLine(string.Format("Email: {0}</p>", _workflow.Request.ContactEmail));

            if (_workflow.Request.PartNames != null)
            {
                _result.AppendLine("<p><b>Список деталей:</b>");
                foreach (string s in _workflow.Request.PartNames)
                    _result.AppendLine("<br>" + s);
                _result.AppendLine("</p>");
            }

            _result.AppendLine("<p><b>О машине:</b><br>");
            _result.AppendLine(string.Format("VIN: {0}<br>", _workflow.Request.vinNumber));
            _result.AppendLine(string.Format("Номер кузова: {0}<br>", _workflow.Request.bodyNumber));
            _result.AppendLine(string.Format("Год выпуска: {0}<br>", GetCarYear(_workflow.Request.CarYear)));
            _result.AppendLine(string.Format("Марка, модель: {0} {1}<br>", _workflow.Request.CarBrandName, _workflow.Request.CarModel));
            _result.AppendLine(string.Format("Модель и объем двигателя: {0}<br>", _workflow.Request.CarEngineModel));
            _result.AppendLine(string.Format("Тип кузова: {0}<br>", _workflow.Request.CarBodyType));
            _result.AppendLine(string.Format("Тип топлива: {0}<br>", _workflow.Request.CarFuelType));
            _result.AppendLine(string.Format("Тип привода: {0}<br>", _workflow.Request.CarDriveType));
            _result.AppendLine(string.Format("Расположение руля: {0}</p>", _workflow.Request.SteeringWheelType));

            if (!string.IsNullOrEmpty(_workflow.Request.Description))
            {
                _result.AppendLine("<p><b>Примечание:</b><br>");
                _result.AppendLine(String.Format("{0}</p>", StringToHtmlString(_workflow.Request.Description)));
            }
        }

        private string GetMessageAlign(WorkflowMessage message)
        {
            return message.IsInbound ? "left" : "right"; ;
        }

        private string GetMessageSender(WorkflowMessage message)
        {
            return message.IsInbound ? _workflow.ContactName : " вас";
        }

        private string StringToHtmlString(string s)
        {
            s = s.Replace("\r", "");
            s = s.Replace("\n", "<br>");
            return s;
        }

        private void CreateMessages()
        {
            _result.AppendLine("<hr>");
            foreach (WorkflowMessage message in _workflow.Messages)
            {
                string align = GetMessageAlign(message);
                string sender = GetMessageSender(message); ;

                _result.AppendLine(string.Format("<p align=\"{0}\"><b>Сообщение от {1}:</b><br>", align, sender));
                _result.AppendLine(string.Format("{0}</p>", StringToHtmlString(message.MessageData)));
            }
        }

        private void CreateButtons()
        {
            _result.AppendLine("<p><a href=\"newmessage\">Отправить сообщение</a>    <a href=\"markasfinished\">Пометить как завершенный</a></p>");
        }

        public string GetHtmlString()
        {
            if (_workflow == null)
                return "";

            _result = new StringBuilder();
            _result.AppendLine("<html>");
            _result.AppendLine("<head>");
            _result.AppendLine("<meta charset=\"utf-8\">");
            _result.AppendLine("</head>");
            _result.AppendLine("<body>");

            CreateRequestInfo();
            CreateMessages();
            CreateButtons();

            _result.AppendLine("</body>\r\n</html>\r\n");
            return _result.ToString();
        }
    }
}
