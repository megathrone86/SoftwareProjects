﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BKSP_Manager_ARM.Data;
using System.Threading;
using AstralOnline.Utils.Errors;
using BKSP_Manager_ARM.Services;
using System.IO;

namespace BKSP_Manager_ARM
{
    public partial class WorkflowsForm : Form
    {
        private List<Workflow> _workflows = new List<Workflow>();
        private System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();

        public WorkflowsForm()
        {
            InitializeComponent();

            _timer.Interval = 5 * 60 * 1000;
            _timer.Tick += new EventHandler(_timer_Tick);
        }

        /// <summary>
        /// асинхронное обновление данных
        /// </summary>
        private bool RefreshData_working = false;
        public void RefreshData()
        {
            if (RefreshData_working)
                return;

            toolStripStatusLabel1.Text = "Обновление данных...";
            RefreshData_working = true;

            Thread t = new Thread(delegate()
            {
                try
                {
                    //получим список новых и обновленных документоборотов
                    List<Workflow> newWorkflows = WorkflowAccessor.Instance.GetNewWorkflows(_workflows);

                    if (IsDisposed)
                        return;

                    Invoke((MethodInvoker)delegate
                    {
                        RefreshData(newWorkflows);
                        RefreshData_working = false;
                    });
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Произошла ошибка при обновлении данных. " + ErrorHandler.GetExceptionDescription(ex));
                }
            });
            t.Start();
        }

        private void SetSelection(Workflow selection)
        {
            if (selection == null)
                listBox1.SelectedIndex = -1;
            else
                listBox1.SelectedIndex = _workflows.FindIndex(w => w.IDEquals(selection));

            listBox1_SelectedValueChanged(null, null);
        }

        /// <summary>
        /// синхронное обновление данных на основе полученных с сервера обновлений
        /// </summary>
        /// <param name="newWorkflows"></param>
        private void RefreshData(List<Workflow> newWorkflows)
        {
            try
            {
                //запомним старое выделение
                Workflow oldSelection = GetSelection();

                //удалим из старого списка все документобороты, которые есть в новом списке (обновленные)
                {
                    List<Workflow> oldWorkflows = _workflows.FindAll(w => (newWorkflows.Find(nw => nw.IDEquals(w)) != null));
                    _workflows.RemoveAll(w => oldWorkflows.Contains(w));
                }
                //пополним список документоборотов
                _workflows.AddRange(newWorkflows);
                //отсортируем
                _workflows.Sort((a, b) => a.CreateDate.CompareTo(b.CreateDate));

                listBox1.DataSource = null;
                listBox1.DataSource = _workflows;
                SetSelection(oldSelection);
                toolStripStatusLabel1.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка обновления данных. " + ErrorHandler.GetExceptionDescription(ex));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "";
            RefreshData();

            _timer.Start();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            webBrowser1.Left = panel1.Right + 1;
            webBrowser1.Width = ClientSize.Width - webBrowser1.Left - 1;

            webBrowser1.Top = 1;
            webBrowser1.Height = statusStrip1.Top - webBrowser1.Top - 1;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Form1_Resize(null, null);
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            listBox1.Left = 1;
            listBox1.Width = panel1.ClientSize.Width - listBox1.Left - 1;

            listBox1.Top = checkBox1.Bottom + 5;
            listBox1.Height = panel1.ClientSize.Height - listBox1.Top - 1;
        }

        public Workflow GetSelection()
        {
            if (listBox1.SelectedValue == null)
                return null;

            return (Workflow)listBox1.SelectedValue;
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            WorkflowVizualizer viz = new WorkflowVizualizer(GetSelection());
            string htmlString = viz.GetHtmlString();

            string path = Path.Combine(Global.AppPath, "t.htm");
            File.WriteAllText(path, htmlString);
            webBrowser1.Navigate(path);
        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.AbsoluteUri.EndsWith("newmessage"))
            {
                e.Cancel = true;
                NewMessageClick();
            }

            if (e.Url.AbsoluteUri.EndsWith("markasfinished"))
            {
                e.Cancel = true;
                MarkAsFinishedClick();
            }
        }

        private void NewMessageClick()
        {
            NewMessageForm form2 = new NewMessageForm(this);
            form2.ShowDialog();
        }

        private void MarkAsFinishedClick()
        {
            MessageBox.Show("Функция еще не реализована.");
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void listBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu cm = new ContextMenu();
                cm.MenuItems.Add(new MenuItem("Обновить", new EventHandler(delegate
                {
                    RefreshData();
                })));
                cm.Show(listBox1, e.Location);
            }
        }
    }
}
