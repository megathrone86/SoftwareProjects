﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using BKSP_Manager_ARM.Services;

namespace BKSP_Manager_ARM.Data
{
    public class MessageAccessor : IDisposable
    {
        public static MessageAccessor Instance = new MessageAccessor();

        public MessageAccessor()
        {
        }

        public void WriteMessage(Workflow workflow, WorkflowMessage message)
        {
            JMessage jmessage = new JMessage()
            {
                content = message.MessageData,
                direction = message.Direction
            };

            string jsonString = JsonConvert.SerializeObject(jmessage);
            string result = JsonSender.PostJson(Global.GetWorkflowMessagesURL(workflow), jsonString);
            //if (string.IsNullOrEmpty(result))
            //    throw new Exception("Сервер вернул пустую строку.");
        }

        public void Dispose()
        {
        }
    }
}
