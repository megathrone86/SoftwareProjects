﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Threading;
using BKSP_Manager_ARM.Services;
using Newtonsoft.Json;

namespace BKSP_Manager_ARM.Data
{
    //http://kbss.ru/blog/lang_c_sharp/1.html

    public class WorkflowAccessor : IDisposable
    {
        public static WorkflowAccessor Instance = new WorkflowAccessor();

        public WorkflowAccessor()
        {
        }

        protected List<Workflow> GetWorkflowsFromServer()
        {
            List<Workflow> result = new List<Workflow>();

            string jsonString = JsonReceiver.GetJson(Global.GetWorkflowsURL());
            JArray ja = JArray.Parse(jsonString);
            foreach (JObject workflowJO in ja)
            {
                Workflow newWorkflow = JsonConvert.DeserializeObject<Workflow>(workflowJO.ToString());
                result.Add(newWorkflow);
            }

            return result;
        }

        protected void ReadWorkflowMessages(Workflow workflow)
        {
            {
                string jsonString = JsonReceiver.GetJson(Global.GetWorkflowMessagesURL(workflow));
                JArray messagesJA = JArray.Parse(jsonString);
                foreach (JObject messageJO in messagesJA)
                {
                    workflow.Messages.Add(new WorkflowMessage()
                    {
                        MessageData = messageJO["content"].ToString(),
                        Direction = messageJO["direction"].ToString()
                    });
                }
            }
        }

        public List<Workflow> GetNewWorkflows(List<Workflow> oldWorkflows)
        {
            List<Workflow> workflowsFromServer = GetWorkflowsFromServer();

            List<Workflow> result = new List<Workflow>();
            foreach (Workflow workflow in workflowsFromServer)
            {
                ReadWorkflowMessages(workflow);

                //если такой документоборот уже есть в локальной базе
                //и количество сообщений совпадает с тем что на сервере
                //то считаем что обновлять его не нужно
                Workflow oldWorkflow = oldWorkflows.Find(w => w.IDEquals(workflow));
                if (oldWorkflow != null)
                {
                    if (oldWorkflow.Messages.Count == workflow.Messages.Count)
                        continue;
                }

                result.Add(workflow);
            }

            return result;
        }

        public void Dispose()
        {
        }
    }
}
