﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using AstralOnline.Utils.Helpers;

namespace BKSP_Manager_ARM.Data
{
    public class Request
    {
        //Наименование детали
        public string[] PartNames;

        //Номер VIN
        public string vinNumber;

        //Номер кузова
        public string bodyNumber;

        //Год выпуска
        public int CarYear;

        //Марка машины
        public string CarBrandName;

        //Модель машины
        public string CarModel;

        //Модель и объем двигателя
        public string CarEngineModel;

        //Тип кузова
        public string CarBodyType;

        //Тип топлива
        public string CarFuelType;

        //Тип привода
        public string CarDriveType;

        //Руль
        public string SteeringWheelType;

        //Контактная информация: имя
        public string ContactName;

        //Контактная информация: телефон
        public string ContactPhone;

        //Контактная информация: email
        public string ContactEmail;

        //Хочу получать информацию об акциях, мероприятиях и других важных событиях компании BK Spare Parts
        public bool Subscribe;

        //Примечания
        public string Description;

        public Request()
        {
            PartNames = new string[0];
        }
    }
}
