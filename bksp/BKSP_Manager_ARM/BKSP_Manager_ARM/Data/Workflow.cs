﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BKSP_Manager_ARM.Data
{
    public class Workflow
    {
        //то что хранится в БД
        public long ID { get; set; }
        public bool Closed { get; set; }
        public Request Request { get; set; }

        //то что хранится локально
        public DateTime CreateDate { get; set; }
        public int State { get; set; }
        public List<WorkflowMessage> Messages { get; set; }

        //вычисляемые значения
        public string ContactName { get { return Request.ContactName; } }
        public string CarBrandName { get { return Request.CarBrandName; } }
        public string CarModel { get { return Request.CarModel; } }

        public Workflow()
        {
            Messages = new List<WorkflowMessage>();
        }

        public bool IDEquals(Workflow w2)
        {
            if (w2 == null)
                return false;
            return ID == w2.ID;
        }

        private void AddToResult(ref string result, string value, string defaultValue)
        {
            string addResult = string.IsNullOrEmpty(value) ? defaultValue : value;

            if (string.IsNullOrEmpty(addResult))
                result += addResult;
            else
            {
                if (!string.IsNullOrEmpty(addResult))
                    result += " " + addResult;
            }
        }

        public override string ToString()
        {
            string result = "";
            AddToResult(ref result, ContactName, "[Мистер Икс]");
            AddToResult(ref result, CarBrandName, "");
            AddToResult(ref result, CarModel, "");

            if (string.IsNullOrEmpty(result))
                result = "[нет данных]";
            return result;
        }
    }
}
