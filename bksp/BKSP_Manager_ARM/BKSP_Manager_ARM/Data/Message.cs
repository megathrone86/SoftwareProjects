﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BKSP_Manager_ARM.Data
{
    public class WorkflowMessage
    {
        public long WorkflowServerID;
        public string MessageData;
        public string Direction;

        public bool IsInbound
        {
            get { return Direction == "USER_TO_OPERATOR"; }
        }
    }
}
