package com.xozm.shared;

public class LockableObject {
    private volatile Thread _lockerThread;

    public boolean Lock() {
        if (_lockerThread == Thread.currentThread())
            return false;

        synchronized (this) {
            while (_lockerThread != null) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            _lockerThread = Thread.currentThread();
        }

        return true;
    }

    public void Unlock(boolean doUnlock) {
        if (doUnlock)
            _lockerThread = null;
    }
}
