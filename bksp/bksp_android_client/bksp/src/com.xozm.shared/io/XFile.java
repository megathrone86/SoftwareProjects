package com.xozm.shared.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Hozeev on 26.12.13.
 */
public class XFile {
    public static byte[] getBytes(File file) throws IOException {
        byte[] bytes = new byte[(int) file.length()];
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
        buf.read(bytes, 0, bytes.length);
        buf.close();

        return bytes;
    }

    public static void writeBytes(byte[] bytes, File file) throws IOException {
        BufferedOutputStream buf = new BufferedOutputStream(new FileOutputStream(file));
        buf.write(bytes);
        buf.close();
    }
}
