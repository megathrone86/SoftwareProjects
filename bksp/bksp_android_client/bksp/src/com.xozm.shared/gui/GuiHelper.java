package com.xozm.shared.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

/**
 * Created by Hozeev on 04.12.13.
 */
public class GuiHelper {
    //Применить стиль к диниамически создаваемому объекту невозможно. Поэтому грузим его из R.layout.custom_object
    /*Пример лейаута:

    <?xml version="1.0" encoding="utf-8"?>
    <com.xozm.shared.gui.TextView
      style="@style/TextView"
      xmlns:android="http://schemas.android.com/apk/res/android">
    </com.xozm.shared.gui.TextView> */
    public static TextView CreateTextView(ViewGroup parent, int resId, Object tag) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView tw = (TextView) inflater.inflate(resId, null);
        tw.setTextAppearance(parent.getContext(), android.R.attr.textAppearanceMedium);
        tw.setPadding(4, 2, 4, 2);
        tw.setTag(tag);

        if (parent != null)
            parent.addView(tw);
        return tw;
    }

    //Применить стиль к диниамически создаваемому объекту невозможно. Поэтому грузим его из R.layout.custom_object
    /*Пример лейаута:

    <?xml version="1.0" encoding="utf-8"?>
    <com.xozm.shared.gui.XImageButton
      style="@style/XImageButton"
      xmlns:android="http://schemas.android.com/apk/res/android">
    </com.xozm.shared.gui.XImageButton> */
    public static XImageButton CreateXImageButton(ViewGroup parent, int resId, Object tag) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        XImageButton bt = (XImageButton) inflater.inflate(resId, null);
        bt.setPadding(4, 2, 4, 2);
        bt.setTag(tag);

        if (parent != null)
            parent.addView(bt);
        return bt;
    }

    //Применить стиль к диниамически создаваемому объекту невозможно. Поэтому грузим его из R.layout.custom_object
    /*Пример лейаута:

    <?xml version="1.0" encoding="utf-8"?>
    <com.xozm.shared.gui.XImageView
      style="@style/XImageView"
      xmlns:android="http://schemas.android.com/apk/res/android">
    </com.xozm.shared.gui.XImageView> */
    public static XImageView CreateXImageView(ViewGroup parent, int resId, Object tag, boolean clickable) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        XImageView iv = (XImageView) inflater.inflate(resId, null);

        iv.setPadding(4, 2, 4, 2);
        iv.setTag(tag);
        iv.setClickable(clickable);
        iv.setFocusable(clickable);

        if (parent != null)
            parent.addView(iv);
        return iv;
    }

    //Применить стиль к диниамически создаваемому объекту невозможно. Поэтому грузим его из R.layout.custom_object
    /*Пример лейаута:

    <?xml version="1.0" encoding="utf-8"?>
    <com.xozm.shared.gui.XImageView
      style="@style/XImageView"
      xmlns:android="http://schemas.android.com/apk/res/android">
    </com.xozm.shared.gui.XImageView> */
    public static LinearLayout CreateLinearLayout(ViewGroup parent, int resId, int orientation, int width, int height, Object tag) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) inflater.inflate(resId, null);

        ll.setOrientation(orientation);
        ll.setLayoutParams(new LinearLayout.LayoutParams(width, height));
        ll.setTag(tag);

        if (parent != null)
            parent.addView(ll);
        return ll;
    }

    public static LinearLayout CreateLinearLayout(ViewGroup parent, int orientation, int width, int height, Object tag) {
        LinearLayout ll = new LinearLayout(parent.getContext());
        ll.setOrientation(orientation);
        ll.setLayoutParams(new LinearLayout.LayoutParams(width, height));
        ll.setTag(tag);

        if (parent != null)
            parent.addView(ll);
        return ll;
    }

    public static ViewGroup FindTab(TabHost tabHost, int tabIndex) {
        return (ViewGroup) tabHost.getTabWidget().getChildAt(tabIndex);
    }

    public static TextView FindTabHeader(TabHost tabHost, int tabIndex) {
        return (TextView) tabHost.getTabWidget().getChildAt(tabIndex).findViewById(android.R.id.title);
    }
}
