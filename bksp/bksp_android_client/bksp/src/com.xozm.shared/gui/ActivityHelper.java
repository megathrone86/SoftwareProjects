package com.xozm.shared.gui;

import android.app.Activity;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Hozeev on 24.12.13.
 */
public class ActivityHelper {
    private Activity activity;

    public ActivityHelper(Activity activity) {
        this.activity = activity;
    }

    public XImageView findXImageView(int id) {
        return (XImageView) activity.findViewById(id);
    }

    public TextView findTextView(int id) {
        return (TextView) activity.findViewById(id);
    }

    private CheckBox findCheckBox(int id) {
        return (CheckBox) activity.findViewById(id);
    }

    public EditText findEditText(int id) {
        return (EditText) activity.findViewById(id);
    }

    public XImageButton findXImageButton(int id) {
        return (XImageButton) activity.findViewById(id);
    }

    public LinearLayout findLinearLayout(int id) {
        return (LinearLayout) activity.findViewById(id);
    }

    public String getEditTextValue(int id) {
        EditText et = findEditText(id);
        return et.getText().toString();
    }

    public void setEditTextValue(int id, String s) {
        EditText et = findEditText(id);
        et.setText(s);
    }

    public boolean getCheckBoxValue(int id) {
        CheckBox cb = findCheckBox(id);
        return cb.isChecked();
    }

    public void setCheckBoxValue(int id, boolean b) {
        CheckBox cb = findCheckBox(id);
        cb.setChecked(b);
    }
}
