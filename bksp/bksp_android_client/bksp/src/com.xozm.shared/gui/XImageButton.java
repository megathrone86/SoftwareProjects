package com.xozm.shared.gui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * Created by Hozeev on 24.12.13.
 * Этот класс содержит кастомные атрибуты.
 * Для того, чтобы можно было использовать их в styles.xml, они должны быть сначала перечислены в attrs.xml.
 */

public class XImageButton extends ImageButton {
    //Значения:
    //byX
    //byY
    protected String keepRatio = "";

    public XImageButton(Context context) {
        super(context);
    }

    public XImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyAttributes(attrs);
    }

    public XImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyAttributes(attrs);
    }

    protected void applyAttributes(AttributeSet attrs) {
        if (attrs == null)
            return;
        keepRatio = attrs.getAttributeValue(null, "keepRatio");
    }

    protected double getRatio() {
        Drawable d = super.getDrawable();
        if (d == null)
            return 1;
        return d.getIntrinsicWidth() / d.getIntrinsicHeight();
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (getKeepRatio() == null)
            return;

        int parentWidth = this.getMeasuredWidth();
        int parentHeight = this.getMeasuredHeight();

        if (getKeepRatio().equals("byX"))
            parentHeight = (int) (parentWidth / getRatio());

        if (getKeepRatio().equals("byY"))
            parentWidth = (int) (parentHeight * getRatio());

        this.setMeasuredDimension(parentWidth, parentHeight);
    }

    public String getKeepRatio() {
        return keepRatio;
    }

    public void setKeepRatio(String keepRatio) {
        this.keepRatio = keepRatio;
    }
}
