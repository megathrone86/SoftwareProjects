package com.xozm.shared.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;

import com.xozm.shared.enums.EnumEntry;
import com.xozm.shared.enums.EnumEntryList;

import java.util.ArrayList;

/**
 * Created by x on 07.10.13.
 */
public class ComboBoxHelper implements View.OnFocusChangeListener, View.OnClickListener {
    private EditText targetEditText;
    private ListAdapter adapter;
    private Activity parentActivity;

    public ComboBoxHelper(Activity parentActivity, EditText targetEditText, ListAdapter adapter) {
        this.parentActivity = parentActivity;
        this.targetEditText = targetEditText;
        this.adapter = adapter;
    }

    public static void CreateComboBox(Activity parentActivity, int targetEditTextResId, int textArrayResId) {
        final EditText targetEditText = (EditText) parentActivity.findViewById(targetEditTextResId);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(parentActivity,
                textArrayResId, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ComboBoxHelper cbHelper = new ComboBoxHelper(parentActivity, targetEditText, adapter);
        targetEditText.setOnFocusChangeListener(cbHelper);
        targetEditText.setOnClickListener(cbHelper);
    }

    public static void CreateComboBox(Activity parentActivity, int targetEditTextResId,
                                      EnumEntryList enumEntryList) {
        ArrayList<CharSequence> arrayList = new ArrayList<>();
        for (EnumEntry e : enumEntryList.getList())
            arrayList.add(e.descr);

        final ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(
                parentActivity.getApplicationContext(), android.R.layout.simple_spinner_item,
                arrayList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final EditText targetEditText = (EditText) parentActivity.findViewById(targetEditTextResId);

        ComboBoxHelper cbHelper = new ComboBoxHelper(parentActivity, targetEditText, adapter);
        targetEditText.setOnFocusChangeListener(cbHelper);
        targetEditText.setOnClickListener(cbHelper);
    }

    protected void showDialog() {
        try {
            AlertDialog.Builder dlg = new AlertDialog.Builder(parentActivity);
            dlg.setTitle(targetEditText.getHint());
            dlg.setAdapter(adapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        Object o = adapter.getItem(which);
                        targetEditText.setText(o.toString());
                        dialog.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            dlg.create();
            dlg.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b)
            return;
        showDialog();
    }

    @Override
    public void onClick(View view) {
        showDialog();
    }
}
