package com.xozm.shared.helpers;

import android.os.Environment;

import java.io.File;

public class StorageHelper {
    private static String _appsDataDir = "/data/";

    private static File getAppDataRoot() {
        return null;
    }

    public static File getAppDataDir(String packageName) throws Exception {
        File extFile = Environment.getExternalStorageDirectory();
        if ((extFile == null) || (!extFile.exists()))
            return null;

        File result = new File(extFile.getPath() + _appsDataDir + packageName);
        if (!result.exists() && !result.mkdirs()) {
            throw new Exception("mkdirs() fail");
        }
        return result;
    }

    public static boolean externalStorageConnected() {
        File extFile = Environment.getExternalStorageDirectory();
        return (extFile != null) && extFile.exists();
    }

    public static void createDirForFile(File file) throws Exception {
        File parent = file.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new Exception("mkdirs() fail");
        }
    }
}
