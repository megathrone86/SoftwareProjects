package com.xozm.shared.helpers;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Hozeev on 04.12.13.
 */
public class DateTimeHelper {
    public static Date getCurrentTime() {
        return Calendar.getInstance().getTime();
    }
}
