package com.xozm.shared.helpers;

/**
 * Created by Hozeev on 01.12.13.
 */
public class IntHelper {
    public static int parseInt(String s, int defaultValue) {
        if (StringHelper.StringIsEmpty(s))
            return defaultValue;

        return Integer.parseInt(s);
    }

    public static long parseLong(String s, int defaultValue) {
        if (StringHelper.StringIsEmpty(s))
            return defaultValue;

        return Long.parseLong(s);
    }
}
