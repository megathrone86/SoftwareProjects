package com.xozm.shared.helpers;

/**
 * Created by Hozeev on 02.12.13.
 */
public class ExceptionsHelper {
    public static String getExceptionDescription(Exception ex) {
        if (!StringHelper.StringIsEmpty(ex.getLocalizedMessage()))
            return ex.getLocalizedMessage();

        if (!StringHelper.StringIsEmpty(ex.getMessage()))
            return ex.getMessage();

        return ex.toString();
    }
}
