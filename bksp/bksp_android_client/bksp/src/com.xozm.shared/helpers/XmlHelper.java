package com.xozm.shared.helpers;

import android.annotation.TargetApi;
import android.os.Build;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XmlHelper {
    @TargetApi(Build.VERSION_CODES.FROYO)
    public static byte[] DocumentToBytes(Document doc) throws Exception {
        Source source = new DOMSource(doc);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Result result = new StreamResult(out);
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.transform(source, result);
        out.flush();
        return out.toByteArray();
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    public static String DocumentToString(Document doc) throws Exception {
        Source source = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        Result result = new StreamResult(writer);
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.transform(source, result);
        writer.flush();
        return writer.toString();
    }

    public static Element CreateElement(Node parent, String name) {
        Document doc = parent.getOwnerDocument();
        Element newElement = doc.createElement(name);
        parent.appendChild(newElement);
        return newElement;
    }

    public static Element CreateElement(Node parent, String name, String value) {
        Document doc = parent.getOwnerDocument();
        Element newElement = doc.createElement(name);
        parent.appendChild(newElement);
        newElement.appendChild(doc.createTextNode(value));
        return newElement;
    }

    public static Element CreateCDataElement(Node parent, String name, String value) {
        Document doc = parent.getOwnerDocument();
        Element newElement = doc.createElement(name);
        parent.appendChild(newElement);
        newElement.appendChild(doc.createCDATASection(value));
        return newElement;
    }

    public static void CreateAttr(Element element, String name, String value) {
        Document doc = element.getOwnerDocument();
        Attr attr = doc.createAttribute(name);
        attr.setValue(value);
        element.setAttributeNode(attr);
    }

    public static String GetAttr(Node node, String name) {
        Node attrNode = node.getAttributes().getNamedItem(name);
        if (attrNode == null)
            return "";
        else
            return attrNode.getNodeValue();
    }

    public static List<Node> GetNodeList(NodeList nodeList) {
        List<Node> result = new ArrayList<>(nodeList.getLength());

        for (int i = 0; i < nodeList.getLength(); i++) {
            result.add(nodeList.item(i));
        }

        return result;
    }
}
