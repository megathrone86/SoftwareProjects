package com.xozm.shared.helpers;

/**
 * Created by Hozeev on 01.12.13.
 */
public class BooleanHelper {
    public static boolean parseBoolean(String s, boolean defaultValue) {
        if (StringHelper.StringIsEmpty(s))
            return defaultValue;

        return Boolean.parseBoolean(s);
    }
}
