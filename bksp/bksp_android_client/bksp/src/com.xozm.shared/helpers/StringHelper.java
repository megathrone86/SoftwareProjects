package com.xozm.shared.helpers;

public class StringHelper {
    public static boolean StringIsEmpty(CharSequence s) {
        return s == null || (s.length() == 0);
    }
}
