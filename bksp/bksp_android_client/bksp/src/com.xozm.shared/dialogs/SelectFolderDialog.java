package com.xozm.shared.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xozm.shared.helpers.ExceptionsHelper;
import com.xozm.shared.helpers.RandomHelper;

import java.io.File;

public class SelectFolderDialog extends Activity implements OnClickListener,
        DialogListener {
    private SelectFolderDialogAdapter _adapter;

    private TextView _txtCurrentDir;
    private Button _btUpper;
    private Button _btCreateNew;
    private ListView _lstDirs;
    private Button _btOK;
    private Button _btCancel;
    private File _currentDir;
    private int _btOKResourceID;
    private int _btCancelResourceID;
    private String bt_okText;
    private String bt_cancelText;
    private String bt_upText;
    private String bt_createfolderText;
    private String msg_errorcreatingfolderText;
    private String msg_enternewdirnameText;

    //TODO: потом поискать более удачное решение
    public void SetDialogProps(String bt_okText, String bt_cancelText, String bt_upText, String bt_createfolderText,
                               String msg_errorcreatingfolderText, String msg_enternewdirnameText) {
        this.bt_okText = bt_okText;
        this.bt_cancelText = bt_cancelText;
        this.bt_upText = bt_upText;
        this.bt_createfolderText = bt_createfolderText;
        this.msg_errorcreatingfolderText = msg_errorcreatingfolderText;
        this.msg_enternewdirnameText = msg_enternewdirnameText;
    }

    public void SetCurrentDir(File currentDir) {
        if (currentDir == null)
            this._currentDir = this._currentDir.getParentFile();
        else {
            try {
                this._currentDir = currentDir;
            } catch (Exception ex) {
                ex.printStackTrace();
                this._currentDir = Environment.getRootDirectory();
            }
        }

        UpdateCurrentDir();

        _txtCurrentDir.setText(this._currentDir.getAbsolutePath());
    }

    private void UpdateCurrentDir() {
        if (_adapter == null) {
            _adapter = new SelectFolderDialogAdapter(getItems(), this);
            _lstDirs.setAdapter(_adapter);
        } else {
            _adapter.Files = getItems();
            _adapter.notifyDataSetChanged();
        }

        _btCreateNew.setEnabled(this._currentDir.canWrite());
    }

    public static void ShowDialog(Activity parentActivity, String currentDir,
                                  int requestCode, int btOKResourceID, int btCancelResourceID) {
        try {
            Intent myIntent = new Intent(parentActivity.getApplicationContext(),
                    SelectFolderDialog.class);
            myIntent.putExtra("currentDir", currentDir);
            myIntent.putExtra("btOKResourceID", btOKResourceID);
            myIntent.putExtra("btCancelResourceID", btCancelResourceID);
            parentActivity.startActivityForResult(myIntent, requestCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private LinearLayout createBottomButtonsLayout() {
        LinearLayout bottomButtonsLayout = new LinearLayout(this);
        bottomButtonsLayout.setOrientation(LinearLayout.HORIZONTAL);
        bottomButtonsLayout.setGravity(Gravity.RIGHT);

        _btCancel = new Button(this);
        _btCancel.setText(bt_cancelText);
        _btCancel.setOnClickListener(this);
        if (_btCancelResourceID > 0)
            _btCancel.setCompoundDrawablesWithIntrinsicBounds(
                    _btCancelResourceID, 0, 0, 0);

        _btOK = new Button(this);
        _btOK.setText(bt_okText);
        _btOK.setOnClickListener(this);
        if (_btOKResourceID > 0)
            _btOK.setCompoundDrawablesWithIntrinsicBounds(_btOKResourceID, 0,
                    0, 0);

        bottomButtonsLayout.addView(_btCancel);
        bottomButtonsLayout.addView(_btOK);

        return bottomButtonsLayout;
    }

    private LinearLayout createTopButtonsLayout() {
        LinearLayout topButtonsLayout = new LinearLayout(this);
        topButtonsLayout.setOrientation(LinearLayout.HORIZONTAL);

        _btUpper = new Button(this);
        _btUpper.setText(bt_upText);
        _btUpper.setOnClickListener(this);

        _btCreateNew = new Button(this);
        _btCreateNew.setText(bt_createfolderText);
        _btCreateNew.setOnClickListener(this);

        topButtonsLayout.addView(_btUpper);
        topButtonsLayout.addView(_btCreateNew);

        return topButtonsLayout;
    }

    private ListView createLstDirs() {
        ListView lstDirs = new ListView(this);
        lstDirs.setPadding(5, 5, 5, 5);
        return lstDirs;
    }

    private TextView createTxtCurrentDir() {
        TextView txtCurrentDir = new TextView(this);
        txtCurrentDir.setPadding(5, 5, 5, 5);
        txtCurrentDir.setTextSize(20);
        Typeface tf = txtCurrentDir.getTypeface();
        tf = Typeface.create(tf, Typeface.BOLD);
        txtCurrentDir.setTypeface(tf);
        return txtCurrentDir;
    }

    private RelativeLayout createListAndButtonsLayout() {
        RelativeLayout listAndButtonsLayout = new RelativeLayout(this);

        LinearLayout bottomButtonsLayout = createBottomButtonsLayout();
        bottomButtonsLayout.setId(RandomHelper.Random.nextInt());

        _lstDirs = createLstDirs();
        _lstDirs.setId(RandomHelper.Random.nextInt());

        {
            RelativeLayout.LayoutParams rparams = new RelativeLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
            rparams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
                    RelativeLayout.TRUE);
            listAndButtonsLayout.addView(bottomButtonsLayout, rparams);
        }

        {
            RelativeLayout.LayoutParams rparams = new RelativeLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
            rparams.addRule(RelativeLayout.ABOVE, bottomButtonsLayout.getId());
            listAndButtonsLayout.addView(_lstDirs, rparams);
        }

        return listAndButtonsLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            this._btOKResourceID = getIntent().getIntExtra("btOKResourceID", 0);
            this._btCancelResourceID = getIntent().getIntExtra(
                    "btCancelResourceID", 0);

            _txtCurrentDir = createTxtCurrentDir();
            LinearLayout topButtonsLayout = createTopButtonsLayout();
            RelativeLayout listAndButtonsLayout = createListAndButtonsLayout();

            LinearLayout mainLayout = new LinearLayout(this);
            mainLayout.setOrientation(LinearLayout.VERTICAL);
            mainLayout.addView(_txtCurrentDir);
            mainLayout.addView(topButtonsLayout);
            mainLayout.addView(listAndButtonsLayout);

            setContentView(mainLayout);

            SetCurrentDir(new File(getIntent().getStringExtra("currentDir")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File[] getItems() {
        File[] files = _currentDir.listFiles();
        if (files == null)
            files = new File[0];

        File parentDir = _currentDir.getParentFile();
        _btUpper.setEnabled(parentDir != null);

        File[] result = new File[files.length];
        int realCount = 0;
        for (File file : files) {
            if (file.getAbsolutePath().length() == 0)
                continue;

            if (file.isDirectory() /* || showFiles */) {
                result[realCount] = file;
                realCount++;
            }
        }

        File[] result2 = new File[realCount];
        if (realCount > 0)
            System.arraycopy(result, 0, result2, 0, realCount);

        return result2;
    }

    @Override
    public void onClick(View arg0) {
        if (arg0 == _btUpper) {
            SetCurrentDir(null);
            return;
        }

        if (arg0 == _btCreateNew) {
            InputStringDialog dlg = new InputStringDialog(this);
            dlg.SetDialogListener(this);
            dlg.ShowDialog(msg_enternewdirnameText);
            return;
        }

        if (arg0 == _btOK) {
            Intent result = new Intent();
            if (_currentDir == null)
                result.putExtra("currentDir", "");
            else
                result.putExtra("currentDir", _currentDir.getAbsolutePath());
            setResult(RESULT_OK, result);
            finish();
            return;
        }

        if (arg0 == _btCancel) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }
    }

    @Override
    public void DialogClosed(Object sender, boolean cancelled) {
        if (cancelled)
            return;

        InputStringDialog dlg = (InputStringDialog) sender;
        if (dlg != null) {
            try {
                File newDirectory = new File(_currentDir.getAbsolutePath()
                        + File.separator + dlg.getInput());
                newDirectory.mkdirs();
                UpdateCurrentDir();
            } catch (Exception ex) {
                ex.printStackTrace();
                SimpleMessageBox.Show(msg_errorcreatingfolderText + ExceptionsHelper.getExceptionDescription(ex),
                        getApplicationContext());
            }
        }
    }
}