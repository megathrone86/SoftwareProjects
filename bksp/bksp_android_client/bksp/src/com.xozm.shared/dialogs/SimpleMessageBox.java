package com.xozm.shared.dialogs;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by Hozeev on 02.12.13.
 */
public class SimpleMessageBox {
    public static void Show(String msg, Context context) {
        try {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
