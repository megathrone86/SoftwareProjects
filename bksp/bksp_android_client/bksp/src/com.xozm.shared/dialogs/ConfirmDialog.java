package com.xozm.shared.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class ConfirmDialog implements OnClickListener {
    public int OperationCode;

    private Activity parentActivity;
    private DialogListener dialogListener;
    private AlertDialog.Builder alert;
    private String bt_okText;
    private String bt_cancelText;

    public ConfirmDialog(Activity parentActivity, int operationCode) {
        this.parentActivity = parentActivity;
        this.OperationCode = operationCode;
        this.dialogListener = null;
    }

    public void SetDialogProps(String bt_okText, String bt_cancelText) {
        this.bt_okText = bt_okText;
        this.bt_cancelText = bt_cancelText;
    }

    public void SetDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    public void ShowDialog(String message) {
        alert = new AlertDialog.Builder(parentActivity);
        alert.setMessage(message);

        alert.setPositiveButton(bt_okText, this);
        alert.setNegativeButton(bt_cancelText, this);

        parentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alert.show();
            }
        });
    }

    @Override
    public void onClick(DialogInterface arg0, int arg1) {
        if ((arg1 == DialogInterface.BUTTON_POSITIVE) && (dialogListener != null))
            dialogListener.DialogClosed(this, false);

        if ((arg1 == DialogInterface.BUTTON_NEGATIVE) && (dialogListener != null))
            dialogListener.DialogClosed(this, true);
    }
}
