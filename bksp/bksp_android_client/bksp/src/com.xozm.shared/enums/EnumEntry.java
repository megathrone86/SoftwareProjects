package com.xozm.shared.enums;

/**
 * Created by Hozeev on 17.12.13.
 */
public class EnumEntry {
    public Object key;
    public String descr;

    public EnumEntry(Object key, String descr) {
        this.key = key;
        this.descr = descr;
    }
}
