package com.xozm.shared.enums;

import java.util.ArrayList;

/**
 * Created by Hozeev on 17.12.13.
 */
public class EnumEntryList {
    private ArrayList<EnumEntry> list = new ArrayList<>();

    public EnumEntryList() {
    }

    public ArrayList<EnumEntry> getList() {
        return list;
    }

    public void add(Object key, String descr) {
        list.add(new EnumEntry(key, descr));
    }

    public Object getKey(String s) {
        for (EnumEntry e : list) {
            if (e.descr.equals(s) || e.key.toString().equals(s))
                return e.key;
        }
        return null;
    }

    public String getDescr(Object o) {
        for (EnumEntry e : list) {
            if (e.key == o)
                return e.descr;
        }
        return null;
    }
}
