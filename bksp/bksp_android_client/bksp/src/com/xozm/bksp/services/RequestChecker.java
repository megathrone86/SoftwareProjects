package com.xozm.bksp.services;

import com.xozm.bksp.data.Request;
import com.xozm.shared.helpers.StringHelper;

/**
 * Created by x on 08.10.13.
 */
public class RequestChecker {
    public Request request;
    private String message;

    public RequestChecker(Request request) {
        this.request = request;
        this.message = "";

        doCheck();
    }

    public String getMessage() {
        return message;
    }

    public boolean hasErrors() {
        return message.length() > 0;
    }

    private void addError(String error) {
        if (message.length() == 0)
            message = error;
        else
            message = message + "\r\n" + error;
    }

    private void doCheck() {
        if (request.vinNumber.length() <= 0 && request.bodyNumber.length() <= 0)
            addError("Не указан номер VIN или номер кузова");

        if (StringHelper.StringIsEmpty(request.contactName))
            addError("Не указано контактное имя  (в настройках)");


        if (StringHelper.StringIsEmpty(request.contactPhone) && StringHelper.StringIsEmpty(request.contactEmail))
            addError("Не указан контактный телефон или e-mail (в настройках)");
//        if (request.carYear == 0) {
//            addError("Не указан год выпуска.");
//        } else {
//            if (request.carYear < 1900 || request.carYear > Calendar.getInstance().get(Calendar.YEAR) + ximagebutton_inflate)
//                addError("Неверно указан год выпуска.");
//        }
    }
}
