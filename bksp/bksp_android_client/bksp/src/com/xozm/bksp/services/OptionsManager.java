package com.xozm.bksp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xozm.bksp.Global;
import com.xozm.bksp.data.JOptions;
import com.xozm.shared.LockableObject;
import com.xozm.shared.helpers.StorageHelper;
import com.xozm.shared.io.XFile;

import java.io.File;

public class OptionsManager extends LockableObject {
    public static OptionsManager Instance = new OptionsManager();
    private static String FileName = "bksp_options0000.json";
    private volatile JOptions options = null;

    public void updateOptions(JOptions options) {
        boolean locked = Lock();

        try {
            this.options = options;

            saveOptions();
        } finally {
            Unlock(locked);
        }
    }

    public JOptions getOptions() {
        boolean locked = Lock();

        try {
            if (options == null) {
                File optionsFile = new File(StorageHelper.getAppDataDir(Global.getPackageName()) + "/" + FileName);
                if (optionsFile.exists()) {
                    byte[] bytes = XFile.getBytes(optionsFile);

                    ObjectMapper mapper = new ObjectMapper();
                    options = mapper.readValue(bytes, JOptions.class);
                } else
                    options = new JOptions();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            Unlock(locked);
        }

        return options;
    }

    public void saveOptions() {
        boolean locked = Lock();

        try {
            String path = StorageHelper.getAppDataDir(Global.getPackageName()) + "/" + FileName;
            File file = new File(path);
            File tmpFile = new File(path + ".tmp");

            ObjectMapper mapper = new ObjectMapper();
            byte[] body = mapper.writeValueAsBytes(getOptions());

            try {
                StorageHelper.createDirForFile(tmpFile);
                XFile.writeBytes(body, tmpFile);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (tmpFile.exists()) {
                    if (file.exists())
                        file.delete();
                    tmpFile.renameTo(file);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            Unlock(locked);
        }
    }
}
