package com.xozm.bksp.services;

import android.annotation.TargetApi;
import android.os.Build;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xozm.bksp.Global;
import com.xozm.bksp.data.JMessage;
import com.xozm.bksp.data.JWorkflow;
import com.xozm.bksp.data.Workflow;
import com.xozm.shared.helpers.DateTimeHelper;
import com.xozm.shared.helpers.JsonHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.net.URL;

/**
 * Created by x on 09.10.13.
 */
public class MessageSender {
    private Workflow workflow;
    private long serverID;
    private String messageText;

    public MessageSender(Workflow workflow, String messageText) {
        this.workflow = workflow;
        this.messageText = messageText;
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    private String getMessageJson() throws Exception {
        JMessage jMessage = new JMessage();
        jMessage.content = messageText;
        jMessage.direction = "USER_TO_OPERATOR";

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(jMessage);
    }

    public void work() {
        try {
            URL url = new URL(Global.getWorkflowMessagesURL(workflow.serverID));
            HttpPost httpPost = new HttpPost(url.toURI());
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Accept-Encoding", "application/json");
            httpPost.setHeader("Accept-Language", "en-US");

            String jsonString = getMessageJson();
            httpPost.setEntity(new StringEntity(jsonString, "UTF-8"));

            HttpClient httpClient = new DefaultHttpClient();
            httpClient.execute(httpPost);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
