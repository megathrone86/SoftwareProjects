package com.xozm.bksp.services;

import android.annotation.TargetApi;
import android.os.Build;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xozm.bksp.Global;
import com.xozm.bksp.data.JWorkflow;
import com.xozm.bksp.data.Workflow;
import com.xozm.shared.helpers.DateTimeHelper;
import com.xozm.shared.helpers.JsonHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.net.URL;

/**
 * Created by x on 09.10.13.
 */
public class RequestSender {
    private Workflow _workflow;
    private long _serverID;

    public RequestSender(Workflow workflow) {
        _workflow = workflow;
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    private String getWorkflowJson(Workflow workflow) throws Exception {
        JWorkflow jWorkflow = new JWorkflow();
        jWorkflow.closed = false;
        jWorkflow.request = workflow.request;

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(jWorkflow);
    }

    public void work() {
        try {
            URL url = new URL(Global.getWorkflowsURL());
            HttpPost httpPost = new HttpPost(url.toURI());
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Accept-Encoding", "application/json");
            httpPost.setHeader("Accept-Language", "en-US");

            String jsonString = getWorkflowJson(_workflow);
            httpPost.setEntity(new StringEntity(jsonString, "UTF-8"));

            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpPost);
            JSONObject jsonResponse = JsonHelper.JSONObjectfromHttpResponse(httpResponse);
            parseResponse(jsonResponse);

            _workflow.sendDate = DateTimeHelper.getCurrentTime().toString();
            _workflow.serverID = _serverID;

            WorkflowsManager.Instance.saveWorkflows();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void parseResponse(JSONObject jsonResponse) throws Exception {
        _serverID = jsonResponse.getLong("id");

        if (_serverID <= 0)
            throw new Exception("Неправильный формат ответа.");
    }
}
