package com.xozm.bksp.services;

import android.app.IntentService;
import android.content.Intent;

import com.xozm.bksp.Global;
import com.xozm.bksp.data.TextMessage;
import com.xozm.bksp.data.Workflow;
import com.xozm.shared.LockableObject;
import com.xozm.shared.helpers.JsonHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by x on 11.01.14.
 */
public class WorkflowUpdaterService extends IntentService {
    public static WorkflowUpdaterService Instance = null;
    private static int updateIntervalInMinutes = 60;
    private static int stopCheckIntervalInMsecs = 5 * 1000; //изменяется по ходу пьесы
    private LockableObject lockableObject = new LockableObject();
    private boolean stopping = false;
    private Date lastUpdate;

    public WorkflowUpdaterService() {
        super("WorkflowUpdaterService");

        Instance = this;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            mainFunc();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void mainFunc() throws Exception {
        int checksPerMinute = 1000 * updateIntervalInMinutes / stopCheckIntervalInMsecs;

        while (!stopping) {
            updateWorkflows();
            lastUpdate = Calendar.getInstance().getTime();

            for (int i = 0; i < updateIntervalInMinutes * checksPerMinute; i++) {
                if (stopping)
                    return;
                Thread.sleep(stopCheckIntervalInMsecs);
            }
        }
    }

    private void updateWorkflows() throws Exception {
        boolean locked = lockableObject.Lock();

        try {
            for (Workflow w : WorkflowsManager.Instance.getWorkflows()) {
                if (w.serverID <= 0 || w.closed)
                    continue;

                updateWorkflow(w);
                updateWorkflowMessages(w);

                WorkflowsManager.Instance.addOrUpdateWorkflow(w);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            lockableObject.Unlock(locked);
        }

    }

    public void updateSeparateWorkflowMessages(Workflow w) {
        boolean locked = lockableObject.Lock();
        try {
            updateWorkflowMessages(w);
            WorkflowsManager.Instance.addOrUpdateWorkflow(w);
            lastUpdate = Calendar.getInstance().getTime();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            lockableObject.Unlock(locked);
        }
    }

    private void updateWorkflow(Workflow w) throws Exception {
        String url = Global.getWorkflowURL(w.serverID);
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-Type", "application/json");
        httpGet.setHeader("Accept-Encoding", "application/json");
        httpGet.setHeader("Accept-Language", "en-US");

        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = httpClient.execute(httpGet);
        JSONObject jsonResponse = JsonHelper.JSONObjectfromHttpResponse(httpResponse);

        boolean newClosed = jsonResponse.getBoolean("closed");
        if (newClosed != w.closed)
            workflowStatusChanged(w, newClosed);
        w.closed = newClosed;
    }

    private void updateWorkflowMessages(Workflow w) throws Exception {
        String url = Global.getWorkflowMessagesURL(w.serverID);
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-Type", "application/json");
        httpGet.setHeader("Accept-Encoding", "application/json");
        httpGet.setHeader("Accept-Language", "en-US");

        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = httpClient.execute(httpGet);
        JSONArray jsonResponse = JsonHelper.JSONArrayfromHttpResponse(httpResponse);

        for (int i = 0; i < jsonResponse.length(); i++) {
            JSONObject jsonObject = jsonResponse.getJSONObject(i);

            TextMessage newTextMessage = new TextMessage();
            newTextMessage.id = jsonObject.getString("id");

            boolean tmFound = false;
            for (TextMessage tm : w.textMessages) {
                if (tm.id.equals(newTextMessage.id)) {
                    tmFound = true;
                    break;
                }
            }
            if (tmFound)
                continue;

            newTextMessage.content = jsonObject.getString("content");
            newTextMessage.direction = jsonObject.getString("direction");

            SimpleDateFormat parserSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            String date = jsonObject.getString("date").substring(0, 23);
            newTextMessage.date = parserSDF.parse(date);
            newMessageReceived(w, newTextMessage);
            w.textMessages.add(newTextMessage);
        }
    }

    private void newMessageReceived(Workflow w, TextMessage newTextMessage) {
        //TODO: вывести уведомление в трей
    }

    private void workflowStatusChanged(Workflow w, boolean newClosed) {
        //TODO: вывести уведомление в трей
    }
}
