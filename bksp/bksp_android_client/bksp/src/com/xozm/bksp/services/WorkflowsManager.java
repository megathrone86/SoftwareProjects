package com.xozm.bksp.services;

import android.util.Log;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xozm.bksp.Global;
import com.xozm.bksp.data.Workflow;
import com.xozm.shared.LockableObject;
import com.xozm.shared.helpers.StorageHelper;
import com.xozm.shared.io.XFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WorkflowsManager extends LockableObject {
    public static WorkflowsManager Instance = new WorkflowsManager();
    private static String FileName = "bksp_workflows0000.json";
    private volatile ArrayList<Workflow> workflows = null;

    public void addOrUpdateWorkflow(Workflow workflow) {
        boolean locked = Lock();

        try {
            List<Workflow> workflows = getWorkflows();

            for (Workflow w : workflows) {
                if (w.workflowGuid.equals(workflow.workflowGuid)) {
                    workflows.remove(w);
                    break;
                }
            }

            workflows.add(workflow);

            saveWorkflows();
        } finally {
            Unlock(locked);
        }
    }

    public void deleteWorkflow(Workflow workflow) {
        boolean locked = Lock();

        try {
            List<Workflow> workflows = getWorkflows();

            for (Workflow w : workflows) {
                if (w.workflowGuid.equals(workflow.workflowGuid)) {
                    workflows.remove(w);
                    break;
                }
            }

            saveWorkflows();
        } finally {
            Unlock(locked);
        }
    }

    public ArrayList<Workflow> getWorkflows() {
        boolean locked = Lock();

        try {
            if (workflows == null) {
                File workflowsFile = new File(StorageHelper.getAppDataDir(Global.getPackageName()) + "/" + FileName);
                if (workflowsFile.exists()) {
                    byte[] bytes = XFile.getBytes(workflowsFile);

                    ObjectMapper mapper = new ObjectMapper();
                    workflows = mapper.readValue(bytes, new TypeReference<ArrayList<Workflow>>() {
                    });
                } else
                    workflows = new ArrayList<>();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            Unlock(locked);
        }

        return workflows;
    }

    public void saveWorkflows() {
        boolean locked = Lock();

        try {
            String path = StorageHelper.getAppDataDir(Global.getPackageName()) + "/" + FileName;
            File file = new File(path);
            File tmpFile = new File(path + ".tmp");

            ObjectMapper mapper = new ObjectMapper();
            byte[] body = mapper.writeValueAsBytes(workflows);

            try {
                StorageHelper.createDirForFile(tmpFile);
                XFile.writeBytes(body, tmpFile);
            } catch (Exception ex) {
                Log.w("Ошибка при сохранении списка документов.", ex.toString());
            } finally {
                if (tmpFile.exists()) {
                    if (file.exists())
                        file.delete();
                    tmpFile.renameTo(file);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            Unlock(locked);
        }

        Log.w("", "settings saved");
    }

    public Workflow findByGuid(String guid) {
        for (Workflow w : getWorkflows()) {
            if (w.workflowGuid.equals(guid))
                return w;
        }

        return null;
    }
}
