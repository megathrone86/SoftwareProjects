package com.xozm.bksp.services;

import com.xozm.bksp.data.Workflow;

public class RequestSaver {
    public Workflow workflow;

    public RequestSaver(Workflow workflow) {
        this.workflow = workflow;
    }

    public void work() {
        WorkflowsManager.Instance.addOrUpdateWorkflow(workflow);
    }
}
