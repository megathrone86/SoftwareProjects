package com.xozm.bksp.gui;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xozm.bksp.R;
import com.xozm.bksp.services.OptionsManager;
import com.xozm.bksp.services.WorkflowsManager;
import com.xozm.bksp.data.Workflow;
import com.xozm.shared.gui.GuiHelper;
import com.xozm.shared.gui.XImageButton;
import com.xozm.shared.helpers.RandomHelper;
import com.xozm.shared.helpers.StringHelper;

import java.util.ArrayList;

/**
 * Created by Hozeev on 25.12.13.
 */
public class WorkflowsActivityRefresher {
    private final ArrayList<LinearLayout> rows = new ArrayList<>();
    private WorkflowsActivity activity;
    private LinearLayout emptyWorkflowsMessageLayout;
    private LinearLayout emptyOptionsMessageLayout;

    public WorkflowsActivityRefresher(WorkflowsActivity activity) {
        this.activity = activity;
    }

    private void removeRow(LinearLayout ll) {
        activity.getWorkflowsListLayout().removeView(ll);
        rows.remove(ll);
    }

    private boolean removeDeletedWorkflows() {
        for (LinearLayout ll : rows) {
            Workflow w = (Workflow) ll.getTag();
            if (w == null) {
                removeRow(ll);
                return true;
            } else {
                Workflow wDB = WorkflowsManager.Instance.findByGuid(w.workflowGuid);
                if (wDB == null) {
                    removeRow(ll);
                    return true;
                }
            }
        }
        return false;
    }

    public LinearLayout findByGuid(String guid) {
        for (LinearLayout ll : rows) {
            Workflow w = (Workflow) ll.getTag();
            if (w.workflowGuid.equals(guid))
                return ll;
        }
        return null;
    }

    private void createNewRow(final Workflow w) {
        LinearLayout ll = GuiHelper.CreateLinearLayout(activity.getWorkflowsListLayout(), LinearLayout.VERTICAL, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, w);
        rows.add(0, ll);
        ll.setOnClickListener(activity.currentOrderClickListener);

        {
            LinearLayout ll1 = GuiHelper.CreateLinearLayout(ll, LinearLayout.HORIZONTAL, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, RowComponent.LL1);
            ll1.setOnClickListener(activity.currentOrderClickListener);

            TextView tw1 = GuiHelper.CreateTextView(ll1, R.layout.textview_inflate, RowComponent.CreateDate);
            tw1.setOnClickListener(activity.currentOrderClickListener);

            TextView tw2 = GuiHelper.CreateTextView(ll1, R.layout.textview_inflate, RowComponent.SendDate);
            tw2.setOnClickListener(activity.currentOrderClickListener);
        }
    }

    private void updateRow(LinearLayout ll) {
        Workflow w = (Workflow) ll.getTag();
        w = WorkflowsManager.Instance.findByGuid(w.workflowGuid);
        ll.setTag(w);

        {
            TextView tw = (TextView) ll.findViewWithTag(RowComponent.CreateDate);
            tw.setText("Создан " + w.createDate);
        }

        {
            TextView tw = (TextView) ll.findViewWithTag(RowComponent.SendDate);
            if (StringHelper.StringIsEmpty(w.sendDate))
                tw.setText("Не отправлен");
            else
                tw.setText("Отправлен " + w.sendDate);
        }
    }

    public void refreshWorkflows() {
        ArrayList<Workflow> workflows = WorkflowsManager.Instance.getWorkflows();

        //удалим с формы удаленные отправки
        while (removeDeletedWorkflows()) ;

        //теперь добавим новые отправки на форму
        for (Workflow w : workflows) {
            if (findByGuid(w.workflowGuid) == null) {
                createNewRow(w);
            }
        }

        //теперь обновим все что осталось
        for (LinearLayout ll : rows) {
            updateRow(ll);
        }

        if (OptionsManager.Instance.getOptions().isEmpty())
            createEmptyOptionsMessage();
        else
            deleteEmptyOptionsMessage();

        if (rows.size() == 0)
            createEmptyWorkflowsMessage();
        else
            deleteEmptyWorkflowsMessage();
    }

    private void createEmptyWorkflowsMessage() {
        if (emptyWorkflowsMessageLayout != null)
            return;

        emptyWorkflowsMessageLayout = GuiHelper.CreateLinearLayout(activity.getWorkflowsListLayout(), LinearLayout.VERTICAL, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, null);

        TextView tw = GuiHelper.CreateTextView(emptyWorkflowsMessageLayout, R.layout.textview_inflate, null);
        tw.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tw.setText("У вас нет ни одного заказа.");

        XImageButton ib = GuiHelper.CreateXImageButton(emptyWorkflowsMessageLayout, R.layout.ximagebutton_inflate, null);
        ib.setImageResource(R.drawable.new_order);
        ib.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ib.setOnClickListener(activity.btNewOrderClickListener);
    }

    private void deleteEmptyWorkflowsMessage() {
        if (emptyWorkflowsMessageLayout == null)
            return;

        activity.getWorkflowsListLayout().removeView(emptyWorkflowsMessageLayout);
    }

    private void createEmptyOptionsMessage() {
        if (emptyOptionsMessageLayout != null)
            return;

        emptyOptionsMessageLayout = GuiHelper.CreateLinearLayout(activity.getWorkflowsListLayout(), LinearLayout.VERTICAL, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, null);

        TextView tw = GuiHelper.CreateTextView(emptyOptionsMessageLayout, R.layout.textview_inflate, null);
        tw.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tw.setText("Не заполнены настройки.");

        XImageButton ib = GuiHelper.CreateXImageButton(emptyOptionsMessageLayout, R.layout.ximagebutton_inflate, null);
        ib.setImageResource(R.drawable.options);
        ib.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ib.setOnClickListener(activity.btOptionsClickListener);
    }

    private void deleteEmptyOptionsMessage() {
        if (emptyOptionsMessageLayout != null)
            return;

        activity.getWorkflowsListLayout().removeView(emptyOptionsMessageLayout);
    }

    enum RowComponent {
        LL, LL1, CreateDate, SendDate, LL2, BTEdit, BTDelete, BTSend, BTView;
        private final int id;

        private RowComponent() {
            id = 1 + RandomHelper.Random.nextInt();
        }

        public int toInt() {
            return id;
        }
    }
}
