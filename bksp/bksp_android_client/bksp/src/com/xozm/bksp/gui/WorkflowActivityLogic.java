package com.xozm.bksp.gui;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.xozm.bksp.Global;
import com.xozm.bksp.R;
import com.xozm.bksp.services.OptionsManager;
import com.xozm.bksp.services.RequestChecker;
import com.xozm.bksp.services.RequestSaver;
import com.xozm.bksp.services.RequestSender;
import com.xozm.bksp.data.JOptions;
import com.xozm.bksp.data.Request;
import com.xozm.bksp.data.Workflow;
import com.xozm.bksp.data.enums.CarBodyType;
import com.xozm.bksp.data.enums.CarDriveType;
import com.xozm.bksp.data.enums.CarFuelType;
import com.xozm.bksp.data.enums.CarSteeringWheelType;
import com.xozm.shared.gui.ActivityHelper;
import com.xozm.shared.helpers.IntHelper;

/**
 * Created by Hozeev on 25.12.13.
 */
public class WorkflowActivityLogic {
    private WorkflowActivity activity;

    public WorkflowActivityLogic(WorkflowActivity activity) {
        this.activity = activity;
    }

    public Workflow getCurrentWorkflow() {
        return activity.getCurrentWorkflow();
    }

    public ActivityHelper getActivityHelper() {
        return activity.getActivityHelper();
    }

    public void DataToGui() {
        Request request = getCurrentWorkflow().request;

        {
            String partNames = "";
            for (String s : request.partNames)
                partNames += s + "\n";
            if (partNames.length() > 0)
                partNames = partNames.substring(0, partNames.length() - 1);
            getActivityHelper().setEditTextValue(R.id.etPartName, partNames);
        }

        getActivityHelper().setEditTextValue(R.id.etVin, request.vinNumber);
        getActivityHelper().setEditTextValue(R.id.etBodyNum, request.bodyNumber);

        if (request.carYear == 0)
            getActivityHelper().setEditTextValue(R.id.etYear, "");
        else
            getActivityHelper().setEditTextValue(R.id.etYear, Integer.toString(request.carYear));

        getActivityHelper().setEditTextValue(R.id.etBrand, request.carBrandName);
        getActivityHelper().setEditTextValue(R.id.etModel, request.carModel);
        getActivityHelper().setEditTextValue(R.id.etEngine, request.carEngineModel);
        getActivityHelper().setEditTextValue(R.id.etBodyType, CarBodyType.getDescr(request.carBodyType));
        getActivityHelper().setEditTextValue(R.id.etFuelType, CarFuelType.getDescr(request.carFuelType));
        getActivityHelper().setEditTextValue(R.id.etDriveType, CarDriveType.getDescr(request.carDriveType));
        getActivityHelper().setEditTextValue(R.id.etSteering, CarSteeringWheelType.getDescr(request.steeringWheelType));
        getActivityHelper().setEditTextValue(R.id.etDescription, request.description);
    }

    private void GuiToData() {
        Request request = new Request();

        {
            String s = getActivityHelper().getEditTextValue(R.id.etPartName);
            String[] s2 = s.split("\n");
            request.partNames = s2;
        }

        request.vinNumber = getActivityHelper().getEditTextValue(R.id.etVin);
        request.bodyNumber = getActivityHelper().getEditTextValue(R.id.etBodyNum);
        request.carYear = IntHelper.parseInt(getActivityHelper().getEditTextValue(R.id.etYear), 0);
        request.carBrandName = getActivityHelper().getEditTextValue(R.id.etBrand);
        request.carModel = getActivityHelper().getEditTextValue(R.id.etModel);
        request.carEngineModel = getActivityHelper().getEditTextValue(R.id.etEngine);
        request.carBodyType = CarBodyType.getKey(getActivityHelper().getEditTextValue(R.id.etBodyType));
        request.carFuelType = CarFuelType.getKey(getActivityHelper().getEditTextValue(R.id.etFuelType));
        request.carDriveType = CarDriveType.getKey(getActivityHelper().getEditTextValue(R.id.etDriveType));
        request.steeringWheelType = CarSteeringWheelType.getKey(getActivityHelper().getEditTextValue(R.id.etSteering));
        request.description = getActivityHelper().getEditTextValue(R.id.etDescription);

        JOptions jo = OptionsManager.Instance.getOptions();
        request.contactName = jo.contactName;
        request.contactPhone = jo.contactPhone;
        request.contactEmail = jo.contactEmail;
        request.subscribe = jo.subscribe;

        getCurrentWorkflow().request = request;
    }

    public void btCancelClick(View view) {
        try {
            activity.finish();
            activity.setResult(Activity.RESULT_CANCELED, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void btSaveClick(View view) {
        try {
            //TODO: disable all
            GuiToData();

            final RequestSaver requestSaver = new RequestSaver(activity.getCurrentWorkflow());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final RequestChecker checker = new RequestChecker(activity.getCurrentWorkflow().request);
                        if (checker.hasErrors()) {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(activity.getApplicationContext(),
                                            checker.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                        requestSaver.work();

                        //TODO: enable all

                        activity.setResult(1);
                        activity.finish();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void btSendClick(View view) {
        try {
            //TODO: disable all
            GuiToData();

            final RequestSaver requestSaver = new RequestSaver(activity.getCurrentWorkflow());
            final RequestSender requestSender = new RequestSender(activity.getCurrentWorkflow());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final RequestChecker checker = new RequestChecker(activity.getCurrentWorkflow().request);
                        if (checker.hasErrors()) {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(activity.getApplicationContext(),
                                            checker.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        }

                        requestSaver.work();
                        requestSender.work();

                        //TODO: enable all

                        activity.setResult(1);
                        activity.finish();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void btNewMessageClick(View view) {
        try {
            Intent intent = new Intent(activity, MessageActivity.class);
            intent.putExtra("workflowGuid", getCurrentWorkflow().workflowGuid);
            activity.startActivityForResult(intent, Global.ActivityCode.MessageActivity.toInt());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
