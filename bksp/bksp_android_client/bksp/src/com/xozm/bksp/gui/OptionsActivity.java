package com.xozm.bksp.gui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.xozm.bksp.Global;
import com.xozm.bksp.R;
import com.xozm.bksp.services.OptionsManager;
import com.xozm.bksp.data.JOptions;
import com.xozm.shared.gui.ActivityHelper;

/**
 * Created by Hozeev on 25.12.13.
 */
public class OptionsActivity extends Activity {
    private ActivityHelper activityHelper = new ActivityHelper(this);
    private JOptions jo;

    private void DataToGui() {
        activityHelper.setEditTextValue(R.id.etContactName, jo.contactName);
        activityHelper.setEditTextValue(R.id.etContactPhone, jo.contactPhone);
        activityHelper.setEditTextValue(R.id.etContactEmail, jo.contactEmail);
        activityHelper.setCheckBoxValue(R.id.checkBox, jo.subscribe);
    }

    private void GuiToData() {
        jo.contactName = activityHelper.getEditTextValue(R.id.etContactName);
        jo.contactPhone = activityHelper.getEditTextValue(R.id.etContactPhone);
        jo.contactEmail = activityHelper.getEditTextValue(R.id.etContactEmail);
        jo.subscribe = activityHelper.getCheckBoxValue(R.id.checkBox);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.options_activity);

        activityHelper.findXImageButton(R.id.btCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btCancelClick(view);
            }
        });
        activityHelper.findXImageButton(R.id.btSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btSaveClick(view);
            }
        });
        activityHelper.findXImageView(R.id.bksp_logo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.bkspLogoClicked(view);
            }
        });

        jo = OptionsManager.Instance.getOptions();
        DataToGui();
    }

    public void btCancelClick(View view) {
        finish();
    }

    public void btSaveClick(View view) {
        try {
            GuiToData();
            OptionsManager.Instance.updateOptions(jo);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}