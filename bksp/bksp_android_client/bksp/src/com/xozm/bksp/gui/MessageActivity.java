package com.xozm.bksp.gui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.xozm.bksp.Global;
import com.xozm.bksp.R;
import com.xozm.bksp.data.Workflow;
import com.xozm.bksp.services.MessageSender;
import com.xozm.bksp.services.RequestChecker;
import com.xozm.bksp.services.RequestSender;
import com.xozm.bksp.services.WorkflowUpdaterService;
import com.xozm.bksp.services.WorkflowsManager;
import com.xozm.shared.gui.ActivityHelper;
import com.xozm.shared.helpers.StringHelper;

/**
 * Created by Hozeev on 25.12.13.
 */
public class MessageActivity extends Activity {
    private ActivityHelper activityHelper = new ActivityHelper(this);
    private Workflow currentWorkflow;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.message_activity);

        String workflowGuid = getIntent().getStringExtra("workflowGuid");
        currentWorkflow = WorkflowsManager.Instance.findByGuid(workflowGuid);

        activityHelper.findXImageButton(R.id.btCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btCancelClick(view);
            }
        });
        activityHelper.findXImageButton(R.id.btSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btSendClick(view);
            }
        });
        activityHelper.findXImageView(R.id.bksp_logo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.bkspLogoClicked(view);
            }
        });
    }

    public void btCancelClick(View view) {
        finish();
    }

    public void btSendClick(View view) {
        try {
            String text = activityHelper.getEditTextValue(R.id.etMessageText);
            final MessageSender messageSender = new MessageSender(currentWorkflow, text);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        messageSender.work();
                        WorkflowUpdaterService.Instance.updateSeparateWorkflowMessages(currentWorkflow);

                        //TODO: enable all

                        setResult(1);
                        finish();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();

            //TODO: disable all
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}