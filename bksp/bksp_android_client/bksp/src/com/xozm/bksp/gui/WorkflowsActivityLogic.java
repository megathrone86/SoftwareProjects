package com.xozm.bksp.gui;

import android.content.Intent;
import android.view.View;

import com.xozm.bksp.Global;
import com.xozm.bksp.data.Workflow;

/**
 * Created by Hozeev on 25.12.13.
 */
public class WorkflowsActivityLogic {
    private WorkflowsActivity activity;

    public WorkflowsActivityLogic(WorkflowsActivity activity) {
        this.activity = activity;
    }

    public void currentOrderClick(View view) {
        try {
            if (view == null)
                return;
            if (view.getTag() != null && view.getTag().getClass() != Workflow.class) {
                currentOrderClick((View) view.getParent());
                return;
            }

            Workflow w = (Workflow) view.getTag();

            Intent intent = new Intent(activity, WorkflowActivity.class);
            intent.putExtra("workflowGuid", w.workflowGuid);
            activity.startActivityForResult(intent, Global.ActivityCode.WorkflowActivity.toInt());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void btNewOrderClick(View view) {
        try {
            Intent intent = new Intent(activity, WorkflowActivity.class);
            activity.startActivityForResult(intent, Global.ActivityCode.WorkflowActivity.toInt());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void btOptionsClick(View view) {
        try {
            Intent intent = new Intent(activity, OptionsActivity.class);
            activity.startActivityForResult(intent, Global.ActivityCode.OptionsActivity.toInt());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


//            {
//                TextView tw = GuiHelper.CreateTextView(getApplicationContext(), ll2, RowComponent.BTDelete);
//                tw.setText("Удалить");
//                tw.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        try {
//                            WorkflowsManager.Instance.deleteWorkflow(w);
//                            //mainActivity.clearCurrentWorkflow();
//                        } catch (Exception ex) {
//                            ex.printStackTrace();
//                        }
//                    }
//                });
//            }
}
