package com.xozm.bksp.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.xozm.bksp.Global;
import com.xozm.bksp.R;
import com.xozm.bksp.services.WorkflowUpdaterService;
import com.xozm.shared.gui.ActivityHelper;
import com.xozm.shared.helpers.RandomHelper;

/**
 * Created by Hozeev on 23.12.13.
 */
public class WorkflowsActivity extends Activity {

    public View.OnClickListener currentOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            logic.currentOrderClick(view);
        }
    };
    public View.OnClickListener btNewOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            logic.btNewOrderClick(view);
        }
    };
    public View.OnClickListener btOptionsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            logic.btOptionsClick(view);
        }
    };
    private ActivityHelper activityHelper = new ActivityHelper(this);
    private WorkflowsActivityLogic logic = new WorkflowsActivityLogic(this);
    private WorkflowsActivityRefresher refresher = new WorkflowsActivityRefresher(this);
    private LinearLayout workflowsListLayout;

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.workflows_activity);

            workflowsListLayout = activityHelper.findLinearLayout(R.id.workflowslistlayout);

            activityHelper.findXImageButton(R.id.btNewOrder).setOnClickListener(btNewOrderClickListener);
            activityHelper.findXImageButton(R.id.btOptions).setOnClickListener(btOptionsClickListener);
            activityHelper.findXImageView(R.id.bksp_logo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Global.bkspLogoClicked(view);
                }
            });

            refreshWorkflows();

            startMainService();
            startMainService();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public LinearLayout getWorkflowsListLayout() {
        return workflowsListLayout;
    }

    public void refreshWorkflows() {
        refresher.refreshWorkflows();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == Global.ActivityCode.WorkflowActivity.toInt())
                refreshWorkflows();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void startMainService() {
        Intent myIntent = new Intent(this, WorkflowUpdaterService.class);
        startService(myIntent);
    }

    private void stopMainService() {
        Intent myIntent = new Intent(this, WorkflowUpdaterService.class);
        stopService(myIntent);
    }
}