package com.xozm.bksp.gui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xozm.bksp.Global;
import com.xozm.bksp.R;
import com.xozm.bksp.services.WorkflowUpdaterService;
import com.xozm.bksp.services.WorkflowsManager;
import com.xozm.bksp.data.TextMessage;
import com.xozm.bksp.data.Workflow;
import com.xozm.bksp.data.enums.CarBodyType;
import com.xozm.bksp.data.enums.CarDriveType;
import com.xozm.bksp.data.enums.CarFuelType;
import com.xozm.bksp.data.enums.CarSteeringWheelType;
import com.xozm.shared.gui.ActivityHelper;
import com.xozm.shared.gui.XImageButton;
import com.xozm.shared.helpers.ComboBoxHelper;
import com.xozm.shared.helpers.StringHelper;

/**
 * Created by Hozeev on 25.12.13.
 */
public class WorkflowActivity extends Activity {
    private Workflow currentWorkflow;
    private ActivityHelper activityHelper = new ActivityHelper(this);
    private WorkflowActivityLogic logic = new WorkflowActivityLogic(this);
    private FormMode formMode;
    private LinearLayout messagesLL;
    private LinearLayout requestForm;
    private XImageButton btCancel;
    private XImageButton btSave;
    private XImageButton btSend;
    private XImageButton btNewMessage;

    public ActivityHelper getActivityHelper() {
        return activityHelper;
    }

    public Workflow getCurrentWorkflow() {
        return currentWorkflow;
    }

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.workflow_activity);

            String workflowGuid = getIntent().getStringExtra("workflowGuid");
            if (StringHelper.StringIsEmpty(workflowGuid)) {
                currentWorkflow = new Workflow();
                formMode = FormMode.Add;
            } else {
                currentWorkflow = WorkflowsManager.Instance.findByGuid(workflowGuid);
                formMode = StringHelper.StringIsEmpty(currentWorkflow.sendDate) ? FormMode.Edit : FormMode.View;
            }

            messagesLL = activityHelper.findLinearLayout(R.id.messagesForm);
            requestForm = activityHelper.findLinearLayout(R.id.requestForm);
            btCancel = activityHelper.findXImageButton(R.id.btCancel);
            btSave = activityHelper.findXImageButton(R.id.btSave);
            btSend = activityHelper.findXImageButton(R.id.btSend);
            btNewMessage = activityHelper.findXImageButton(R.id.btNewMessage);

            activityHelper.findTextView(R.id.header).setText(formMode.toString());
            btCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logic.btCancelClick(view);
                }
            });
            btSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logic.btSaveClick(view);
                }
            });
            btSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logic.btSendClick(view);
                }
            });
            btNewMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logic.btNewMessageClick(view);
                }
            });
            activityHelper.findXImageView(R.id.bksp_logo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Global.bkspLogoClicked(view);
                }
            });

            refreshWorkflow();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void refreshWorkflow() {
        currentWorkflow = WorkflowsManager.Instance.findByGuid(currentWorkflow.workflowGuid);

        applyFormMode();
        logic.DataToGui();
    }

    @SuppressLint("NewApi")
    private void applyFormMode() {
        if (formMode == FormMode.View) {
            for (int i = 0; i < requestForm.getChildCount(); i++) {
                requestForm.getChildAt(i).setEnabled(false);
            }

            messagesLL.removeAllViews();
            currentWorkflow.sortMessages();
            for (TextMessage textMessage : currentWorkflow.textMessages) {
                LayoutInflater inflater = (LayoutInflater) messagesLL.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout rowLL = (LinearLayout) inflater.inflate(R.layout.messagerow_inflate, null);
                messagesLL.addView(rowLL);
                rowLL.setGravity(textMessage.isInbound() ? Gravity.RIGHT : Gravity.LEFT);

                ((TextView) rowLL.findViewById(R.id.messageHeader)).setText(textMessage.getHeader());
                ((TextView) rowLL.findViewById(R.id.messageText)).setText(textMessage.content);

                btSave.setVisibility(View.GONE);
                btSend.setVisibility(View.GONE);
            }
        } else {
            ComboBoxHelper.CreateComboBox(this, R.id.etBodyType, CarBodyType.getMap());
            ComboBoxHelper.CreateComboBox(this, R.id.etDriveType, CarDriveType.getMap());
            ComboBoxHelper.CreateComboBox(this, R.id.etFuelType, CarFuelType.getMap());
            ComboBoxHelper.CreateComboBox(this, R.id.etSteering, CarSteeringWheelType.getMap());

            btNewMessage.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == Global.ActivityCode.MessageActivity.toInt()) {
                refreshWorkflow();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    enum FormMode {
        Add, Edit, View;

        public String toString() {
            switch (this) {
                case Add:
                    return "Создание нового запроса";
                case Edit:
                    return "Редактирование запроса";
                case View:
                    return "Просмотр запроса";
                default:
                    return null;
            }
        }
    }
}