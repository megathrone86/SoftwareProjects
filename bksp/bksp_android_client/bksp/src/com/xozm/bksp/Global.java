package com.xozm.bksp;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.xozm.shared.helpers.RandomHelper;

/**
 * Created by Hozeev on 15.12.13.
 */
public class Global {
    public static String getWorkflowsURL() {
        return "http://automobileserv.appspot.com/workflow/";
    }

    public static String getWorkflowURL(long workflowServerID) {
        return getWorkflowsURL() + Long.toString(workflowServerID) + "/";
    }

    public static String getWorkflowMessagesURL(long workflowServerID) {
        return getWorkflowURL(workflowServerID) + "messages/";
    }

    public static void bkspLogoClicked(View view) {
        try {
            Intent i = new Intent("android.intent.action.VIEW", Uri.parse("http://bksp40.ru"));
            view.getContext().startActivity(i);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getPackageName() {
        return Global.class.getPackage().getName();
    }

    public enum ActivityCode {
        WorkflowActivity, OptionsActivity, MessageActivity;
        private final int id;

        private ActivityCode() {
            id = 1 + Math.abs(RandomHelper.Random.nextInt(10000));
        }

        public int toInt() {
            return id;
        }
    }
}
