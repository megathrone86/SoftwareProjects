package com.xozm.bksp.data.enums;

import com.xozm.shared.enums.EnumEntryList;

/**
 * Created by Hozeev on 17.12.13.
 */
public enum CarDriveType {
    // Передний
    FRONT_WHEEL,
    // Полный
    FULL,
    // Задний
    REAR;
    private static EnumEntryList map = null;

    public static EnumEntryList getMap() {
        if (map == null) {
            map = new EnumEntryList();
            map.add(CarDriveType.FRONT_WHEEL, "Передний");
            map.add(CarDriveType.FULL, "Полный");
            map.add(CarDriveType.REAR, "Задний");
        }
        return map;
    }

    public static CarDriveType getKey(String s) {
        return (CarDriveType) getMap().getKey(s);
    }

    public static String getDescr(CarDriveType t) {
        return getMap().getDescr(t);
    }
}
