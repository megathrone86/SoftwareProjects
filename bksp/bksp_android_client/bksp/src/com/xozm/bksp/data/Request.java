package com.xozm.bksp.data;

import com.xozm.bksp.data.enums.CarBodyType;
import com.xozm.bksp.data.enums.CarDriveType;
import com.xozm.bksp.data.enums.CarFuelType;
import com.xozm.bksp.data.enums.CarSteeringWheelType;

/**
 * Created by Hozeev on 03.10.13.
 */

public class Request {
    //Имя пользователя
    public String user;
    //Наименование детали
    public String[] partNames;
    //Номер VIN
    public String vinNumber;
    //Номер кузова (для машин японского и американского рынка)
    public String bodyNumber;
    //Год выпуска
    public int carYear;
    //Марка машины
    public String carBrandName;
    //Модель машины
    public String carModel;
    //Модель или объем двигателя
    public String carEngineModel;
    //Тип кузова
    public CarBodyType carBodyType;
    //Топливо:
    public CarFuelType carFuelType;
    //Тип привода:
    public CarDriveType carDriveType;
    //Руль:
    public CarSteeringWheelType steeringWheelType;
    //Примечания
    public String description;
    //Контактная информация: имя
    public String contactName;
    //Контактная информация: телефон
    public String contactPhone;
    //Контактная информация: email
    public String contactEmail;
    //Хочу получать информацию об акциях, мероприятиях и других важных событиях компании BK Spare Parts
    public boolean subscribe;

    public Request() {
        partNames = new String[0];
    }
}
