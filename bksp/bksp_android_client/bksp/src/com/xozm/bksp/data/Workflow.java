package com.xozm.bksp.data;

import com.xozm.shared.helpers.DateTimeHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Created by x on 08.10.13.
 */
public class Workflow {
    //Уникальный локальный идентификатор документоборота
    public String workflowGuid;
    //Дата отправки
    public String sendDate;
    //Идентицикатор отправки на сервере (по которому будем запрашивать обновления)
    //Если отправка еще не произведена, равен "-ximagebutton_inflate"
    public long serverID;
    //Дата создания документоборота
    public String createDate;
    //Запрос
    public Request request;
    //Список сообщений
    public List<TextMessage> textMessages;
    //Закрыт ли
    public boolean closed;

    public Workflow() {
        request = new Request();

        workflowGuid = UUID.randomUUID().toString();
        createDate = DateTimeHelper.getCurrentTime().toString();
        sendDate = "";
        serverID = -1;
        textMessages = new ArrayList<>();
    }

    public void sortMessages() {
        Collections.sort(textMessages, new Comparator<TextMessage>() {
            @Override
            public int compare(TextMessage textMessage, TextMessage textMessage2) {
                try {
                    return textMessage.date.compareTo(textMessage2.date);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return 0;
                }
            }
        });
    }
}
