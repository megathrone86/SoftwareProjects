package com.xozm.bksp.data.enums;

import com.xozm.shared.enums.EnumEntryList;

/**
 * Created by Hozeev on 17.12.13.
 */
public enum CarFuelType {
    // Бензин
    GASOLINE,
    // Дизель
    DIESEL,
    // Газ
    GAS;
    private static EnumEntryList map = null;

    public static EnumEntryList getMap() {
        if (map == null) {
            map = new EnumEntryList();
            map.add(CarFuelType.GASOLINE, "Бензин");
            map.add(CarFuelType.DIESEL, "Дизель");
            map.add(CarFuelType.GAS, "Газ");
        }
        return map;
    }

    public static CarFuelType getKey(String s) {
        return (CarFuelType) getMap().getKey(s);
    }

    public static String getDescr(CarFuelType t) {
        return getMap().getDescr(t);
    }
}
