package com.xozm.bksp.data.enums;

import com.xozm.shared.enums.EnumEntryList;

/**
 * Created by Hozeev on 17.12.13.
 */
public enum CarSteeringWheelType {
    //Левый
    LEFT,
    //Правый
    RIGHT;
    private static EnumEntryList map = null;

    public static EnumEntryList getMap() {
        if (map == null) {
            map = new EnumEntryList();
            map.add(CarSteeringWheelType.LEFT, "Левый");
            map.add(CarSteeringWheelType.RIGHT, "Правый");
        }
        return map;
    }

    public static CarSteeringWheelType getKey(String s) {
        return (CarSteeringWheelType) getMap().getKey(s);
    }

    public static String getDescr(CarSteeringWheelType t) {
        return getMap().getDescr(t);
    }
}
