package com.xozm.bksp.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xozm.shared.helpers.StringHelper;

/**
 * Created by Hozeev on 26.12.13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JOptions {
    //Контактная информация: имя
    public String contactName;
    //Контактная информация: телефон
    public String contactPhone;
    //Контактная информация: email
    public String contactEmail;
    //Хочу получать информацию об акциях, мероприятиях и других важных событиях компании BK Spare Parts
    public boolean subscribe;

    public boolean isEmpty() {
        return StringHelper.StringIsEmpty(contactName) ||
                (StringHelper.StringIsEmpty(contactPhone) && StringHelper.StringIsEmpty(contactEmail));
    }
}
