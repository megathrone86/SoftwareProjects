package com.xozm.bksp.data.enums;

import com.xozm.shared.enums.EnumEntryList;

/**
 * Created by Hozeev on 17.12.13.
 */
public enum CarBodyType {
    // Cедан
    SEDAN,
    // Хетчбек
    HATCHBACK,
    // Универсал
    UNIVERSAL,
    // Купе
    COUPE,
    // Фастбэк
    FASTBACK;
    private static EnumEntryList map = null;

    public static EnumEntryList getMap() {
        if (map == null) {
            map = new EnumEntryList();
            map.add(CarBodyType.SEDAN, "Седан");
            map.add(CarBodyType.HATCHBACK, "Хетчбек");
            map.add(CarBodyType.UNIVERSAL, "Универсал");
            map.add(CarBodyType.COUPE, "Купе");
            map.add(CarBodyType.FASTBACK, "Фастбэк");
        }
        return map;
    }

    public static CarBodyType getKey(String s) {
        return (CarBodyType) getMap().getKey(s);
    }

    public static String getDescr(CarBodyType t) {
        return getMap().getDescr(t);
    }
}
