package com.xozm.bksp.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by x on 08.10.13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextMessage {
    public String id;
    public String content;
    public Date date;
    public String direction;

    public boolean isInbound() {
        return direction.equals("OPERATOR_TO_USER");
    }

    public String getHeader() {
        String result;
        if (isInbound())
            result = "Оператор, ";
        else
            result = "Вы, ";

        SimpleDateFormat dateFormatter = new SimpleDateFormat();
        result += dateFormatter.format(date);
        return result;
    }
}
