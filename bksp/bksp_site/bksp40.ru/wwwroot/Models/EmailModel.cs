﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BK.Models
{
    public class EmailModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Пожалуйста, введите ваше имя")]
        public string Subject { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Пожалуйста, введите e-mail")]
        [RegularExpression("^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})$", ErrorMessage = "Не правильный email")]
        public string From { get; set; }
       
        public string To { get; set; }

        public string Telefon { get; set; }

        public List<string> DetailName { get; set; }

        public string BodyNumber { get; set; }

        public bool IHaveBodyNumber { get; set; }

        public string DateRelease { get; set; }

        public string Mark { get; set; }

        public string Models { get; set; }

        public string Engine { get; set; }

        public string BodyModel { get; set; }

        public string TypeDrive { get; set; }

        public string Wheel { get; set; }

        public bool Subscribe { get; set; }

        public EmailModel()
        {            
            To = "orders.bksp@gmail.com";
        }
    }
}