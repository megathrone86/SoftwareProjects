﻿using BK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BK.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(EmailModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {                    
                    if (string.IsNullOrEmpty(model.From))
                    {
                        ViewData["error"] = "Введите пожалуйста или e-mail, чтобы наш менеджер мог связаться с вами."; 
                        
                    }
                    else if(!model.IHaveBodyNumber && string.IsNullOrEmpty(model.BodyNumber)){
                        ViewData["error"] = "Введите пожалуйста Номер VIN или Кузов либо отметте, что он отсутствует."; 
                    }
                    else
                    {
                        new EmailController().SendEmail(model).Deliver();
                        ViewData["success"] = "Наш менеджер свяжется с вами в ближайшее время.";                                               
                    }
                    return View(model);
                }
                catch (Exception e)
                {                    
                    ViewData["error"] = "Произошла ошибка. Пожалуйста повторите попытку позднее или свяжитесь с нами по телефону.";
                    return View(model);
                }
            }
            return View(model);
        }      

        public ActionResult Default()
        {
            return View();
        }

    }
}
