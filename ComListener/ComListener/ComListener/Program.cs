﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ComListener {
    class Program {
        const int BufferSize = 10 * 1024;

        static void Main(string[] args) {
            try {
                if (args.Length != 2)
                    throw new ArgumentException();

                string port = args[0];
                string filename = args[1];

                bool exit = false;
                new Thread(() => {
                    SerialPort input = new SerialPort(port, 9600, Parity.None, 8, StopBits.One);
                    input.Open();
                    byte[] buffer = new byte[BufferSize];
                    input.Encoding = Encoding.ASCII;

                    while (!exit) {
                        string data = input.ReadExisting();
                        File.AppendAllText(filename, data);
                    }

                    input.Close();
                }).Start();

                Console.WriteLine("Press ESC to exit");

                while (true) {
                    var n = Console.ReadKey();
                    if (n.Key == ConsoleKey.Escape) {
                        exit = true;
                        break;
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
                return;
            }
        }
    }
}
