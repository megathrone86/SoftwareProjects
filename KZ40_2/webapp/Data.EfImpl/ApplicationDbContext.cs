﻿using Data.EfImpl.AuthDo;
using Data.EfImpl.Do;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Data.EfImpl
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public static DbContextOptions DefaultOptions { get; }

        public DbSet<NewsEntryDo> News { get; set; }
        public DbSet<ArticleDo> Articles { get; set; }

        static ApplicationDbContext()
        {
            DbContextOptionsBuilder builder = new DbContextOptionsBuilder();
            //var connectionString = Configuration.GetConnectionString("SqlConnection");
            //TODO: заменить хардкод на чтение SqlConnection из конфига
            var connectionString = "Server=(localdb)\\mssqllocaldb;Database=kz40_db;Trusted_Connection=True;MultipleActiveResultSets=true";
            DefaultOptions = builder.UseSqlServer(connectionString).Options;
        }

        public ApplicationDbContext() : base(DefaultOptions)
        {
        }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

#if DEBUG
        public override void Dispose()
        {
            base.Dispose();
        }
#endif
    }
}
