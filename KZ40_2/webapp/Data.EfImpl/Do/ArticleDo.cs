﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Data.Abstract.Do;

namespace Data.EfImpl.Do
{
    public class ArticleDo : IArticleDo
    {
        public DateTime CreateDate { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVisible { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [NotMapped]
        public object AbstractId { get => Id; set => Id = Convert.ToInt32(value); }
    }
}
