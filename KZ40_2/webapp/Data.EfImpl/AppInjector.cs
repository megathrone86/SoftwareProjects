﻿using Data.Abstract.Repository;
using Data.EfImpl.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Data.EfImpl
{
    public class AppInjector
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddTransient<INewsRepository, NewsRepository>();
            services.AddTransient<IDocsRepository, DocsRepository>();
        }
    }
}
