﻿using Microsoft.AspNetCore.Identity;

namespace Data.EfImpl.AuthDo
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }                

        public ApplicationUser()
        {
        }
    }
}
