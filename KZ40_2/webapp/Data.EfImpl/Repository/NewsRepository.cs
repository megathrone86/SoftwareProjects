﻿using Data.Abstract.Do;
using Data.Abstract.Repository;
using Data.EfImpl.Do;
using System.Linq;

namespace Data.EfImpl.Repository
{
    class NewsRepository : INewsRepository
    {
        ApplicationDbContext ctx;

        public NewsRepository(ApplicationDbContext ctx)
        {
            this.ctx = ctx;
        }

        public void Add(INewsEntryDo entity)
        {
            ctx.News.Add(entity as NewsEntryDo);
            ctx.SaveChanges();
        }

        public void Delete(object id)
        {
            ctx.News.Remove(Get(id) as NewsEntryDo);
            ctx.SaveChanges();
        }

        public void Edit(INewsEntryDo entity)
        {
            var dbEntity = Get(entity.AbstractId) as NewsEntryDo;
            dbEntity.Body = entity.Body;
            dbEntity.CreateDate = entity.CreateDate;
            dbEntity.Header = entity.Header;
            dbEntity.IsDeleted = entity.IsDeleted;

            ctx.SaveChanges();
        }

        public INewsEntryDo Get(object id)
        {
            return ctx.News.FirstOrDefault(t => t.Id == (int)id);
        }

        public INewsEntryDo[] GetFirst10()
        {
            return ctx.News.OrderBy(t => t.CreateDate).Take(10).ToArray();
        }
    }
}
