﻿using Data.Abstract.Do;
using Data.Abstract.Repository;
using Data.EfImpl.Do;
using System.Linq;

namespace Data.EfImpl.Repository
{
    public class DocsRepository : IDocsRepository
    {
        ApplicationDbContext ctx;

        public DocsRepository(ApplicationDbContext ctx)
        {
            this.ctx = ctx;
        }

        public void Add(IArticleDo entity)
        {
            ctx.Articles.Add(entity as ArticleDo);
            ctx.SaveChanges();
        }

        public void Delete(object id)
        {
            ctx.Articles.Remove(Get(id) as ArticleDo);
            ctx.SaveChanges();
        }

        public void Edit(IArticleDo entity)
        {
            var dbEntity = Get(entity.AbstractId) as ArticleDo;
            dbEntity.Body = entity.Body;
            dbEntity.CreateDate = entity.CreateDate;
            dbEntity.Header = entity.Header;
            dbEntity.IsDeleted = entity.IsDeleted;
            dbEntity.IsVisible = entity.IsVisible;

            ctx.SaveChanges();
        }

        public IArticleDo Get(object id)
        {
            return ctx.Articles.FirstOrDefault(t => t.Id == (int)id);
        }

        public IArticleDo[] GetAll()
        {
            return ctx.Articles.OrderBy(t => t.CreateDate).ToArray();
        }

        public IArticleDo[] GetFirst10()
        {
            return ctx.Articles.OrderBy(t => t.CreateDate).Take(10).ToArray();
        }
    }
}
