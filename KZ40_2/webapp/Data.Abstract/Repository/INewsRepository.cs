﻿using Data.Abstract.Do;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Abstract.Repository
{
    public interface INewsRepository
    {
        INewsEntryDo[] GetFirst10();
        INewsEntryDo Get(object id);
        void Add(INewsEntryDo entity);
        void Edit(INewsEntryDo entity);
        void Delete(object id);
    }
}
