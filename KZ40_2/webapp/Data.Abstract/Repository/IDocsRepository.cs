﻿using Data.Abstract.Do;

namespace Data.Abstract.Repository
{
    public interface IDocsRepository
    {
        IArticleDo[] GetFirst10();
        IArticleDo[] GetAll();
        IArticleDo Get(object id);
        void Add(IArticleDo entity);
        void Edit(IArticleDo entity);
        void Delete(object id);
    }
}
