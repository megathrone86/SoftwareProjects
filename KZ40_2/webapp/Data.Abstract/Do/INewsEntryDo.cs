﻿using System;

namespace Data.Abstract.Do
{
    public interface INewsEntryDo : IBaseDo
    {
        DateTime CreateDate { get; set; }
        string Header { get; set; }
        string Body { get; set; }
        bool IsDeleted { get; set; }
    }
}
