﻿using System;

namespace Data.Abstract.Do
{
    public interface IBaseDo
    {
        object AbstractId { get; set; }
    }
}
