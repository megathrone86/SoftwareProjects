﻿using System;

namespace Data.Abstract.Do
{
    public interface IArticleDo : IBaseDo
    {
        DateTime CreateDate { get; set; }
        string Body { get; set; }
        string Header { get; set; }
        bool IsDeleted { get; set; }
        bool IsVisible { get; set; }
    }
}
