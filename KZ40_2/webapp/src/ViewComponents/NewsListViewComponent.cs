﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapp.Services;

namespace webapp.ViewComponents
{
    public class NewsListViewComponent : ViewComponent
    {
        NewsService newsService;

        public NewsListViewComponent(NewsService newsService)
        {
            this.newsService = newsService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var r = newsService.GetFirst10Requests();
            var model = r.Select(t => ModelConverter.ToNewsBriefModel(t));
            return View(model);
        }
    }
}
