﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using webapp.Services;
using System.Net;
using System.Globalization;
using Microsoft.AspNetCore.Diagnostics;
using webapp.Auth;
using NetCoreHelpers;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Data.EfImpl.Do;
using Microsoft.EntityFrameworkCore;
using Data.EfImpl.AuthDo;
using Data.EfImpl;
using Microsoft.AspNetCore.Rewrite;
using Data.Abstract.Repository;

namespace webapp
{
    public class Startup
    {
        IServiceProvider serviceProvider;
        string connectionString;

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            this.connectionString = configuration.GetConnectionString("SqlConnection");

            FileHelper.Init(env);

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"appsettings.production.json", optional: true);

            //if (env.IsDevelopment())
            //{
            //    builder.AddApplicationInsightsSettings(developerMode: true);
            //}

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            CaptchaHelper.SecretKey = Configuration["ReCaptchaSecretKey"];
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            var connectionString = Configuration.GetConnectionString("SqlConnection");
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/Login";
                });

            services.AddSession();

            services.AddTransient<UsersService>();
            services.AddTransient<NewsService>();
            services.AddTransient<DocsService>();
            services.AddTransient<FilesService>();
            services.AddTransient<IEmailSender, AuthMessageSender>();

            AppInjector.Configure(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            UsersService usersService)
        {
            app.UseAuthentication();

            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler(
                    builder =>
                    {
                        builder.Run(
                          async
                          handlerContext =>
                          {
                              handlerContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                              handlerContext.Response.ContentType = "text/html";

                              var error = handlerContext.Features.Get<IExceptionHandlerFeature>();
                              if (error != null)
                              {
                                  ErrorLogger.Log(error.Error);
                                  handlerContext.Response.Redirect("/Home/Error");
                              }
                          });
                    });
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Welcome}/{id?}");
            });

            app.UseRewriter(new RewriteOptions().AddRedirectToHttps());

            CultureInfo cultureInfo = new CultureInfo("ru-RU");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
            CultureInfo.CurrentCulture = cultureInfo;
            CultureInfo.CurrentUICulture = cultureInfo;

            context.Database.EnsureCreated();
            context.Database.Migrate();

            //если использовать await, то context задиспоузится до завершения задачи и это вызовет ошибку
            AddInitialData(userManager, roleManager, usersService).Wait();
        }

        async Task AddInitialData(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            UsersService usersService)
        {
            foreach (var roleName in new string[]
            {
                "admin"
            })
            {
                var role = await roleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    var result = await roleManager.CreateAsync(new IdentityRole { Name = roleName });
                    if (!result.Succeeded)
                        throw new Exception("Не удалось создать роль.");
                }
            }

            foreach (string adminVkId in LoginsInfo.AdminVKIds)
            {
                var mainUser = await userManager.FindByLoginAsync(LoginsInfo.VKProviderName, adminVkId);
                if (mainUser == null)
                    continue;

                await usersService.AddRoleIfNotExists(mainUser, "admin");
            }
        }
    }
}
