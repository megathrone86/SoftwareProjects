﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Abstract.Repository;
using Data.Abstract.Do;

namespace webapp.Services
{
    public class NewsService
    {
        INewsRepository newsRepository;

        public NewsService(INewsRepository newsRepository)
        {
            this.newsRepository = newsRepository;
        }

        public void Add(INewsEntryDo entity)
        {
            newsRepository.Add(entity);
        }

        public void Edit(INewsEntryDo entity)
        {
            newsRepository.Edit(entity);
        }

        public void Delete(string id)
        {
            var entity = Get(id);
            entity.IsDeleted = true;
            Edit(entity);
        }

        public List<INewsEntryDo> GetFirst10Requests()
        {
            var result = newsRepository.GetFirst10();
            var list = result.Where(t => !t.IsDeleted).OrderByDescending(t => t.CreateDate).Take(10).ToList();
            return list;
        }

        public INewsEntryDo Get(object id)
        {
            return newsRepository.Get(id);
        }
    }
}
