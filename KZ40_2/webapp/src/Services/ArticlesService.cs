﻿using System.Collections.Generic;
using System.Linq;
using Data.Abstract.Repository;
using Data.Abstract.Do;

namespace webapp.Services
{
    public class DocsService
    {
        IDocsRepository docsRepository;

        public DocsService(IDocsRepository docsRepository)
        {
            this.docsRepository = docsRepository;
        }

        public void Add(IArticleDo entity)
        {
            docsRepository.Add(entity);
        }

        public void Edit(IArticleDo entity)
        {
            docsRepository.Edit(entity);
        }

        public void Delete(string id)
        {
            var entity = Get(id);
            entity.IsDeleted = true;
            Edit(entity);
        }

        public List<IArticleDo> GetFirst10()
        {
            var result = docsRepository.GetFirst10();
            var list = result.Where(t => !t.IsDeleted).OrderByDescending(t => t.CreateDate).Take(10).ToList();
            return list;
        }

        public IArticleDo Get(object id)
        {
            return docsRepository.Get(id);
        }
    }
}
