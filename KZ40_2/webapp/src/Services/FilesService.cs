﻿using System.Linq;
using NetCoreHelpers;
using System.IO;
using System;
using Data.EfImpl.Do;

namespace webapp.Services
{
    public class FilesService
    {
        string StoragePath { get { return Path.Combine(FileHelper.AppRoot, "files"); } }

        public FilesService()
        {
            if (!Directory.Exists(StoragePath))
                Directory.CreateDirectory(StoragePath);
        }

        public string GetFilePath(string fileName)
        {
            return Path.Combine(StoragePath, fileName);
        }

        public void Add(Models.File file)
        {
            if (string.IsNullOrEmpty(file.Name))
                throw new ArgumentException();

            var filePath = GetFilePath(file.Name);
            if (System.IO.File.Exists(filePath))
                throw new Exception("Файл с таким именем уже существует.");

            System.IO.File.WriteAllBytes(filePath, file.Content);
        }

        public void Delete(string fileName)
        {
            var filePath = GetFilePath(fileName);
            System.IO.File.Delete(filePath);
        }

        public string[] GetFiles()
        {
            return Directory.GetFiles(StoragePath).Select(t => Path.GetFileName(t)).ToArray();
        }

        public byte[] Get(string fileName)
        {
            if (fileName.Any(c => Path.GetInvalidFileNameChars().Contains(c)))
                throw new ArgumentException();

            var filePath = GetFilePath(fileName);
            return System.IO.File.ReadAllBytes(filePath);
        }
    }
}
