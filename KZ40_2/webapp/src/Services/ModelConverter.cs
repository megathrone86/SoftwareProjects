﻿using System;
using Data.Abstract.Do;
using Data.EfImpl.AuthDo;
using Data.EfImpl.Do;
using webapp.Auth;
using webapp.Models;

namespace webapp.Services
{
    public class ModelConverter
    {
        public static NewsBriefModel ToNewsBriefModel(INewsEntryDo entity)
        {
            return new NewsBriefModel()
            {
                Body = entity.Body,
                CreateDate = entity.CreateDate.ToLocalTime(),
                Header = entity.Header,
                Id = entity.AbstractId
            };
        }

        public static NewsEntryDo ToNewsEntryDo(NewsBriefModel entity)
        {
            var r = new NewsEntryDo()
            {
                Body = entity.Body,
                CreateDate = entity.CreateDate,
                Header = entity.Header,
                AbstractId = entity.Id
            };
            return r;
        }

        public static ArticleDo ToArticleDo(DocModel entity)
        {
            var r = new ArticleDo()
            {
                Body = entity.Body,
                CreateDate = entity.CreateDate,
                Header = entity.Header,
                AbstractId = entity.Id,
                IsVisible = entity.IsVisible
            };
            return r;
        }

        internal static DocBriefModel ToDocBriefModel(IArticleDo t)
        {
            return new DocBriefModel()
            {
                Id = t.AbstractId.ToString(),
                Header = t.Header,
                Body = t.Body,
                CreateDate = t.CreateDate,
            };
        }

        internal static DocModel ToDocModel(IArticleDo t)
        {
            return new DocModel()
            {
                Id = t.AbstractId.ToString(),
                Header = t.Header,
                Body = t.Body,
                CreateDate = t.CreateDate,
                IsVisible = t.IsVisible
            };
        }

        internal static UserViewModel ToUserViewModel(ApplicationUser src)
        {
            return new UserViewModel()
            {
                Id = src.Id,
                //CreateDate = src.,
            };
        }
    }
}
