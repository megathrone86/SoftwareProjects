﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Identity;
using Data.EfImpl.AuthDo;
using Data.EfImpl;

namespace webapp.Services
{
    public class UsersService
    {
        UserManager<ApplicationUser> userManager;
        ApplicationDbContext context;

        //TODO: сервис должен юзать репозиторий, а не лезть в контекст напрямую
        public UsersService(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public async Task<List<ApplicationUser>> FindAll(string search)
        {
            var userHeaders = GetAll();
            string trimmedLowerSearch = search == null ? null : search.Trim().ToLower();
            var filteredHeaders = string.IsNullOrEmpty(trimmedLowerSearch) ? userHeaders :
                userHeaders.FindAll(user => FitToFilter(user, trimmedLowerSearch));

            List<string> userIds = new List<string>();
            foreach (var header in filteredHeaders)
            {
                if (!userIds.Contains(header.Id))
                    userIds.Add(header.Id);
            }

            var result = new List<ApplicationUser>();
            foreach (var userId in userIds)
            {
                var user = await userManager.FindByIdAsync(userId);
                result.Add(user);
            }

            return result;
        }

        bool FitToFilter(ApplicationUser user, string trimmedLowerSearch)
        {
            //if (user.DisplayName != null && user.DisplayName.ToLower().Contains(trimmedLowerSearch))
            //    return true;
            if (user.Id != null && user.Id.ToLower().Contains(trimmedLowerSearch))
                return true;
            return false;
        }

        public List<ApplicationUser> GetAll()
        {
            //var result = new List<ApplicationUser>();

            //var query = new TableQuery<ApplicationUser>();
            //query.Select(new List<string> { "Id", "DisplayName" });
            //TableContinuationToken token = null;
            //do
            //{
            //    var r = await context.UserTable.ExecuteQuerySegmentedAsync<ApplicationUser>(query, token);
            //    result.AddRange(r.Results);
            //    token = r.ContinuationToken;
            //} while (token != null);

            //return result;
            return new List<ApplicationUser> { };
        }

        public async Task<bool> UserExists(string loginProvider, string loginUid)
        {
            var user = await userManager.FindByLoginAsync(loginProvider, loginUid);
            return user != null;
        }

        public async Task<ApplicationUser> CreateUser(string providerName, string providerId, string providerDisplayName, string userName)
        {
            var user = new ApplicationUser()
            {
                DisplayName = userName,
                UserName = $"{providerName}-{providerId}"
            };

            {
                var result = await userManager.CreateAsync(user);
                if (!result.Succeeded)
                    throw new Exception($"Не удалось создать пользователя ({result}).");
            }

            {
                var result = await userManager.AddLoginAsync(user, new UserLoginInfo(providerName, providerId, providerDisplayName));
                if (!result.Succeeded)
                    throw new Exception($"Не удалось добавить информацию о способе входа ({result}).");
            }

            return user;
        }

        public async Task AddRoleIfNotExists(ApplicationUser user, string role)
        {
            var r = await userManager.IsInRoleAsync(user, role);
            if (!r)
            {
                var r2 = await userManager.AddToRoleAsync(user, role);
                if (!r2.Succeeded)
                    throw new Exception($"Не удалось присвоить роль {role} пользователю {user}.");
            }
        }
    }
}
