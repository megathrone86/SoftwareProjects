﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace webapp.Models
{
    public class DocModel
    {
        public string Id { get; set; }

        [DisplayName("Дата")]
        [DataType(DataType.DateTime)]
        public DateTime CreateDate { get; set; }

        [DisplayName("Заголовок")]
        public string Header { get; set; }

        [DisplayName("HTML текст")]
        [DataType(DataType.MultilineText)]
        public string Body { get; set; }

        [DisplayName("Отображать?")]
        public bool IsVisible { get; set; }
    }
}
