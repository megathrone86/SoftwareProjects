﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using webapp.Auth;

namespace webapp.Models
{
    public class UserViewModel
    {
        [Display(Name = "Внутренний ИД")]
        public string Id { get; set; }

        [Required]
        [MaxLength(150)]
        [Display(Name = "Имя")]
        public string UserName { get; set; }

        [Display(Name = "Роль")]
        public string UserRole { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime CreateDate { get; set; }

        public IList<UserLoginInfo> CurrentLogins { get; set; }

        public IList<AuthenticationDescription> OtherLogins { get; set; }

        public static IEnumerable<SelectListItem> Roles
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem() { Text = "user" },
                    new SelectListItem() { Text = "moder" },
                    new SelectListItem() { Text = "admin" }
                };
            }
        }

        string GetUserRole(ICollection<IdentityRole> roles)
        {
            //TODO: разобраться с этим
            if (roles.Any(r => r.Name == "ADMIN"))
                return "admin";
            if (roles.Any(r => r.Name == "MODER"))
                return "moder";
            return "user";
        }
    }
}
