﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace webapp.Models
{
    public class DocBriefModel
    {
        public string Id { get; set; }

        [DisplayName("Дата")]
        public DateTime CreateDate { get; set; }

        [DisplayName("Заголовок")]
        public string Header { get; set; }

        [DisplayName("HTML текст")]
        public string Body { get; set; }
    }
}
