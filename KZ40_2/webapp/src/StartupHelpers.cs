﻿using System;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using webapp.Models;

namespace webapp.StartupHelpers
{
    //public class CustomUserValidator : IUserValidator<ApplicationUser>
    //{
    //    static List<string> forbiddenWords = new List<string>()
    //    {
    //        "admin", "хуй", "пизда"
    //    };

    //    public Task<IdentityResult> ValidateAsync(UserManager<ApplicationUser> manager, ApplicationUser user)
    //    {
    //        List<IdentityError> errors = new List<IdentityError>();

    //        foreach (var s in forbiddenWords)
    //        {
    //            if (user.UserName.ToLower().Contains(s))
    //            {
    //                errors.Add(new IdentityError
    //                {
    //                    Description = "Имя пользователя содержит запрещенные слова"
    //                });
    //                break;
    //            }
    //        }

    //        return Task.FromResult(errors.Count == 0 ?
    //            IdentityResult.Success : IdentityResult.Failed(errors.ToArray()));
    //    }
    //}

    //public class UserClaimsPrincipalFactory<TUser> : IUserClaimsPrincipalFactory<TUser>
    //        where TUser : class
    //{
    //    public UserClaimsPrincipalFactory(
    //        UserManager<TUser> userManager,
    //        IOptions<IdentityOptions> optionsAccessor)
    //    {
    //        if (userManager == null)
    //            throw new ArgumentNullException(nameof(userManager));
    //        if (optionsAccessor?.Value == null)
    //            throw new ArgumentNullException(nameof(optionsAccessor));

    //        UserManager = userManager;
    //        Options = optionsAccessor.Value;
    //    }

    //    public UserManager<TUser> UserManager { get; }

    //    public IdentityOptions Options { get; }

    //    public virtual async Task<ClaimsPrincipal> CreateAsync(TUser user)
    //    {
    //        throw new NotImplementedException();

    //        if (user == null)
    //            throw new ArgumentNullException(nameof(user));

    //        var userId = await UserManager.GetUserIdAsync(user);
    //        var userName = await UserManager.GetUserNameAsync(user);
    //        var id = new ClaimsIdentity(
    //            Options.Cookies.ApplicationCookieAuthenticationScheme,
    //            Options.ClaimsIdentity.UserNameClaimType,
    //            Options.ClaimsIdentity.RoleClaimType);
    //        id.AddClaim(new Claim(Options.ClaimsIdentity.UserIdClaimType, userId));
    //        id.AddClaim(new Claim(Options.ClaimsIdentity.UserNameClaimType, userName));
    //        if (UserManager.SupportsUserSecurityStamp)
    //            id.AddClaim(new Claim(Options.ClaimsIdentity.SecurityStampClaimType,
    //                await UserManager.GetSecurityStampAsync(user)));

    //        if (UserManager.SupportsUserRole)
    //        {
    //            var roles = await UserManager.GetRolesAsync(user);
    //            foreach (var roleName in roles)
    //            {
    //                id.AddClaim(new Claim(Options.ClaimsIdentity.RoleClaimType, roleName));
    //            }
    //        }
    //        if (UserManager.SupportsUserClaim)
    //            id.AddClaims(await UserManager.GetClaimsAsync(user));

    //        return new ClaimsPrincipal(id);
    //    }
    //}
}
