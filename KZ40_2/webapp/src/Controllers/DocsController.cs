﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using webapp.Services;
using webapp.Models;

namespace webapp.Controllers
{
    public class DocsController : Controller
    {
        DocsService docsService;

        public DocsController(DocsService docsService)
        {
            this.docsService = docsService;
        }

        public IActionResult Index()
        {
            var r = docsService.GetFirst10();
            var model = r.Select(t => ModelConverter.ToDocBriefModel(t));
            return View(model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult AddDoc(string id)
        {
            var model = new DocModel() { CreateDate = DateTime.UtcNow, IsVisible = true };
            return PartialView("DocEditView", model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult EditDoc(string id)
        {
            var model = ModelConverter.ToDocModel(docsService.Get(id));
            return PartialView("DocEditView", model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult DeleteDoc(string id)
        {
            docsService.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult SaveDoc(DocModel dto)
        {
            if (string.IsNullOrEmpty(dto.Header) || string.IsNullOrEmpty(dto.Body) ||
                dto.CreateDate < new DateTime(2015, 1, 1) || dto.CreateDate > DateTime.Now.AddMinutes(1))
                throw new ArgumentException();

            var entity = ModelConverter.ToArticleDo(dto);
            if (dto.Id == null)
            {
                docsService.Add(entity);
            }
            else
            {
                docsService.Edit(entity);
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
