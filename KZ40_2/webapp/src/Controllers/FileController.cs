﻿using Microsoft.AspNetCore.Mvc;
using webapp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;

namespace webapp.Controllers
{
    public class FileController : Controller
    {
        FilesService filesService;

        public FileController(FilesService filesService)
        {
            this.filesService = filesService;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Index()
        {
            var model = filesService.GetFiles();
            return View(model);
        }

        [HttpGet]
        public IActionResult Get(string fn)
        {
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(fn, out contentType);
            contentType = contentType ?? "application/octet-stream";

            var path = filesService.GetFilePath(fn);
            var result = new FileStreamResult(new FileStream(path, FileMode.Open), contentType);
            if (contentType.StartsWith("application"))
                result.FileDownloadName = fn;
            return result;
        }

        [HttpPost]
        public IActionResult Delete(string fn)
        {
            filesService.Delete(fn);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Add(IFormFile file)
        {
            if (file == null)
                return RedirectToAction("Index");

            byte[] content;
            using (var fileStream = file.OpenReadStream())
            using (var ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                content = ms.ToArray();
            }

            var fileName = Path.GetFileName(file.FileName);

            var newFile = new Models.File() { Content = content, Name = fileName };
            filesService.Add(newFile);
            return RedirectToAction("Index");
        }
    }
}
