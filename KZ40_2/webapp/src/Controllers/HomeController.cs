﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using webapp.Services;
using webapp.Models;

namespace webapp.Controllers
{
    public class HomeController : Controller
    {
        NewsService newsService;

        public HomeController(NewsService newsService)
        {
            this.newsService = newsService;
        }

        public IActionResult Welcome()
        {
            var r = newsService.GetFirst10Requests();
            var model = r.Select(t => ModelConverter.ToNewsBriefModel(t));
            return View(model);
        }

        [HttpGet]
        public IActionResult GetNews()
        {
            var r = newsService.GetFirst10Requests();
            var model = r.Select(t => ModelConverter.ToNewsBriefModel(t));
            return PartialView("NewsPartial", model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult EditNews(string id)
        {
            var model = ModelConverter.ToNewsBriefModel(newsService.Get(id));
            return PartialView("NewsEntryView", model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult AddNews(string id)
        {
            var model = new NewsBriefModel() { CreateDate = DateTime.Now };
            return PartialView("NewsEntryView", model);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult DeleteNews(string id)
        {
            newsService.Delete(id);
            return RedirectToAction(nameof(Welcome));
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult SaveNews(NewsBriefModel dto)
        {
            if (string.IsNullOrEmpty(dto.Header) || string.IsNullOrEmpty(dto.Body) ||
                dto.CreateDate < new DateTime(2015, 1, 1) || dto.CreateDate > DateTime.Now.AddMinutes(1))
                throw new ArgumentException();

            var entity = ModelConverter.ToNewsEntryDo(dto);
            if (dto.Id == null)
            {
                newsService.Add(entity);
            }
            else
            {
                newsService.Edit(entity);
            }

            return RedirectToAction(nameof(Welcome));
        }

        public IActionResult Production()
        {
            return View();
        }

        public IActionResult Services()
        {
            return View();
        }
    }
}
