﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapp.Models;
using webapp.Services;
using webapp.Auth;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Collections.Generic;
using Data.EfImpl.AuthDo;

namespace webapp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        readonly UserManager<ApplicationUser> userManager;
        readonly SignInManager<ApplicationUser> signInManager;
        readonly ILogger logger;
        readonly UsersService usersService;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILoggerFactory loggerFactory,
            UsersService usersService)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = loggerFactory.CreateLogger<AccountController>();
            this.usersService = usersService;
        }

        void FillAppIds()
        {
#if DEBUG
            ViewData["VkAppId"] = "6255247";
#else
            throw new Exception();
#endif
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginVK(string returnUrl, string uid, string first_name, string last_name)
        {
            string loginProviderName = LoginsInfo.VKProviderName;
            string loginProviderDisplayName = LoginsInfo.VKProviderDisplayName;

            var user = await userManager.FindByLoginAsync(loginProviderName, uid);
            if (user == null)
            {
                string name = first_name;
                if (!string.IsNullOrEmpty(last_name))
                    name += " " + last_name;
                user = await usersService.CreateUser(LoginsInfo.VKProviderName, uid, LoginsInfo.VKProviderDisplayName, name);
                logger.LogInformation(3, $"User created a new account via {loginProviderName}.");
            }

            if (LoginsInfo.AdminVKIds.Contains(uid))
                await usersService.AddRoleIfNotExists(user, "admin");

            if (user != null)
            {
                await signInManager.SignInAsync(user, isPersistent: true);

                return RedirectToLocal(returnUrl);
            }
            else
            {
                throw new Exception("Ошибка входа.");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await signInManager.SignOutAsync();
            logger.LogInformation(4, "User logged out.");
            //return RedirectToAction(nameof(ContaminationController.Index), "Contamination");
            return RedirectToAction(nameof(HomeController.Welcome), "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            FillAppIds();
            ViewData["ReturnUrl"] = WebUtility.UrlEncode(returnUrl);
            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Welcome), "Home");
            }
        }
    }
}
