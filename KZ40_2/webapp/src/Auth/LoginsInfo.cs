﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace webapp.Auth
{
    public class LoginsInfo
    {
        public static readonly string VKProviderName = "VK";
        public static readonly string VKProviderDisplayName = "ВКонтакте";

        public static readonly string[] AdminVKIds = new string[]
        {
            "1674956",
            "138470662"
        };
    }
}
