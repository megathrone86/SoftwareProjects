﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;

namespace XSDValidator
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();

            textBox1.Text = Program.Settings.SchemaPath;
            textBox2.Text = Program.Settings.DocumentPath;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            using (var dlg = new OpenFileDialog() { Filter = "XSD Scheme | *.xsd" })
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    textBox1.Text = dlg.FileName;
                }
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            using (var dlg = new OpenFileDialog() { Filter = "XML Document | *.xml" })
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    textBox2.Text = dlg.FileName;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                Program.Settings.SchemaPath = textBox1.Text;
                Program.Settings.DocumentPath = textBox2.Text;
                Program.SaveSettings();
            } catch (Exception ex)
            {
                sb.AppendLine($"Warning: unable to save settings. {ex}");
            }

            try
            {
                XmlSchemaSet schemaSet = new XmlSchemaSet();

                using (var fs = new FileStream(Program.Settings.SchemaPath, FileMode.Open))
                using (var reader = XmlReader.Create(fs, null, Program.Settings.SchemaPath))
                {
                    schemaSet.Add(XmlSchema.Read(reader, null));
                }

                schemaSet.CompilationSettings = new XmlSchemaCompilationSettings();
                schemaSet.Compile();

                XmlDocument doc = new XmlDocument();
                doc.Load(Program.Settings.DocumentPath);
                doc.Schemas = schemaSet;
                doc.Validate((s, a) => { sb.AppendLine(a.Message); });
            } catch (Exception ex)
            {
                sb.AppendLine(ex.ToString());
            }

            textBox3.Text = sb.ToString();
        }
    }
}
