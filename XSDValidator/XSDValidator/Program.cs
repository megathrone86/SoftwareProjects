﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace XSDValidator
{
    static class Program
    {
        const string settingsPath = "settings.xml";
        public static Settings Settings;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LoadSettings();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static void LoadSettings()
        {
            Settings = new Settings();

            try
            {
                if (File.Exists(settingsPath))
                {
                    using (var fs = new FileStream(settingsPath, FileMode.Open))
                    {
                        Settings = (Settings)new XmlSerializer(typeof(Settings)).Deserialize(fs);
                    }
                }
            } catch { }

        }

        public static void SaveSettings()
        {
            using (var fs = new FileStream(settingsPath, FileMode.Create))
            {
                new XmlSerializer(typeof(Settings)).Serialize(fs, Settings);
            }
        }
    }

    public class Settings
    {
        public string DocumentPath { get; set; }
        public string SchemaPath { get; set; }
    }
}
