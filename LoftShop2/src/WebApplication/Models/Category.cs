﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShop.Models
{
    public class Category : Entity
    {
        public string Name { get; set; }
        public bool IsVisible { get; set; }
    }
}
