﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShop.Models
{
    public class Product : Entity
    {
        public bool IsVisible { get; set; }
        public List<Category> Categories { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Discount { get; set; }
        public string[] Images { get; set; }

        public Product()
        {
            IsVisible = true;
        }
    }
}
