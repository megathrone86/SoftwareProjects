﻿using LoftShop.ViewModels;
using LoftShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoftShop.Models;

namespace LoftShop.Data
{
    public class CategoriesManager
    {
        public static string SummaryName { get { return "Все категории"; } }

        CategoryRepository categoryRepository = new CategoryRepository();

        public async Task<IEnumerable<Category>> GetAllCategories()
        {
            var items = await categoryRepository.GetItemsAsync(d => true);
            return items;
        }

        public async Task<IEnumerable<Category>> GetVisibleCategories()
        {
            var items = await categoryRepository.GetItemsAsync(d => d.IsVisible);
            var list = items.ToList();
            list.Sort(new Comparison<Category>((t1, t2) => t1.Name.CompareTo(t2.Name)));
            return list;
        }

        public async Task<Category> GetCategory(string catId)
        {
            var item = await categoryRepository.GetItemAsync(catId);
            return item;
        }

        public async Task UpdateCategory(Category category)
        {
            await categoryRepository.UpdateItemAsync(category.Id.ToString(), category);
        }

        public async Task AddCategory(string catName)
        {
            if (string.IsNullOrEmpty(catName))
                throw new ArgumentException();

            var existingItemId = await GetCategoryIdByName(catName);
            if (existingItemId == null)
            {
                var item = new Category() { Name = catName };
                await categoryRepository.CreateItemAsync(item);
            }
        }

        public async Task DeleteCategory(string catId)
        {
            if (!string.IsNullOrEmpty(catId))
                await categoryRepository.DeleteItemAsync(catId);
        }

        public async Task<string> GetCategoryIdByName(string catName)
        {
            if (string.IsNullOrEmpty(catName) || StringSmartComparer.Equal(catName, SummaryName))
                return null;

            var result = await categoryRepository.GetItemAsync(c => c.Name == catName);
            return result != null ? result.Id : null;
        }
    }
}
