﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LoftShop.Models;

namespace LoftShop.Data
{
    public class CategoryRepository : BaseRepository<Category>
    {
        protected override string CollectionName { get { return "Category"; } }

        protected override void OnCollectionCreated()
        {
            CreateItemAsync(new Category() { Name = "Диваны", IsVisible = true }).Wait();
            CreateItemAsync(new Category() { Name = "Столы", IsVisible = true }).Wait();
            CreateItemAsync(new Category() { Name = "Стулья", IsVisible = true }).Wait();
        }
    }

    public class ProductRepository : BaseRepository<Product>
    {
        protected override string CollectionName { get { return "Product"; } }

        protected override async void OnCollectionCreated()
        {
            var list = new List<Product>()
            {
                await CreateProductTest("Диваны", "Диван большой", 100000),
                await CreateProductTest("Стулья", "Стул битардский", 20000),
                await CreateProductTest("Диваны", "Пуфик", 60000),
                await CreateProductTest("Диваны", "Нано диван", 10000),
                await CreateProductTest("Диваны", "Раскладной гигантский", 160000),
                await CreateProductTest("Диваны", "Черная вдова", 120000)
            };

            var rep = new ProductRepository();
            rep.CreateItemsAsync(list);
        }

        async Task<Product> CreateProductTest(string catName, string name, int price)
        {
            var catRep = new CategoryRepository();
            var cat = await catRep.GetItemAsync(t => t.Name == catName);
            return new Product()
            {
                Id = Guid.NewGuid().ToString(),
                Categories = new List<Category>() { new Category() { Id = cat.Id } },
                Name = name,
                Price = price
            };
        }
    }
}
