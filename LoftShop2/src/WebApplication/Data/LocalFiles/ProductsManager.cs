﻿using LoftShop.ViewModels;
using LoftShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShop.Data
{
    public class ProductsManager
    {
        ProductRepository productRepository = new ProductRepository();

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            var items = await productRepository.GetItemsAsync(d => true);
            return items;
        }

        public async Task<IEnumerable<Product>> GetVisibleProducts()
        {
            var items = await productRepository.GetItemsAsync(d => d.IsVisible);
            return items;
        }

        public async Task<List<Product>> GetVisibleProducts(List<string> categories)
        {
            var data = await productRepository.GetItemsAsync(t => t.IsVisible);
            var d = data.Where(p =>
            {
                if (categories.Count <= 0 || categories.Contains(null))
                    return true;
                foreach (var cat in categories)
                {
                    var pcat = p.Categories != null ? p.Categories.Find(t => t.Id == cat) : null;
                    if (pcat != null)
                        return true;
                }
                return false;
            });

            return d.ToList();
        }

        public async Task<Product> GetProduct(string productId)
        {
            var result = await productRepository.GetItemAsync(productId);
            return result;
        }
    }
}
