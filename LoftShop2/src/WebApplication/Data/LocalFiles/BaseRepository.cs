﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LoftShop.Models;
using System.IO;
using LoftShop.Helpers;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;

namespace LoftShop.Data
{
    public abstract class BaseRepository
    {
        public const string UserKeyParamName = @"Authentication:DocumentDB:UserKey";
        public static string UserKey;

        protected const string EndpointUri = @"https://loftshop40.documents.azure.com:443/";
        protected const string DbName = @"LoftShop40";
    }

    public abstract class BaseRepository<T> : BaseRepository, IDisposable where T : Entity
    {
        protected abstract string CollectionName { get; }

        protected string CollectionDir { get { return Path.Combine(FileHelper.LocalFolderPath, CollectionName); } }
        protected string CollectionPath { get { return Path.Combine(CollectionDir, "data.json"); } }

        public BaseRepository()
        {
            CreateDatabaseIfNotExistsAsync().Wait();
            CreateCollectionIfNotExistsAsync().Wait();
        }

        protected abstract void OnCollectionCreated();

        public async Task<T> GetItemAsync(string id)
        {
            var list = GetItems();
            return list.Find(t => t.Id == id);
        }

        public async Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> predicate)
        {
            var list = GetItems();
            var func = predicate.Compile();
            return list.FindAll(t => func(t));
        }

        public async Task<T> GetItemAsync(Expression<Func<T, bool>> predicate)
        {
            var list = GetItems();
            var func = predicate.Compile();
            return list.Find(t => func(t));
        }

        public async Task CreateItemAsync(T item)
        {
            var list = GetItems();
            list.Add(item);
            WriteItems(list);
        }

        public async Task CreateItemsAsync(IEnumerable<T> items)
        {
            var list = GetItems();
            list.AddRange(items);
            WriteItems(list);
        }

        public async Task<Document> UpdateItemAsync(string id, T item)
        {
            var list = GetItems();
            list.RemoveAll(t => t.Id == id);
            list.Add(item);
            WriteItems(list);

            return null;
        }

        public async Task DeleteItemAsync(string id)
        {
            var list = GetItems();
            list.RemoveAll(t => t.Id == id);
            WriteItems(list);
        }

        private async Task CreateDatabaseIfNotExistsAsync()
        {
            if (!Directory.Exists(FileHelper.LocalFolderPath))
                Directory.CreateDirectory(FileHelper.LocalFolderPath);
        }

        private async Task CreateCollectionIfNotExistsAsync()
        {
            if (!Directory.Exists(CollectionDir))
            {
                Directory.CreateDirectory(CollectionDir);
                OnCollectionCreated();
            }
        }

        List<T> GetItems()
        {
            if (File.Exists(CollectionPath))
            {
                string fileContent = File.ReadAllText(CollectionPath);
                return JsonConvert.DeserializeObject<List<T>>(fileContent);
            } else
                return new List<T>();
        }

        void WriteItems(List<T> items)
        {
            string fileContent = JsonConvert.SerializeObject(items);
            File.WriteAllText(CollectionPath, fileContent);
        }

        public void Dispose()
        {
        }
    }
}
