﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LoftShop.Models;

namespace LoftShop.Data
{
    //public abstract class BaseRepository
    //{
    //    public const string UserKeyParamName = @"Authentication:DocumentDB:UserKey";
    //    public static string UserKey;

    //    protected const string EndpointUri = @"https://loftshop40.documents.azure.com:443/";
    //    protected const string DbName = @"LoftShop40";
    //}

    //public abstract class BaseRepository<T> : BaseRepository, IDisposable where T : class
    //{
    //    protected abstract string CollectionName { get; }

    //    protected DocumentClient client;

    //    public BaseRepository()
    //    {
    //        client = new DocumentClient(new Uri(EndpointUri), UserKey);
    //        CreateDatabaseIfNotExistsAsync().Wait();
    //        CreateCollectionIfNotExistsAsync().Wait();
    //    }

    //    protected abstract void OnCollectionCreated();

    //    public async Task<T> GetItemAsync(string id)
    //    {
    //        try
    //        {
    //            Document document = await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DbName, CollectionName, id));
    //            return (T)(dynamic)document;
    //        } catch (DocumentClientException e)
    //        {
    //            if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
    //                return null;
    //            else
    //                throw;
    //        }
    //    }

    //    public async Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> predicate)
    //    {
    //        IDocumentQuery<T> query = client.CreateDocumentQuery<T>(
    //            UriFactory.CreateDocumentCollectionUri(DbName, CollectionName),
    //            new FeedOptions { MaxItemCount = -1 })
    //            .Where(predicate)
    //            .AsDocumentQuery();

    //        List<T> results = new List<T>();
    //        while (query.HasMoreResults)
    //        {
    //            results.AddRange(await query.ExecuteNextAsync<T>());
    //        }

    //        return results;
    //    }

    //    public async Task<T> GetItemAsync(Expression<Func<T, bool>> predicate)
    //    {
    //        IDocumentQuery<T> query = client.CreateDocumentQuery<T>(
    //            UriFactory.CreateDocumentCollectionUri(DbName, CollectionName),
    //            new FeedOptions { MaxItemCount = 1 })
    //            .Where(predicate)
    //            .AsDocumentQuery();

    //        List<T> results = new List<T>();
    //        while (query.HasMoreResults)
    //        {
    //            results.AddRange(await query.ExecuteNextAsync<T>());
    //        }

    //        return results.Count > 0 ? results.First() : null;
    //    }

    //    public async Task<Document> CreateItemAsync(T item)
    //    {
    //        return await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DbName, CollectionName), item);
    //    }

    //    public async Task<Document> UpdateItemAsync(string id, T item)
    //    {
    //        return await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DbName, CollectionName, id), item);
    //    }

    //    public async Task DeleteItemAsync(string id)
    //    {
    //        await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(DbName, CollectionName, id));
    //    }

    //    private async Task CreateDatabaseIfNotExistsAsync()
    //    {
    //        try
    //        {
    //            await client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(DbName));
    //        } catch (DocumentClientException e)
    //        {
    //            if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
    //            {
    //                await client.CreateDatabaseAsync(new Database { Id = DbName });
    //            } else
    //            {
    //                throw;
    //            }
    //        }
    //    }

    //    private async Task CreateCollectionIfNotExistsAsync()
    //    {
    //        try
    //        {
    //            await client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(DbName, CollectionName));
    //        } catch (DocumentClientException e)
    //        {
    //            if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
    //            {
    //                await client.CreateDocumentCollectionAsync(
    //                    UriFactory.CreateDatabaseUri(DbName),
    //                    new DocumentCollection { Id = CollectionName },
    //                    new RequestOptions { OfferThroughput = 400 });
    //                OnCollectionCreated();
    //            } else
    //            {
    //                throw;
    //            }
    //        }
    //    }

    //    public void Dispose()
    //    {
    //        client?.Dispose();
    //    }
    //}
}
