﻿using LoftShop.ViewModels;
using LoftShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoftShop.Models;

namespace LoftShop.Data
{
    //public class CategoriesManager
    //{
    //    public static string SummaryName { get { return "Все категории"; } }

    //    CategoryRepository categoryRepository = new CategoryRepository();

    //    public async Task<IEnumerable<CategoryViewModel>> GetAllCategories()
    //    {
    //        var cats = await categoryRepository.GetItemsAsync(d => true);
    //        return cats.Select(cat => new CategoryViewModel() { Id = cat.Id, Name = cat.Name, Visible = cat.IsVisible })
    //            .ToList();
    //    }

    //    public async Task<IEnumerable<CategoryViewModel>> GetVisibleCategories()
    //    {
    //        var cats = await categoryRepository.GetItemsAsync(d => d.IsVisible);
    //        return cats.Select(cat => new CategoryViewModel() { Id = cat.Id, Name = cat.Name })
    //            .ToList();
    //    }

    //    public async Task<CategoryViewModel> GetCategory(string catId)
    //    {
    //        var cat = await categoryRepository.GetItemAsync(catId);
    //        return new CategoryViewModel() { Id = cat.Id, Name = cat.Name, Visible = cat.IsVisible };
    //    }

    //    public async Task UpdateCategory(Category category)
    //    {
    //        await categoryRepository.UpdateItemAsync(category.Id.ToString(), category);
    //    }

    //    public async Task AddCategory(string catName)
    //    {
    //        var existingItemId = await GetCategoryIdByName(catName);
    //        if (existingItemId == Guid.Empty)
    //        {
    //            var item = new Category() { Name = catName };
    //            await categoryRepository.CreateItemAsync(item);
    //        }
    //    }

    //    public async Task DeleteCategory(string catId)
    //    {
    //        if (!string.IsNullOrEmpty(catId))
    //            await categoryRepository.DeleteItemAsync(catId);
    //    }

    //    public async Task<Guid> GetCategoryIdByName(string catName)
    //    {
    //        if (string.IsNullOrEmpty(catName) || StringSmartComparer.Equal(catName, SummaryName))
    //            return Guid.Empty;

    //        var result = await categoryRepository.GetItemAsync(c => c.Name == catName);
    //        return result != null ? result.Id : Guid.Empty;
    //    }
    //}
}
