﻿using System;

namespace LoftShop.ViewModels
{
    public class ProductViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Visible { get; set; }
        public int Price { get; set; }
        public int Discount { get; set; }
        public string Description { get; set; }
    }
}
