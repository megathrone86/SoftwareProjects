﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LoftShop.Data;
using LoftShop.ViewModels;
using LoftShop.Models;

namespace LoftShop.Controllers
{
    public class ShopController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var pManager = new ProductsManager();
            var products = await pManager.GetVisibleProducts(new List<string>() { null });
            var model = products.Select(t => GetViewModel(t)).ToList();
            ViewData["Products"] = model;
            return View("ProductsView");
        }

        public async Task<IActionResult> ShowCategory(string catName)
        {
            var pManager = new ProductsManager();
            var cManager = new CategoriesManager();

            var categoryId = await cManager.GetCategoryIdByName(catName);
            var products = await pManager.GetVisibleProducts(new List<string>() { categoryId });
            var model = products.Select(t => GetViewModel(t)).ToList();
            ViewData["Products"] = model;
            return View("ProductsView");
        }

        public async Task<IActionResult> ShowProduct(string productId)
        {
            var pManager = new ProductsManager();
            var cManager = new CategoriesManager();

            var product = await pManager.GetProduct(productId);
            return View("ProductView", GetViewModel(product));
        }

        ProductViewModel GetViewModel(Product p)
        {
            var result = new ProductViewModel()
            {
                Id = p.Id,
                Name = p.Name,
                Price = p.Price,
                Discount = p.Discount
            };
            return result;
        }
    }
}
