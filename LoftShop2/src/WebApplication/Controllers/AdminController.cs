﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LoftShop.Data;
using LoftShop.Models;
using System;
using System.Linq;
using LoftShop.ViewModels;

namespace LoftShop.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Categories()
        {
            var manager = new CategoriesManager();
            var categories = await manager.GetAllCategories();
            var model = categories.Select(t => GetCategoryViewModel(t)).ToList();
            return View(model);
        }

        public async Task<IActionResult> DeleteCategory(string catId)
        {
            var manager = new CategoriesManager();
            await manager.DeleteCategory(catId);
            return RedirectToAction("Categories");
        }

        public async Task<IActionResult> ShowCategory(string catId)
        {
            var manager = new CategoriesManager();
            var category = await manager.GetCategory(catId);
            var model = GetCategoryViewModel(category);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditCategory(string Id, string Name, bool Visible)
        {
            var manager = new CategoriesManager();
            await manager.UpdateCategory(new Category() { Id = Id, Name = Name, IsVisible = Visible });
            return RedirectToAction("Categories");
        }

        [HttpPost]
        public async Task<IActionResult> AddCategory(string catName)
        {
            var manager = new CategoriesManager();
            await manager.AddCategory(catName);
            return RedirectToAction("Categories");
        }

        public async Task<IActionResult> Products()
        {
            var manager = new ProductsManager();
            var products = await manager.GetAllProducts();
            var model = products.Select(t => GetProductViewModel(t)).ToList();
            return View(model);
        }

        ProductViewModel GetProductViewModel(Product item)
        {
            return new ProductViewModel() { Id = item.Id, Name = item.Name, Visible = item.IsVisible };
        }

        ProductViewModel GetProductViewModelFull(Product item)
        {
            return new ProductViewModel() { Id = item.Id, Name = item.Name, Visible = item.IsVisible, Description = item.Description };
        }

        CategoryViewModel GetCategoryViewModel(Category item)
        {
            return new CategoryViewModel() { Id = item.Id, Name = item.Name, Visible = item.IsVisible };
        }
    }
}
