﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoftShop.Helpers
{
    public class ErrorLogger
    {
        static readonly string ErrorFolder = "_ERRORS";

        public static void Log(Exception ex)
        {
            string dir = Path.Combine(FileHelper.AppRoot, ErrorFolder);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            string filePath = Path.Combine(dir, "daylog.txt");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Время: {DateTime.Now}");
            sb.AppendLine($"Ошибка: {ex}");
            sb.AppendLine("======================================");

            if (File.Exists(filePath))
            {
                var dt = File.GetCreationTime(filePath);
                if (dt.Date != DateTime.Now.Date)
                    File.Delete(filePath);
            }

            File.AppendAllText(filePath, sb.ToString());
        }
    }
}
