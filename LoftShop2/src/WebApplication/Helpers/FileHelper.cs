﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShop.Helpers
{
    public class FileHelper
    {
        public static string LocalFolderName { get { return "LocalFilesDB"; } }
        public static string LocalFolderPath { get { return AppRoot + "\\" + LocalFolderName; } }
        public static string AppRoot { get; private set; }

        public static void Init(IHostingEnvironment env)
        {
            AppRoot = env.WebRootPath;
        }
    }
}
