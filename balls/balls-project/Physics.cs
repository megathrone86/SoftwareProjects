﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace balls_project
{
    public class Physics
    {
        public static int BallSize = 60;
        public static int BallSpeed = 100;
        public static long TouchTimeout = 400;

        public static int BallHalfSize
        {
            get
            {
                return BallSize / 2;
            }
        }

        public List<Ball> Balls;
        public Size ClientSize;

        public Physics()
        {
            Balls = new List<Ball>();
        }

        public void MoveBall(Ball ball, long time)
        {
            ball.Pos.X += ball.Speed.X * time / 1000;
            ball.Pos.Y += ball.Speed.Y * time / 1000;
        }

        public void CalcBallInterceptWithEdges(Ball ball)
        {
            if (ball.Pos.X < BallHalfSize)
            {
                ball.Pos.X = BallHalfSize;
                ball.Speed.X = -ball.Speed.X;
                ball.LastTouchedBall = null;
            }

            if (ball.Pos.Y < BallHalfSize)
            {
                ball.Pos.Y = BallHalfSize;
                ball.Speed.Y = -ball.Speed.Y;
                ball.LastTouchedBall = null;
            }

            if (ball.Pos.X > ClientSize.Width - BallHalfSize)
            {
                ball.Pos.X = ClientSize.Width - BallHalfSize;
                ball.Speed.X = -ball.Speed.X;
                ball.LastTouchedBall = null;
            }

            if (ball.Pos.Y > ClientSize.Height - BallHalfSize)
            {
                ball.Pos.Y = ClientSize.Height - BallHalfSize;
                ball.Speed.Y = -ball.Speed.Y;
                ball.LastTouchedBall = null;
            }
        }

        private float GetDistanceQ(PointF p1, PointF p2)
        {
            return (p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y);
        }

        private float GetDistance(PointF p1, PointF p2)
        {
            return (float)Math.Sqrt(GetDistanceQ(p1, p2));
        }

        private PointF GetPerpendicularVector(PointF v1, bool ccw)
        {
            float x = -v1.Y;
            float y = v1.X;

            if ((v1.X > 0) && (v1.Y > 0))
            {
                x = -x;
                y = -y;
            }

            if ((v1.X < 0) && (v1.Y > 0))
            {
                x = -x;
                y = -y;
            }

            return new PointF(x, y);
        }

        private float GetVectorLength(PointF vec)
        {
            return (float)Math.Sqrt(vec.X * vec.X + vec.Y * vec.Y);
        }

        private PointF GetNormalizedVector(PointF vec)
        {
            float length = GetVectorLength(vec);
            return new PointF(vec.X / length, vec.Y / length);
        }

        public void CalcBallInterceptWithAnotherBall(Ball ball, Ball ball2)
        {
            if (ball == ball2)
                return;

            if ((ball.LastTouchedBall == ball2) &&
                (ball.LastTouchTime + TouchTimeout > Environment.TickCount))
                return;

            if ((ball2.LastTouchedBall == ball) &&
                (ball2.LastTouchTime + TouchTimeout > Environment.TickCount))
                return;

            float ballSizeQ = BallSize * BallSize;
            float distanceQ = GetDistanceQ(ball2.Pos, ball.Pos);
            float deltaQ = distanceQ - ballSizeQ;

            if (Math.Abs(deltaQ) > ballSizeQ / 8)
                return;

            {
                float t = ball.Speed.X;
                ball.Speed.X = ball2.Speed.X;
                ball2.Speed.X = t;
            }

            {
                float t = ball.Speed.Y;
                ball.Speed.Y = ball2.Speed.Y;
                ball2.Speed.Y = t;
            }

            ball.LastTouchedBall = ball2;
            ball.LastTouchTime = Environment.TickCount;
            ball2.LastTouchedBall = ball;
            ball2.LastTouchTime = Environment.TickCount;
        }

        //public void CalcBallInterceptWithAnotherBall(Ball ball, Ball ball2)
        //{
        //    if (ball == ball2)
        //        return;

        //    if ((ball.Speed.X == 0) && (ball.Speed.Y == 0))
        //        return;

        //    float ballSizeQ = BallSize * BallSize;

        //    float distanceQ = GetDistanceQ(ball2.Pos, ball.Pos);
        //    if (distanceQ > ballSizeQ * 1f)
        //        return;

        //    ball1_pos = ball.Pos;

        //    PointF center = new PointF(
        //        (ball2.Pos.X + ball.Pos.X) / 2,
        //        (ball2.Pos.Y + ball.Pos.Y) / 2
        //        );

        //    PointF totalSpeed = new PointF(
        //        ball.Speed.X - ball2.Speed.X,
        //        ball.Speed.Y - ball2.Speed.Y
        //        );

        //    float totalSpeed_length = GetVectorLength(totalSpeed);
        //    if (totalSpeed_length == 0)
        //        return;

        //    PointF t = new PointF(
        //        ball.Pos.X + BallHalfSize * (totalSpeed.X / totalSpeed_length),
        //        ball.Pos.Y + BallHalfSize * (totalSpeed.Y / totalSpeed_length)
        //        );

        //    float side = GetDistance(center, t);

        //    float cos_b = (BallHalfSize * BallHalfSize * 2 - side * side) /
        //        (2 * BallHalfSize * BallHalfSize);
        //    float sin_b = (float)Math.Sqrt(1f - cos_b * cos_b);

        //    //ball2
        //    {
        //        float spd = totalSpeed_length * cos_b;
        //        spd = BallSpeed;

        //        ball2.Speed = new PointF(
        //            center.X - ball.Pos.X,
        //            center.Y - ball.Pos.Y);

        //        ball2.Speed = GetNormalizedVector(ball2.Speed);
        //        ball2.Speed.X = ball2.Speed.X * spd;
        //        ball2.Speed.Y = ball2.Speed.Y * spd;
        //    }

        //    //ball1
        //    {
        //        float spd = totalSpeed_length * sin_b;
        //        spd = BallSpeed;

        //        ball.Speed = GetPerpendicularVector(ball2.Speed, false);
        //        ball.Speed = GetNormalizedVector(ball.Speed);
        //        ball.Speed.X = ball.Speed.X * spd;
        //        ball.Speed.Y = ball.Speed.Y * spd;
        //    }

        //    pause = true;
        //}

        private bool working = false;
        public void MoveAll(long timeDelta)
        {
            if (working)
                return;

            working = true;

            try
            {
                foreach (Ball ball in Balls)
                {
                    //продвинем шарик вперед
                    MoveBall(ball, timeDelta);
                    //проверим, не уперся ли он в границу экрана (нужно поменять вектор скорости)
                    CalcBallInterceptWithEdges(ball);

                    //проверим, пересечение с другими шариками
                    for (int i = 0; i < Balls.Count; i++)
                        CalcBallInterceptWithAnotherBall(ball, Balls[i]);
                }
            }
            finally            
            {
                working = false;
            }
        }
    }
}
