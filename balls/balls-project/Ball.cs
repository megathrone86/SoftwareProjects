﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace balls_project
{
    public class Ball
    {
        public PointF Pos;
        public PointF Speed;

        public Ball LastTouchedBall;
        public long LastTouchTime;

        public Pen Pen;

        public override string ToString()
        {
            return Pen.Color.ToString();
        }

        public Ball()
        {
            Random randomGen = new Random();
            Color randomColor = Color.FromArgb(randomGen.Next(255),
                randomGen.Next(255), randomGen.Next(255));
            Pen = new Pen(randomColor);
        }

        public void Draw(Graphics g)
        {
            Rectangle rect = new Rectangle(
                (int)(Pos.X - Physics.BallHalfSize),
                 (int)(Pos.Y - Physics.BallHalfSize),
                 (int)(Physics.BallSize),
                 (int)(Physics.BallSize));
            g.DrawEllipse(Pen, rect);
        }
    }
}
