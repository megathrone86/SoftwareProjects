﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace balls_project
{
    public partial class Form1 : Form
    {        
        private Timer timer;
        private Physics physics = new Physics();

        public Form1()
        {
            InitializeComponent();
            
            DoubleBuffered = true;

            lastCall = Environment.TickCount;
            timer = new Timer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 20;
            timer.Start();
        }

        private long lastCall;
        void timer_Tick(object sender, EventArgs e)
        {
            long timeDelta = Environment.TickCount - lastCall;
            if (timeDelta>100)
                timeDelta = 100;
            physics.MoveAll(timeDelta);

            lastCall = Environment.TickCount;
            DoRefresh();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            ResizeForm();
        }

        public void ResizeForm()
        {
            pictureBox1.Top = 10;
            pictureBox1.Left = 0;
            pictureBox1.Width = ClientSize.Width;
            pictureBox1.Height = ClientSize.Height - pictureBox1.Top;

            physics.ClientSize = pictureBox1.ClientSize;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            ResizeForm();
        }

        public void DoRefresh()
        {
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            DrawAll(e.Graphics, pictureBox1.ClientRectangle);            
        }

        private static Brush brush = new SolidBrush(Color.White);
        private void DrawAll(Graphics g, Rectangle clientRect)
        {            
            g.FillRectangle(brush, pictureBox1.ClientRectangle);

            foreach (Ball ball in physics.Balls)
                ball.Draw(g);            
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Ball ball = new Ball();
                ball.Pos.X = e.Location.X;
                ball.Pos.Y = e.Location.Y;
                physics.Balls.Add(ball);

                DoRefresh();
            }

            if (e.Button == MouseButtons.Right)
            {
                foreach (Ball ball in physics.Balls)
                {
                    float dX = e.Location.X - ball.Pos.X;
                    float dY = e.Location.Y - ball.Pos.Y;

                    float vectorLength =(float) Math.Sqrt(dX * dX + dY * dY);
                    ball.Speed.X = Physics.BallSpeed * dX / vectorLength;
                    ball.Speed.Y = Physics.BallSpeed * dY / vectorLength;
                }
            }            
        }
    }
}
