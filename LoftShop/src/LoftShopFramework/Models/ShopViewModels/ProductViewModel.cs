﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShopFramework.Models.ShopViewModels
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int Discount { get; set; }
    }
}
