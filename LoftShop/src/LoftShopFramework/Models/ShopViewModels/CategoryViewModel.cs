﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShopFramework.Models.ShopViewModels
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
    }
}
