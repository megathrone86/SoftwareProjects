﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShopFramework.Data {
    public class CategoryRepository {
        ApplicationDbContext context;

        public CategoryRepository() {
            this.context = new ApplicationDbContext(new Microsoft.EntityFrameworkCore.DbContextOptions<ApplicationDbContext>());
        }
        public CategoryRepository(ApplicationDbContext context) {
            this.context = context;
        }

        public List<Category> GetCategories() {
            return context.Categories.ToList();
        }
    }
}
