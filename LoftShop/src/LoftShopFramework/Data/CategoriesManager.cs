﻿using LoftShopFramework.Models.ShopViewModels;
using LoftShopFramework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShopFramework.Data
{
    public class CategoriesManager
    {
        static List<Category> data;

        static CategoriesManager()
        {
            data = new List<Category>()
            {
                new Category() { Id = Guid.NewGuid(), Name = "Диваны" },
                new Category() { Id = Guid.NewGuid(), Name = "Столы" },
                new Category() { Id = Guid.NewGuid(), Name = "Стулья" }
            };
        }

        public string SummaryName { get { return "Все категории"; } }

        public IEnumerable<CategoryViewModel> GetCategories()
        {
            return data.ConvertAll<CategoryViewModel>(c => GetViewModel(c));
        }

        CategoryViewModel GetViewModel(Category c)
        {
            return new CategoryViewModel() { Name = c.Name };
        }

        public Guid GetCategoryByName(string name)
        {
            if (string.IsNullOrEmpty(name) || StringSmartComparer.Equal(name, SummaryName))
                return Guid.Empty;

            return data.First(c => StringSmartComparer.Equal(c.Name, name)).Id;
        }
    }
}
