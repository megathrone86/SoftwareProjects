﻿using LoftShopFramework.Models.ShopViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShopFramework.Data
{
    public class ProductsManager
    {
        static List<Product> data;

        static ProductsManager()
        {
            data = new List<Product>()
            {
                CreateProductTest("Диваны", "Диван большой", 100000),
                CreateProductTest("Стулья", "Стул битардский", 20000),
                CreateProductTest("Диваны", "Пуфик", 60000),
                CreateProductTest("Диваны", "Нано диван", 10000),
                CreateProductTest("Диваны", "Раскладной гигантский", 160000),
                CreateProductTest("Диваны", "Черная вдова", 120000),
            };
        }

        static Product CreateProductTest(string catName, string name, int price)
        {
            var catId = new CategoriesManager().GetCategoryByName(catName);
            return new Product()
            {
                Id = Guid.NewGuid(),
                Categories = new List<Guid>() { catId },
                Name = name,
                Price = price
            };
        }

        public IEnumerable<ProductViewModel> GetProducts(List<Guid> categories)
        {
            return data.Where(p =>
            {
                if (categories.Contains(Guid.Empty))
                    return true;
                foreach (var cat in categories)
                {
                    if (p.Categories.Contains(cat))
                        return true;
                }
                return false;
            }).ToList().ConvertAll<ProductViewModel>(p => GetViewModel(p));
        }

        ProductViewModel GetViewModel(Product p)
        {
            var result = new ProductViewModel()
            {
                Name = p.Name,
                Price = p.Price,
                Discount = p.Discount
            };
            return result;
        }
    }
}
