﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LoftShopFramework.Data
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public List<Guid> Categories { get; internal set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public int Discount { get; set; }
    }
}
