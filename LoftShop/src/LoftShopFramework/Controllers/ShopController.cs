﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LoftShopFramework.Data;

namespace LoftShopFramework.Controllers
{
    public class ShopController : Controller
    {
        public IActionResult Index()
        {
            var pManager = new ProductsManager();
            var products = pManager.GetProducts(new List<Guid>() { Guid.Empty });
            return View("ProductsView");
        }

        public IActionResult ShowCategory(string catName)
        {
            var pManager = new ProductsManager();
            var cManager = new CategoriesManager();

            var categoryId = cManager.GetCategoryByName(catName);
            var products = pManager.GetProducts(new List<Guid>() { categoryId });
            return View("ProductsView");
        }
    }
}
