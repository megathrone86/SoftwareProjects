﻿using System;

namespace LoftShopFramework.Services
{
    internal class StringSmartComparer
    {
        internal static bool Equal(string name1, string name2)
        {
            if (name1 == name2)
                return true;
            if (name1 == null || name2 == null)
                return false;
            return name1.ToLower().Trim() == name2.ToLower().Trim();
        }
    }
}