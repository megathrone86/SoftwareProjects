﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Shared.Helpers;
using Windows.ApplicationModel.Background;

namespace update_task {
    public sealed class UpdateFeedsTask : IBackgroundTask {
        public async void Run(IBackgroundTaskInstance taskInstance) {
            var deferral = taskInstance.GetDeferral();
            DebugHelper.WriteLine("task started");

            try {
                await Global.Model.LoadData();
                await Global.Model.UpdateAll(false);
                //save is already implemented in ProcessUpdateTasks
            } catch(Exception ex) {
                DebugHelper.WriteLine(ex);
            } finally {
                DebugHelper.WriteLine("task completed");
                deferral.Complete();
            }
        }
    }
}
