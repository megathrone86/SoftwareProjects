﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Helpers {
    public class TextAttribute : Attribute {
        public string Text;
        public TextAttribute(string text) {
            Text = text;
        }
    }

    public static class EnumExtender {
        public static string GetEnumDescription(Enum value) {
            FieldInfo fi = value.GetType().GetRuntimeField(value.ToString());
            TextAttribute[] attributes =
                (TextAttribute[])fi.GetCustomAttributes(
                    typeof(TextAttribute), false);

            if(attributes.Length > 0) {
                return MyLocalizationHelper.Instance.GetValue(attributes[0].Text);
            } else {
                return value.ToString();
            }
        }
    }
}
