﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Shared.Helpers {
    public class AsyncHelper {
        public static async Task RunOnUI(Action action, bool useUI) {
            if(useUI)
                await RunOnUI(CoreApplication.MainView.CoreWindow.Dispatcher, action, useUI);
            else
                await RunOnUI(null, action, useUI);
        }

        public static async Task RunOnUI(CoreDispatcher Dispatcher, Action action, bool useUI) {
            if(useUI)
                await Dispatcher.RunAsync(CoreDispatcherPriority.High,
                        () => { action(); });
            else
                action();
        }
    }
}
