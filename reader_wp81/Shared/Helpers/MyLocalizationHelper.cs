﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Helpers {
    public class MyLocalizationHelper : LocalizationHelper {
        public static MyLocalizationHelper Instance = new MyLocalizationHelper();

        protected override string DefaultLanguage { get { return "en"; } }
        protected override string ForceLanguage { get { return DebugParams.TestForceLanguage; } }

        protected override void FillValues(Dictionary<string, Dictionary<string, string>> values) {
            values.Add("en", new Dictionary<string, string>() {
                {"Shared.Ok", "ok" },
                {"Shared.Cancel", "cancel" },
                {"Shared.Save", "save" },
                {"Shared.ErrorMsg", "An error has been occured." },
                {"Shared.Add", "add" },
                {"Shared.Refresh", "refresh" },
                {"Shared.ReadAll", "read all" },
                {"Shared.Options", "options" },

                {"AddFeedPage.Header", "Add new feed" },
                {"AddFeedPage.ValidationErrorMsg1", "Non-actual validation data. New validation is required." },
                {"AddFeedPage.ValidationErrorMsg2", "Empty URL." },
                {"AddFeedPage.ValidationErrorMsg3", "You are already following this feed." },

                {"DeleteFeedDialog.Title", "delete confirmation" },
                {"DeleteFeedDialog.Message", "Are you sure to delete {0}?" },

                {"FeedsPage.Header", "Feeds" },
                {"FeedsPage.UpdateFeed", "update" },
                {"FeedsPage.DeleteFeed", "delete" },
                {"FeedsPage.LastUpdated", "Last updated" },
                {"FeedsPage.NewMessages", "New messages" },

                {"MessageListPage.UnreadMessages", "Unread messages:" },
                {"MessageListPage.ReadMessages", "Read messages:" },
                {"MessageListPage.Title", "Messages" },
                {"MessageListPage.Title2", "New messages ({0})" },
                {"MessageListPage.Title3", "All messages ({0}/{1})" },
                {"MessageListPage.ShowUnread", "show unread" },
                {"MessageListPage.ShowAll", "show all" },
                {"MessageListPage.MarkAll", "mark all messages as read" },
                {"MessageListPage.MarkAllQuestion", "Are you sure to mark {0} messages as read?" },

                {"MessagePage.Prev", "previous" },
                {"MessagePage.Next", "next" },
                {"MessagePage.Share", "share" },
                {"MessagePage.Of", "of" },
                {"MessagePage.MakeRead", "make read" },
                {"MessagePage.LoadAttachments", "load all attachments" },
                {"MessagePage.NotSupportedMessage", "File type {0} is not supported." },

                {"OptionsPage.Title", "Options" },
                {"OptionsPage.FeedUpdateInterval", "How often should feeds be updated?" },
                {"OptionsPage.MessageReadInterval", "How fast message becomes read?" },

                {"Time.JustNow", "just now" },
                {"Time.MinutesAgo", "{0} minutes ago" },
                {"Time.MinuteAgo", "{0} minute ago" },
                {"Time.HoursAgo", "{0} hours ago" },
                {"Time.HourAgo", "{0} hour ago" },
                {"Time.DaysAgo", "{0} days ago" },
                {"Time.DayAgo", "{0} day ago" },

                {"MessageReadInterval.VeryFast", "Very fast" },
                {"MessageReadInterval.Fast", "Fast" },
                {"MessageReadInterval.Normal", "Normal" },
                {"MessageReadInterval.Slow", "Slow" },

                {"FeedUpdateInterval.DoNotUpdate", "Do not update" },
                {"FeedUpdateInterval.Hour1", "Every 1 hour" },
                {"FeedUpdateInterval.Hour2", "Every 2 hours" },
                {"FeedUpdateInterval.Hour4", "Every 4 hours" },
                {"FeedUpdateInterval.Hour8", "Every 8 hours" },
                {"FeedUpdateInterval.Hour12", "Every 12 hours" },
                {"FeedUpdateInterval.Day", "Every day" },

                {"FeedStatus.Updating", "Updating..." },
                {"FeedStatus.Queued", "Queued" },
                {"FeedStatus.UpdateFailedMsg", "Update has failed" },
            });

            values.Add("ru", new Dictionary<string, string>() {
                {"Shared.Ok", "ок" },
                {"Shared.Cancel", "отмена" },
                {"Shared.Save", "сохранить" },
                {"Shared.ErrorMsg", "Произошла ошибка." },
                {"Shared.Add", "добавить" },
                {"Shared.Refresh", "обновить" },
                {"Shared.ReadAll", "читать все" },
                {"Shared.Options", "настройки" },

                {"AddFeedPage.Header", "Добавить новый источник" },
                {"AddFeedPage.ValidationErrorMsg1", "Данные проверки неактуальны, требуется новая проверка." },
                {"AddFeedPage.ValidationErrorMsg2", "Не указан URL источника." },
                {"AddFeedPage.ValidationErrorMsg3", "Вы уже подписаны на этот источник." },

                {"DeleteFeedDialog.Title", "подтвердите удаление" },
                {"DeleteFeedDialog.Message", "Вы уверены, что хотите отписаться от {0}?" },

                {"FeedsPage.Header", "Источники" },
                {"FeedsPage.UpdateFeed", "обновить" },
                {"FeedsPage.DeleteFeed", "удалить" },
                {"FeedsPage.LastUpdated", "Обновлено" },
                {"FeedsPage.NewMessages", "Новых сообщений" },

                {"MessageListPage.UnreadMessages", "Новые сообщения:" },
                {"MessageListPage.ReadMessages", "Прочитанные сообщения:" },
                {"MessageListPage.Title", "Сообщения" },
                {"MessageListPage.Title2", "Новые сообщения ({0})" },
                {"MessageListPage.Title3", "Все сообщения ({0}/{1})" },
                {"MessageListPage.ShowUnread", "только новые" },
                {"MessageListPage.ShowAll", "новые + старые" },
                {"MessageListPage.MarkAll", "отметить все сообщения как прочитанные" },
                {"MessageListPage.MarkAllQuestion", "Вы уверены, что хотите отметить {0} сообщений как прочитанные?" },

                {"MessagePage.Prev", "предыдущее" },
                {"MessagePage.Next", "следующее" },
                {"MessagePage.Share", "поделиться" },
                {"MessagePage.Of", "из" },
                {"MessagePage.MakeRead", "прочитано" },
                {"MessagePage.LoadAttachments", "загрузить все вложения" },
                {"MessagePage.NotSupportedMessage", "Файлы с типом {0} не поддерживаются." },

                {"OptionsPage.Title", "Настройки" },
                {"OptionsPage.FeedUpdateInterval", "Как часто обновлять сообщения?" },
                {"OptionsPage.MessageReadInterval", "Как быстро переводить сообщения в прочитанные?" },

                {"Time.JustNow", "только что" },
                {"Time.MinutesAgo", "{0} минут назад" },
                {"Time.MinuteAgo", "{0} минуту назад" },
                {"Time.HoursAgo", "{0} часов назад" },
                {"Time.HourAgo", "{0} час назад" },
                {"Time.DaysAgo", "{0} дней назад" },
                {"Time.DayAgo", "{0} день назад" },

                {"MessageReadInterval.VeryFast", "Очень быстро" },
                {"MessageReadInterval.Fast", "Быстро" },
                {"MessageReadInterval.Normal", "Нормально" },
                {"MessageReadInterval.Slow", "Медленно" },

                {"FeedUpdateInterval.DoNotUpdate", "Не обновлять" },
                {"FeedUpdateInterval.Hour1", "Каждый час" },
                {"FeedUpdateInterval.Hour2", "Каждые 2 часа" },
                {"FeedUpdateInterval.Hour4", "Каждые 4 часа" },
                {"FeedUpdateInterval.Hour8", "Каждые 8 часов" },
                {"FeedUpdateInterval.Hour12", "Каждые 12 часов" },
                {"FeedUpdateInterval.Day", "Каждый день" },

                {"FeedStatus.Updating", "Обновляется..." },
                {"FeedStatus.Queued", "В очереди" },
                {"FeedStatus.UpdateFailedMsg", "Ошибка обновления" },
            });
        }
    }
}
