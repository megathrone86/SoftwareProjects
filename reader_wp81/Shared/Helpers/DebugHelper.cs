﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Helpers {
    public class DebugHelper {
        public static void WriteLine(object o) {
#if DEBUG
            if(o == null)
                Debug.WriteLine("[null object]");
            else
                Debug.WriteLine(o.ToString());
#endif
        }
    }
}
