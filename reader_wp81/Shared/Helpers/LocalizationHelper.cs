﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System.UserProfile;
using Windows.UI.Xaml;

namespace Shared.Helpers {
    public abstract class LocalizationHelper {
        protected abstract string DefaultLanguage { get; }
        protected virtual string ForceLanguage { get { return null; } }

        private Dictionary<string, Dictionary<string, string>> values;
        private Dictionary<string, string> currentDictionary = null;

        public LocalizationHelper() {
            values = new Dictionary<string, Dictionary<string, string>>();
            FillValues(values);
        }

        protected abstract void FillValues(Dictionary<string, Dictionary<string, string>> values);

        private Dictionary<string, string> GetCurrentDictionary() {
            if(ForceLanguage != null)
                return GetCurrentDictionary(ForceLanguage);

            string currentLang = GlobalizationPreferences.Languages[0].ToLower();
            return GetCurrentDictionary(currentLang);
        }

        private Dictionary<string, string> GetCurrentDictionary(string currentLang) {
            if(string.IsNullOrEmpty(currentLang))
                return null;

            if(values.ContainsKey(currentLang))
                return values[currentLang];

            if(currentLang.Contains('-')) {
                string[] ts = currentLang.Split('-');
                currentLang = ts[0];

                if(values.ContainsKey(currentLang))
                    return values[currentLang];
            }

            return null;
        }

        private void InitDictionary() {
            if(currentDictionary == null)
                currentDictionary = GetCurrentDictionary();
            if(currentDictionary == null)
                currentDictionary = GetCurrentDictionary(DefaultLanguage);
        }

        public string GetValue(string key) {
            InitDictionary();

            if(currentDictionary == null || !currentDictionary.ContainsKey(key))
                return null;
            return currentDictionary[key];
        }

        public void InitResources() {
            InitDictionary();

            if(currentDictionary == null)
                return;

            foreach(var kvp in currentDictionary) {
                if(string.IsNullOrEmpty(kvp.Key))
                    continue;

                Application.Current.Resources.Add(kvp.Key, kvp.Value);
            }
        }
    }
}
