﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Shared.Helpers {
    public static class FileHelper {
        public static async Task WriteDataToFileAsync(string fileName, string dataString) {
            byte[] data = Encoding.Unicode.GetBytes(dataString);

            await WriteDataToFileAsync(fileName, data);
        }

        public static async Task WriteDataToFileAsync(string fileName, byte[] data) {
            var folder = ApplicationData.Current.LocalFolder;
            var file = await folder.CreateFileAsync(fileName + ".tmp", CreationCollisionOption.ReplaceExisting);

            using(var s = await file.OpenStreamForWriteAsync()) {
                await s.WriteAsync(data, 0, data.Length);
            }

            await file.RenameAsync(fileName, NameCollisionOption.ReplaceExisting);
        }

        public static async Task<string> ReadFileContentsAsync(string fileName) {
            var folder = ApplicationData.Current.LocalFolder;

            try {
                var files = await folder.GetFilesAsync();
                var file = (from f in files
                            where f.Name.ToUpper() == fileName.ToUpper()
                            select f).FirstOrDefault();
                if(file == null)
                    return null;

                var stream = await file.OpenStreamForReadAsync();
                StringBuilder sb = new StringBuilder();
                using(var streamReader = new StreamReader(stream, Encoding.Unicode)) {
                    sb.Append(streamReader.ReadToEnd());
                }

                return sb.ToString();
            } catch(Exception ex) {
                DebugHelper.WriteLine(ex);
                return string.Empty;
            }
        }

        public static async Task<bool> FileExists(string fileName) {
            var folder = ApplicationData.Current.LocalFolder;

            try {
                var file = await folder.GetFileAsync(fileName);

                return file != null;
            } catch(Exception ex) {
                DebugHelper.WriteLine(ex);
                return false;
            }
        }

        public static async Task<StorageFile> GetFile(string fileName) {
            var folder = ApplicationData.Current.LocalFolder;

            var file = await folder.GetFileAsync(fileName);
            return file;
        }
    }
}
