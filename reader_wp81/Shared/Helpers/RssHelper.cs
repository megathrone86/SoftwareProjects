﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;

namespace Shared.Helpers {
    public class RssHelper {
        public class RssFileInfo {
            public string url { get; set; }
            public string type { get; set; }
        }
        public class RssEntryInfo {
            public string description { get; set; }
            public string guid { get; set; }
            public string link { get; set; }
            public DateTime pubDate { get; set; }
            public string title { get; set; }
            public RssFileInfo[] files { get; set; }
        }
        public class RssInfo {
            public string Link { get; set; }
            public string Title { get; set; }

            public List<RssEntryInfo> Entries { get; internal set; } = new List<RssEntryInfo>();
        }

        public async Task<RssInfo> LoadRss(string url) {
            CancellationTokenSource cts = new CancellationTokenSource();
            cts.CancelAfter(Global.FeedUpdateTimeoutSeconds * 1000);

            try {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url, cts.Token);

                byte[] data = await response.Content.ReadAsByteArrayAsync();

                string tempFileName = "temp";

                await FileHelper.WriteDataToFileAsync(tempFileName, data);

                var file = await FileHelper.GetFile(tempFileName);
                XmlDocument doc = await XmlDocument.LoadFromFileAsync(file);

                RssInfo result = new RssInfo();

                result.Title = doc.SelectSingleNode("rss/channel/title").InnerText;
                result.Link = url;//doc.SelectSingleNode("rss/channel/atom:link").InnerText;

                foreach(var n in doc.SelectNodes("rss/channel/item")) {
                    var files = new List<RssFileInfo>();
                    foreach(var n2 in n.SelectNodes("enclosure")) {
                        var newFile = new RssFileInfo();

                        var attrs = n2.Attributes.ToArray();
                        foreach(var attrNode in attrs) {
                            switch(attrNode.NodeName) {
                                case "url":
                                    newFile.url = attrNode.NodeValue.ToString();
                                    break;
                                case "type":
                                    newFile.type = attrNode.NodeValue.ToString();
                                    break;
                            }
                        }

                        files.Add(newFile);
                    }

                    result.Entries.Add(new RssEntryInfo() {
                        guid = n.SelectSingleNode("guid").InnerText,
                        link = n.SelectSingleNode("link").InnerText,
                        title = n.SelectSingleNode("title").InnerText,
                        pubDate = ParseRssDateTime(n.SelectSingleNode("pubDate").InnerText),
                        description = n.SelectSingleNode("description").InnerText,
                        files = files.ToArray()
                    });
                }

                return result;
            } catch(Exception ex) {
                DebugHelper.WriteLine(ex);
                throw ex;
            }
        }

        private DateTime ParseRssDateTime(string src) {
            try {
                if(src.IndexOf(" +") > 0)
                    src = src.Remove(src.IndexOf(" +"));
                if(src.IndexOf(" EDT") > 0)
                    src = src.Remove(src.IndexOf(" EDT"));

                return DateTime.Parse(src);
            } catch(Exception ex) {
                throw new Exception($"Cannot parse {src} as DateTime.", ex);
            }
        }
    }
}
