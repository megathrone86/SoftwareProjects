﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Data;
using Windows.Storage;

namespace Shared {
    public static class Global {
        public static int MessagesLifeTimeDays = 30;
        public static int MaxMessagesPerPage = 1000;
        public static int FeedUpdateTimeoutSeconds = 60;

        public static Model Model = new Model();

        public static List<Message> FindMessages(Feed filterFeed, bool onlyNew) {
            int total;
            return FindMessages(filterFeed, onlyNew, MaxMessagesPerPage, out total);
        }

        public static List<Message> FindMessages(Feed filterFeed, bool onlyNew, out int total) {
            return FindMessages(filterFeed, onlyNew, MaxMessagesPerPage, out total);
        }

        public static List<Message> FindMessages(Feed filterFeed, bool onlyNew, int maxMessages) {
            int total;
            return FindMessages(filterFeed, onlyNew, maxMessages, out total);
        }

        public static List<Message> FindMessages(Feed filterFeed, bool onlyNew, int maxMessages, out int total) {
            var result = Model.GetMessages(filterFeed, onlyNew, maxMessages, out total);
            return result;
        }
    }
}
