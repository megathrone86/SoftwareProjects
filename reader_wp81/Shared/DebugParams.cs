﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Shared {
    public static class DebugParams {
#if DEBUG
        public static string NewFeedDefaultValue = "http://www.nasa.gov/rss/dyn/image_of_the_day.rss";
        public static string TestForceLanguage = "ru";
#else
        public static string NewFeedDefaultValue = "http://";
        public static string TestForceLanguage = null;
#endif
    }
}
