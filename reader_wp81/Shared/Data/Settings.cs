﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Shared.Helpers;

namespace Shared.Data {
    public class Settings {
        public MessageReadIntervalEnum MessageReadInterval { get; set; }
        public FeedUpdateIntervalEnum FeedUpdateInterval { get; set; }

        public enum MessageReadIntervalEnum {
            [TextAttribute("MessageReadInterval.VeryFast")]
            VeryFast,
            [TextAttribute("MessageReadInterval.Fast")]
            Fast,
            [TextAttribute("MessageReadInterval.Normal")]
            Normal,
            [TextAttribute("MessageReadInterval.Slow")]
            Slow
        }

        public enum FeedUpdateIntervalEnum {
            [TextAttribute("FeedUpdateInterval.DoNotUpdate")]
            Off,
            [TextAttribute("FeedUpdateInterval.Hour1")]
            Hr1,
            [TextAttribute("FeedUpdateInterval.Hour2")]
            Hr2,
            [TextAttribute("FeedUpdateInterval.Hour4")]
            Hr4,
            [TextAttribute("FeedUpdateInterval.Hour8")]
            Hr8,
            [TextAttribute("FeedUpdateInterval.Hour12")]
            Hr12,
            [TextAttribute("FeedUpdateInterval.Day")]
            Day
        }

        public Settings() {
            MessageReadInterval = MessageReadIntervalEnum.Normal;
            FeedUpdateInterval = FeedUpdateIntervalEnum.Hr4;
        }

        public double GetMessageReadInterval() {
            switch(MessageReadInterval) {
                case MessageReadIntervalEnum.VeryFast: return 0.2;
                case MessageReadIntervalEnum.Fast: return 0.5;
                case MessageReadIntervalEnum.Normal: return 1.0;
                case MessageReadIntervalEnum.Slow: return 2.0;
                default: return 1.0;
            }
        }

        public uint GetFeedUpdateInterval() {
            switch(FeedUpdateInterval) {
                case FeedUpdateIntervalEnum.Off: return 0;
                case FeedUpdateIntervalEnum.Hr1: return 60;
                case FeedUpdateIntervalEnum.Hr2: return 60 * 2;
                case FeedUpdateIntervalEnum.Hr4: return 60 * 4;
                case FeedUpdateIntervalEnum.Hr8: return 60 * 8;
                case FeedUpdateIntervalEnum.Hr12: return 60 * 12;
                case FeedUpdateIntervalEnum.Day: return 60 * 24;
                default: return 0;
            }
        }

        public int GetMessageReadIntervalMs() {
            return (int)(1000.0 * GetMessageReadInterval());
        }
    }
}
