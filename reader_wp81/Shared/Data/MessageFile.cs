﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;

namespace Shared.Data {
    public class MessageFile {
        public string Type { get; set; }
        public string Url { get; set; }
    }
}
