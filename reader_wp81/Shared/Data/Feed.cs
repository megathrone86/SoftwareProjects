﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Shared.Helpers;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Core;

namespace Shared.Data {
    public class Feed {
        public string Url { get; set; }
        public bool BackgroundUpdate { get; set; }
        public double UpdateIntervalHours { get; set; }
        public string Title { get; set; }

        private DateTime lastUpdate = DateTime.Now;
        public DateTime LastUpdate {
            get { return lastUpdate; }
            set {
                lastUpdate = value;
                RaiseInfoUpdated();
            }
        }

        private string currentStatus;
        [XmlIgnore]
        public string CurrentStatus {
            get { return currentStatus; }
            set {
                currentStatus = value;
                RaiseInfoUpdated();
            }
        }

        [XmlIgnore]
        public List<Message> Messages {
            get {
                lock (Global.Model.MessagesLocker) {
                    var result = Global.Model.Messages.FindAll(t => t.Feed == this);
                    return result;
                }
            }
        }

        public event Action InfoUpdated;

        public void RaiseInfoUpdated() {
            if(InfoUpdated != null)
                InfoUpdated();
        }

        public async void UpdateByRss(RssHelper.RssInfo result, bool useUI) {
            await AsyncHelper.RunOnUI(() => {
                    this.LastUpdate = DateTime.Now;
                }, useUI);

            lock (Global.Model.MessagesLocker) {
                foreach(var m in result.Entries) {
                    if(Global.Model.Messages.Exists(t => t.Guid == m.guid))
                        continue;

                    if(Model.IsMessageOld(m.pubDate))
                        continue;

                    var files = new List<MessageFile>();
                    foreach (var f in m.files) {
                        files.Add(new MessageFile() { Url = f.url, Type = f.type });
                    }

                    var message = new Message() {
                        FeedUrl = this.Url,
                        Description = m.description,
                        Guid = m.guid,
                        Link = m.link,
                        PubDate = m.pubDate,
                        Title = m.title,
                        Files = files.ToArray(),
                        Feed = this
                    };

                    Global.Model.Messages.Add(message);
                }
            }
        }
    }
}
