﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shared.Helpers;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Shared.Data {
    public class Model {
        public object FeedsLocker = new object();
        public List<Feed> Feeds { get; private set; }

        public object MessagesLocker = new object();
        public List<Message> Messages { get; private set; }

        public object SettingsLocker = new object();
        public Settings Settings { get; private set; }

        private const string feedsFileName = "feeds";
        private const string messagesFileName = "messages";
        private const string settingsFileName = "settings";

        public event Action DataLoaded;

        internal List<Message> GetMessages(Feed filterFeed, bool onlyNew, int maxMessages, out int total) {
            List<Message> result = new List<Message>();
            lock (Global.Model.MessagesLocker) {
                total = 0;
                if(Global.Model.Messages != null) {
                    foreach(var m in Global.Model.Messages) {
                        if(filterFeed != null && m.Feed != filterFeed)
                            continue;
                        if(onlyNew && m.IsRead)
                            continue;

                        if(result.Count < maxMessages)
                            result.Add(m);

                        total++;
                    }
                }
            }

            return result;
        }

        public async Task LoadData() {
            var feeds = await LoadFeeds() ?? new List<Feed>();
            var messages = await LoadMessages() ?? new List<Message>();
            var settings = await LoadSettings() ?? new Settings();

            lock (FeedsLocker) {
                Feeds = feeds;
            }

            lock (MessagesLocker) {
                Messages = messages;
            }

            lock (SettingsLocker) {
                Settings = settings;
            }

            if(DataLoaded != null) {
                DataLoaded();
            }
        }

        private async Task<List<Feed>> LoadFeeds() {
            try {
                string fileContent = await FileHelper.ReadFileContentsAsync(feedsFileName);
                if(!string.IsNullOrEmpty(fileContent)) {
                    var r = SerializeHelper.Deserialize<List<Feed>>(fileContent);
                    return r;
                }
            } catch(Exception ex) {
                DebugHelper.WriteLine(ex);
            }

            return null;
        }

        private async Task<List<Message>> LoadMessages() {
            try {
                string fileContent = await FileHelper.ReadFileContentsAsync(messagesFileName);
                if(!string.IsNullOrEmpty(fileContent)) {
                    var r = SerializeHelper.Deserialize<List<Message>>(fileContent);

                    //additional processing
                    Global.Model.SortMessages(r);
                    return r;
                }
            } catch { }

            return null;
        }

        private async Task<Settings> LoadSettings() {
            try {
                string fileContent = await FileHelper.ReadFileContentsAsync(settingsFileName);
                if(!string.IsNullOrEmpty(fileContent)) {
                    var r = SerializeHelper.Deserialize<Settings>(fileContent);

                    return r;
                }
            } catch { }

            return null;
        }

        public async Task SaveFeedsAndMessages() {

            var feeds = new List<Feed>();
            lock (FeedsLocker) {
                if(Feeds != null)
                    feeds.AddRange(Feeds);
            }

            var messages = new List<Message>();
            lock (MessagesLocker) {
                if(Messages != null)
                    messages.AddRange(Messages);
            }

            {
                string s = SerializeHelper.Serialize(feeds);
                await FileHelper.WriteDataToFileAsync(feedsFileName, s);
            }

            {
                string s = SerializeHelper.Serialize(messages);
                await FileHelper.WriteDataToFileAsync(messagesFileName, s);
            }

            {
                string s = SerializeHelper.Serialize(Settings);
                await FileHelper.WriteDataToFileAsync(settingsFileName, s);
            }
        }

        public async Task SaveSettings() {
            string s = SerializeHelper.Serialize(Settings);
            await FileHelper.WriteDataToFileAsync(settingsFileName, s);
        }

        public static bool IsMessageOld(DateTime pubDate) {
            return DateTime.Now.Subtract(pubDate).TotalDays > Global.MessagesLifeTimeDays;
        }

        internal void ClearOldMessages() {
            lock (MessagesLocker) {
                var messagesToDelete = Messages.FindAll(m => IsMessageOld(m.PubDate));
                Messages.RemoveAll(m => messagesToDelete.Contains(m));
            }
        }

        public void SortMessages(List<Message> messages) {
            lock (MessagesLocker) {
                messages.Sort((c1, c2) => c1.PubDate.CompareTo(c2.PubDate));
            }
        }

        private bool updateAllWorking = false;
        private object updateTasksLocker = new object();
        private List<UpdateTask> updateTasks = new List<UpdateTask>();
        public async Task UpdateAll(bool useUI) {
            var feeds = new List<Feed>(Global.Model.Feeds);
            lock (Global.Model.FeedsLocker) {
                feeds.AddRange(Global.Model.Feeds);
            }

            await Update(feeds, useUI);
        }

        public async Task Update(List<Feed> feeds, bool useUI) {
            List<Feed> queuedFeeds = new List<Feed>();
            lock (updateTasksLocker) {
                foreach(var feed in feeds) {
                    var task = updateTasks.Find(t => t.Feed == feed);
                    if(task == null) {
                        updateTasks.Add(new UpdateTask() { Feed = feed });
                        queuedFeeds.Add(feed);
                    }
                }
            }

            await AsyncHelper.RunOnUI(() => {
                foreach(var queuedFeed in queuedFeeds) {
                    if(queuedFeed.CurrentStatus != MyLocalizationHelper.Instance.GetValue("FeedStatus.Updating"))
                        queuedFeed.CurrentStatus = MyLocalizationHelper.Instance.GetValue("FeedStatus.Queued");
                }
            }, useUI);

            if(!updateAllWorking) {
                updateAllWorking = true;
                await ProcessUpdateTasks(useUI);
            }
        }

        private async Task ProcessUpdateTasks(bool useUI) {
            while(updateTasks.Count > 0) {
                UpdateTask task;
                lock (updateTasksLocker) {
                    task = updateTasks[0];
                    updateTasks.RemoveAt(0);
                }

                await UpdateFeed(task.Feed, useUI);

                lock (MessagesLocker) {
                    Global.Model.ClearOldMessages();
                    Global.Model.SortMessages(Global.Model.Messages);
                }

                await Global.Model.SaveFeedsAndMessages();
            }

            updateAllWorking = false;
        }

        public async Task UpdateFeed(Feed feed, bool useUI) {

            try {
                await AsyncHelper.RunOnUI(() => {
                    feed.CurrentStatus = MyLocalizationHelper.Instance.GetValue("FeedStatus.Updating");
                }, useUI);

                RssHelper helper = new RssHelper();
                var result = await helper.LoadRss(feed.Url);

                feed.UpdateByRss(result, useUI);
            } catch(Exception ex) {
                await AsyncHelper.RunOnUI(() => {
                    feed.CurrentStatus = $"{MyLocalizationHelper.Instance.GetValue("FeedStatus.UpdateFailedMsg")}: " + ex.Message;
                }, useUI);
            } finally {
                await AsyncHelper.RunOnUI(() => {
                    feed.CurrentStatus = null;
                }, useUI);
            }
        }

        public async void DeleteFeed(Feed feed) {
            lock (Global.Model.FeedsLocker) {
                if(!Global.Model.Feeds.Remove(feed))
                    return;
            }

            lock (Global.Model.MessagesLocker) {
                Global.Model.Messages.RemoveAll(m => m.Feed == feed);
            }

            await Global.Model.SaveFeedsAndMessages();
        }
    }
}
