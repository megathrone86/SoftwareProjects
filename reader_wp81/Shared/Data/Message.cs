﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Shared.Helpers;
using Windows.Storage;

namespace Shared.Data {
    public class Message {
        public string FeedUrl { get; set; }

        public string Description { get; set; }
        public string Guid { get; set; }
        public string Link { get; set; }
        public DateTime PubDate { get; set; }
        public string Title { get; set; }

        public bool IsRead { get; set; }

        private Feed feed;
        [XmlIgnore]
        public Feed Feed {
            get {
                if(feed == null) {
                    lock (Global.Model.FeedsLocker) {
                        if(Global.Model.Feeds != null)
                            feed = Global.Model.Feeds.Find(f => f.Url == this.FeedUrl);
                    }
                }

                return feed;
            }
            set {
                feed = value;
            }
        }

        public MessageFile[] Files { get; set; }
    }
}
