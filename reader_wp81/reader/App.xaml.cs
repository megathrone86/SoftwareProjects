﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using reader.Common;
using reader.Helpers;
using Shared;
using Shared.Helpers;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace reader {
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application {
        private TransitionCollection transitions;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App() {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;

            this.UnhandledException += App_UnhandledException;
        }

        private void App_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(Pages.ErrorPage), e.Exception);
            e.Handled = true;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e) {
#if DEBUG
            //if(System.Diagnostics.Debugger.IsAttached) {
            //this.DebugSettings.EnableFrameRateCounter = true;
            //}
#endif
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if(rootFrame == null) {
                await Global.Model.LoadData();

                MyLocalizationHelper.Instance.InitResources();

                await BackgroundHelper.RegisterTask();

                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // MT: Register the Frame with the SuspensionManager.  
                SuspensionManager.RegisterFrame(rootFrame, "rootFrameKey");

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                if(e.PreviousExecutionState == ApplicationExecutionState.Terminated) {
                    try {
                        await SuspensionManager.RestoreAsync();
                    } catch(Exception ex) {
                        DebugHelper.WriteLine(ex);
                    }
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if(rootFrame.Content == null) {
                // Removes the turnstile navigation for startup.
                if(rootFrame.ContentTransitions != null) {
                    this.transitions = new TransitionCollection();
                    foreach(var c in rootFrame.ContentTransitions) {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
                rootFrame.Navigated += RootFrame_Navigated;

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if(!rootFrame.Navigate(typeof(Pages.FeedsPage), e.Arguments)) {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
            UpdateTaskCompletedSubscribe();
        }

        private Page lastPage;

        private void RootFrame_Navigated(object sender, NavigationEventArgs e) {
            UpdateTaskCompletedSubscribe();
            lastPage = e.Content as Page;
        }

        private void UpdateTaskCompletedSubscribe() {
            BackgroundHelper.Task.Completed -= Task_Completed;
            BackgroundHelper.Task.Completed += Task_Completed;
        }

        private async void Task_Completed(Windows.ApplicationModel.Background.BackgroundTaskRegistration sender, Windows.ApplicationModel.Background.BackgroundTaskCompletedEventArgs args) {
            DebugHelper.WriteLine("task completed catched");

            if(lastPage != null) {
                await lastPage.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => {
                    await Global.Model.LoadData();
                    lastPage.Frame.Navigate(lastPage.GetType());
                });
            }
        }

        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e) {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e) {
            var deferral = e.SuspendingOperation.GetDeferral();

            await SuspensionManager.SaveAsync();

            deferral.Complete();
        }
    }
}