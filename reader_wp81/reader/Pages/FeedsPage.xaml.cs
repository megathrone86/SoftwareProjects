﻿using reader.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using reader.Helpers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Shared.Data;
using Shared;
using Shared.Helpers;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace reader.Pages {
    public sealed partial class FeedsPage : Page {
        private NavigationHelper navigationHelper;
        private ViewModel viewModel = new ViewModel();

        public FeedsPage() {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        public NavigationHelper NavigationHelper {
            get { return this.navigationHelper; }
        }

        public ViewModel DefaultViewModel {
            get { return this.viewModel; }
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
        }

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void btAddNewFeed_Click(object sender, RoutedEventArgs e) {
            Frame.Navigate(typeof(AddFeedPage));
        }

        private void grdMain_Tapped(object sender, TappedRoutedEventArgs e) {
        }

        public class ViewModel : BaseViewModel {
            private bool canUpdate = true;
            public bool CanUpdate {
                get { return canUpdate; }
                set {
                    canUpdate = value;
                    RaisePropertyUpdated("CanUpdate");
                }
            }

            public ObservableCollection<FeedInfo> Feeds {
                get {
                    var result = new ObservableCollection<FeedInfo>();

                    lock (Global.Model.FeedsLocker) {
                        if(Global.Model.Feeds != null) {
                            foreach(var feed in Global.Model.Feeds) {
                                result.Add(new FeedInfo(feed, this));
                            }
                        }
                    }

                    return result;
                }
            }

            public ViewModel() {
                Global.Model.DataLoaded += () => {
                    RaisePropertyUpdated("Feeds");
                };
            }

            public class FeedInfo : BaseViewModel {
                public Feed Feed { get; private set; }
                private ViewModel viewModel;

                public FeedInfo(Feed feed, ViewModel viewModel) {
                    this.Feed = feed;
                    this.viewModel = viewModel;

                    feed.InfoUpdated += Feed_InfoUpdated;
                }

                private void Feed_InfoUpdated() {
                    this.RaisePropertyUpdated("Title");
                    this.RaisePropertyUpdated("LastUpdateText");
                    this.RaisePropertyUpdated("Status");
                }

                public string Title { get { return Feed.Title; } }

                public string LastUpdateText {
                    get {
                        int n = (int)DateTime.Now.Subtract(Feed.LastUpdate).TotalMinutes;
                        string ts = MyLocalizationHelper.Instance.GetValue("Time.JustNow");

                        if(n > 0) {
                            ts = (n > 1) ? string.Format(MyLocalizationHelper.Instance.GetValue("Time.MinutesAgo"), n) :
                                string.Format(MyLocalizationHelper.Instance.GetValue("Time.MinuteAgo"), n);
                        }

                        if(n > 59) {
                            n = n / 60;
                            ts = (n > 1) ? string.Format(MyLocalizationHelper.Instance.GetValue("Time.HoursAgo"), n) :
                                string.Format(MyLocalizationHelper.Instance.GetValue("Time.HourAgo"), n);
                        }

                        if(n > 24) {
                            n = n / 24;
                            ts = (n > 1) ? string.Format(MyLocalizationHelper.Instance.GetValue("Time.DaysAgo"), n) :
                                string.Format(MyLocalizationHelper.Instance.GetValue("Time.DayAgo"), n);
                        }

                        return $"{MyLocalizationHelper.Instance.GetValue("FeedsPage.LastUpdated")}: {ts}.";
                    }
                }

                public string Status {
                    get {
                        if(Feed.CurrentStatus != null)
                            return Feed.CurrentStatus;
                        else {
                            int totalNewMessages;
                            Global.FindMessages(Feed, true, out totalNewMessages);
                            return $"{MyLocalizationHelper.Instance.GetValue("FeedsPage.NewMessages")}: {totalNewMessages}. {LastUpdateText}";
                        }
                    }
                }
            }
        }

        private async void btUpdateAll_Click(object sender, RoutedEventArgs e) {
            if(!DefaultViewModel.CanUpdate)
                return;

            await Global.Model.UpdateAll(true);
        }

        private async void btViewAll_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                Frame.Navigate(typeof(MessageListPage), null);
            }
        }

        private async void grdItemRoot_Tapped(object sender, TappedRoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                var item = sender as FrameworkElement;
                object dataContext = item.DataContext;
                Frame.Navigate(typeof(MessageListPage), (dataContext as ViewModel.FeedInfo).Feed.Url);
            }
        }

        private void grdItemRoot_RightTapped(object sender, RightTappedRoutedEventArgs e) {
            var item = sender as FrameworkElement;
            object dataContext = item.DataContext;

            OpenPopup((dataContext as ViewModel.FeedInfo).Feed, sender);
            e.Handled = true;
        }

        private void OpenPopup(Feed f, object sender) {
            FrameworkElement fe = sender as FrameworkElement;
            if(fe.Name != "grdItemRoot") {
                object o = fe.FindName("grdItemRoot");
                if(o != null)
                    fe = o as FrameworkElement;
            }

            MenuFlyout mf = (MenuFlyout)this.Resources["FeedMenuFlyout"];
            mf.Placement = FlyoutPlacementMode.Bottom;
            mf.ShowAt(fe);
        }

        private async void MenuFlyoutItem_Delete_Click(object sender, RoutedEventArgs e) {
            var item = sender as FrameworkElement;
            object dataContext = item.DataContext;
            var feed = (dataContext as ViewModel.FeedInfo).Feed;

            ContentDialog dlg = new ContentDialog() {
                PrimaryButtonText = MyLocalizationHelper.Instance.GetValue("Shared.Ok"),
                SecondaryButtonText = MyLocalizationHelper.Instance.GetValue("Shared.Cancel"),
                Content = new TextBlock() {
                    Text = string.Format(MyLocalizationHelper.Instance.GetValue("DeleteFeedDialog.Message"), feed.Title)
                }
            };
            var root = dlg.Content;

            dlg.Closed += (s, a) => {
                if(a.Result == ContentDialogResult.Primary) {
                    Global.Model.DeleteFeed(feed);
                    viewModel.RaisePropertyUpdated("Feeds");
                    viewModel.RaisePropertyUpdated("CanUpdate");
                }
            };
            await dlg.ShowAsync();
        }

        private async void btOptions_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                Frame.Navigate(typeof(OptionsPage));
            }
        }

        private async void MenuFlyoutItem_Update_Click(object sender, RoutedEventArgs e) {
            var item = sender as FrameworkElement;
            object dataContext = item.DataContext;
            var feed = (dataContext as ViewModel.FeedInfo).Feed;

            await Global.Model.Update(new List<Feed>() { feed }, true);
        }

        private void feedMenuButton_Tapped(object sender, TappedRoutedEventArgs e) {
            var item = sender as FrameworkElement;
            object dataContext = item.DataContext;

            OpenPopup((dataContext as ViewModel.FeedInfo).Feed, sender);
            e.Handled = true;
        }
    }
}
