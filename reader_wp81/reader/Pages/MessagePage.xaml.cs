﻿using reader.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using reader.Helpers;
using Windows.UI.Core;
using Windows.ApplicationModel.DataTransfer;
using Shared.Data;
using Shared.Helpers;
using Shared;

namespace reader.Pages {
    public sealed partial class MessagePage : Page {
        private NavigationHelper navigationHelper;
        private ViewModel viewModel = new ViewModel();
        private DateTime startMessageViewTime;

        public MessagePage() {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the<see cref= "NavigationHelper" /> associated with this <see cref = "Page" />.
        /// </ summary >
        public NavigationHelper NavigationHelper {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref = "Page" />.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ViewModel DefaultViewModel {
            get { return this.viewModel; }
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
            string data = e.NavigationParameter as string;
            var args = SerializeHelper.Deserialize<MessagePageNavigateArgs>(data);

            List<Message> allMessages;
            Message message;
            args.Load(out allMessages, out message);
            viewModel.AllMessages = allMessages;
            viewModel.Message = message;

            if(e.PageState != null && e.PageState.ContainsKey("MessageGuid")) {
                string messageGuid = e.PageState["MessageGuid"] as string;

                lock (Global.Model.MessagesLocker) {
                    viewModel.Message = Global.Model.Messages.Find(m => m.Guid == messageGuid);
                }
            }

            SetMessage(viewModel.Message);

            if(e.PageState != null && e.PageState.ContainsKey("MessageHaveBeenRead")) {
                viewModel.MessageHaveBeenRead = (bool)e.PageState["MessageHaveBeenRead"];
            }
        }

        private async void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
            await OnChangeMessageOrExit();

            e.PageState["MessageGuid"] = viewModel.Message.Guid;
            e.PageState["IsMessageLoaded"] = viewModel.IsMessageLoaded;
            e.PageState["MessageHaveBeenRead"] = viewModel.MessageHaveBeenRead;
        }

        private async void SetMessage(Message message) {
            viewModel.IsMessageLoaded = false;
            viewModel.AttachmentsVisible = false;
            viewModel.CanMoveToNext = false;
            viewModel.CanMoveToPrev = false;
            await Task.Delay(1);

            viewModel.MessageHaveBeenRead = false;
            startMessageViewTime = DateTime.Now;
            viewModel.Message = message;

            //falls out of the MVVM conception :(
            webView.NavigateToString(viewModel.Message.Description);

            viewModel.UpdateMessageReadState();
        }

        private async Task OnChangeMessageOrExit() {
            TimeSpan messageViewTime = DateTime.Now.Subtract(startMessageViewTime);

            if(viewModel.MessageHaveBeenRead && !viewModel.Message.IsRead) {
                viewModel.Message.IsRead = true;
                await Global.Model.SaveFeedsAndMessages();
            }
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public class ViewModel : BaseViewModel {
            private Message message;
            public Message Message {
                get {
                    return message;
                }
                internal set {
                    message = value;
                    RaisePropertyUpdated("Title");
                    RaisePropertyUpdated("SecondTitle");
                    RaisePropertyUpdated("PubDate");
                    RaisePropertyUpdated("Body");
                    UpdateMessageReadState();
                }
            }

            public Visibility ProgressBarVisibility {
                get {
                    return IsMessageLoaded ? Visibility.Collapsed : Visibility.Visible;
                }
            }

            public Visibility WebViewVisibility {
                get {
                    return IsMessageLoaded ? Visibility.Visible : Visibility.Collapsed;
                }
            }

            public Visibility AttachmentsAvailable {
                get {
                    return IsMessageLoaded && Message != null && Message.Files != null
                        && Message.Files.Length > 0 ? Visibility.Visible : Visibility.Collapsed;
                }
            }

            public bool attachmentsVisible;
            public bool AttachmentsVisible {
                get {
                    return attachmentsVisible;
                }
                set {
                    attachmentsVisible = value;
                    RaisePropertyUpdated("AttachmentsVisibility");
                }
            }

            public Visibility AttachmentsVisibility { get {
                    return attachmentsVisible ? Visibility.Visible : Visibility.Collapsed; } }

            private bool isMessageLoaded;
            public bool IsMessageLoaded {
                get {
                    return isMessageLoaded;
                }
                set {
                    isMessageLoaded = value;

                    if(value) {
                        RaisePropertyUpdated("IsMessageLoaded");
                        RaisePropertyUpdated("ProgressBarVisibility");
                        RaisePropertyUpdated("WebViewVisibility");
                        RaisePropertyUpdated("AttachmentsAvailable");
                        UpdateMessageReadState();
                    }
                }
            }

            public bool CanMarkAsRead {
                get {
                    return Message != null && IsMessageLoaded && !MessageHaveBeenRead && !Message.IsRead;
                }
            }

            public bool MessageHaveBeenRead;

            public string Title {
                get {
                    return Message != null ? Message.Title : "";
                }
            }

            public string PubDate {
                get {
                    return Message != null ? Message.PubDate.ToString() : null;
                }
            }

            public string SecondTitle {
                get {
                    int n = AllMessages.IndexOf(Message);
                    return $"{n + 1} {MyLocalizationHelper.Instance.GetValue("MessagePage.Of")} {AllMessages.Count}";
                }
            }

            public string Body {
                get {
                    return Message != null ? Message.Description : null;
                }
            }

            public List<FileViewModel> Files {
                get {
                    var result = new List<FileViewModel>();
                    if(Message != null && Message.Files != null) {
                        foreach(var mf in Message.Files) {
                            result.Add(new FileViewModel(mf));
                        }
                    }

                    return result;
                }
            }

            private bool сanMoveToNext;
            public bool CanMoveToNext {
                get { return сanMoveToNext; }
                set {
                    сanMoveToNext = value;
                    RaisePropertyUpdated("CanMoveToNext");
                }
            }

            private bool сanMoveToPrev;
            public bool CanMoveToPrev {
                get { return сanMoveToPrev; }
                set {
                    сanMoveToPrev = value;
                    RaisePropertyUpdated("CanMoveToPrev");
                }
            }

            public List<Message> AllMessages { get; internal set; }

            public IconElement IsNewIcon {
                get {
                    BitmapIcon result = CanMarkAsRead ?
                        new BitmapIcon() { UriSource = new Uri(@"ms-appx:///Assets/lamp/lamp_on.png") } :
                        new BitmapIcon() { UriSource = new Uri(@"ms-appx:///Assets/lamp/lamp_off.png") };
                    return result;
                }
            }

            public void UpdateMessageReadState() {
                RaisePropertyUpdated("IsNewIcon");
                RaisePropertyUpdated("CanMarkAsRead");

                if(message == null) {
                    CanMoveToNext = false;
                    CanMoveToPrev = false;
                    return;
                }

                if(!message.IsRead && !MessageHaveBeenRead) {
                    CanMoveToNext = false;
                    CanMoveToPrev = false;
                    return;
                }

                if(message.IsRead || MessageHaveBeenRead) {
                    var nextMessage = FindNextMessage();
                    CanMoveToNext = nextMessage != null;

                    var prevMessage = FindPrevMessage();
                    CanMoveToPrev = prevMessage != null;
                    return;
                }
            }

            public Message FindNextMessage() {
                int currentMessageIndex = AllMessages.IndexOf(Message);
                int newMessageIndex = currentMessageIndex + 1;
                if(newMessageIndex > AllMessages.Count - 1)
                    return null;
                else
                    return AllMessages[newMessageIndex];
            }

            public Message FindPrevMessage() {
                int currentMessageIndex = AllMessages.IndexOf(Message);
                int newMessageIndex = currentMessageIndex - 1;
                if(newMessageIndex < 0)
                    return null;
                else
                    return AllMessages[newMessageIndex];
            }

            public class FileViewModel : BaseViewModel {
                private MessageFile mf;

                public bool IsImage {
                    get {
                        string typel = mf.Type.ToLower();
                        return typel == "image" || typel.StartsWith("image/");
                    }
                }

                public bool IsUnknown { get { return !IsImage; } }

                public string NotSupportedMessage { get {
                        return string.Format(MyLocalizationHelper.Instance.GetValue("MessagePage.NotSupportedMessage"), mf.Type);
                    } }

                public FileViewModel(MessageFile mf) {
                    this.mf = mf;
                }
            }
        }

        private async void btNextMessage_Click(object sender, RoutedEventArgs e) {
            viewModel.CanMoveToNext = false;
            viewModel.CanMoveToPrev = false;
            await OnChangeMessageOrExit();

            var nextMessage = viewModel.FindNextMessage();

            if(nextMessage != null)
                SetMessage(nextMessage);
            else
                navigationHelper.GoBack();
        }

        private async void btPrevMessage_Click(object sender, RoutedEventArgs e) {
            viewModel.CanMoveToNext = false;
            viewModel.CanMoveToPrev = false;
            await OnChangeMessageOrExit();

            var prevMessage = viewModel.FindPrevMessage();

            if(prevMessage != null)
                SetMessage(prevMessage);
            else
                navigationHelper.GoBack();
        }

        private async void webView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args) {
            await AddHacksToWebView();

            viewModel.IsMessageLoaded = true;
        }

        private async void backButton_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                navigationHelper.GoBack();
            }
        }

        private async Task AddHacksToWebView() {

            string test = @"
{
  var onscrollfunction=function(){
    currentPos = window.innerHeight + document.body.scrollTop;
    totalHeight = document.body.offsetHeight;
 
    //window.external.notify('scroll '+currentPos+'/'+totalHeight); 
    
    if (currentPos>=totalHeight)
      window.external.notify('scrolled_to_end');    
  };

  document.onscroll=onscrollfunction;
}";

            Message targetMessage = viewModel.Message;
            webView.ScriptNotify += async (s, e) => {
                if(e.Value == "scrolled_to_end") {
                    await Task.Delay(Global.Model.Settings.GetMessageReadIntervalMs());

                    if(targetMessage == viewModel.Message) {
                        viewModel.MessageHaveBeenRead = true;
                        viewModel.UpdateMessageReadState();
                    }
                }
            };

            await webView.InvokeScriptAsync("eval", new[] { test });

            Task task = new Task(async delegate {
                await Task.Delay(Global.Model.Settings.GetMessageReadIntervalMs());

                var dispatcher = this.Dispatcher;
                await AsyncHelper.RunOnUI(async () => {
                    string test2 = @"
{
    currentPos = window.innerHeight + document.body.scrollTop;
    totalHeight = document.body.offsetHeight;
 
    //window.external.notify('scroll '+currentPos+'/'+totalHeight); 
    
    if (currentPos>=totalHeight)
        window.external.notify('scrolled_to_end');
}";
                    await webView.InvokeScriptAsync("eval", new[] { test2 });
                }, true);
            });

            task.Start();
        }

        private bool dataTransferManagerInitialized = false;
        private async void btShare_Click(object sender, RoutedEventArgs e) {
            string messageText = null;

            if(!dataTransferManagerInitialized) {
                DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
                dataTransferManager.DataRequested += (s, a) => {
                    if(messageText == null)
                        return;

                    DataRequest request = a.Request;
                    request.Data.Properties.Title = viewModel.Message.Title;
                    request.Data.Properties.Description = "";

                    DataRequestDeferral deferral = request.GetDeferral();
                    try {
                        request.Data.SetText(messageText);
                    } finally {
                        deferral.Complete();
                    }
                };
                dataTransferManagerInitialized = true;
            }

            string test = @"
{
    text = document.body.innerText;
    window.external.notify('document_text '+text);
}";
            NotifyEventHandler handler = (s, a) => {
                string token = "document_text ";
                if(a.Value.StartsWith(token)) {
                    messageText = a.Value.Substring(token.Length);
                }
            };

            webView.ScriptNotify += handler;
            await webView.InvokeScriptAsync("eval", new[] { test });
            webView.ScriptNotify -= handler;

            if(messageText != null)
                DataTransferManager.ShowShareUI();
        }

        private void btIsNew_Click(object sender, RoutedEventArgs e) {
            if(!viewModel.CanMarkAsRead)
                return;

            viewModel.MessageHaveBeenRead = true;
            viewModel.UpdateMessageReadState();
        }

        private DataTemplate GetTemplate(ViewModel.FileViewModel dataContext) {
            //if(dataContext.IsImage)
            //    return this.Resources["fileImageTemplate"] as DataTemplate;

            return this.Resources["fileUnknownTemplate"] as DataTemplate;
        }

        private void itemRoot_Loaded(object sender, RoutedEventArgs e) {
            var root = (sender as ContentControl);
            var dataContext = root.DataContext as ViewModel.FileViewModel;

            root.ContentTemplate = GetTemplate(dataContext);
            root.Content = dataContext;
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e) {
            viewModel.AttachmentsVisible = !viewModel.AttachmentsVisible;
        }
    }
}
