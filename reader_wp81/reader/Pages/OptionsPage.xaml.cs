﻿using reader.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using reader.Helpers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Shared.Data;
using Shared.Helpers;
using Shared;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace reader.Pages {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OptionsPage : Page {
        private NavigationHelper navigationHelper;
        private ViewModel viewModel = new ViewModel();

        public OptionsPage() {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.        
        /// </summary>
        public ViewModel DefaultViewModel {
            get { return this.viewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public class ViewModel : BaseViewModel {
            public ComboBoxRecord<Settings.FeedUpdateIntervalEnum> FeedUpdateInterval { get; set; }
            public List<ComboBoxRecord<Settings.FeedUpdateIntervalEnum>> FeedUpdateIntervalItems { get; private set; }

            public ComboBoxRecord<Settings.MessageReadIntervalEnum> MessageReadInterval { get; set; }
            public List<ComboBoxRecord<Settings.MessageReadIntervalEnum>> MessageReadIntervalItems { get; private set; }

            private IEnumerable<T> GetEnumValues<T>() {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }

            public class ComboBoxRecord<T> {
                public string Description { get; set; }
                public T Value { get; set; }

                public override string ToString() {
                    return Description;
                }

                public override bool Equals(object obj) {
                    if(obj == null)
                        return false;

                    if(obj is ComboBoxRecord<T>) {
                        return Value.Equals((obj as ComboBoxRecord<T>).Value);
                    }

                    return false;
                }

                public override int GetHashCode() {
                    return Value.GetHashCode();
                }

                public ComboBoxRecord(T value, string descr) {
                    Value = value;
                    Description = descr;
                }
            }

            public ViewModel() {
                {
                    var list = GetEnumValues<Settings.FeedUpdateIntervalEnum>();

                    FeedUpdateIntervalItems = new List<ComboBoxRecord<Settings.FeedUpdateIntervalEnum>>();

                    foreach(var item in list) {
                        FeedUpdateIntervalItems.Add(new ComboBoxRecord<Settings.FeedUpdateIntervalEnum>(item,
                            EnumExtender.GetEnumDescription(item)));
                    }

                    FeedUpdateInterval = FeedUpdateIntervalItems.Find(t => t.Value == Global.Model.Settings.FeedUpdateInterval);
                }

                {
                    var list = GetEnumValues<Settings.MessageReadIntervalEnum>();

                    MessageReadIntervalItems = new List<ComboBoxRecord<Settings.MessageReadIntervalEnum>>();

                    foreach(var item in list) {
                        MessageReadIntervalItems.Add(new ComboBoxRecord<Settings.MessageReadIntervalEnum>(item,
                            EnumExtender.GetEnumDescription(item)));
                    }

                    MessageReadInterval = MessageReadIntervalItems.Find(t => t.Value == Global.Model.Settings.MessageReadInterval);
                }
            }
        }

        private async void btSave_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                Global.Model.Settings.FeedUpdateInterval = viewModel.FeedUpdateInterval.Value;
                Global.Model.Settings.MessageReadInterval = viewModel.MessageReadInterval.Value;

                await Global.Model.SaveSettings();

                await BackgroundHelper.RegisterTask();

                navigationHelper.GoBack();
            }
        }

        private async void btCancel_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                navigationHelper.GoBack();
            }
        }

        private async void backButton_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                navigationHelper.GoBack();
            }
        }
    }
}
