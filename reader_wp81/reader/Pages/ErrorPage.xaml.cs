﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using reader.Helpers;
using Shared.Helpers;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace reader.Pages {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ErrorPage : Page {
        private ViewModel viewModel = new ViewModel();

        public ErrorPage() {
            this.InitializeComponent();
        }

        public ViewModel DefaultViewModel {
            get { return viewModel; }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            viewModel.Exception = e.Parameter as Exception;
            viewModel.RaisePropertyUpdated("ErrorDescription");
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            base.OnNavigatedFrom(e);

            Application.Current.Exit();
        }

        public class ViewModel : BaseViewModel {
            public Exception Exception { get; internal set; }

            public string ErrorDescription {
                get {
                    StringBuilder result = new StringBuilder();
                    result.AppendLine(MyLocalizationHelper.Instance.GetValue("Shared.ErrorMsg"));
                    result.AppendLine("");
                    result.AppendLine(Exception.Message);

                    if(Exception.StackTrace != null)
                        result.AppendLine(Exception.StackTrace);

                    return result.ToString();
                }
            }
        }
    }
}
