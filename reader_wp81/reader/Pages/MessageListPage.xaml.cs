﻿using reader.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using reader.Helpers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Shared.Data;
using Shared;
using Shared.Helpers;
using Windows.UI.Text;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace reader.Pages {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MessageListPage : Page {
        private NavigationHelper navigationHelper;
        private ViewModel viewModel = new ViewModel();

        public MessageListPage() {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.        
        /// </summary>
        public ViewModel DefaultViewModel {
            get { return this.viewModel; }
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
            string feedUrl = e.NavigationParameter as string;
            viewModel.FilterFeed = Global.Model.Feeds.Find(f => f.Url == feedUrl);

            if(e.PageState != null) {
                if(e.PageState.ContainsKey("ShowOnlyNew"))
                    viewModel.ShowOnlyNew = (bool)e.PageState["ShowOnlyNew"];
            }
        }

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
            e.PageState.Add("ShowOnlyNew", viewModel.ShowOnlyNew);
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public class ViewModel : BaseViewModel {
            private bool filterFeedSet = false;
            private Feed filterFeed;
            public Feed FilterFeed {
                get { return filterFeed; }
                set {
                    filterFeed = value;
                    filterFeedSet = true;

                    UpdateMessages();
                }
            }

            private bool showOnlyNew = true;
            public bool ShowOnlyNew {
                get {
                    return showOnlyNew;
                }
                set {
                    showOnlyNew = value;
                    RaisePropertyUpdated("CurrentListModeIcon");
                    RaisePropertyUpdated("CurrentListModeText");
                    RaisePropertyUpdated("OldMessages");
                    RaisePropertyUpdated("Title");
                    RaisePropertyUpdated("OldMessagesVisibility");
                }
            }

            public Visibility OldMessagesVisibility {
                get {
                    return ShowOnlyNew ? Visibility.Collapsed : Visibility.Visible;
                }
            }

            public List<MessageInfo> AllMessages { get; private set; }

            public List<MessageInfo> NewMessages {
                get {
                    return AllMessages.FindAll(m => !m.Message.IsRead);
                }
            }

            public List<MessageInfo> OldMessages {
                get {
                    return ShowOnlyNew ? new List<MessageInfo>() : AllMessages.FindAll(m => m.Message.IsRead);
                }
            }

            public string Title {
                get {
                    if(!filterFeedSet)
                        return MyLocalizationHelper.Instance.GetValue("MessageListPage.Title");

                    if(ShowOnlyNew)
                        return string.Format(MyLocalizationHelper.Instance.GetValue("MessageListPage.Title2"), NewMessages.Count);
                    else {
                        int nm = NewMessages.Count;
                        int om = OldMessages.Count;

                        return string.Format(MyLocalizationHelper.Instance.GetValue("MessageListPage.Title3"), nm, nm + om);
                    }
                }
            }

            private SymbolIcon ShowAllIcon, ShowOnlyNewIcon;

            public SymbolIcon CurrentListModeIcon {
                get {
                    return ShowOnlyNew ? ShowOnlyNewIcon : ShowAllIcon;
                }
            }

            public string CurrentListModeText {
                get {
                    return ShowOnlyNew ? MyLocalizationHelper.Instance.GetValue("MessageListPage.ShowUnread") :
                        MyLocalizationHelper.Instance.GetValue("MessageListPage.ShowAll");
                }
            }

            public ViewModel() {
                ShowAllIcon = new SymbolIcon(Symbol.ViewAll);
                ShowOnlyNewIcon = new SymbolIcon(Symbol.Mail);

                UpdateMessages();
            }

            public void UpdateMessages() {
                AllMessages = new List<MessageInfo>();

                if(filterFeedSet) {
                    int totalMessages;
                    var messages = Global.FindMessages(filterFeed, false, out totalMessages);

                    foreach(var message in messages) {
                        AllMessages.Add(new MessageInfo(message, this));
                    }
                }

                RefreshMessages();
            }

            public void RefreshMessages() {
                RaisePropertyUpdated("NewMessages");
                RaisePropertyUpdated("OldMessages");
                RaisePropertyUpdated("Title");
            }

            public class MessageInfo {
                public Message Message { get; private set; }

                public MessageInfo(Message message, ViewModel viewModel) {
                    this.Message = message;
                }

                private void Feed_InfoUpdated() {
                }

                public string Title { get { return Message.Title; } }

                public string PubDate { get { return Message.PubDate.ToString().Trim(); } }

                public FontWeight TitleWeight { get { return Message.IsRead ? FontWeights.Light : FontWeights.SemiBold; } }

                public FontWeight FooterWeight { get { return Message.IsRead ? FontWeights.ExtraLight : FontWeights.Normal; } }

                public string FeedName {
                    get {
                        if(Message.Feed != null)
                            return Message.Feed.Title;
                        else
                            return "";
                    }
                }
            }
        }

        private async void Grid_Tapped(object sender, TappedRoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                var item = sender as FrameworkElement;
                object dataContext = item.DataContext;

                var allMessages = new List<Message>();
                foreach(var messageInfo in viewModel.NewMessages) {
                    allMessages.Add(messageInfo.Message);
                }
                foreach(var messageInfo in viewModel.OldMessages) {
                    allMessages.Add(messageInfo.Message);
                }

                var message = allMessages.Find(m => m.Guid == (dataContext as ViewModel.MessageInfo).Message.Guid);

                var args = new MessagePageNavigateArgs(allMessages, message);
                Frame.Navigate(typeof(MessagePage), SerializeHelper.Serialize(args));
            }
        }

        private async void backButton_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                navigationHelper.GoBack();
            }
        }

        private async void btViewMode_Click(object sender, RoutedEventArgs e) {
            using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                await Task.Delay(1);

                viewModel.ShowOnlyNew = !viewModel.ShowOnlyNew;

                await Task.Delay(1);
            }

            if(!viewModel.ShowOnlyNew) {
                GeneralTransform transform = tbReadMessages.TransformToVisual(sbMessages);
                Point textBoxPosition = transform.TransformPoint(new Point(0, 0));
                double offset = textBoxPosition.Y + sbMessages.VerticalOffset;
                sbMessages.ChangeView(null, offset, null);
            }
        }

        private async void btMarkAllAsRead_Click(object sender, RoutedEventArgs e) {
            if(viewModel.AllMessages.Count <= 0)
                return;

            ContentDialog dlg = new ContentDialog() {
                PrimaryButtonText = MyLocalizationHelper.Instance.GetValue("Shared.Ok"),
                SecondaryButtonText = MyLocalizationHelper.Instance.GetValue("Shared.Cancel"),
                Content = new TextBlock() {
                    Text = string.Format(MyLocalizationHelper.Instance.GetValue("MessageListPage.MarkAllQuestion"), viewModel.NewMessages.Count)
                }
            };
            var root = dlg.Content;

            dlg.Closed += async (s, a) => {
                if(a.Result == ContentDialogResult.Primary) {
                    using(var pbToken = ProgressHelper.AddProgress(this.PageContentGrid)) {
                        await Task.Delay(1);

                        foreach(var m in viewModel.NewMessages)
                            m.Message.IsRead = true;

                        await Global.Model.SaveFeedsAndMessages();

                        viewModel.RefreshMessages();
                    }
                }
            };
            await dlg.ShowAsync();
        }
    }
}
