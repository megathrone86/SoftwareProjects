﻿using reader.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using System.Net;
using System.Xml;
using reader.Helpers;
using Shared.Helpers;
using Shared;
using Shared.Data;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace reader.Pages {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddFeedPage : Page {
        public class ViewModel : DependencyObject {
            public string ErrorText {
                get { return (string)GetValue(ErrorTextProperty); }
                set { SetValue(ErrorTextProperty, value); }
            }
            public static readonly DependencyProperty ErrorTextProperty =
                DependencyProperty.Register("ErrorText", typeof(string), typeof(ViewModel), new PropertyMetadata(null));

            public string URLText {
                get { return (string)GetValue(URLTextProperty); }
                set { SetValue(URLTextProperty, value); }
            }
            public static readonly DependencyProperty URLTextProperty =
                DependencyProperty.Register("URLText", typeof(string), typeof(ViewModel), new PropertyMetadata(DebugParams.NewFeedDefaultValue));

            public bool IsBusy {
                get { return (bool)GetValue(IsBusyProperty); }
                set {
                    SetValue(IsBusyProperty, value);
                    Visibility = IsBusy ? Visibility.Visible : Visibility.Collapsed;
                }
            }
            public static readonly DependencyProperty IsBusyProperty =
                DependencyProperty.Register("IsBusy", typeof(bool), typeof(ViewModel), new PropertyMetadata(false));

            public bool IsUrlGood {
                get { return (bool)GetValue(IsUrlGoodProperty); }
                set { SetValue(IsUrlGoodProperty, value); }
            }
            public static readonly DependencyProperty IsUrlGoodProperty =
                DependencyProperty.Register("IsUrlGood", typeof(bool), typeof(ViewModel), new PropertyMetadata(false));

            public Visibility Visibility {
                get { return (Visibility)GetValue(VisibilityProperty); }
                set { SetValue(VisibilityProperty, value); }
            }
            public static readonly DependencyProperty VisibilityProperty =
                DependencyProperty.Register("Visibility", typeof(Visibility), typeof(ViewModel), new PropertyMetadata(Visibility.Collapsed));

            public string LastVerifiedUrl { get; set; }
            public RssHelper.RssInfo LastRssInfo { get; set; }
        }

        private ViewModel viewModel = new ViewModel();
        private NavigationHelper navigationHelper;

        public AddFeedPage() {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        public NavigationHelper NavigationHelper {
            get { return this.navigationHelper; }
        }

        public ViewModel DefaultViewModel {
            get { return viewModel; }
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
        }

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void backButton_Click(object sender, RoutedEventArgs e) {
            navigationHelper.GoBack();
        }

        private void tbUrl_LostFocus(object sender, RoutedEventArgs e) {
            StartUrlVerifying();
        }

        private void tbUrl_KeyDown(object sender, KeyRoutedEventArgs e) {
            if(e.Key == Windows.System.VirtualKey.Enter) {
                tbUrl.IsEnabled = false;
                tbUrl.IsEnabled = true;
            }
        }

        private async void StartUrlVerifying() {
            //stupid hack. waiting for the URLText to change
            await Task.Delay(1);

            string url = viewModel.URLText.Trim();

            if(!string.IsNullOrEmpty(url)) {
                viewModel.IsBusy = true;
                viewModel.IsUrlGood = false;
                viewModel.ErrorText = "";

                try {
                    RssHelper helper = new RssHelper();
                    viewModel.LastRssInfo = await helper.LoadRss(url);
                    viewModel.ErrorText = "";
                    viewModel.IsUrlGood = true;
                    viewModel.LastVerifiedUrl = url;
                } catch(Exception ex) {
                    viewModel.ErrorText = MyLocalizationHelper.Instance.GetValue("Shared.ErrorMsg") + ex.Message;
                    viewModel.IsUrlGood = false;
                    viewModel.LastVerifiedUrl = null;
                    viewModel.LastRssInfo = null;
                } finally {
                    viewModel.IsBusy = false;
                }
            }
        }

        private async void btOk_Click(object sender, RoutedEventArgs e) {
            try {
                if(viewModel.URLText != viewModel.LastVerifiedUrl) {
                    viewModel.ErrorText = MyLocalizationHelper.Instance.GetValue("AddFeedPage.ValidationErrorMsg1");
                    return;
                }

                if(string.IsNullOrEmpty(viewModel.URLText)) {
                    viewModel.ErrorText = MyLocalizationHelper.Instance.GetValue("AddFeedPage.ValidationErrorMsg2");
                    return;
                }

                var info = viewModel.LastRssInfo;

                lock (Global.Model.FeedsLocker) {
                    if(Global.Model.Feeds.Exists(f => f.Url == info.Link)) {
                        viewModel.ErrorText = MyLocalizationHelper.Instance.GetValue("AddFeedPage.ValidationErrorMsg3");
                        return;
                    }
                }

                var newFeed = new Feed() { BackgroundUpdate = true, UpdateIntervalHours = 24, Url = info.Link, Title = info.Title };
                newFeed.UpdateByRss(info, true);

                lock (Global.Model.FeedsLocker) {
                    Global.Model.Feeds.Add(newFeed);
                }

                await Global.Model.SaveFeedsAndMessages();

                Frame.GoBack();
            } catch(Exception ex) {
                DebugHelper.WriteLine(ex);
                throw ex;
            }
        }
    }
}
