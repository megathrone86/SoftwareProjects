﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Shared;
using Shared.Data;

namespace reader.Pages {
    public class MessagePageNavigateArgs {
        public List<string> AllMessagesGuids { get; set; }
        public string MessageGuid { get; set; }

        public MessagePageNavigateArgs() {
        }
        public MessagePageNavigateArgs(List<Message> allMessages, Message message) {
            AllMessagesGuids = new List<string>();
            foreach(var m in allMessages)
                AllMessagesGuids.Add(m.Guid);

            MessageGuid = message.Guid;
        }

        public void Load(out List<Message> AllMessages, out Message Message) {
            lock (Global.Model.MessagesLocker) {
                AllMessages = new List<Message>();
                foreach(string guid in AllMessagesGuids)
                    AllMessages.Add(Global.Model.Messages.Find(m => m.Guid == guid));

                Message = Global.Model.Messages.Find(m => m.Guid == MessageGuid);
            }
        }
    }
}