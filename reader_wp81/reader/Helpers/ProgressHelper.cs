﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Helpers;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace reader.Helpers {
    internal class ProgressHelper {
        internal class ProgressBarToken : IDisposable {
            private Grid hostGrid;
            public Grid Grid { get; private set; }
            internal ProgressBarToken(Grid grid, Grid hostGrid) {
                this.Grid = grid;
                this.hostGrid = hostGrid;
            }

            public void Dispose() {
                DeleteGrid();
            }

            private async void DeleteGrid() {
                await AsyncHelper.RunOnUI(() => { hostGrid.Children.Remove(Grid); }, true);
            }
        }

        internal static ProgressBarToken AddProgress(Grid root) {
            Grid newGrid = new Grid() {
                Background = new SolidColorBrush(Windows.UI.Color.FromArgb(60, 0, 0, 0)),
            };

            if(root.ColumnDefinitions.Count > 0) {
                newGrid.SetValue(Grid.ColumnSpanProperty, root.ColumnDefinitions.Count);
            }

            if(root.RowDefinitions.Count > 0) {
                newGrid.SetValue(Grid.RowSpanProperty, root.RowDefinitions.Count);
            }

            newGrid.Children.Add(new ProgressBar() { IsIndeterminate = true });

            root.Children.Add(newGrid);

            return new ProgressBarToken(newGrid, root);
        }
    }
}
