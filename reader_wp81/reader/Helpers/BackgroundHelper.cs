﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Shared.Helpers;
using Windows.ApplicationModel.Background;

namespace reader.Helpers {
    public class BackgroundHelper {
        public static BackgroundTaskRegistration Task;

        public static async Task RegisterTask() {
            await RegisterTask("UpdateFeedsTask", Global.Model.Settings.GetFeedUpdateInterval());
        }

        private static async Task RegisterTask(string myTaskName, uint intervalMins) {
            DebugHelper.WriteLine("registering task " + myTaskName);

            foreach(var cur in BackgroundTaskRegistration.AllTasks) {
                if(cur.Value.Name == myTaskName) {
                    DebugHelper.WriteLine("task already registered");
                    cur.Value.Unregister(false);
                }
            }

            if(intervalMins <= 0)
                return;

            // Windows Phone app must call this to use trigger types (see MSDN)
            await BackgroundExecutionManager.RequestAccessAsync();

            BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder { Name = myTaskName, TaskEntryPoint = "update_task.UpdateFeedsTask" };
            taskBuilder.SetTrigger(new TimeTrigger(intervalMins, false));
            Task = taskBuilder.Register();

            DebugHelper.WriteLine("task registered");
        }
    }
}
