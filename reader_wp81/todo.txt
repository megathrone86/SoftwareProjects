- сделать возможность настраивать срок хранения сообщений
- cделать возможность быстрой прокрутки
- сделать поиск по сообщениям
- дарк тема

- сделать живые тайлы

- добавить поддержку вложений и ссылок
<item>
<title>Oct. 15, 1997, Launch of Cassini Spacecraft to Saturn</title>
<link>http://www.nasa.gov/image-feature/oct-15-1997-launch-of-cassini-to-saturn</link>
<description>On Oct. 15 1997, a seven-year journey to the ringed planet Saturn began with the liftoff of a Titan IVB/Centaur carrying the Cassini orbiter and its attached Huygens probe. This spectacular streak shot was taken from Hangar AF on Cape Canaveral Air Force Station, with a solid rocket booster retrieval ship in the foreground.</description>
<enclosure type="image/jpeg" length="649722" url="http://www.nasa.gov/sites/default/files/thumbnails/image/4857944569_df2f68d079_o_0.jpg"/>
<guid isPermaLink="false">http://www.nasa.gov/image-feature/oct-15-1997-launch-of-cassini-to-saturn</guid>
<pubDate>Thu, 15 Oct 2015 12:41 EDT</pubDate>
<source url="http://www.nasa.gov/rss/dyn/image_of_the_day.rss">NASA Image of the Day</source>
