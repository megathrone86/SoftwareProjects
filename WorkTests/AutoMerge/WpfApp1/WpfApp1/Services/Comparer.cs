﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Services
{
    public class Comparer
    {
        public List<DiffLine> GetDiffList(string[] sourceLines, string[] modifiedLines)
        {
            var result = new List<DiffLine>();

            Dictionary<int, int> sourceToModified = new Dictionary<int, int>(sourceLines.Length);
            {
                int lastModifiedLineIndex = -1;
                for (int i = 0; i < sourceLines.Length; i++)
                {
                    var sourceLine = sourceLines[i];

                    for (int j = lastModifiedLineIndex + 1; j < modifiedLines.Length; j++)
                    {
                        var modifiedLine = modifiedLines[j];
                        if (sourceLine.Trim() == modifiedLine.Trim())
                        {
                            sourceToModified[i] = j;
                            lastModifiedLineIndex = j;
                            break;
                        }
                    }
                }
            }

            int modifiedIndex = 0;
            for (int sourceIndex = 0; sourceIndex < sourceLines.Length; sourceIndex++)
            {
                if (sourceToModified.ContainsKey(sourceIndex))
                {
                    var mappedModifiedIndex = sourceToModified[sourceIndex];
                    if (mappedModifiedIndex == modifiedIndex)
                    {
                        result.Add(new DiffLine() { Type = DiffLineType.Unchanged, Text = sourceLines[sourceIndex] });
                        modifiedIndex = mappedModifiedIndex + 1;
                    }
                    else
                    {
                        for (int i = modifiedIndex; i < mappedModifiedIndex; i++)
                        {
                            result.Add(new DiffLine() { Type = DiffLineType.Added, Text = modifiedLines[i] });
                        }

                        result.Add(new DiffLine() { Type = DiffLineType.Unchanged, Text = sourceLines[sourceIndex] });
                        modifiedIndex = mappedModifiedIndex + 1;
                    }
                }
                else
                {
                    result.Add(new DiffLine() { Type = DiffLineType.Deleted, Text = sourceLines[sourceIndex] });
                }
            }

            for (int i = modifiedIndex; i < modifiedLines.Length; i++)
            {
                result.Add(new DiffLine() { Type = DiffLineType.Added, Text = modifiedLines[i] });
            }

            return result;
        }
    }
}
