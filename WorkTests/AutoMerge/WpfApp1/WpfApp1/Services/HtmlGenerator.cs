﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Services
{
    public class HtmlGenerator
    {
        private List<MergedLine> data;

        public string Output { get; private set; }

        public HtmlGenerator(List<MergedLine> data)
        {
            this.data = data;
            Output = GetOutput();
        }

        string GetColor(DiffLineType? type)
        {
            if (type.HasValue)
            {
                switch (type.Value)
                {
                    case DiffLineType.Added: return "green";
                    case DiffLineType.Deleted: return "red";
                }
            }
            return "black";
        }

        string LineToHtml(string line)
        {
            if (string.IsNullOrWhiteSpace(line))
                line = "\t";
            return System.Net.WebUtility.HtmlEncode(line);
        }

        string FormLine(bool conflict, DiffLineType? lineType, string line)
        {
            var color = GetColor(lineType);
            var preStyle = "";
            if (conflict)
                preStyle = " style='background-color:orange;'";

            var lineText = $"<pre{preStyle}>{LineToHtml(line)}</pre>";
            return $"<font color='{color}'>{lineText}</font>";
        }

        string GetActualLine(MergedLine line)
        {
            if (line.Line1Type == DiffLineType.Unchanged && line.Line2Type == DiffLineType.Unchanged)
                return line.Line1;
            if (line.Line1Discarded)
                return line.Line2;
            if (line.Line2Discarded)
                return line.Line1;
            if (line.Line1Type == DiffLineType.Deleted || line.Line2Type == DiffLineType.Deleted)
                return "";
            if (line.Line1Type == DiffLineType.Added)
                return line.Line1;
            if (line.Line2Type == DiffLineType.Added)
                return line.Line2;
            throw new Exception();
        }

        string GetOutput()
        {
            var text1 = string.Concat(data.Select(t => FormLine(t.Conflict, t.Line1Type, t.Line1)));
            var text2 = string.Concat(data.Select(t => FormLine(t.Conflict, t.Line2Type, t.Line2)));
            var text3 = string.Concat(data.Select(t =>
            {
                if (t.Conflict)
                {
                    return $@"
<pre style='background-color:#ff5050'>{LineToHtml(t.Line1)}</pre>
<pre style='background-color:#bbbbff'>{LineToHtml(t.Line2)}</pre>
";
                }
                else
                    return $"<pre>{GetActualLine(t)}</pre>";
            }));

            var text = $@"
<html>
<head>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<style>
* {{
    box-sizing: border-box;
}}
pre {{
    margin: 0;
}}
.row {{
    display: flex;
}}
.column {{
    flex: 50%;
    padding: 10px;
}}
</style>
</head>
<body>
<h2>Merge</h2>
<div class='row'>
  <div class='column' style='background-color:#ddd;'>
    <h2>Diff 1</h2>
    {text1}
  </div>
  <div class='column' style='background-color:#eee;'>
    <h2>Diff 2</h2>
    {text2}
  </div>
</div>
<h2>Merge result</h2>
{text3}
</body>
</html>";

            return text;
        }
    }
}
