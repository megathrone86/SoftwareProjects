﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Services
{
    public class DiffLine
    {
        public DiffLineType Type { get; set; }
        public string Text { get; set; }

        public bool IsAdded => Type == DiffLineType.Added;
        public bool IsDeleted => Type == DiffLineType.Deleted;
        public bool IsUnchanged => Type == DiffLineType.Unchanged;

#if DEBUG
        public override string ToString()
        {
            return $"{Type} \t\t\t{Text}";
        }
#endif
    }
}
