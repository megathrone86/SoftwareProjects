﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Services
{
    public class MergedLine
    {
        public bool Conflict { get; set; }

        public DiffLineType? Line1Type { get; set; }
        public string Line1 { get; set; }

        public DiffLineType? Line2Type { get; set; }
        public string Line2 { get; set; }
        public bool Line1Discarded { get; set; }
        public bool Line2Discarded { get; set; }
    }
}
