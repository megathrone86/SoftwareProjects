﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Services
{
    public class Merger
    {
        string sourceFilePath, modifiedFile1Path, modifiedFile2Path;
        public List<MergedLine> MergeResult { get; private set; } = new List<MergedLine>();

        public Merger(string sourceFilePath, string modifiedFile1Path, string modifiedFile2Path)
        {
            this.sourceFilePath = sourceFilePath;
            this.modifiedFile1Path = modifiedFile1Path;
            this.modifiedFile2Path = modifiedFile2Path;

            Merge();
        }

        void AddLine(DiffLine line1, DiffLine line2)
        {
            MergeResult.Add(new MergedLine()
            {
                Line1Type = line1?.Type,
                Line1 = line1?.Text,
                Line2Type = line2?.Type,
                Line2 = line2?.Text
            });
        }

        void AddConflictLine(DiffLine line1, DiffLine line2)
        {
            MergeResult.Add(new MergedLine()
            {
                Conflict = true,
                Line1Type = line1?.Type,
                Line1 = line1?.Text,
                Line2Type = line2?.Type,
                Line2 = line2?.Text
            });
        }

        private void Merge()
        {
            var sourceLines = File.ReadAllLines(sourceFilePath);
            var modified1Lines = File.ReadAllLines(modifiedFile1Path);
            var modified2Lines = File.ReadAllLines(modifiedFile2Path);

            var comparer = new Comparer();
            var diff1 = comparer.GetDiffList(sourceLines, modified1Lines);
            var diff2 = comparer.GetDiffList(sourceLines, modified2Lines);

            int d1 = 0, d2 = 0;
            while (d1 < diff1.Count || d2 < diff2.Count)
            {
                DiffLine currentLine1() { return diff1[d1]; }
                DiffLine currentLine2() { return diff2[d2]; }

                if (d1 >= diff1.Count && d2 < diff2.Count)
                {
                    AddLine(null, currentLine2());
                    d2++;
                }
                else
                if (d1 < diff1.Count && d2 >= diff2.Count)
                {
                    AddLine(currentLine1(), null);
                    d1++;
                }
                else
                if (currentLine1().IsUnchanged && currentLine2().IsUnchanged ||
                    ((currentLine1().IsAdded && currentLine2().IsAdded) || (currentLine1().IsDeleted && currentLine2().IsDeleted))
                        && currentLine1().Text.Trim() == currentLine2().Text.Trim())
                {
                    AddLine(currentLine1(), currentLine2());
                    d1++;
                    d2++;
                }
                else
                if (currentLine1().IsAdded && currentLine2().IsUnchanged)
                {
                    AddLine(currentLine1(), null);
                    d1++;
                }
                else
                if (currentLine1().IsDeleted && currentLine2().IsUnchanged)
                {
                    AddLine(currentLine1(), null);
                    d1++;
                    d2++;
                }
                else
                if (currentLine1().IsUnchanged && currentLine2().IsAdded)
                {
                    AddLine(null, currentLine2());
                    d2++;
                }
                else
                if (currentLine1().IsUnchanged && currentLine2().IsDeleted)
                {
                    AddLine(null, currentLine2());
                    d1++;
                    d2++;
                }
                else
                if (currentLine1().Type != DiffLineType.Unchanged && currentLine2().Type != DiffLineType.Unchanged)
                {
                    AddConflictLine(currentLine1(), currentLine2());

                    if (currentLine1().IsAdded)
                        d1++;
                    if (currentLine2().IsAdded)
                        d2++;
                }
                else
                    throw new Exception();
            }
        }
    }
}
