﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1.Helpers
{
    public class ShowInBrowserCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            var text = parameter?.ToString();
            return !string.IsNullOrEmpty(text);
        }

        public void Execute(object parameter)
        {
            var text = parameter?.ToString();
            File.WriteAllText("temp_output.htm", text);
            System.Diagnostics.Process.Start("temp_output.htm");
        }
    }
}
