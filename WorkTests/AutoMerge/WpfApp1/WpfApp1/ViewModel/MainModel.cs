﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp1.Helpers;
using WpfApp1.Services;

namespace WpfApp1.ViewModel
{
    public class MainModel : INotifyPropertyChanged
    {
        public string sourceFilePath;
        public string SourceFilePath
        {
            get { return sourceFilePath; }
            set
            {
                sourceFilePath = value;
                OnPropertyChanged(nameof(SourceFilePath));
                OnFilesChanged();
            }
        }

        public string modifiedFile1Path;
        public string ModifiedFile1Path
        {
            get { return modifiedFile1Path; }
            set
            {
                modifiedFile1Path = value;
                OnPropertyChanged(nameof(ModifiedFile1Path));
                OnFilesChanged();
            }
        }

        public string modifiedFile2Path;
        public string ModifiedFile2Path
        {
            get { return modifiedFile2Path; }
            set
            {
                modifiedFile2Path = value;
                OnPropertyChanged(nameof(ModifiedFile2Path));
                OnFilesChanged();
            }
        }

        string resultContent;
        public string ResultContent
        {
            get { return resultContent; }
            set
            {
                resultContent = value;
                OnPropertyChanged(nameof(ResultContent));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand OpenSourceFileDialogCommand { get; set; }
        public ICommand OpenModifiedFile1DialogCommand { get; set; }
        public ICommand OpenModifiedFile2DialogCommand { get; set; }
        public ShowInBrowserCommand ShowInBrowserCommand { get; set; }

        public MainModel()
        {
            OpenSourceFileDialogCommand = new OpenFileDialogCommand((path) => SourceFilePath = path);
            OpenModifiedFile1DialogCommand = new OpenFileDialogCommand((path) => ModifiedFile1Path = path);
            OpenModifiedFile2DialogCommand = new OpenFileDialogCommand((path) => ModifiedFile2Path = path);
            ShowInBrowserCommand = new ShowInBrowserCommand();

            SourceFilePath = @"Test\OpenFileDialogCommand.cs";
            ModifiedFile1Path = @"Test\OpenFileDialogCommand_1.cs";
            ModifiedFile2Path = @"Test\OpenFileDialogCommand_2.cs";
        }

        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        void OnFilesChanged()
        {
            if (new string[] { SourceFilePath, ModifiedFile1Path, ModifiedFile2Path }.All(s => !string.IsNullOrEmpty(s)))
            {
                try
                {
                    var merger = new Merger(SourceFilePath, ModifiedFile1Path, ModifiedFile2Path);
                    var data = merger.MergeResult;
                    var generator = new HtmlGenerator(data);
                    ResultContent = generator.Output;
                }
                catch (Exception ex)
                {
                    ResultContent = $"<h2>Error</h2><pre>{ex}</pre>";
                }
            }
            else
                ResultContent = null;
        }
    }
}
