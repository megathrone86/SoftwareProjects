﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1.Commands
{
    public class OpenFileDialogCommand : ICommand
    {
        Action<string> onSelected;

        public event EventHandler CanExecuteChanged;

        public OpenFileDialogCommand(Action<string> onSelected)
        {
            this.onSelected = onSelected;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            string s = "test";

            onSelected?.Invoke(s);
        }
    }
}
