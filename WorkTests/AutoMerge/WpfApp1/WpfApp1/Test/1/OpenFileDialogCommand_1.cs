﻿using System;
using System.Collections.Generic;
using test;
using test2;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1.Commands
{
    public class OpenFileDialogCommand : ICommand
    {
        Action<string> onSelected;

        
        public OpenFileDialogCommand(Action<string> onSelected)
        {
            this.onSelected = onSelected1;
        }

        public bool CanExecute(object parameter)
        {
            return false;
        }

        public void Execute(object parameter)
        {
            onSelected?.Invoke(s);
        }
    }
}
