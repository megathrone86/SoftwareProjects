﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1.Commands
{
    public class OpenFileDialogCommand : ICommand
    {
        Action<string> onSelected2;

        public event EventHandler CanExecuteChanged;
		
		public int test;

        public OpenFileDialogCommand(Action<string> onSelected)
        {
            this.onSelected = onSelected2;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            string s = "test2";

            onSelected?.Invoke(s);
        }
    }
}
