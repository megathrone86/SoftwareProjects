﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ConsoleApp1.Helpers
{
    public static class MethodInfoHelper
    {
        public static MethodInfo GetMethodInfo(Expression<Action> expression)
        {
            var member = expression.Body as MethodCallExpression;
            if (member != null)
                return member.Method;
            throw new ArgumentException("Expression is not a method", "expression");
        }

        public static MethodInfo GetMethodInfo<Tin, Tout>(Expression<Func<Tin, Tout>> expression)
        {
            var member = expression.Body as MethodCallExpression;
            if (member != null)
                return member.Method;
            throw new ArgumentException("Expression is not a method", "expression");
        }
    }
}
