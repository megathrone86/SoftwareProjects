﻿using ConsoleApp1.Helpers;
using ConsoleApp1.Optimize1;
using ConsoleApp1.Optimize2;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ConsoleApp1
{
    class Program
    {
        static int Fcalls = 0;
        static int F(int n)
        {
            Fcalls++;
            return n * n;
        }

        static void Main(string[] args)
        {
            Expression<Func<int, int, int>> expression = (x, y) =>
                    F(x) > F(y) ? F(x) : (F(x) < F(2 * y) ? F(2 * y) : F(y) * Math.Max(x, y));

            Fcalls = 0;
            Console.WriteLine($"Raw call. Result: {expression.Compile()(1, 2)}");
            Console.WriteLine($"F calls: {Fcalls}");

            Fcalls = 0;
            Console.WriteLine($"Optimized1 call. Result: {Optimizer1<int, int>.Optimize(F, expression, 1, 2)}");
            Console.WriteLine($"F calls: {Fcalls}");

            Fcalls = 0;
            var methodInfo = ((Func<int, int>)F).Method;
            Console.WriteLine($"Optimized2 call. Result: {Optimizer2.Optimize2(methodInfo, expression, 1, 2)}");
            Console.WriteLine($"F calls: {Fcalls}");

            Console.ReadKey();
        }
    }
}
