﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using WpfApplication1.Commands;
using WpfApplication1.Service;

namespace WpfApplication1.ViewModel
{
    /// <summary>
    /// Главная модель приложения
    /// </summary>
    public class MainModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand ScanCommand { get; set; }

        /// <summary>
        /// Адрес для сканирования
        /// </summary>
        public string Host { get; set; } = "192.168.1.1";

        /// <summary>
        /// Выполняется ли сканирование
        /// </summary>
        bool isScanning;
        public bool IsScanning
        {
            get
            {
                return isScanning;
            }
            set
            {
                isScanning = value;
                OnPropertyChanged(nameof(IsScanning));
                IsNotScanning = !IsScanning;
            }
        }

        bool isNotScanning = true;
        public bool IsNotScanning
        {
            get { return isNotScanning; }
            set
            {
                isNotScanning = value;
                OnPropertyChanged(nameof(IsNotScanning));
            }
        }

        /// <summary>
        /// Есть ли ошибки сканирования
        /// </summary>
        bool hasError;
        public bool HasError
        {
            get { return hasError; }
            set
            {
                hasError = value;
                OnPropertyChanged(nameof(HasError));
            }
        }

        /// <summary>
        /// Есть ли валидные результаты сканирования
        /// </summary>
        bool hasResults;
        public bool HasResults
        {
            get { return hasResults; }
            set
            {
                hasResults = value;
                OnPropertyChanged(nameof(HasResults));
            }
        }

        /// <summary>
        /// Сообщение об ошибке (в результате сканирования)
        /// </summary>
        string errorMessage;
        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
                HasError = !string.IsNullOrEmpty(errorMessage);
            }
        }

        /// <summary>
        /// Результаты сканирования (список полей и значений)
        /// </summary>
        IEnumerable<ScanResultRow> scanResults;
        public IEnumerable<ScanResultRow> ScanResults
        {
            get { return scanResults; }
            set
            {
                scanResults = value;
                OnPropertyChanged(nameof(ScanResults));
            }
        }

        public MainModel()
        {
            ScanCommand = new AsyncCommand(OnScan);
        }

        /// <summary>
        /// Функция которая запускает асинхронное сканирование
        /// </summary>
        /// <returns></returns>
        async Task OnScan()
        {
            await Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                ScanResults = null;
                IsScanning = true;
                ErrorMessage = null;
                HasResults = false;
                HasError = false;
            }));

            try
            {
                var scanner = new Scanner();
                var result = await scanner.Scan(Host);
                var scanResults = result.Select(t => new ScanResultRow()
                {
                    FieldName = t.Key,
                    FieldValue = t.Value
                });

                await Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    ScanResults = scanResults;
                    HasResults = ScanResults.Any(t => !string.IsNullOrEmpty(t.FieldValue));
                    if (!hasResults)
                        ErrorMessage = "Устройство недоступно или не отвечает.";
                }));
            } finally
            {
                IsScanning = false;
            }
        }

        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
