﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.ViewModel
{
    /// <summary>
    /// Результат сканирования (строка таблицы)
    /// </summary>
    public class ScanResultRow
    {
        /// <summary>
        /// Имя поля
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Значение поля
        /// </summary>
        public string FieldValue { get; set; }
    }
}
