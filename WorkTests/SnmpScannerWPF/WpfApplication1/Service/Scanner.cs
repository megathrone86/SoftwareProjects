﻿using SnmpSharpNet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace WpfApplication1.Service
{
    /// <summary>
    /// Класс для сканирования указанного адреса
    /// </summary>
    public class Scanner
    {
        const string DeviceName = "1.3.6.1.2.1.1.5";
        const string MAC = "1.3.6.1.2.1.2.2.1.6.1";
        const string DeviceModel = "1.3.6.1.2.1.47.1.1.1.1.13";
        const string OSVersion = "1.3.6.1.2.1.1.1";

        /// <summary>
        /// фиксированный список параметров для сканирования
        /// </summary>
        Dictionary<string, string> oids = new Dictionary<string, string>()
        {
            { DeviceName, "Имя устройства"},
            { MAC, "MAC"},
            { DeviceModel, "Модель устройства"},
            { OSVersion, "Версия ОС или прошивки"}
        };

        /// <summary>
        /// Основная функция, которая выполняет сканирование
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public async Task<IDictionary<string, string>> Scan(string host)
        {
            var result = new Dictionary<string, string>();
            await Task.Run(() =>
            {
                var info = GetAllInfo(host);
                foreach (var oid in oids)
                {
                    result[oid.Value] = GetOidDescr(info, oid.Key);
                }
            });
            return result;
        }

        /// <summary>
        /// Выцепляет все строки, относящиеся к какому-либо oid, из скачанной информации
        /// </summary>
        /// <param name="info"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        string GetOidDescr(Dictionary<string, string> info, string oid)
        {
            var request = info.Where(t => t.Key.StartsWith(oid)).Select(t => t.Value);
            return string.Join("\r\n", request);
        }

        /// <summary>
        /// Скачивает всю информацию о системе
        /// </summary>
        /// <param name="host"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Dictionary<string, string> GetAllInfo(string host)
        {
            var result = new Dictionary<string, string>();

            List<uint> tableColumns = new List<uint>();

            //new OctetString("public")
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2);
            IpAddress peer = new IpAddress(host);
            if (!peer.Valid)
                throw new Exception("Неверный адрес.");

            using (var target = new UdpTarget((IPAddress)peer))
            {
                Pdu bulkPdu = Pdu.GetBulkPdu();
                bulkPdu.VbList.Add("0");
                bulkPdu.NonRepeaters = 0;
                bulkPdu.MaxRepetitions = 1000;
                var curOid = new Oid();

                while (true)
                {
                    SnmpPacket res = null;
                    try
                    {
                        res = target.Request(bulkPdu, param);
                    } catch (Exception ex)
                    {
                        break;
                    }
                    if (res == null)
                        break;

                    if (res.Version != SnmpVersion.Ver2)
                        break;

                    if (res.Pdu.ErrorStatus != 0)
                        break;

                    bool duplicateDetected = false;
                    foreach (var vb in res?.Pdu?.VbList)
                    {
                        var oid = vb.Oid.ToString();
                        if (!result.ContainsKey(oid))
                            result[oid] = vb.Value.ToString();
                        else
                        {
                            duplicateDetected = true;
                            break;
                        }
                    }

                    if (duplicateDetected)
                        break;

                    if (bulkPdu.VbList.Count <= 0)
                        break;

                    var lastOid = (Oid)bulkPdu.VbList.Last().Oid.Clone();
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(lastOid);
                    bulkPdu.NonRepeaters = 0;
                    bulkPdu.MaxRepetitions = 1000;

                    Thread.Sleep(100);
                }

                var str = string.Join("\r\n", result.Select(t => $"{t.Key} = {t.Value}"));
                Debug.Write(str);

                return result;
            }
        }
    }
}
