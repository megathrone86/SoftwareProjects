﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestBalance.Data;
using TestBalance.Domain;
using TestBalance.Models;

namespace TestBalance.Services
{
    public class CurrencyService
    {
        internal IEnumerable<CurrencyEditModel> GetCurrencyEditList()
        {
            using (var ctx = new BalanceDbContext())
            {
                return ctx.Currency.Select(t => new CurrencyEditModel()
                {
                    Id = t.Id,
                    IsSystem = t.IsSystem,
                    Name = t.ShortName
                }).ToList();
            }
        }

        internal void DeleteCurrency(int id)
        {
            using (var ctx = new BalanceDbContext())
            {
                var items = ctx.Currency.Where(t => t.Id == id && !t.IsSystem);
                ctx.Currency.RemoveRange(items);
                ctx.SaveChanges();
            }
        }

        internal void AddCurrency(CurrencyEditModel model)
        {
            using (var ctx = new BalanceDbContext())
            {
                var currency = new Currency()
                {
                    ShortName = model.Name,
                    IsSystem = false
                };
                ctx.Currency.Add(currency);
                ctx.Balance.Add(new Balance()
                {
                    Currency = currency,
                    Value = 0
                });
                ctx.SaveChanges();
            }
        }

        internal bool HasCurrency(string name)
        {
            using (var ctx = new BalanceDbContext())
            {
                return ctx.Currency.Any(t => t.ShortName == name);
            }
        }
    }
}