﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestBalance.Data;
using TestBalance.Domain;
using TestBalance.Models;

namespace TestBalance.Services
{
    public class HistoryService
    {
        public IEnumerable<HistoryModel> GetHistory()
        {
            using (var ctx = new BalanceDbContext())
            {
                return ctx.BalanceOperation.Select(t => new HistoryModel()
                {
                    CreateDate = t.CreateDate,
                    CurrencyName = t.Balance.Currency.ShortName,
                    BeforeSum = t.BeforeSum,
                    AfterSum = t.AfterSum,
                    Delta = t.AfterSum - t.BeforeSum,
                    UserInfo = t.UserInfo
                }).OrderByDescending(t => t.CreateDate).ToList();
            }
        }
    }
}