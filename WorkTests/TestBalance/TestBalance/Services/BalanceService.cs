﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestBalance.Data;
using TestBalance.Domain;
using TestBalance.Models;

namespace TestBalance.Services
{
    public class BalanceService
    {
        internal IEnumerable<BalanceModel> GetBalances()
        {
            using (var ctx = new BalanceDbContext())
            {
                var result = ctx.Balance.Select(t => new BalanceModel()
                {
                    Name = t.Currency.ShortName,
                    Sum = Math.Round(t.Value, 2)
                }).ToList();
                return result;
            }
        }

        internal void UpdateBalanceSum(int currencyId, decimal sum, string сallerIp, string callerAgent)
        {
            using (var ctx = new BalanceDbContext())
            {
                var balance = ctx.Balance.First(t => t.CurrencyId == currencyId);
                var beforeSum = balance.Value;
                balance.Value += sum;

                var balanceOperation = new BalanceOperation()
                {
                    BeforeSum = beforeSum,
                    AfterSum = balance.Value,
                    Balance = balance,
                    UserInfo = $"IP: {сallerIp}\r\nUser Agent:{callerAgent}"
                };
                if (balanceOperation.UserInfo.Length > 200)
                    balanceOperation.UserInfo = balanceOperation.UserInfo.Substring(0, 200);
                ctx.BalanceOperation.Add(balanceOperation);
                ctx.SaveChanges();
            }
        }

        internal IEnumerable<CurrencyModel> GetCurrencyList()
        {
            using (var ctx = new BalanceDbContext())
            {
                return ctx.Currency.Select(t => new CurrencyModel()
                {
                    Id = t.Id,
                    Name = t.ShortName
                }).ToList();
            }
        }
    }
}