namespace TestBalance.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Balances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrencyId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 30, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.CurrencyId, unique: true);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShortName = c.String(maxLength: 10),
                        IsSystem = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ShortName, unique: true);
            
            CreateTable(
                "dbo.BalanceOperations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        BalanceId = c.Int(nullable: false),
                        BeforeSum = c.Decimal(nullable: false, precision: 30, scale: 2),
                        AfterSum = c.Decimal(nullable: false, precision: 30, scale: 2),
                        UserInfo = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Balances", t => t.BalanceId, cascadeDelete: true)
                .Index(t => t.BalanceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BalanceOperations", "BalanceId", "dbo.Balances");
            DropForeignKey("dbo.Balances", "CurrencyId", "dbo.Currencies");
            DropIndex("dbo.BalanceOperations", new[] { "BalanceId" });
            DropIndex("dbo.Currencies", new[] { "ShortName" });
            DropIndex("dbo.Balances", new[] { "CurrencyId" });
            DropTable("dbo.BalanceOperations");
            DropTable("dbo.Currencies");
            DropTable("dbo.Balances");
        }        
    }
}
