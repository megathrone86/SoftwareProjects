namespace TestBalance.Migrations
{
    using Domain;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TestBalance.Data.BalanceDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(TestBalance.Data.BalanceDbContext context)
        {
            if (!context.Currency.Any(t => t.ShortName == "RUR"))
            {
                var currency = new Currency() { ShortName = "RUR", IsSystem = true };
                context.Currency.Add(currency);
                context.Balance.Add(new Balance() { Currency = currency, Value = 0 });
            }

            if (!context.Currency.Any(t => t.ShortName == "USD"))
            {
                var currency = new Currency() { ShortName = "USD", IsSystem = true };
                context.Currency.Add(currency);
                context.Balance.Add(new Balance() { Currency = currency, Value = 0 });
            }
        }
    }
}
