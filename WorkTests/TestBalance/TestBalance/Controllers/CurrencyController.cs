﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Web.Mvc;
using TestBalance.Models;
using TestBalance.Services;

namespace TestBalance.Controllers
{
    public class CurrencyController : BaseController
    {
        readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        readonly CurrencyService сurrencyService = new CurrencyService();

        public ActionResult Currency()
        {
            return View();
        }

        public ActionResult CurrencyView()
        {
            var model = сurrencyService.GetCurrencyEditList();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CurrencyDelete(int? id)
        {
            сurrencyService.DeleteCurrency(id.Value);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult CurrencyAdd()
        {
            return PartialView(new CurrencyEditModel());
        }

        [HttpPost]
        public ActionResult CurrencyAdd(CurrencyEditModel model)
        {
            var nameDuplicate = сurrencyService.HasCurrency(model.Name);
            if (nameDuplicate)
                ModelState.AddModelError(nameof(model.Name), "Валюта с таким именем уже есть.");

            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            try
            {
                сurrencyService.AddCurrency(model);
            } catch (Exception ex)
            {
                logger.Error(ex);
                ModelState.AddModelError("general", $"Произошла ошибка: {ex.Message}");
                return PartialView(model);
            }

            return RedirectToAction(nameof(CurrencyAdd));
        }
    }
}