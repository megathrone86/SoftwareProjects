﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Web.Mvc;
using TestBalance.Models;
using TestBalance.Services;

namespace TestBalance.Controllers
{
    public class HistoryController : BaseController
    {
        readonly HistoryService historyService = new HistoryService();

        public ActionResult History()
        {
            var model = historyService.GetHistory();
            return View(model);
        }
    }
}