﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web.Mvc;
using TestBalance.Models;
using TestBalance.Services;

namespace TestBalance.Controllers
{
    public class BaseController : Controller
    {
        readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = false;

            logger.Error(ex);
        }
    }
}