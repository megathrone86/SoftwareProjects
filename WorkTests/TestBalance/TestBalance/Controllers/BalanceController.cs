﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web.Mvc;
using TestBalance.Models;
using TestBalance.Services;

namespace TestBalance.Controllers
{
    public class BalanceController : BaseController
    {
        const string SubmitAdd = "Начислить";
        const string SubmitSubtract = "Списать";

        readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        readonly BalanceService balanceService = new BalanceService();

        public ActionResult Balance()
        {
            return View();
        }

        public ActionResult BalanceView()
        {
            var model = balanceService.GetBalances();
            return PartialView(model);
        }

        public ActionResult BalanceEdit()
        {
            var model = new BalanceEditModel() { CurrencyList = balanceService.GetCurrencyList() };
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult BalanceEdit(BalanceEditModel model, string submit)
        {
            //чтобы продемонстрировать прогресс бар
            Thread.Sleep(150);

            model.Sum = Math.Round(model.Sum, 2);
            if (model.Sum <= 0)
                ModelState.AddModelError(nameof(model.Sum), "Сумма должна быть больше 0.");

            if (!ModelState.IsValid)
            {
                model.CurrencyList = balanceService.GetCurrencyList();
                return PartialView(model);
            }

            try
            {
                var сallerIp = Request.ServerVariables["REMOTE_ADDR"];
                var callerAgent = Request.UserAgent;

                switch (submit)
                {
                    case SubmitAdd:
                        balanceService.UpdateBalanceSum(model.CurrencyId, model.Sum, сallerIp, callerAgent);
                        break;
                    case SubmitSubtract:
                        balanceService.UpdateBalanceSum(model.CurrencyId, -model.Sum, сallerIp, callerAgent);
                        break;
                    default:
                        throw new ArgumentException(nameof(submit));
                }

            } catch (Exception ex)
            {
                logger.Error(ex);
                ModelState.AddModelError("general", $"Произошла ошибка: {ex.Message}");
                model.CurrencyList = balanceService.GetCurrencyList();
                return PartialView(model);
            }

            return RedirectToAction(nameof(BalanceEdit));
        }
    }
}