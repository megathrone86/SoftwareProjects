﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using TestBalance.Domain;
using TestBalance.Migrations;

namespace TestBalance.Data
{
    public class BalanceDbContext : DbContext
    {
        public DbSet<Currency> Currency { get; set; }
        public DbSet<Balance> Balance { get; set; }
        public DbSet<BalanceOperation> BalanceOperation { get; set; }

        static BalanceDbContext()
        {
            Database.SetInitializer<BalanceDbContext>(
                new MigrateDatabaseToLatestVersion<BalanceDbContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BalanceConfiguration());
            modelBuilder.Configurations.Add(new BalanceOperationConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }

    public class BalanceConfiguration : EntityTypeConfiguration<Balance>
    {
        public BalanceConfiguration()
        {
            Property(p => p.Value).HasPrecision(30, 2);
        }
    }

    public class BalanceOperationConfiguration : EntityTypeConfiguration<BalanceOperation>
    {
        public BalanceOperationConfiguration()
        {
            Property(p => p.AfterSum).HasPrecision(30, 2);
            Property(p => p.BeforeSum).HasPrecision(30, 2);
        }
    }
}