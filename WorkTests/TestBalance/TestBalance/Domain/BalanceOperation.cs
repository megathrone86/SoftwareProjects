﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestBalance.Domain
{
    public class BalanceOperation
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; } = DateTime.Now;
        [ForeignKey(nameof(Balance))]
        public int BalanceId { get; set; }
        public decimal BeforeSum { get; set; }
        public decimal AfterSum { get; set; }
        [MaxLength(200)]
        public string UserInfo { get; set; }

        public virtual Balance Balance { get; set; }
    }
}