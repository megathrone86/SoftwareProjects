﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestBalance.Domain
{
    public class Currency
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        [MaxLength(10)]
        public string ShortName { get; set; }
        public bool IsSystem { get; set; }
    }
}