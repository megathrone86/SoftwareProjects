﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestBalance.Domain
{
    public class Balance
    {
        public int Id { get; set; }
        [ForeignKey(nameof(Currency))]
        [Index(IsUnique = true)]
        public int CurrencyId { get; set; }
        public decimal Value { get; set; }

        public virtual Currency Currency { get; set; }
    }
}