﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestBalance.Domain;

namespace TestBalance.Models
{
    public class BalanceModel
    {
        public decimal Sum { get; set; }

        public string Name { get; set; }
    }
}