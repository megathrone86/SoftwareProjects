﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestBalance.Domain;

namespace TestBalance.Models
{
    public class CurrencyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}