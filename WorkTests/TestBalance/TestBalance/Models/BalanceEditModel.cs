﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestBalance.Models
{
    public class BalanceEditModel
    {
        [DisplayName("Сумма")]
        [Required(ErrorMessage = "Сумма должна быть указана")]
        [RegularExpression(@"\d{1,27}(,\d{1,2})?")]
        public decimal Sum { get; set; }

        [DisplayName("Валюта")]
        [Required(ErrorMessage = "Валюта должна быть указана")]
        public int CurrencyId { get; set; }

        public IEnumerable<CurrencyModel> CurrencyList { get; set; }
    }
}