﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestBalance.Domain;

namespace TestBalance.Models
{
    public class HistoryModel
    {
        public DateTime CreateDate { get; set; }
        public string CurrencyName { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal BeforeSum { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal AfterSum { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal Delta { get; set; }
        public string UserInfo { get; set; }
    }
}