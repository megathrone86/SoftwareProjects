﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestBalance.Domain;

namespace TestBalance.Models
{
    public class CurrencyEditModel
    {
        public int Id { get; set; }
        [DisplayName("Название")]
        [Required(ErrorMessage = "Название должно быть указано")]
        public string Name { get; set; }
        public bool IsSystem { get; set; }
    }
}