﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using ClientsWPF.Annotations;
using ClientsWPF.Model;
using ClientsWPF.View;
using ClientsWPF.ViewModel.UserControl;

namespace ClientsWPF.ViewModel
{
    public class MainViewModel : IMainViewModel, INotifyPropertyChanged
    {
        private BaseViewModel currentViewModel;
        public BaseViewModel CurrentViewModel
        {
            get { return currentViewModel; }
            set
            {
                currentViewModel = value;
                OnPropertyChanged(nameof(CurrentViewModel));
            }
        }

        public bool IsLoading => operationsCounter > 0;

        int operationsCounter = 0;
        public int OperationsCounter
        {
            get { return operationsCounter; }
            set
            {
                operationsCounter = value;
                OnPropertyChanged(nameof(IsLoading));
            }
        }

        public MainViewModel()
        {
            SetCurrentViewModel(new ConnectViewModel());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SetCurrentViewModel(BaseViewModel viewModel)
        {
            CurrentViewModel = viewModel;
            CurrentViewModel.MainViewModel = this;
        }
    }

    public interface IMainViewModel
    {
        void SetCurrentViewModel(BaseViewModel viewModel);
        bool IsLoading { get; }
        int OperationsCounter { get; set; }
    }
}