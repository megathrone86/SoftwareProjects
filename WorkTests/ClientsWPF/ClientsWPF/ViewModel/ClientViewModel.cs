﻿using ClientsWPF.Model;

namespace ClientsWPF.ViewModel
{
    public class ClientViewModel
    {
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Vip { get; set; }

        public ClientViewModel(Client client)
        {
            ClientId = client.ClientId;
            Name = client.Vip ? $"[VIP]{client.Name}" : client.Name;
            Address = client.Address;
            Vip = client.Vip;
        }
    }
}