﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ClientsWPF.Annotations;
using ClientsWPF.Commands;
using ClientsWPF.Model;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class ClientsViewModel : BaseViewModel, INotifyPropertyChanged
    {
        ClientsService clientsService = new ClientsService();
        OrdersService ordersService = new OrdersService();

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddClientCommand { get; private set; }
        public ICommand EditClientCommand { get; private set; }
        public ICommand RemoveClientCommand { get; private set; }

        public ICommand AddOrderCommand { get; private set; }
        public ICommand EditOrderCommand { get; private set; }
        public ICommand RemoveOrderCommand { get; private set; }

        IEnumerable<ClientViewModel> clients;
        public IEnumerable<ClientViewModel> Clients
        {
            get { return clients; }
            set
            {
                clients = value;
                OnPropertyChanged(nameof(Clients));
            }
        }

        ClientViewModel selectedClient;
        public ClientViewModel SelectedClient
        {
            get { return selectedClient; }
            set
            {
                HasSelection = false;
                selectedClient = value;
                OrdersCaption = $"Заказы клиента {selectedClient?.Name}:";
                StartTask(() =>
                {
                    Orders = selectedClient != null ? ordersService.GetByClient(selectedClient.ClientId).Select(t => new OrderViewModel(t)) : null;
                }, () =>
                {
                    HasSelection = selectedClient != null;
                    OnPropertyChanged(nameof(SelectedClient));
                });
            }
        }

        OrderViewModel selectedOrder;
        public OrderViewModel SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                selectedOrder = value;
                OnPropertyChanged(nameof(SelectedOrder));
            }
        }

        bool isDataLoaded;
        public bool IsDataLoaded
        {
            get { return isDataLoaded; }
            set
            {
                Debug.WriteLine($"{DateTime.Now:hh:mm:ss.fff} isDataLoaded = {value}");
                isDataLoaded = value;
                OnPropertyChanged(nameof(IsDataLoaded));
            }
        }

        string ordersCaption;
        public string OrdersCaption
        {
            get { return ordersCaption; }
            set
            {
                ordersCaption = value;
                OnPropertyChanged(nameof(OrdersCaption));
            }
        }

        bool hasSelection;
        public bool HasSelection
        {
            get { return hasSelection; }
            set
            {
                hasSelection = value;
                OnPropertyChanged(nameof(HasSelection));
                OnPropertyChanged(nameof(HasNotSelection));
            }
        }

        public bool HasNotSelection => !hasSelection;

        public IEnumerable<OrderViewModel> orders;
        public IEnumerable<OrderViewModel> Orders
        {
            get { return orders; }
            set
            {
                orders = value;
                OnPropertyChanged(nameof(Orders));
            }
        }

        public ClientsViewModel()
        {
            AddClientCommand = new RelayCommand<object>(t =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientEditViewModel(null));
                });
            });
            EditClientCommand = new RelayCommand<ClientViewModel>(client =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientEditViewModel(client));
                });
            }, t => t != null);
            RemoveClientCommand = new RelayCommand<ClientViewModel>(client =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientDeleteViewModel(client));
                });
            }, t => t != null);
            AddOrderCommand = new RelayCommand<object>(t =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new OrderEditViewModel(null)
                    {
                        ClientId = SelectedClient.ClientId
                    });
                });
            });
            EditOrderCommand = new RelayCommand<OrderViewModel>(order =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new OrderEditViewModel(order));
                });
            }, t => t != null);
            RemoveOrderCommand = new RelayCommand<OrderViewModel>(order =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new OrderDeleteViewModel(order));
                });
            }, t => t != null);

            MainViewModelUpdated += ClientsViewModel_MainViewModelUpdated;
        }

        private void ClientsViewModel_MainViewModelUpdated()
        {
            StartTask(() =>
            {
                Clients = clientsService.GetAll().Select(t => new ClientViewModel(t));
            }, () =>
            {
                IsDataLoaded = true;
            });
            MainViewModelUpdated -= ClientsViewModel_MainViewModelUpdated;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
