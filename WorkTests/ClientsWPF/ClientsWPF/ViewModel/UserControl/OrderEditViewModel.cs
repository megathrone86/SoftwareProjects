﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using ClientsWPF.Commands;
using ClientsWPF.Model;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class OrderEditViewModel : BaseViewModel
    {
        public string Message { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public int? ClientId { get; set; }

        public IEnumerable<ClientViewModel> Clients { get; set; }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public OrderEditViewModel(OrderViewModel order)
        {
            Clients = new ClientsService().GetAll().Select(t => new ClientViewModel(t));

            var ordersService = new OrdersService();

            if (order?.OrderId != null)
            {
                var orderModel = ordersService.Get(order.OrderId);
                Message = $"Редактирование заказа {orderModel.Number}";
                Number = orderModel.Number;
                Description = orderModel.Description;
            } else
                Message = "Добавление нового заказа";
            ClientId = order?.ClientId;

            OkCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    if (ClientId.HasValue)
                    {
                        var orderModel = new Order()
                        {
                            Number = Number,
                            Description = Description,
                            ClientId = ClientId.Value
                        };
                        if (order != null)
                        {
                            orderModel.OrderId = order.OrderId;
                            ordersService.Edit(orderModel);
                        } else
                            ordersService.Add(orderModel);
                        MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                    }
                });
            });
            CancelCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
        }
    }
}