﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using ClientsWPF.Annotations;
using ClientsWPF.Commands;
using ClientsWPF.Data;
using ClientsWPF.Model;
using ClientsWPF.Properties;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class ConnectViewModel : BaseViewModel, INotifyPropertyChanged
    {
        string connectionString;
        public string ConnectionString
        {
            get { return connectionString; }
            set
            {
                connectionString = value;
                OnPropertyChanged(nameof(ConnectionString));
            }
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ConnectViewModel()
        {
            using (var ctx = AppContext.Create())
            {
                ConnectionString = ctx.Database.Connection.ConnectionString;
            }

            OkCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    Settings.Default.DBConnectionString = ConnectionString;

                    using (var ctx = AppContext.Create())
                    {
                        if (ctx.Database.CreateIfNotExists())
                        {
                            TestDataHelper.ExecuteTextScript(ConnectionString);
                        }
                    }

                    Settings.Default.Save();
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
            CancelCommand = new RelayCommand<object>((o) =>
            {
                Environment.Exit(0);
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}