﻿using System;
using System.Threading.Tasks;

namespace ClientsWPF.ViewModel.UserControl
{
    public class BaseViewModel
    {
        public event Action MainViewModelUpdated;

        IMainViewModel mainViewModel;
        public IMainViewModel MainViewModel
        {
            get { return mainViewModel; }
            set
            {
                mainViewModel = value;
                MainViewModelUpdated?.Invoke();
            }
        }

        protected void StartTask(Action action, Action onFinish = null)
        {
            MainViewModel.OperationsCounter++;
#pragma warning disable 1998
            Task.Run(async () =>
#pragma warning restore 1998
            {
                try
                {
                    action?.Invoke();
                } catch (Exception ex)
                {
                    MainViewModel.SetCurrentViewModel(new ErrorViewModel() { Message = ex.ToString() });
                } finally
                {
                    MainViewModel.OperationsCounter--;
                    onFinish?.Invoke();
                }
            });
        }
    }
}