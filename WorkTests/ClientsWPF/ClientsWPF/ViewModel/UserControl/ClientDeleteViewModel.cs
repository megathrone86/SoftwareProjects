﻿using System.Windows.Input;
using ClientsWPF.Commands;
using ClientsWPF.Model;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class ClientDeleteViewModel : BaseViewModel
    {
        public string Message { get; set; }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ClientDeleteViewModel(ClientViewModel client)
        {
            Message = $"Вы действительно хотите удалить клиента {client.Name}?";

            OkCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    new ClientsService().Remove(client.ClientId);
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
            CancelCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
        }
    }
}