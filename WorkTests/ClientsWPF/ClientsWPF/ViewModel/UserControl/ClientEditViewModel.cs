﻿using System.Windows.Input;
using ClientsWPF.Commands;
using ClientsWPF.Model;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class ClientEditViewModel : BaseViewModel
    {
        public string Message { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Vip { get; set; }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ClientEditViewModel(ClientViewModel client)
        {
            var clientsService = new ClientsService();

            if (client?.ClientId != null)
            {
                var clientModel = clientsService.Get(client.ClientId);
                Message = $"Редактирование клиента {clientModel.Name}";
                Name = clientModel.Name;
                Address = clientModel.Address;
                Vip = clientModel.Vip;
            } else
                Message = "Добавление нового клиента";

            OkCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    var clientModel = new Client()
                    {
                        Name = Name,
                        Address = Address,
                        Vip = Vip
                    };
                    if (client != null)
                    {
                        clientModel.ClientId = client.ClientId;
                        clientsService.Edit(clientModel);
                    } else
                        clientsService.Add(clientModel);
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
            CancelCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
        }
    }
}