﻿using System.Windows.Input;
using ClientsWPF.Commands;
using ClientsWPF.Model;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class OrderDeleteViewModel : BaseViewModel
    {
        public string Message { get; set; }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public OrderDeleteViewModel(OrderViewModel order)
        {
            var client = new ClientsService().Get(order.ClientId);
            Message = $"Вы действительно хотите удалить заказ {order.Number} от клиента {client.Name}?";

            OkCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    new OrdersService().Remove(order.OrderId);
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
            CancelCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ClientsViewModel());
                });
            });
        }
    }
}