﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using ClientsWPF.Annotations;
using ClientsWPF.Commands;
using ClientsWPF.Data;
using ClientsWPF.Model;
using ClientsWPF.Properties;
using ClientsWPF.Service;

namespace ClientsWPF.ViewModel.UserControl
{
    public class ErrorViewModel : BaseViewModel, INotifyPropertyChanged
    {
        string message;
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public ICommand OkCommand { get; private set; }

        public ErrorViewModel()
        {
            OkCommand = new RelayCommand<object>((o) =>
            {
                StartTask(() =>
                {
                    MainViewModel.SetCurrentViewModel(new ConnectViewModel());
                });
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}