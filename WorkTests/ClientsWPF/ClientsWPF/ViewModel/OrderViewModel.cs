﻿using ClientsWPF.Model;

namespace ClientsWPF.ViewModel
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public int ClientId { get; set; }

        public OrderViewModel(Order order)
        {
            OrderId = order.OrderId;
            Number = order.Number;
            Description = order.Description;
            ClientId = order.ClientId;
        }
    }
}