﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientsWPF.Model;

namespace ClientsWPF.Data
{
    public class AppContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected AppContext() { }
        protected AppContext(string connStr) : base(connStr) { }

        public static AppContext Create()
        {
            string connStr = Properties.Settings.Default.DBConnectionString;
            var context = string.IsNullOrEmpty(connStr) ? new AppContext() : new AppContext(connStr);
            return context;
        }
    }
}
