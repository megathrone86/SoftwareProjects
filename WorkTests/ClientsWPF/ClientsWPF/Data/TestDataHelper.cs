﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClientsWPF.Data
{
    public class TestDataHelper
    {
        public static void ExecuteTextScript(string connectionString)
        {
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ClientsWPF.Script.testdata.sql"))
            using (StreamReader reader = new StreamReader(stream))
            {
                string sqlScript = reader.ReadToEnd();

                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(sqlScript, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
