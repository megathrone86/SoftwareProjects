﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClientsWPF.Data;
using ClientsWPF.Model;

namespace ClientsWPF.Service
{
    public class OrdersService
    {
        static List<Order> orders = new List<Order>()
            {
                new Order() { OrderId = 0, ClientId = 0, Number = 1, Description = $"Заказ 1 от клиента #{0}" },
                new Order() { OrderId = 1, ClientId = 0, Number = 2, Description = $"Заказ 2 от клиента #{0}"  },
                new Order() { OrderId = 2, ClientId = 0, Number = 3, Description = $"Заказ 3 от клиента #{0}" },
                new Order() { OrderId = 3, ClientId = 1, Number = 4, Description = $"Заказ 1 от клиента #{1}" },
                new Order() { OrderId = 4, ClientId = 1, Number = 5, Description = $"Заказ 2 от клиента #{1}"  },
            };

        public IEnumerable<Order> GetByClient(int clientId)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                return dbContext.Orders.Where(t => t.ClientId == clientId).ToList();
            }
        }

        public void Remove(int id)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                var model = dbContext.Orders.First(t => t.OrderId == id);
                dbContext.Orders.Remove(model);
                dbContext.SaveChanges();
            }
        }

        public Order Get(int id)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                return dbContext.Orders.FirstOrDefault(t => t.OrderId == id);
            }
        }

        public void Edit(Order model)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                var existingModel = dbContext.Orders.First(t => t.OrderId == model.OrderId);
                dbContext.Entry(existingModel).CurrentValues.SetValues(model);
                dbContext.SaveChanges();
            }
        }

        public void Add(Order model)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                dbContext.Orders.Add(model);
                dbContext.SaveChanges();
            }
        }
    }
}
