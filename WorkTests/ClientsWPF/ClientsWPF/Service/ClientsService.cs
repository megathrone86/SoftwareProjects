﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClientsWPF.Annotations;
using ClientsWPF.Data;
using ClientsWPF.Model;

namespace ClientsWPF.Service
{
    public class ClientsService
    {
        static List<Client> clients = new List<Client>()
            {
                new Client() { ClientId = 0, Name = "Иван Иванов", Address = "г. Иваново, ул. Ленина" },
                new Client() { ClientId = 1, Name = "Петр Випов", Address = "г. Виповск, ул. Випная, д. 1", Vip = true }
            };

        public IEnumerable<Client> GetAll()
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                return dbContext.Clients.ToList();
            }
        }

        public void Remove(int id)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                var model = dbContext.Clients.First(t => t.ClientId == id);
                dbContext.Clients.Remove(model);
                dbContext.SaveChanges();
            }
        }

        public Client Get(int? id)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                return dbContext.Clients.FirstOrDefault(t => t.ClientId == id);
            }
        }

        public void Edit(Client model)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                var existingModel = dbContext.Clients.First(t => t.ClientId == model.ClientId);
                dbContext.Entry(existingModel).CurrentValues.SetValues(model);
                dbContext.SaveChanges();
            }
        }

        public void Add(Client model)
        {
            //Thread.Sleep(1000);
            using (var dbContext = AppContext.Create())
            {
                dbContext.Clients.Add(model);
                dbContext.SaveChanges();
            }
        }
    }
}
