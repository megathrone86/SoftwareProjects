﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsWPF.Model
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Vip { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}
