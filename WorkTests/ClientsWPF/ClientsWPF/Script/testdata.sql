declare @cid as int

insert into Clients (Name, Address, Vip) values ('Иван Иванов', 'г. Иваново, ул. Ленина, д. 42', 0)
set @cid = SCOPE_IDENTITY()
insert into Orders (Number, Description, ClientId) values ('101', 'Заказ 101 от Ивана Иванова', @cid)
insert into Orders (Number, Description, ClientId) values ('102', 'Заказ 102 от Ивана Иванова', @cid)
insert into Orders (Number, Description, ClientId) values ('103', 'Заказ 103 от Ивана Иванова', @cid)

insert into Clients (Name, Address, Vip) values ('Сергей Випов', 'г. Виповск, ул. Випная, д. 1', 1)
set @cid = SCOPE_IDENTITY()
insert into Orders (Number, Description, ClientId) values ('201', 'Заказ 201 от С. Випова', @cid)
insert into Orders (Number, Description, ClientId) values ('202', 'Заказ 202 от С. Випова', @cid)

insert into Clients (Name, Address, Vip) values ('Петр Петров', 'г. Днепропетровск, ул. Заречная, д. 7', 1)
set @cid = SCOPE_IDENTITY()
insert into Orders (Number, Description, ClientId) values ('301', 'Заказ 301 от Петра Петрова', @cid)
insert into Orders (Number, Description, ClientId) values ('302', 'Заказ 302 от Петра Петрова', @cid)
insert into Orders (Number, Description, ClientId) values ('303', 'Заказ 303 от Петра Петрова', @cid)