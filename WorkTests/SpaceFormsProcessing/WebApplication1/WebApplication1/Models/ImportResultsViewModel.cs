﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class ImportResultsViewModel
    {
        public string FileName { get; set; }

        [DisplayName("Всего файлов")]
        public int FilesCount { get; internal set; }

        [DisplayName("Добавлено строк")]
        public int RowsAdded { get; set; }

        [DisplayName("Пропущено строк")]
        public int RowsSkipped { get; set; }
    }
}