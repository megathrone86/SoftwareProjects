﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class CandidateViewModel
    {
        public int Id { get; set; }

        [DisplayName("ФИО")]
        [Required]
        [StringLength(250)]
        public string FullName { get; set; }

        [DisplayName("Email")]
        [Required]
        [StringLength(200)]
        [EmailAddress]
        [RegularExpression(Consts.EmailRegex)]
        public string Email { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        [DisplayName("Дата рождения")]
        [Required]
        public DateTime BirthDate { get; set; }

        [DisplayName("Телефон")]
        [Required]
        [StringLength(50)]
        [RegularExpression(Consts.PhoneRegex)]
        public string Phone { get; set; }
    }
}