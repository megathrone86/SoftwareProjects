﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApplication1.DAL;
using WebApplication1.Services;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

#if UseSql
            services.AddTransient<ICandidateService, DatabaseCandidateService>();
#else
            services.AddTransient<ICandidateService, MemoryCandidateService>();
#endif
            services.AddTransient<ImportService, ImportService>();
            services.AddTransient<ExportService, ExportService>();

            var connection = Configuration.GetConnectionString("CandidatesDB");
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connection));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, AppDbContext context)
        {
#if UseSql
            context.Database.Migrate();
#endif

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Candidates}/{action=Index}/{id?}");
            });
        }
    }
}
