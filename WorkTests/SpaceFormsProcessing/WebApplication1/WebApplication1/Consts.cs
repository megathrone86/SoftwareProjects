﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Consts
    {
        public const string PhoneRegex = @"\+?\d{4,25}";
        public const string EmailRegex = @"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,50}";
    }
}
