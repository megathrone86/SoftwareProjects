﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.DAL;

namespace WebApplication1.Services
{
    public class ImportService
    {
        public int RowsAdded => rowsAdded;
        int rowsAdded;

        public int RowsSkipped => rowsSkipped;
        int rowsSkipped;

        ICandidateService candidateService;

        public ImportService(ICandidateService candidateService)
        {
            this.candidateService = candidateService;
        }

        public void Import(IEnumerable<Stream> streams)
        {
            rowsAdded = 0;
            rowsSkipped = 0;

            var actions = streams.Select(stream => new Action(() =>
            {
                try
                {
                    ImportFromStream(stream);
                }
                finally
                {
                    stream.Dispose();
                }
            })).ToArray();

            Parallel.Invoke(actions);
        }

        static Regex EmailLowerRegex = new Regex(Consts.EmailRegex);
        static Regex PhoneRegex = new Regex(Consts.PhoneRegex);

        void ImportFromStream(Stream stream)
        {
            var queue = new ConcurrentQueue<Candidate>();
            var parsing = true;
            var queueUpdatedEvent = new EventWaitHandle(false, EventResetMode.AutoReset);

            Task addingTask = new Task(() =>
            {
                while (parsing || queue.Count > 0)
                {
                    //записываем в БД порциями по 8
                    if (queue.Count > 1 || !parsing)
                    {
                        List<Candidate> candidatesToAdd = new List<Candidate>(queue.Count);
                        while (queue.TryDequeue(out Candidate candidate))
                            candidatesToAdd.Add(candidate);

                        lock (candidateService)
                        {
                            candidateService.Add(candidatesToAdd.ToArray());
                        }
                        Interlocked.Add(ref rowsAdded, candidatesToAdd.Count);
                    }
                    else if (parsing)
                        queueUpdatedEvent.WaitOne();
                }
            });
            addingTask.Start();

            try
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();
                        var candidate = ParseLine(line);
                        if (candidate == null)
                            Interlocked.Increment(ref rowsSkipped);
                        else
                        {
                            queue.Enqueue(candidate);
                            queueUpdatedEvent.Set();
                        }
                    }
                }
            }
            finally
            {
                parsing = false;
                queueUpdatedEvent.Set();
                addingTask.Wait();
            }
        }

        Candidate ParseLine(string line)
        {
            var values = line.Split("\t");
            if (values.Length != 4)
                return null;
            string fio = values[0].Trim();

            DateTime birthdate;
            string birthdateRaw = values[1].Trim();
            if (!DateTime.TryParse(birthdateRaw, out birthdate))
                return null;

            string email = values[2].Trim().ToLower();
            if (!EmailLowerRegex.IsMatch(email))
                return null;

            StringBuilder phoneSb = new StringBuilder(values[3].Length);
            foreach (var c in values[3])
            {
                if (c == '+' || (c >= '0' && c <= '9'))
                    phoneSb.Append(c);
            }
            string phone = phoneSb.ToString();
            if (!PhoneRegex.IsMatch(phone))
                return null;

            return new Candidate()
            {
                FullName = fio,
                BirthDate = birthdate,
                Email = email,
                Phone = phone
            };
        }
    }
}
