﻿using System;
using System.Collections.Generic;
using WebApplication1.DAL;

namespace WebApplication1.Services
{
    public interface ICandidateService
    {
        void Add(params Candidate[] candidates);
        void Delete(int id);
        void Edit(Candidate candidate);
        IEnumerable<Candidate> FindAll(string sort, string order, string search, int limit, int offset, out int total);
        Candidate Get(int id);
        void ForEach(Action<Candidate> action);
    }
}