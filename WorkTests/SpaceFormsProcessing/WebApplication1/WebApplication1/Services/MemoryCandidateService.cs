﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL;

namespace WebApplication1.Services
{
    public class MemoryCandidateService : ICandidateService
    {
        static int lastId = 0;
        static List<Candidate> data = new List<Candidate>()
        {
            //new Candidate() { BirthDate = DateTime.Now.AddDays(-20), FullName = "asdasd", Email = "a@a.a", Phone = "123456", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddYears(-20), FullName = "qwertuy", Email = "b@b.b", Phone = "7890", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddDays(-20), FullName = "asdasd", Email = "a@a.a", Phone = "123456", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddYears(-20), FullName = "qwertuy", Email = "b@b.b", Phone = "7890", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddDays(-20), FullName = "asdasd", Email = "a@a.a", Phone = "123456", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddYears(-20), FullName = "qwertuy", Email = "b@b.b", Phone = "7890", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddDays(-20), FullName = "asdasd", Email = "a@a.a", Phone = "123456", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddYears(-20), FullName = "qwertuy", Email = "b@b.b", Phone = "7890", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddDays(-20), FullName = "asdasd", Email = "a@a.a", Phone = "123456", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddYears(-20), FullName = "qwertuy", Email = "b@b.b", Phone = "7890", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddDays(-20), FullName = "asdasd", Email = "a@a.a", Phone = "123456", Id = lastId++ },
            //new Candidate() { BirthDate = DateTime.Now.AddYears(-20), FullName = "qwertuy", Email = "b@b.b", Phone = "7890", Id = lastId++ },
        };

        public IEnumerable<Candidate> FindAll(string sort, string order, string search, int limit, int offset, out int total)
        {
            var query = data.AsQueryable();

            if (!string.IsNullOrEmpty(search))
            {
                var searchLower = search.ToLower();
                query = query.Where(t => t.FullName.ToLower().Contains(searchLower) || t.Email.Contains(searchLower) ||
                    t.Phone.Contains(searchLower));
            }

            total = query.Count();

            switch (sort)
            {
                case "fullName":
                    query = order == "asc" ? query.OrderBy(t => t.FullName) : query.OrderByDescending(t => t.FullName);
                    break;
                case "email":
                    query = order == "asc" ? query.OrderBy(t => t.Email) : query.OrderByDescending(t => t.Email);
                    break;
                case "birthDate":
                    query = order == "asc" ? query.OrderBy(t => t.BirthDate) : query.OrderByDescending(t => t.BirthDate);
                    break;
                case "phone":
                    query = order == "asc" ? query.OrderBy(t => t.Phone) : query.OrderByDescending(t => t.Phone);
                    break;
            }

            query = query.Skip(offset).Take(limit);

            return query.ToArray();
        }

        public void Delete(int id)
        {
            data.RemoveAll(t => t.Id == id);
        }

        public void Add(params Candidate[] candidates)
        {
            foreach (var candidate in candidates)
            {
                candidate.Id = lastId++;
                data.Add(candidate);

                if (data.Count > 1000)
                    data.RemoveAt(0);
            }
        }

        public void Edit(Candidate candidate)
        {
            Delete(candidate.Id);
            Add(candidate);
        }

        public Candidate Get(int id)
        {
            return data.FirstOrDefault(t => t.Id == id);
        }

        public void ForEach(Action<Candidate> action)
        {
            foreach (var candidate in data)
            {
                action.Invoke(candidate);
            }
        }
    }
}
