﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DAL;

namespace WebApplication1.Services
{
    public class DatabaseCandidateService : ICandidateService
    {
        AppDbContext context;

        public DatabaseCandidateService(AppDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Candidate> FindAll(string sort, string order, string search, int limit, int offset, out int total)
        {
            var query = context.Candidate.AsQueryable();

            if (!string.IsNullOrEmpty(search))
            {
                var searchLower = search.ToLower();
                query = query.Where(t => t.FullName.ToLower().Contains(searchLower) || t.Email.Contains(searchLower) ||
                    t.Phone.Contains(searchLower));
            }

            total = query.Count();

            switch (sort)
            {
                case "fullName":
                    query = order == "asc" ? query.OrderBy(t => t.FullName) : query.OrderByDescending(t => t.FullName);
                    break;
                case "email":
                    query = order == "asc" ? query.OrderBy(t => t.Email) : query.OrderByDescending(t => t.Email);
                    break;
                case "birthDate":
                    query = order == "asc" ? query.OrderBy(t => t.BirthDate) : query.OrderByDescending(t => t.BirthDate);
                    break;
                case "phone":
                    query = order == "asc" ? query.OrderBy(t => t.Phone) : query.OrderByDescending(t => t.Phone);
                    break;
            }

            query = query.Skip(offset).Take(limit);

            return query.ToArray();
        }

        public void Delete(int id)
        {
            var entity = new Candidate() { Id = id };
            context.Candidate.Attach(entity);
            context.Candidate.Remove(entity);
            context.SaveChanges();
        }

        public void Add(params Candidate[] candidates)
        {
            foreach (var candidate in candidates)
            {
                context.Candidate.Add(candidate);
            }
            context.SaveChanges();
        }

        public void Edit(Candidate candidate)
        {
            context.Candidate.Attach(candidate);
            context.SaveChanges();
        }

        public Candidate Get(int id)
        {
            return context.Candidate.First(t => t.Id == id);
        }

        public void ForEach(Action<Candidate> action)
        {
            foreach (var candidate in context.Candidate)
            {
                action.Invoke(candidate);
            }
        }
    }
}
