﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.DAL;

namespace WebApplication1.Services
{
    public class ExportService
    {
        ICandidateService candidateService;

        public ExportService(ICandidateService candidateService)
        {
            this.candidateService = candidateService;
        }

        public byte[] ExportCSV()
        {
            StringBuilder sb = new StringBuilder();
            candidateService.ForEach((candidate) =>
            {
                sb.AppendLine($"{candidate.FullName}\t{candidate.BirthDate.ToShortDateString()}\t{candidate.Email}\t{candidate.Phone} UTC");
            });
            return Encoding.UTF8.GetBytes(sb.ToString());
        }
    }
}
