﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.DAL
{
    public class Candidate
    {
        public int Id { get; set; }

        [MaxLength(250)]
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        [MaxLength(200)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phone { get; set; }
    }
}
