﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.DAL;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    public class ImportController : Controller
    {
        ImportService importService;

        public ImportController(ImportService importService)
        {
            this.importService = importService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(List<IFormFile> files)
        {
            var streams = files.Select(t => t.OpenReadStream());
            importService.Import(streams);

            var model = new ImportResultsViewModel()
            {
                FilesCount = files.Count(),
                RowsAdded = importService.RowsAdded,
                RowsSkipped = importService.RowsSkipped
            };

            return View("ImportResults", model);
        }
    }
}
