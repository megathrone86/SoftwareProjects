﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.DAL;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    public class ExportController : Controller
    {
        ExportService exportService;

        public ExportController(ExportService exportService)
        {
            this.exportService = exportService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ExportCSV()
        {
            var candidatesData = exportService.ExportCSV();

            return new FileContentResult(candidatesData, "application/octet-stream")
            {
                FileDownloadName = $"candidates_{DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd hh:mm:ss")}.csv"
            };
        }
    }
}
