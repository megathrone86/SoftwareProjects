﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.DAL;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    public class CandidatesController : Controller
    {
        ICandidateService candidateService;

        public CandidatesController(ICandidateService candidateService)
        {
            this.candidateService = candidateService;
        }

        CandidateViewModel ConvertToCandidateViewModel(Candidate t)
        {
            return new CandidateViewModel()
            {
                Id = t.Id,
                FullName = t.FullName,
                Email = t.Email,
                BirthDate = t.BirthDate,
                Phone = t.Phone,
            };
        }

        [HttpGet]
        public ActionResult LoadCandidates(string sort, string order, string search, int limit, int offset)
        {
            var rows = candidateService.FindAll(sort, order, search, limit, offset, out int total);

            return Json(new { total, rows });
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ImportCandidates()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EditCandidate(int id)
        {
            var candidate = candidateService.Get(id);
            return PartialView(ConvertToCandidateViewModel(candidate));
        }

        [HttpPost]
        public IActionResult DeleteCandidate(int id)
        {
            candidateService.Delete(id);
            return Ok();
        }

        [HttpPost]
        public IActionResult SaveCandidate(CandidateViewModel model)
        {
            //ModelState.AddModelError("general", "test error");

            if (!ModelState.IsValid)
                return PartialView("EditCandidate", model);

            candidateService.Edit(new Candidate()
            {
                Id = model.Id,
                Email = model.Email,
                BirthDate = model.BirthDate,
                FullName = model.FullName,
                Phone = model.Phone
            });
            return Ok();
        }
    }
}
