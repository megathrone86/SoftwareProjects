﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.Helpers
{
    public class CoordinatesHelper
    {
        //latitude, longitude
        //широта, долгота
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public bool IsValid { get; private set; }
        public string ErrorText { get; private set; }

        public CoordinatesHelper(string source)
        {
            TryParse(source);
        }

        void SetInvalid(string error)
        {
            IsValid = false;
            ErrorText = error;
        }

        void TryParse(string source)
        {
            IsValid = true;

            if (string.IsNullOrEmpty(source))
            {
                SetInvalid("Пустая строка");
                return;
            }

            var ts = source.Split(new char[] { ' ' });
            if (ts.Length < 2)
            {
                SetInvalid("Слишком мало параметров.");
                return;
            }
            if (ts.Length > 2)
            {
                SetInvalid("Слишком много параметров.");
                return;
            }

            double lat, lon;
            if (!double.TryParse(ts[0], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out lat))
            {
                SetInvalid("Неверное значение широты.");
                return;
            }

            if (!double.TryParse(ts[1], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out lon))
            {
                SetInvalid("Неверное значение долготы.");
                return;
            }

            Latitude = lat;
            Longitude = lon;
        }

        public static string GetClusterValue(double latitude, double longitude)
        {
            return CoordinatesHelper.GetClusterValue(latitude) + "_" + CoordinatesHelper.GetClusterValue(longitude);
        }

        public static string GetClusterValue(double coord)
        {
            int n = (int)coord * 100;
            return n.ToString();
        }
    }
}
