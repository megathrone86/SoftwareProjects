﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapp.Helpers;
using Microsoft.AspNetCore.Authorization;
using webapp.Models;
using Microsoft.AspNetCore.Identity;
using webapp.Data;
using webapp.Models.ManageViewModels;
using webapp.Models.ContaminationViewModels;
using System.Net;
using NetCoreHelpers;
using Microsoft.AspNetCore.Http;

namespace webapp.Controllers
{
    public class ContaminationController : Controller
    {
        UserManager<ApplicationUser> userManager;
        ContaminationManager contaminationManager;

        public ContaminationController(UserManager<ApplicationUser> userManager, ContaminationManager contaminationManager)
        {
            this.userManager = userManager;
            this.contaminationManager = contaminationManager;
        }

        public IActionResult Index()
        {
            return View("Overview");
        }

        [Authorize]
        [HttpGet]
        public IActionResult Add()
        {
            ViewData["Title"] = "Добавить информацию о загрязнении.";
            return View("ContaminationRequest");
        }

        [Authorize(Roles = "admin,moder")]
        [HttpPost]
        public async Task<IActionResult> Delete(ContaminationRequestViewModel model)
        {
            await contaminationManager.DeleteRequest(model.PKey, model.RKey);
            return Redirect("/Contamination/ViewRequests");
        }

        [Authorize(Roles = "admin,moder")]
        [HttpPost]
        public async Task<IActionResult> Approve(ContaminationRequestViewModel model)
        {
            var user = await GetCurrentUserAsync();

            if (ModelState.IsValid)
            {
                var parser = new CoordinatesHelper(model.Coordinates);
                if (!parser.IsValid)
                {
                    ModelState.AddModelError("Coordinates", parser.ErrorText);
                    return View(model);
                }

                var newModel = new Contamination()
                {
                    Latitude = parser.Latitude,
                    Longitude = parser.Longitude,
                    Description = model.Description,
                    SenderId = user.Id
                };

                await contaminationManager.Add(newModel.GenerateRowKey());
                await contaminationManager.DeleteRequest(model.PKey, model.RKey);

                return Redirect("/Contamination/ViewRequests");
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add(ContaminationRequestViewModel model, ICollection<IFormFile> photos)
        {
            var user = await GetCurrentUserAsync();

            if (ModelState.IsValid)
            {
                var parser = new CoordinatesHelper(model.Coordinates);
                if (!parser.IsValid)
                {
                    ModelState.AddModelError("Coordinates", parser.ErrorText);
                    return View("ContaminationRequest", model);
                }

                bool isAdmin = await userManager.IsInRoleAsync(user, "admin") || await userManager.IsInRoleAsync(user, "moder");
                if (!isAdmin)
                {
                    var captchaImage = HttpContext.Request.Form["g-recaptcha-response"];
                    var captchaChecker = new CaptchaHelper(captchaImage);
                    if (!captchaChecker.IsValid)
                    {
                        ModelState.AddModelError("", "К сожалению, Вы не прошли проверку для защиты от роботов.");
                        return View("ContaminationRequest", model);
                    }
                }

                var hasDuplicates = await contaminationManager.HasDuplicateByCoords(parser.Latitude, parser.Longitude);
                if (hasDuplicates)
                {
                    ModelState.AddModelError("", "В указанной точке уже зарегистрирована свалка.");
                    return View("ContaminationRequest", model);
                }

                var newModel = new ContaminationRequest()
                {
                    Latitude = parser.Latitude,
                    Longitude = parser.Longitude,
                    Description = model.Description,
                    SenderId = user.Id
                };

                await contaminationManager.AddRequest(newModel.GenerateRowKey());

                user.LastRequestTime = DateTime.Now;
                await userManager.UpdateAsync(user);

                ViewData["Message"] = "Спасибо за обращение. Ваша заявка на добавление информации будет рассмотрена.";
                return View("Success");
            }

            return View("ContaminationRequest", model);
        }

        [Authorize(Roles = "admin,moder")]
        [HttpGet]
        public async Task<IActionResult> ViewRequests()
        {
            var requests = await contaminationManager.GetFirst10Requests();

            var model = new List<ContaminationRequestViewModel>();
            foreach (var r in requests)
            {
                var user = await userManager.FindByIdAsync(r.SenderId);

                model.Add(new ContaminationRequestViewModel(r)
                {
                    Sender = new UserViewModel(user)
                });
            }
            return View("ContaminationRequests", model);
        }

        [Authorize(Roles = "admin,moder")]
        [HttpGet]
        public async Task<IActionResult> ViewRequest(string pkey, string rkey)
        {
            var r = await contaminationManager.GetRequest(pkey, rkey);
            var user = await userManager.FindByIdAsync(r.SenderId);
            var model = new ContaminationRequestViewModel(r)
            {
                Sender = new UserViewModel(user)
            };

            ViewData["Title"] = "Сведения по заявке";
            return View("ContaminationRequest", model);
        }

        [Authorize(Roles = "admin,moder")]
        [HttpGet]
        public async Task<IActionResult> ViewRequestPartial(string pkey, string rkey)
        {
            var r = await contaminationManager.GetRequest(pkey, rkey);
            var user = await userManager.FindByIdAsync(r.SenderId);
            var model = new ContaminationRequestViewModel(r)
            {
                Sender = new UserViewModel(user)
            };

            ViewData["Title"] = "=== сведения по заявке ===";
            return PartialView("ContaminationRequest", model);
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return userManager.GetUserAsync(HttpContext.User);
        }

        [HttpGet]
        public async Task<IActionResult> GetPoints(double p11, double p12, double p21, double p22)
        {
            if (!ActionValidator.Instance.Validate(Request.HttpContext.Connection.RemoteIpAddress))
                return null;

            //TODO: пагинация

            double deltaX = p21 - p11;
            double deltaY = p22 - p12;

            var t1 = new Tuple<double, double>(p11 - deltaX, p12 - deltaY);
            var t2 = new Tuple<double, double>(p21 + deltaX, p22 + deltaY);

            var points = await contaminationManager.GetPlaces(t1, t2);

            var result = new List<object>();

            foreach (var point in points)
            {
                result.Add(new { X = point.Latitude, Y = point.Longitude, PartitionKey = point.PartitionKey, RowKey = point.RowKey });
            }

            return Json(result);
        }

        [HttpGet]
        public async Task<IActionResult> ContaminationView(string pkey, string rkey)
        {
            if (!ActionValidator.Instance.Validate(Request.HttpContext.Connection.RemoteIpAddress))
                throw new WebException("", WebExceptionStatus.TrustFailure);

            var place = await contaminationManager.GetPlace(pkey, rkey);
            var user = await userManager.FindByIdAsync(place.SenderId);
            var model = new ContaminationViewModel(place, user.WantsToBeAnonymous ? null : user);
            return PartialView("ContaminationView", model);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> AddContamination(double latitude, double longitude)
        {
            var request = new ContaminationRequest()
            {
                Latitude = latitude,
                Longitude = longitude
            };
            var viewModel = new ContaminationRequestViewModel(request);

            var hasDuplicates = await contaminationManager.HasDuplicateByCoords(latitude, longitude);
            if (hasDuplicates)
                ModelState.AddModelError("", "В указанной точке уже зарегистрирована свалка.");

            return View("ContaminationRequest", viewModel);
        }
    }
}
