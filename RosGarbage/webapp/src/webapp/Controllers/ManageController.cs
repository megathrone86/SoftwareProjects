﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapp.Models;
using webapp.Models.ManageViewModels;
using webapp.Services;
using AspNetCore.Identity.DynamoDB;
using webapp.Data;
using webapp.Helpers;
using NetCoreHelpers;

namespace webapp.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        UserManager<ApplicationUser> userManager;
        UsersManager usersManager;
        SignInManager<ApplicationUser> signInManager;
        ILogger logger;

        public ManageController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            UsersManager usersManager, ILoggerFactory loggerFactory)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.usersManager = usersManager;
            logger = loggerFactory.CreateLogger<ManageController>();
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ManageAccount()
        {
            var user = await GetCurrentUserAsync();
            ViewData["ShowRemoveButton"] = user.Logins.Count > 1;
            return View(new UserViewModel(user));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageAccount(UserViewModel newUserSettings)
        {
            var user = await GetCurrentUserAsync();
            user.DisplayName = newUserSettings.UserName;
            var result = await userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                string extInfo = "";
                if (result.Errors.Any(e => e.Code == "DuplicateUserName"))
                    extInfo += " Пользователь с таким именем уже есть.";
                if (result.Errors.Any(e => e.Code == "InvalidUserName"))
                    extInfo += " Имя пользователя содержит запрещенные символы.";

                throw new Exception("Не удалось обновить данные профиля." + extInfo);
            }
            return RedirectToAction("ManageAccount");
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> ViewUsers()
        {
            return View("ViewUsers");
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> GetUsers(string search)
        {
            var model = new List<UserViewModel>();

            var users = await usersManager.FindAll(search);
            foreach (var user in users)
            {
                model.Add(new UserViewModel(user));
            }

            return PartialView("UsersList", model);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> SetUserRole(string userId, string role)
        {
            try
            {
                switch (role)
                {
                    case "user":
                        {
                            var user = await userManager.FindByIdAsync(userId);
                            await userManager.RemoveFromRolesAsync(user, new List<string>() { "moder", "admin" });
                        }
                        break;
                    case "moder":
                        {
                            var user = await userManager.FindByIdAsync(userId);
                            await userManager.RemoveFromRolesAsync(user, new List<string>() { "admin" });
                            await userManager.AddToRoleAsync(user, role);
                        }
                        break;
                    //case "admin":
                    //    {
                    //        var user = await userManager.FindByIdAsync(userId);
                    //        await userManager.AddToRoleAsync(user, role);
                    //    }
                    //    break;
                    default:
                        throw new ArgumentException();
                }

                return Json($"Роль пользователя {userId} успешно изменена на {role}");
            } catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Json($"Произошла ошибка: {ex}");
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return userManager.GetUserAsync(HttpContext.User);
        }
    }
}
