﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapp.Models;
using webapp.Services;
using webapp.Auth;
using System.Net;

namespace webapp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ISmsSender smsSender,
            ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }

        void FillAppIds()
        {
#if DEBUG
            ViewData["VkAppId"] = "5958469";
#else
            ViewData["VkAppId"] = "5958451";            
#endif
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginVK(string returnUrl, string uid, string first_name, string last_name)
        {
            string loginProviderName = LoginsInfo.VKProviderName;
            string loginProviderDisplayName = LoginsInfo.VKProviderDisplayName;

            var user = await _userManager.FindByLoginAsync(loginProviderName, uid);
            if (user == null)
            {
                string name = first_name;
                if (!string.IsNullOrEmpty(last_name))
                    name += " " + last_name;
                user = new ApplicationUser() { UserName = Guid.NewGuid().ToString("N"), DisplayName = name };

                {
                    var result = await _userManager.CreateAsync(user);
                    if (!result.Succeeded)
                        throw new Exception("Не удалось создать пользователя.");
                }

                {
                    var result = await _userManager.AddLoginAsync(user, new UserLoginInfo(loginProviderName, uid, loginProviderDisplayName));
                    if (!result.Succeeded)
                        throw new Exception("Не удалось добавить информацию о способе входа.");
                }

                user = await _userManager.FindByLoginAsync(loginProviderName, uid);
            }

            if (user != null)
            {
                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation(3, $"User created a new account via {loginProviderName}.");
                return RedirectToLocal(returnUrl);
            } else
            {
                throw new Exception("Ошибка входа.");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(ContaminationController.Index), "Contamination");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            FillAppIds();
            ViewData["ReturnUrl"] = WebUtility.UrlEncode(returnUrl);
            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            } else
            {
                return RedirectToAction(nameof(ContaminationController.Index), "Contamination");
            }
        }
    }
}
