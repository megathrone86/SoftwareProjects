﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace webapp.Auth
{
    public class LoginsInfo
    {
        public static string VKProviderName = "VK";
        public static string VKProviderDisplayName = "ВКонтакте";
    }
}
