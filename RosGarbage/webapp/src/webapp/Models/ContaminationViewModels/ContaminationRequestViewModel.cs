﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using webapp.Models.ManageViewModels;

namespace webapp.Models.ContaminationViewModels
{
    public class ContaminationRequestViewModel
    {
        public ContaminationRequestViewModel()
        {
        }

        public ContaminationRequestViewModel(ContaminationRequest r)
        {
            this.PKey = r.PartitionKey;
            this.RKey = r.RowKey;
            this.Coordinates = $"{r.Latitude.ToString(CultureInfo.InvariantCulture)} {r.Longitude.ToString(CultureInfo.InvariantCulture)}";
            this.Latitude = r.Latitude;
            this.Longitude = r.Longitude;
            this.Description = r.Description;
            this.CreateDate = r.Timestamp.DateTime;
        }

        public string PKey { get; set; }
        public string RKey { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Координаты", Prompt = "0.00000 0.00000", Description = "Широта и долгота, разделенные запятой")]
        public string Coordinates { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string files { get; set; }

        public string CoordinatesLink
        {
            get
            {
                string coordinatesString = Longitude.ToString(CultureInfo.InvariantCulture) + "," + Latitude.ToString(CultureInfo.InvariantCulture);
                return $"https://yandex.ru/maps/?ll={coordinatesString}&z=15&mode=whatshere&whatshere[point]={coordinatesString}&whatshere[zoom]=15";
            }
        }

        [DataType(DataType.MultilineText)]
        [MaxLength(500)]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Описание")]
        public string DescriptionShort
        {
            get
            {
                const int maxLength = 50;
                return Description == null ? null : Description.Trim().Substring(0, Math.Min(Description.Length, maxLength));
            }
        }

        [Display(Name = "Отправитель")]
        public UserViewModel Sender { get; set; }

        [Display(Name = "Дата отправки")]
        public DateTime CreateDate { get; set; }
    }
}
