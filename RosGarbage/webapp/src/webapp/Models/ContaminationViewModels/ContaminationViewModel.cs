﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using webapp.Models.ManageViewModels;

namespace webapp.Models.ContaminationViewModels
{
    public class ContaminationViewModel
    {
        public ContaminationViewModel(Contamination r)
        {
            this.Description = r.Description;
            this.CreateDate = r.Timestamp.DateTime;
            this.ShowUser = false;
        }

        public ContaminationViewModel(Contamination r, ApplicationUser user) : this(r)
        {
            this.ShowUser = true;
            this.UserName = user.DisplayName;
            this.UserId = user.Id;
        }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Отправитель")]
        public UserViewModel Sender { get; set; }

        [Display(Name = "Дата отправки")]
        public DateTime CreateDate { get; set; }

        public bool ShowUser { get; set; }

        [Display(Name = "Отправитель")]
        public string UserName { get; set; }

        public string UserId { get; set; }
    }
}
