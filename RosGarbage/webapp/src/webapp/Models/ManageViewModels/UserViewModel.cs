﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using ElCamino.AspNetCore.Identity.AzureTable.Model;

namespace webapp.Models.ManageViewModels
{
    public class UserViewModel
    {
        public static IEnumerable<SelectListItem> Roles
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem() { Text = "user" },
                    new SelectListItem() { Text = "moder" },
                    new SelectListItem() { Text = "admin" }
                };
            }
        }

        public UserViewModel()
        {
        }

        public UserViewModel(ApplicationUser src)
        {
            this.Id = src.Id;
            this.UserName = src.DisplayName;
            this.RatingPercent = src.Rating.ToString("0.00");
            this.CurrentLogins = src.Logins.Select(l => new UserLoginInfo(l.LoginProvider, l.ProviderKey, l.ProviderDisplayName)).ToList();
            this.CreateDate = src.Timestamp.DateTime;
            this.UserRole = GetUserRole(src.Roles);
            this.WantsToBeAnonymous = src.WantsToBeAnonymous;
        }

        string GetUserRole(ICollection<IdentityUserRole> roles)
        {
            if (roles.Any(r => r.RoleName == "ADMIN"))
                return "admin";
            if (roles.Any(r => r.RoleName == "MODER"))
                return "moder";
            return "user";
        }

        [Display(Name = "Внутренний ИД")]
        public string Id { get; set; }

        [Required]
        [MaxLength(150)]
        [Display(Name = "Имя")]
        public string UserName { get; set; }

        [Display(Name = "Рейтинг")]
        public string RatingPercent { get; set; }

        [Display(Name = "Роль")]
        public string UserRole { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime CreateDate { get; set; }

        public IList<UserLoginInfo> CurrentLogins { get; set; }

        public IList<AuthenticationDescription> OtherLogins { get; set; }

        [Display(Name = "Не публиковать мое имя")]
        public bool WantsToBeAnonymous { get; set; }
    }
}
