﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ElCamino.AspNetCore.Identity.AzureTable.Model;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using webapp.Helpers;

namespace webapp.Models
{
    public class Contamination : ContaminationBase
    {
        public Contamination()
        {
            History = new List<ContaminationBase>();
        }

        public List<ContaminationBase> History { get; set; }
    }

    public class ContaminationBase : BaseEntity<Contamination>
    {
        public ContaminationBase()
        {
            Timestamp = DateTime.Now;
        }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Description { get; set; }

        public string SenderId { get; set; }

        public override IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            this.PartitionKey = CoordinatesHelper.GetClusterValue(Latitude, Longitude);

            return base.WriteEntity(operationContext);
        }
    }
}
