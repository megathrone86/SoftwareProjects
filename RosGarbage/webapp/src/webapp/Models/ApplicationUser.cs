﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ElCamino.AspNetCore.Identity.AzureTable.Model;

namespace webapp.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }
        public double Rating { get; set; }
        public DateTime? LastRequestTime { get; set; }
        public bool IsBlocked { get; set; }
        public bool WantsToBeAnonymous { get; set; }

        public ApplicationUser()
        {
            Rating = 1d;
        }
    }
}
