﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace webapp.Models
{
    public class BaseEntity<T> : TableEntity where T : BaseEntity<T>
    {
        public BaseEntity()
        {
            Timestamp = DateTime.Now;
        }

        public T GenerateRowKey()
        {
            RowKey = Guid.NewGuid().ToString("N");
            return this as T;
        }
    }
}
