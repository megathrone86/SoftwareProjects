﻿using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapp.Models;
using webapp.Models.ContaminationViewModels;
using System;
using Microsoft.AspNetCore.Identity;

namespace webapp.Data
{
    public class UsersManager
    {
        UserManager<ApplicationUser> userManager;
        ApplicationDbContext context;

        public UsersManager(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public async Task<List<ApplicationUser>> FindAll(string search)
        {
            var userHeaders = await GetAll();
            string trimmedLowerSearch = search == null ? null : search.Trim().ToLower();
            var filteredHeaders = string.IsNullOrEmpty(trimmedLowerSearch) ? userHeaders :
                userHeaders.FindAll(user => FitToFilter(user, trimmedLowerSearch));

            List<string> userIds = new List<string>();
            foreach (var header in filteredHeaders)
            {
                if (!userIds.Contains(header.PartitionKey))
                    userIds.Add(header.PartitionKey);
            }

            var result = new List<ApplicationUser>();
            foreach (var userId in userIds)
            {
                var user = await userManager.FindByIdAsync(userId);
                result.Add(user);
            }

            return result;
        }

        private bool FitToFilter(ApplicationUser user, string trimmedLowerSearch)
        {
            if (user.DisplayName != null && user.DisplayName.ToLower().Contains(trimmedLowerSearch))
                return true;
            if (user.Id != null && user.Id.ToLower().Contains(trimmedLowerSearch))
                return true;
            return false;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var result = new List<ApplicationUser>();

            var query = new TableQuery<ApplicationUser>();
            query.Select(new List<string> { "Id", "DisplayName" });
            TableContinuationToken token = null;
            do
            {
                var r = await context.UserTable.ExecuteQuerySegmentedAsync<ApplicationUser>(query, token);
                result.AddRange(r.Results);
                token = r.ContinuationToken;
            } while (token != null);

            return result;
        }
    }
}
