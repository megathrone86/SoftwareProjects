﻿using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapp.Models;
using webapp.Models.ContaminationViewModels;
using System;
using webapp.Helpers;

namespace webapp.Data
{
    public class ContaminationManager
    {
        ApplicationDbContext context;

        public ContaminationManager(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task AddRequest(ContaminationRequest item)
        {
            await context.ContaminationRequests.ExecuteAsync(TableOperation.Insert(item));
        }

        public async Task<List<ContaminationRequest>> GetFirst10Requests()
        {
            var query = new TableQuery<ContaminationRequest>();
            query.TakeCount = 10;
            var result = await context.ContaminationRequests.ExecuteQuerySegmentedAsync<ContaminationRequest>(query, null);
            var list = result.ToList();
            return list;
        }

        public async Task<ContaminationRequest> GetRequest(string pkey, string rkey)
        {
            var operation = TableOperation.Retrieve<ContaminationRequest>(pkey, rkey);
            var result = await context.ContaminationRequests.ExecuteAsync(operation);
            return result.Result as ContaminationRequest;
        }

        public async Task DeleteRequest(string pkey, string rkey)
        {
            var ent = new ContaminationRequest() { PartitionKey = pkey, RowKey = rkey, ETag = "*" };
            var operation = TableOperation.Delete(ent);
            await context.ContaminationRequests.ExecuteAsync(operation);
        }

        public async Task Add(Contamination contamination)
        {
            await context.Contaminations.ExecuteAsync(TableOperation.Insert(contamination));
        }

        public async Task<List<Contamination>> GetPlaces(Tuple<double, double> t1, Tuple<double, double> t2)
        {
            var centerPartitionKey = CoordinatesHelper.GetClusterValue((t1.Item1 + t2.Item1) / 2, (t1.Item2 + t2.Item2) / 2);

            var query = new TableQuery<Contamination>();
            query.TakeCount = 100;
            query.Select(new List<string> { "Latitude", "Longitude" });
            query.FilterString =
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, centerPartitionKey),
                    TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("Latitude", QueryComparisons.GreaterThanOrEqual, t1.Item1),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("Latitude", QueryComparisons.LessThanOrEqual, t2.Item1)
                    ),
                    TableOperators.And,
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("Longitude", QueryComparisons.GreaterThanOrEqual, t1.Item2),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("Longitude", QueryComparisons.LessThanOrEqual, t2.Item2)
                    )
                ));
            var result = await context.Contaminations.ExecuteQuerySegmentedAsync<Contamination>(query, null);
            var list = result.ToList();
            return list;
        }

        public async Task<Contamination> GetPlace(string pkey, string rkey)
        {
            var operation = TableOperation.Retrieve<Contamination>(pkey, rkey);
            var result = await context.Contaminations.ExecuteAsync(operation);
            return result.Result as Contamination;
        }

        internal async Task<bool> HasDuplicateByCoords(double latitude, double longitude)
        {
            //мы хотим сделать поиск по квадрату 50х50м            
            const double longitudeDelta = 85000 / 50; //на широте 40 длина одного градуса долготы составляет ~85км
            const double latitudeDelta = 111000 / 50; //на любой долготе длина одного градуса широты составляет ~111км

            var r = await GetPlaces(new Tuple<double, double>(latitude - latitudeDelta, longitude - longitudeDelta),
                new Tuple<double, double>(latitude + latitudeDelta, longitude + longitudeDelta));
            if (r.Count > 0)
                return true;

            return false;
        }
    }
}
