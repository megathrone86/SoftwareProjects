﻿using ElCamino.AspNetCore.Identity.AzureTable;
using ElCamino.AspNetCore.Identity.AzureTable.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapp.Models;

namespace webapp.Data
{
    // Update-Database
    // Add-Migration

    public class ApplicationDbContext : IdentityCloudContext
    {
        public ApplicationDbContext() : base() { }

        public CloudTable ContaminationRequests { get; }
        public CloudTable Contaminations { get; }

        public ApplicationDbContext(IdentityConfiguration config) : base(config)
        {
            ContaminationRequests = GetOrCreateTable(nameof(ContaminationRequests));
            Contaminations = GetOrCreateTable(nameof(Contaminations));
        }

        public async Task InitTables()
        {
            //await RoleTable.DeleteIfExistsAsync();
            //await RoleTable.CreateIfNotExistsAsync();

            //await RoleTable.ExecuteAsync(TableOperation.InsertOrMerge(new IdentityRole()
            //{
            //    PartitionKey = "0",
            //    RowKey = "admin",
            //    Name = "admin"
            //}));
            //UserTable
        }

        CloudTable GetOrCreateTable(string tableName)
        {
            var result = Client.GetTableReference(tableName);
            result.CreateIfNotExistsAsync().Wait();
            return result;
        }
    }
}
