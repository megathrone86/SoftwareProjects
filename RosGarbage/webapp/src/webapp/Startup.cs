﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using webapp.Data;
using webapp.Models;
using webapp.Services;
using ElCamino.AspNetCore.Identity.AzureTable;
using ElCamino.AspNetCore.Identity.AzureTable.Model;
using webapp.Helpers;
using System.Net;
using System.Globalization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Identity;
using webapp.Auth;
using NetCoreHelpers;

namespace webapp
{
    public class Startup
    {
        IServiceProvider serviceProvider;

        void Test()
        {
            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            //    "DefaultEndpointsProtocol=https;AccountName=rosgarbage;AccountKey=Z55EgKCXduHM3utdoufvDxEmOa59GhVwGWgn18ixIE6TMHViQb5z/8p9Gp/QPKu6e21sOSdllDAVTuQLH8c5fg==;EndpointSuffix=core.windows.net");
            //CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            //CloudTable table = tableClient.GetTableReference("people");
            //table.CreateIfNotExistsAsync().Wait();
        }

        public Startup(IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;

            Test();

            FileHelper.Init(env);

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"appsettings.production.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            CaptchaHelper.SecretKey = Configuration["ReCaptchaSecretKey"];
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("ASConnection");

            var b = services.AddIdentity<ApplicationUser, IdentityRole>((options) => { });
            b.AddAzureTableStores<ApplicationDbContext>(new Func<IdentityConfiguration>(() =>
                {
                    return new IdentityConfiguration()
                    {
                        TablePrefix = "identity",
                        StorageConnectionString = connectionString,
                        LocationMode = "PrimaryOnly"
                    };
                }));
            b.AddDefaultTokenProviders();
            b.CreateAzureTablesIfNotExists<ApplicationDbContext>();

            services.AddTransient<ContaminationManager, ContaminationManager>();
            services.AddTransient<UsersManager, UsersManager>();

            services.AddMvc();

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
        }

        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            /*ApplicationDbContext context, */UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            } else
            {
                app.UseExceptionHandler(
                    builder =>
                    {
                        builder.Run(
                          async handlerContext =>
                          {
                              handlerContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                              handlerContext.Response.ContentType = "text/html";

                              var error = handlerContext.Features.Get<IExceptionHandlerFeature>();
                              if (error != null)
                              {
                                  ErrorLogger.Log(error.Error);
                                  handlerContext.Response.Redirect("/Home/Error");
                              }
                          });
                    });
            }

            app.UseStaticFiles();

            app.UseIdentity();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Contamination}/{action=Index}/{id?}");
            });

            CultureInfo cultureInfo = new CultureInfo("ru-RU");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
            CultureInfo.CurrentCulture = cultureInfo;
            CultureInfo.CurrentUICulture = cultureInfo;

            var createRole = new Action<string>(async (roleName) =>
            {
                var role = await roleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    var r = await roleManager.CreateAsync(new IdentityRole { Name = roleName });
                    if (!r.Succeeded)
                        throw new Exception("Не удалось создать роль.");
                }
            });
            createRole("admin");
            createRole("moder");

            var mainUser = await userManager.FindByLoginAsync(LoginsInfo.VKProviderName, "1674956");
            if (mainUser != null)
            {
                var r = await userManager.IsInRoleAsync(mainUser, "admin");
                if (!r)
                {
                    var r2 = await userManager.AddToRoleAsync(mainUser, "admin");
                    if (!r2.Succeeded)
                        throw new Exception("Не удалось присвоить права администратора.");
                }
            }
        }
    }
}
